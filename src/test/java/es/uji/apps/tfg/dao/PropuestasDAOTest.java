package es.uji.apps.tfg.dao;

import es.uji.apps.tfg.dto.AsignaturaDTO;
import es.uji.apps.tfg.dto.PersonaDTO;
import es.uji.apps.tfg.dto.PersonasPropuestaDTO;
import es.uji.apps.tfg.dto.PropuestaDTO;
import es.uji.apps.tfg.model.Asignacion;
import es.uji.apps.tfg.services.PropuestasService;
import es.uji.apps.tfg.utils.DateUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)
@ContextConfiguration({ "classpath:applicationContext-test.xml" })
@Transactional
public class PropuestasDAOTest extends TestClass
{

    @Autowired
    PropuestasDAO propuestasDAO;
    @Autowired
    PersonasDAO personasDAO;
    @Autowired
    AsignaturasDAO asignaturasDAO;

    @Autowired
    PropuestasService propuestasService;

    public PropuestasDAOTest()
    {
    }
    
    public int countPropuestas(PersonaDTO personaDTO)
    {
        return propuestasDAO.getPropuestasByAutor(personaDTO.getId(), true, true).size();
        
    }

    @Test
    @Transactional
    public void getAsignacionesByAsignaturaCheckDatosValidos() throws ParseException
    {
        PersonaDTO Miper1 = personasDAO.getUsuario(buildPersona(new Long(6000), "Usuario Prueba 1"));
        PersonaDTO Miper100 = personasDAO.getUsuario(buildPersona(new Long(5000), "Usuario Prueba 100"));
        PersonaDTO Miper101 = personasDAO.getUsuario(buildPersona(new Long(5010), "Usuario Prueba 101"));
        PersonaDTO Miper102 = personasDAO.getUsuario(buildPersona(new Long(5020), "Usuario Prueba 102"));
        AsignaturaDTO Miasi1 = asignaturasDAO.getAsignatura(buildAsignatura("Asignatura Prueba 1", "MI0001", 10, 100, DateUtils.getCurrent(), 1));
        AsignaturaDTO Miasi2 = asignaturasDAO.getAsignatura(buildAsignatura("Asignatura Prueba 2", "MI0002", 10, 100, DateUtils.getCurrent(), 1));

        buildPersonaAsignatura(Miasi1.getCodigo(), Miper100.getId(), "ESTUDIANTE", 5);
        buildPersonaAsignatura(Miasi1.getCodigo(), Miper101.getId(), "ESTUDIANTE", 5);
        buildPersonaAsignatura(Miasi2.getCodigo(), Miper101.getId(), "ESTUDIANTE", 5);
        buildPersonaAsignatura(Miasi1.getCodigo(), Miper102.getId(), "ESTUDIANTE", 5);
        List<Long> tutores = Arrays.asList(1L, 2L, 3L);

        PropuestaDTO Mipro3 = buildPropuesta("Propuesta Prueba 3", tutores, Miasi1, "objetivos", "bibliografia", "descripcion", "observaciones", new BigDecimal(2), new BigDecimal(
                2), Miper1.getId());
        PropuestaDTO Mipro4 = buildPropuesta("Propuesta Prueba 4", tutores, Miasi1, "objetivos", "bibliografia", "descripcion", "observaciones", new BigDecimal(1), new BigDecimal(
                1), Miper1.getId());
        PropuestaDTO Mipro5 = buildPropuesta("Propuesta Prueba 5", tutores, Miasi2, "objetivos", "bibliografia", "descripcion", "observaciones", new BigDecimal(1), new BigDecimal(
                1), Miper1.getId());

        buildPersonaPropuesta(Mipro3.getId(), Miper100.getId(), true);
        buildPersonaPropuesta(Mipro4.getId(), Miper100.getId(), false);
        buildPersonaPropuesta(Mipro4.getId(), Miper101.getId(), true);
        buildPersonaPropuesta(Mipro5.getId(), Miper101.getId(), true);
        buildCargaProfesor(Miper1.getId(), Miasi1.getCodigo(), 5);
        List<Asignacion> a = propuestasService.getAsignacionesByAsignatura(Miasi1.getCodigo(), -1L);
        for (Asignacion asig : a) {
            if (asig.getAceptada() == 0 && asig.getCodigo_asignatura().compareTo(Miasi1.getCodigo()) == 0 && asig.getPersona_id().longValue() == Miper100.getId()) {
                Assert.assertEquals(asig.getPropuesta_aceptada(), Mipro3.getNombre() + " (" + Miper1.getNombreCompleto() + ")");
            }
            else {
                Assert.assertNull(asig.getPropuesta_aceptada());
            }
        }
        Assert.assertEquals(4, a.size());
    }

    @Test
    public void getPropuestas()
    {
        PersonaDTO personaDTO = personasDAO.getUsuario(per1.getId());
        Assert.assertEquals(4, propuestasDAO.getPropuestasByAutor(personaDTO.getId(), true, true).size());
    }

    @Test
    public void getAsignacionesByAsignatura() {

        //TODO ESTO?
//        List<Object[]> a = propuestasDAO.getAsignacionesByAsignatura(asi1.getCodigo());
//        Assert.assertNotNull(a);
//        Assert.assertNotEquals(0, a.size());
    }

    @Test
    public void getAsignacionesByPropuesta()
    {
        List<PersonasPropuestaDTO> a = propuestasDAO.getAsignacionesByPropuesta(pro4.getId());
        Assert.assertNotNull(a);
        Assert.assertEquals(2, a.size());
    }

    @Test
    public void getAsignacion()
    {
        Assert.assertNotNull(propuestasDAO.getAsignacion(per100.getId(), pro3.getId()));
    }

    @Test
    public void getPropuesta()
    {
        Assert.assertNotNull(propuestasDAO.getPropuesta(pro1.getId()));
    }

    @Test
    public void getPropuestasByAsignatura()
    {
        List<PropuestaDTO> propuestas = propuestasDAO.getPropuestasByAsignatura(asi1.getCodigo());
        Assert.assertEquals(3, propuestas.size());
    }

    @Test
    public void getPropuestasByAutor()
    {
        List<PropuestaDTO> propuestas = propuestasDAO.getPropuestasByAutor(per1.getId(), true, true);
        Assert.assertEquals(4, propuestas.size());
    }

    @Test
    public void getPropuestasByStudent()
    {
        List<PropuestaDTO> propuestas = propuestasDAO.getPropuestasByEstudiante(per100);
        Assert.assertEquals(2, propuestas.size());
        propuestas = propuestasDAO.getPropuestasByEstudiante(per101);
        Assert.assertEquals(1, propuestas.size());
        propuestas = propuestasDAO.getPropuestasByEstudiante(per102);
        Assert.assertEquals(0, propuestas.size());

    }

    @Test
    public void getSolicitudesByStudent()
    {
        List<PropuestaDTO> solicitudes = propuestasDAO.getSolicitudesByStudent(per100.getId(), asi1.getCodigo());
        Assert.assertEquals(1, solicitudes.size());

    }

    @Test
    @Transactional
    public void create() {
        String nombre = "Test";
        String objetivos = "Objetivos test";
        String bibliografia = "Bibliografia test";
        String descripcion = "Descripcion test";
        String observaciones = "OBservaciones test";
        BigDecimal plazas = new BigDecimal(5);
        BigDecimal espera = new BigDecimal(5);
        int antes = countPropuestas(per1);
        List<Long> tutores = Arrays.asList(1L, 2L, 3L);
        long nueva = propuestasDAO.create(nombre, tutores, asi1.getCodigo(), objetivos, bibliografia, descripcion, observaciones, plazas, espera, per1.getId());
        PropuestaDTO propuestaDTO = propuestasDAO.getPropuesta(nueva);
        int despues = countPropuestas(per1);
        Assert.assertNotNull(propuestaDTO);
        Assert.assertEquals(antes, despues - 1);
    }

    @Test
    @Transactional
    public void update() {
        List<Long> tutores = Arrays.asList(1L, 2L, 3L);
        PropuestaDTO actualizadaDTO = propuestasDAO.update(pro1.getId(), "TestActualizada", tutores, asi1.getCodigo(), pro1.getObjetivos(),
                pro1.getBibliografia(), pro1.getDescripcion(), pro1.getObservaciones(), pro1.getPlazas(), pro1.getEspera());
        Assert.assertEquals(actualizadaDTO.getNombre(), "TestActualizada");
    }

    @Test
    public void delete()
    {
        String nombre = "Test";
        String objetivos = "Objetivos test";
        String bibliografia = "Bibliografia test";
        String descripcion = "Descripcion test";
        String observaciones = "OBservaciones test";
        BigDecimal plazas = new BigDecimal(5);
        BigDecimal espera = new BigDecimal(5);
        PersonaDTO autor = personasDAO.getUsuario(per1.getId());
        List<Long> tutores = Arrays.asList(1L, 2L, 3L);
        AsignaturaDTO asignatura = asignaturasDAO.getAsignatura(asi1.getCodigo());
        long nueva = propuestasDAO.create(nombre, tutores, asignatura.getCodigo(), objetivos, bibliografia, descripcion, observaciones, plazas, espera, per1.getId());
        int antes = countPropuestas(autor);
        PropuestaDTO propuestaDTO = propuestasDAO.getPropuesta(nueva);
        propuestasDAO.delete(propuestaDTO);
        int despues = countPropuestas(autor);
        Assert.assertEquals(antes, despues + 1);
    }

    @Test
    public void createAsignaciones()
    {
        Long persona_id = per1.getId();
        Long propuesta_id = pro1.getId();
        Long editor_persona_id = per10.getId();
        Date current = DateUtils.getCurrent();
        try
        {
            propuestasDAO.updateAsignaciones(persona_id, propuesta_id, editor_persona_id, "", current);
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
        }
    }

    @Test
    public void updateAsignaciones()
    {
        Long persona_id = per1.getId();
        Long propuesta_id = pro2.getId();
        Long editor_persona_id = per10.getId();
        Date current = DateUtils.getCurrent();
        try
        {
            propuestasDAO.updateAsignaciones(persona_id, propuesta_id, editor_persona_id, "", current);
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
        }
        PersonasPropuestaDTO ppDTO = propuestasDAO.getAsignacion(persona_id, propuesta_id);
        Assert.assertNotNull(ppDTO);

    }

    @Test
    @Transactional
    public void addSolicitud()
    {
        PropuestaDTO propuestaDTO = propuestasDAO.getPropuesta(pro3.getId());
        PersonaDTO personaDTO = personasDAO.getUsuario(per10.getId());
        propuestasDAO.addSolicitud(propuestaDTO, personaDTO.getId(), true, "");
    }

    @Test
    public void removeSolicitud()
    {
        propuestasDAO.removeSolicitud(per1.getId(), pro1.getId());
    }

    @Test
    public void aceptarSolicitud()
    {
        PropuestaDTO propuestaDTO = propuestasDAO.getPropuesta(pro2.getId());
        PersonaDTO personaDTO = personasDAO.getUsuario(per10.getId());
        propuestasDAO.addSolicitud(propuestaDTO, personaDTO.getId(), false, "");
        PersonasPropuestaDTO solicitud = propuestasDAO.getAsignacion(per10.getId(), pro2.getId());
        propuestasDAO.aceptarSolicitud(solicitud);
    }

    @Test
    @Transactional
    public void redistribuirPlazas()
    {
        PropuestaDTO original = propuestasDAO.getPropuesta(pro4.getId());
        if (original.getTfgPersonasPropuestas() != null && original.getTfgPersonasPropuestas().size() >= 1)
        {
            for (PersonasPropuestaDTO asignacion : original.getTfgPersonasPropuestas())
            {
                if (asignacion.getTfgExtPersona().getId() == per100.getId())
                {
                    Assert.assertEquals(0, asignacion.getAceptada().intValue());
                }
            }
        }

        int solicitudesPersona101 = propuestasDAO.getPropuestasByEstudiante(per101).size();
        Assert.assertEquals(1, solicitudesPersona101);
        propuestasDAO.removeSolicitud(per101.getId(), pro4.getId());
        Assert.assertEquals(0, propuestasDAO.getPropuestasByEstudiante(per101).size());
        propuestasService.redistribuirPlazas(pro4.getId());
        boolean aceptada = false;
        PropuestaDTO modificada = propuestasDAO.getPropuesta(pro4.getId());

        for (PersonasPropuestaDTO asignacion : modificada.getTfgPersonasPropuestas())
        {
            if (asignacion.getTfgExtPersona().getId() == per100.getId() && asignacion.getTfgPropuesta().getId() == pro4.getId())
            {
                aceptada = asignacion.getAceptada().intValue() == 1;
            }
        }
        Assert.assertTrue(aceptada);
    }
}
