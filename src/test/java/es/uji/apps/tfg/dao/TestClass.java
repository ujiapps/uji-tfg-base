package es.uji.apps.tfg.dao;

import es.uji.apps.tfg.dto.*;
import es.uji.apps.tfg.utils.AcademicYear;
import es.uji.apps.tfg.utils.DateUtils;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.*;

@RunWith(SpringJUnit4ClassRunner.class)
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)
@ContextConfiguration({ "classpath:applicationContext-test.xml" })
@Transactional
public class TestClass extends BaseDAODatabaseImpl
{
    @Autowired
    public PropuestasDAO propuestasDAO;
    @Autowired
    public PersonasDAO personasDAO;
    @Autowired
    public AsignaturasDAO asignaturasDAO;

    public static PersonaDTO per1;
    public static PersonaDTO per10;
    public static PersonaDTO per100;
    public static PersonaDTO per101;
    public static PersonaDTO per102;
    public static AsignaturaDTO asi1;
    public static AsignaturaDTO asi10;
    public static PropuestaDTO pro1;
    public static PropuestaDTO pro2;
    public static PropuestaDTO pro3;
    public static PropuestaDTO pro4;
    public static boolean isInitialized = false;

    @Transactional
    public Long buildPersona(Long id, String nombre)
    {
        PersonaDTO personaDTO = new PersonaDTO();
        personaDTO.setId(id);
        personaDTO.setNombre(nombre);
        personaDTO.setApellido1("Apellido1 de " + id);
        personaDTO.setApellido2("Apellido2 de " + id);
        personaDTO.setEmail("user" + id + "@uji.es");
        Set<PropuestaDTO> propuestas = new HashSet<PropuestaDTO>();
        // propuestas.add(new PropuestaDTO());
        //TODO Hacer test
//        personaDTO.setTfgPropuestas(propuestas);
        Set<CargaProfesorDTO> cargas = new HashSet<CargaProfesorDTO>();
        // cargas.add(new CargaProfesorDTO());
        personaDTO.setTfgCargaProfesores(cargas);
        Set<AsignaturasPersonaDTO> asigs = new HashSet<AsignaturasPersonaDTO>();
        // asigs.add(new AsignaturasPersonaDTO());
        personaDTO.setTfgExtAsignaturasPersonas(asigs);
        Set<PersonasPropuestaDTO> ppdto = new HashSet<PersonasPropuestaDTO>();
        // ppdto.add(new PersonasPropuestaDTO());
        personaDTO.setTfgPersonasPropuestas(ppdto);
        entityManager.persist(personaDTO);

        return personaDTO.getId();
    }

    @Transactional
    public String buildAsignatura(String nombre, String codigo, long creditos, long matriculas, Date fecha, long estudio_id)
    {

        AsignaturaDTO asig = new AsignaturaDTO(codigo, AcademicYear.getCurrent());
        asig.setNombre(nombre);
        asig.setCreditos(new BigDecimal(creditos));
        asig.setMatriculas(new BigDecimal(matriculas));
        asig.setTfgAsignaturasConfig(null);
        asig.setEstudioId(new BigDecimal(estudio_id));
        entityManager.persist(asig);
        AsignaturasConfigDTO config = new AsignaturasConfigDTO(codigo, AcademicYear.getCurrent());

        config.setFechaLimite(fecha);
        config.setOrdenacion("NOTA_MEDIA");
        entityManager.persist(config);

        asig.setTfgAsignaturasConfig(config);

        return asig.getCodigo();
    }

    @Transactional
    public void buildPersonaAsignatura(String codigo_asignatura, Long persona_id, String relacion, long creditos)
    {
        AsignaturasPersonaDTOPK pk1_1 = new AsignaturasPersonaDTOPK();
        pk1_1.setPersonaId(persona_id);
        pk1_1.setCodigoAsignatura(codigo_asignatura);
        pk1_1.setTipoRelacion(relacion);
        AsignaturasPersonaDTO ap1_1 = new AsignaturasPersonaDTO();
        ap1_1.setId(pk1_1);
        ap1_1.setCreditos(new BigDecimal(creditos));
        entityManager.persist(ap1_1);
    }

    @Transactional
    public PropuestaDTO buildPropuesta(String nombre, List<Long> tutores, AsignaturaDTO asignatura, String objetivos, String bibliografia, String descripcion,
                                       String observaciones, BigDecimal plazas, BigDecimal espera, Long perId) {
        return propuestasDAO.getPropuesta(propuestasDAO.create(nombre, tutores, asignatura.getCodigo(), objetivos, bibliografia, descripcion, observaciones, plazas, espera, perId));

    }

    @Transactional
    public void buildPersonaPropuesta(long propuesta_id, Long persona_id, boolean aceptada)
    {
        PropuestaDTO propuestaDTO = propuestasDAO.getPropuesta(propuesta_id);
        PersonaDTO personaDTO = personasDAO.getUsuario(persona_id);
        propuestasDAO.addSolicitud(propuestaDTO, personaDTO.getId(), aceptada, "Comentarios");

    }

    @Transactional
    public void buildCargaProfesor(Long persona_id, String codigo_asignatura, int carga) {
        personasDAO.createCargaProfesor(persona_id, codigo_asignatura, carga, new Long(1));
    }

    @Before
    @Transactional
    public void runOnce() throws ParseException
    {
        if (isInitialized)
            return;

        // String script = "classpath:inicializacionTests.sql";
        // Resource resource = ctx.getResource(script);
        // JdbcTestUtils.executeSqlScript(template, resource, true);
        Long persona_id = buildPersona(new Long(1), "Usuario Prueba 1");
        per1 = personasDAO.getUsuario(persona_id);
        per10 = personasDAO.getUsuario(buildPersona(new Long(10), "Usuario Prueba 10"));
        per100 = personasDAO.getUsuario(buildPersona(new Long(100), "Usuario Prueba 100"));
        per101 = personasDAO.getUsuario(buildPersona(new Long(101), "Usuario Prueba 101"));
        per102 = personasDAO.getUsuario(buildPersona(new Long(102), "Usuario Prueba 102"));
        asi1 = asignaturasDAO.getAsignatura(buildAsignatura("Asignatura Prueba 1", "0001", 10, 100, DateUtils.getDate("22/07/2014 07:22:00"), 1));
        asi10 = asignaturasDAO.getAsignatura(buildAsignatura("Asignatura Prueba 10", "0010", 10, 100, DateUtils.getDate("22/06/2016 06:22:11"), 2));
        buildPersonaAsignatura(asi1.getCodigo(), per1.getId(), "COORDINADOR", 0);
        buildPersonaAsignatura(asi1.getCodigo(), per1.getId(), "PROFESOR", 5);
        buildPersonaAsignatura(asi10.getCodigo(), per1.getId(), "PROFESOR", 2);
        buildPersonaAsignatura(asi1.getCodigo(), per10.getId(), "PROFESOR", 5);
        buildPersonaAsignatura(asi1.getCodigo(), per100.getId(), "ESTUDIANTE", 5);
        buildPersonaAsignatura(asi1.getCodigo(), per101.getId(), "ESTUDIANTE", 5);
        buildPersonaAsignatura(asi1.getCodigo(), per102.getId(), "ESTUDIANTE", 5);
        List<Long> tutores = Arrays.asList(1L, 2L, 3L);


        pro1 = buildPropuesta("Propuesta Prueba 1", tutores, asi1, "objetivos", "bibliografia", "descripcion", "observaciones", new BigDecimal(5), new BigDecimal(
                5), per1.getId());
        pro2 = buildPropuesta("Propuesta Prueba 2", tutores, asi10, "objetivos", "bibliografia", "descripcion", "observaciones", new BigDecimal(5),
                new BigDecimal(5), per1.getId());
        pro3 = buildPropuesta("Propuesta Prueba 3", tutores, asi1, "objetivos", "bibliografia", "descripcion", "observaciones", new BigDecimal(2), new BigDecimal(
                2), per1.getId());
        pro4 = buildPropuesta("Propuesta Prueba 4", tutores, asi1, "objetivos", "bibliografia", "descripcion", "observaciones", new BigDecimal(1), new BigDecimal(
                1), per1.getId());
//        buildPersonaPropuesta(pro1.getIdAsignaturaDTOPK(), per1.getIdAsignaturaDTOPK(), true);
//        buildPersonaPropuesta(pro3.getIdAsignaturaDTOPK(), per1.getIdAsignaturaDTOPK(), false);
        buildPersonaPropuesta(pro3.getId(), per100.getId(), true);
        buildPersonaPropuesta(pro4.getId(), per100.getId(), false);
        buildPersonaPropuesta(pro4.getId(), per101.getId(), true);
        buildCargaProfesor(per1.getId(), asi1.getCodigo(), 5);
        // isInitialized = true;
    }

    @Test
    public void empty()
    {
        Assert.assertNotNull(entityManager);
    }
}
