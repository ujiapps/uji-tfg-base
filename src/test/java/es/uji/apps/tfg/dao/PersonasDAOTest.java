package es.uji.apps.tfg.dao;

import java.math.BigDecimal;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import es.uji.apps.tfg.dto.CargaProfesorDTO;

@RunWith(SpringJUnit4ClassRunner.class)
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)
@ContextConfiguration({ "classpath:applicationContext-test.xml" })
@Transactional
public class PersonasDAOTest extends TestClass
{
    @Autowired
    PersonasDAO personasDAO;

    public PersonasDAOTest()
    {
    }

    @Test
    public void getCargaProfesorTest()
    {
        CargaProfesorDTO existe = personasDAO.getCargaProfesorDTO(per1.getId(), asi1.getCodigo());
        Assert.assertNotNull(existe);
    }
    
    @Test
    public void coordinaAsignaturaTest()
    {
        Assert.assertFalse(personasDAO.coordinaAsignatura(per10.getId(), asi1.getCodigo()));
        Assert.assertTrue(personasDAO.coordinaAsignatura(per1.getId(), asi1.getCodigo()));
    }

    @Test
    public void createCargaProfesorTest()
    {
        CargaProfesorDTO testResult = personasDAO.createCargaProfesor(per100.getId(), asi10.getCodigo(), 5, per1.getId());
        Assert.assertNotNull(testResult);
    }

    @Test
    public void deleteCargaProfesorTest()
    {
        personasDAO.createCargaProfesor(per1.getId(), asi1.getCodigo(), 5, per1.getId());
        CargaProfesorDTO existe = personasDAO.getCargaProfesorDTO(per1.getId(), asi1.getCodigo());
        Assert.assertNotNull(existe);

        boolean result = personasDAO.deleteCargaProfesor(per1.getId(), asi1.getCodigo());
        Assert.assertTrue(result);

        existe = personasDAO.getCargaProfesorDTO(per1.getId(), asi1.getCodigo());
        Assert.assertNull(existe);
    }

    @Test
    public void distribuirCargaProfesoresTest()
    {
        boolean result = personasDAO.distribuirCargaProfesores(asi10.getCodigo(), per1.getId());
        Assert.assertTrue(result);

        CargaProfesorDTO carga1 = personasDAO.getCargaProfesorDTO(per1.getId(), asi1.getCodigo());
        CargaProfesorDTO carga10 = personasDAO.getCargaProfesorDTO(per10.getId(), asi1.getCodigo());
        Assert.assertEquals(carga1.getCarga(), carga10.getCarga());
        BigDecimal esperado = new BigDecimal(1);
        BigDecimal recibido = new BigDecimal(carga1.getCarga().floatValue());
        Assert.assertEquals(esperado, recibido);
    }

    @Test
    public void getCargaTotalProfesorTest()
    {
        personasDAO.distribuirCargaProfesores(asi1.getCodigo(), per1.getId());
        personasDAO.distribuirCargaProfesores(asi10.getCodigo(), per1.getId());
        Integer total = personasDAO.getCargaTotalProfesor(per1.getId());
        Integer esperado = 1;
        Assert.assertEquals(total, esperado);
    }
}
