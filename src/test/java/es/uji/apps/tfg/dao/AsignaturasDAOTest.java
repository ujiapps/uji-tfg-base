package es.uji.apps.tfg.dao;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import es.uji.apps.tfg.utils.AcademicYear;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;

import es.uji.apps.tfg.dto.AsignaturaDTO;
import es.uji.apps.tfg.dto.AsignaturasConfigDTO;
import es.uji.apps.tfg.utils.DateUtils;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)
@ContextConfiguration({ "classpath:applicationContext-test.xml" })
@Transactional
public class AsignaturasDAOTest extends TestClass
{

    @Autowired
    AsignaturasDAO asignaturasDAO;

    public void AsingaturasDAOTest() {
        
    }
    
    @Test
    public void getAsignaturas()
    {
        List<AsignaturaDTO> listado = asignaturasDAO.getAsignaturas();
        Assert.assertEquals(2, listado.size());
    }

    // @Test
    // @Transactional
    // public void getAsignatura() throws Exception
    // {
    // addAsignatura();
    // AsignaturaDTO asig = asignaturasDAO.getAsignatura(1);
    // Assert.assertNotNull(asig);
    // }

    @Test
    public void addAsignatura()
    {
        AsignaturaDTO nueva = new AsignaturaDTO("1002", AcademicYear.getCurrent());
        nueva.setCreditos(new BigDecimal(12));
        nueva.setMatriculas(new BigDecimal(120));       
        nueva.setNombre("Asignatura 1002");
                
        entityManager.persist(nueva);
    }

    @Test    
    public void setFechaLimiteTest()
    {
        Date nuevafecha = DateUtils.getCurrent();        
        
        AsignaturaDTO nueva = asignaturasDAO.getAsignatura(asi10.getCodigo());
        AsignaturasConfigDTO config = nueva.getTfgAsignaturasConfig();
        config.setFechaLimite(nuevafecha);
        entityManager.merge(config);
        nueva.setTfgAsignaturasConfig(config);
        entityManager.merge(nueva);
        Assert.assertEquals(new SimpleDateFormat("dd-MM-YYYY").format(nuevafecha), new SimpleDateFormat("dd-MM-YYYY").format(nueva.getTfgAsignaturasConfig().getFechaLimite()));
    }
}
