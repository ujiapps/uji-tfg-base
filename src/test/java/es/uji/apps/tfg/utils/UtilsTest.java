package es.uji.apps.tfg.utils;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import es.uji.apps.tfg.services.NotificacionesService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/applicationContext-test.xml" })
public class UtilsTest
{

    @Test
    public void tidyClean()
    {
        String limpio = StringUtils.tidyClean("a<BR >a<BR>aaa<br>aaa<bR>");
        Assert.assertEquals("a" + '\n' + "<br />a" + '\n' + "<br />aaa" + '\n' + "<br />aaa" + '\n' + "<br />" + '\n' + "", limpio);
    }

    @Test
    public void tidyClean2()
    {
        String limpio = StringUtils.tidyClean("a<BR style=\"color: rgb(34, 34, 34);\">a<BR>aaa<br>aaa<bR>");
        Assert.assertEquals("a" + '\n' + "<br style=\"color: rgb(34, 34, 34);\" />a" + '\n' + "<br />aaa" + '\n' + "<br />aaa" + '\n' + "<br />" + '\n' + "", limpio);
    }
    
    @Test
    public void esFechaFutura()
    {
        Date now = new Date();
        Date futura = new Date(now.getTime() + 86400);
        Assert.assertTrue(DateUtils.esFechaFutura(new Timestamp(futura.getTime())));
    }

    @Test
    public void esFechaFuturaFalla()
    {
        Date now = new Date();
        Date futura = new Date(now.getTime() - 86400);
        Assert.assertFalse(DateUtils.esFechaFutura(new Timestamp(futura.getTime())));
    }

    @Test
    public void tieneTurnoHabilitado()
    {
        Date now = new Date();
        Date fechaTurno = new Date(now.getTime() - 86400 * 1000);
        Date fechaFin = new Date(fechaTurno.getTime() + 86400 * 5 * 1000);
        Assert.assertTrue(DateUtils.tieneTurnoHabilitado(new Timestamp(fechaTurno.getTime()), new Timestamp(fechaFin.getTime())));
    }

    @Test
    public void noTieneTurnoHabilitado()
    {
        Date now = new Date();
        Date fechaTurno = new Date(now.getTime() + 86400 * 1000);
        Date fechaFin = new Date(fechaTurno.getTime() + 86400 * 5 * 1000);

        Assert.assertFalse(DateUtils.tieneTurnoHabilitado(new Timestamp(fechaTurno.getTime()), new Timestamp(fechaFin.getTime())));
    }

    @Test
    public void noTieneTurnoHabilitadoPorFechaNula()
    {
        Date now = new Date();
        Date fechaTurno = new Date(now.getTime() + 86400 * 1000);
        Date fechaFin = new Date(fechaTurno.getTime() + 86400 * 5 * 1000);
        Assert.assertFalse(DateUtils.tieneTurnoHabilitado(null, new Timestamp(fechaFin.getTime())));
    }

    @Test
    public void getTextoParseado()
    {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("reemplazo", "reemplazado");

        Assert.assertEquals("Texto reemplazado", NotificacionesService.getTextoParseado("Texto #REEMPLAZO#", map));
    }

    @Test
    public void getTextoParseado2()
    {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("reemplazo", "reemplazado");
        map.put("otro", "un");
        Assert.assertEquals("Texto reemplazado y un trozo más", NotificacionesService.getTextoParseado("Texto #REEMPLAZO# y #OTRO# trozo más", map));
    }

    @Test
    public void getTextoParseado3()
    {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("reemplazo", "reemplazado");
        map.put("otro", "un");
        Assert.assertEquals("Texto reemplazado y un trozo reemplazado más",
                NotificacionesService.getTextoParseado("Texto #REEMPLAZO# y #OTRO# trozo #REEMPLAZO# más", map));
    }

    @Test
    public void getTextParseadoKeyInexistente()
    {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("reemplazo", "reemplazado");
        map.put("otro", "un");
        map.put("noexiste", "nada");
        Assert.assertEquals("Texto reemplazado y un trozo reemplazado más",
                NotificacionesService.getTextoParseado("Texto #REEMPLAZO# y #OTRO# trozo #REEMPLAZO# más", map));
    }
}
