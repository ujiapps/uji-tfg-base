package es.uji.apps.tfg.services;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import es.uji.apps.tfg.dto.*;
import es.uji.apps.tfg.excepciones.AsignaturaNoConfiguradaException;
import es.uji.apps.tfg.utils.AcademicYear;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import es.uji.apps.tfg.excepciones.PropuestaException;
import es.uji.apps.tfg.model.Propuesta;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/applicationContext-test.xml" })
public class PropuestasServiceTest
{
    PropuestasService propuestasService = new PropuestasService();

    private AsignaturaDTO buildAsignatura(String codigo)
    {
        AsignaturasConfigDTO conf = new AsignaturasConfigDTO(codigo, AcademicYear.getCurrent());
        conf.setOrdenacion("NOTA_MEDIA");
        conf.setFechaLimite(new Date((new Date()).getTime() + 86400000));
        //conf.setIdAsignaturaConfigDTOPK(new AsignaturasConfigDTOPK(codigo, AcademicYear.getCurrent()));

        AsignaturaDTO asignaturaDTO = new AsignaturaDTO(codigo, AcademicYear.getCurrent());
        asignaturaDTO.setCreditos(new BigDecimal(10));
        asignaturaDTO.setNombre("TEST ASIGNATURA(" + codigo + ")");
        asignaturaDTO.setEstudioId(new BigDecimal(1));
        asignaturaDTO.setTfgAsignaturasConfig(conf);
        return asignaturaDTO;
    }

    private PropuestaDTO buildPropuestaEnEspera(Long id, Long solicitanteAceptado)
    {
        PropuestaDTO propuestaNueva = new PropuestaDTO(id);
        propuestaNueva.setEspera(new BigDecimal(1));
        propuestaNueva.setPlazas(new BigDecimal(1));
        propuestaNueva.setNombre("Test Propuesta (" + id.toString() + ")");
        propuestaNueva.setFechaCreacion(new Date());
        //TODO Hacer test
        //propuestaNueva.setTfgExtPersona(new PersonaDTO(9999L));
        PersonasPropuestaDTOPK ppDTOPK = new PersonasPropuestaDTOPK();
        ppDTOPK.setPersonaId(solicitanteAceptado);
        ppDTOPK.setPropuestaId(id);
        PersonasPropuestaDTO inscripcion = new PersonasPropuestaDTO();
        inscripcion.setId(ppDTOPK);
        inscripcion.setTfgExtPersona(new PersonaDTO(solicitanteAceptado));
        inscripcion.setTfgPropuesta(propuestaNueva);
        inscripcion.setAceptada(new BigDecimal(1));

        propuestaNueva.setTfgPersonasPropuestas(Arrays.asList(inscripcion));
        propuestaNueva.setTfgExtAsignatura(buildAsignatura("TEST1000"));

        return propuestaNueva;
    }

    private PropuestaDTO buildPropuestaConPlazas(Long id)
    {
        PropuestaDTO propuestaNueva = new PropuestaDTO(id);
        propuestaNueva.setEspera(new BigDecimal(2));
        propuestaNueva.setPlazas(new BigDecimal(2));
        propuestaNueva.setNombre("Test Propuesta (" + id.toString() + ")");
        propuestaNueva.setFechaCreacion(new Date());
        //TODO Hacer Test
//        propuestaNueva.setTfgExtPersona(new PersonaDTO(2000L));
        PersonasPropuestaDTOPK ppDTOPK = new PersonasPropuestaDTOPK();
        ppDTOPK.setPersonaId(1000L);
        ppDTOPK.setPropuestaId(id);
        PersonasPropuestaDTO inscripcion = new PersonasPropuestaDTO();
        inscripcion.setId(ppDTOPK);
        inscripcion.setTfgExtPersona(new PersonaDTO(1000L));
        inscripcion.setTfgPropuesta(propuestaNueva);
        inscripcion.setAceptada(new BigDecimal(1));

        propuestaNueva.setTfgPersonasPropuestas(Arrays.asList(inscripcion));
        propuestaNueva.setTfgExtAsignatura(buildAsignatura("TEST1000"));

        return propuestaNueva;
    }

    private PropuestaDTO buildPropuestaSinPlazas(Long id)
    {
        PropuestaDTO propuestaNueva = new PropuestaDTO(id);
        propuestaNueva.setEspera(new BigDecimal(1));
        propuestaNueva.setPlazas(new BigDecimal(1));
        propuestaNueva.setNombre("Test Propuesta (" + id.toString() + ")");
        propuestaNueva.setFechaCreacion(new Date());
        //TODO hacer test
//        propuestaNueva.setTfgExtPersona(new PersonaDTO(2222L));
        PersonasPropuestaDTOPK ppDTOPK = new PersonasPropuestaDTOPK();
        ppDTOPK.setPersonaId(1111L);
        ppDTOPK.setPropuestaId(id);
        PersonasPropuestaDTO inscripcion = new PersonasPropuestaDTO();
        inscripcion.setId(ppDTOPK);
        inscripcion.setTfgExtPersona(new PersonaDTO(1111L));
        inscripcion.setTfgPropuesta(propuestaNueva);
        inscripcion.setAceptada(new BigDecimal(1));
        PersonasPropuestaDTOPK ppDTOPK2 = new PersonasPropuestaDTOPK();
        ppDTOPK2.setPersonaId(2222L);
        ppDTOPK2.setPropuestaId(id);
        PersonasPropuestaDTO inscripcion2 = new PersonasPropuestaDTO();
        inscripcion.setId(ppDTOPK2);
        inscripcion2.setTfgExtPersona(new PersonaDTO(2222L));
        inscripcion2.setTfgPropuesta(propuestaNueva);
        inscripcion2.setAceptada(new BigDecimal(0));

        propuestaNueva.setTfgPersonasPropuestas(Arrays.asList(inscripcion, inscripcion2));
        propuestaNueva.setTfgExtAsignatura(buildAsignatura("TEST1000"));

        return propuestaNueva;
    }

    private List<PersonasPropuestaDTO> buildAsignaciones(List<PropuestaDTO> propuestas, Long id)
    {
        List<PersonasPropuestaDTO> salida = new ArrayList<PersonasPropuestaDTO>();
        for (PropuestaDTO propuesta : propuestas)
        {
            for (PersonasPropuestaDTO pp : propuesta.getTfgPersonasPropuestas())
            {
                if (pp.getId().getPersonaId() == id)
                {
                    salida.add(pp);
                }
            }
        }
        return salida;
    }

    @Test
    public void testBuildPropuestaConEspera()
    {
        Assert.assertEquals(1, new Propuesta(buildPropuestaEnEspera(1L, 1000L), true).getDisponibilidad());
    }

    @Test
    public void testBuildPropuestaConPlazas()
    {
        Assert.assertEquals(0, new Propuesta(buildPropuestaConPlazas(1L), true).getDisponibilidad());
    }

    @Test
    public void testBuildPropuestaSinPlazas()
    {
        Assert.assertEquals(-1, new Propuesta(buildPropuestaSinPlazas(1L), true).getDisponibilidad());
    }

    @Test(expected = PropuestaException.class)
    public void testDosPropuestasInscritas() throws PropuestaException, AsignaturaNoConfiguradaException {
        List<PersonasPropuestaDTO> asignaciones = Arrays.asList(new PersonasPropuestaDTO(), new PersonasPropuestaDTO());
        propuestasService.comprobarDisponibilidadDePropuesta(buildPropuestaConPlazas(1L), asignaciones);

    }

    @Test
    public void testNingunaPropuestaInscrita() throws PropuestaException, AsignaturaNoConfiguradaException {
        propuestasService.comprobarDisponibilidadDePropuesta(buildPropuestaConPlazas(1000L), null);
        Assert.assertNull(null);

    }

    @Test(expected = PropuestaException.class)
    public void testYaInscritoEnPropuesta() throws PropuestaException, AsignaturaNoConfiguradaException {
        PropuestaDTO propuestaNueva = buildPropuestaConPlazas(1L);

        List<PropuestaDTO> inscritas = Arrays.asList(propuestaNueva);
        propuestasService.comprobarDisponibilidadDePropuesta(propuestaNueva, buildAsignaciones(inscritas, 1000L));
    }

    @Test(expected = PropuestaException.class)
    public void testYaInscritoEnOtraPropuestaEnEspera() throws PropuestaException, AsignaturaNoConfiguradaException {
        Long yo = 2000L;
        Long otro = 1000L;
        PropuestaDTO propuestaNueva = buildPropuestaEnEspera(1L, otro);
        PropuestaDTO propuestaInscrita = buildPropuestaEnEspera(2L, otro);
        PersonasPropuestaDTOPK ppDTOPK = new PersonasPropuestaDTOPK();
        ppDTOPK.setPersonaId(yo);
        ppDTOPK.setPropuestaId(2L);
        PersonasPropuestaDTO inscripcionMiaEstandoYoEnEspera = new PersonasPropuestaDTO();
        inscripcionMiaEstandoYoEnEspera.setId(ppDTOPK);
        inscripcionMiaEstandoYoEnEspera.setTfgExtPersona(new PersonaDTO(yo));
        inscripcionMiaEstandoYoEnEspera.setTfgPropuesta(propuestaInscrita);
        inscripcionMiaEstandoYoEnEspera.setAceptada(new BigDecimal(0));
        List<PersonasPropuestaDTO> asignaciones = new ArrayList<PersonasPropuestaDTO>();

        asignaciones.add(propuestaInscrita.getTfgPersonasPropuestas().get(0));
        asignaciones.add(inscripcionMiaEstandoYoEnEspera);
        propuestaInscrita.setTfgPersonasPropuestas(asignaciones);

        propuestasService.comprobarDisponibilidadDePropuesta(propuestaNueva, buildAsignaciones(Arrays.asList(propuestaInscrita), yo));
    }

    @Test(expected = PropuestaException.class)
    public void testYaInscritoEnOtraPropuestaConPlazas() throws PropuestaException, AsignaturaNoConfiguradaException {
        PropuestaDTO propuestaNueva = buildPropuestaConPlazas(1L);
        PropuestaDTO propuestaInscrita = buildPropuestaConPlazas(2L);

        propuestasService.comprobarDisponibilidadDePropuesta(propuestaNueva, buildAsignaciones(Arrays.asList(propuestaInscrita), 1000L));
    }

    @Test(expected = PropuestaException.class)
    public void testYaInscritoEnPropuestaConPlazasQueAhoraEstaEnEspera() throws PropuestaException, AsignaturaNoConfiguradaException {
        PropuestaDTO propuestaNueva = buildPropuestaConPlazas(1L);
        PropuestaDTO propuestaInscrita = buildPropuestaConPlazas(2L);
        PersonaDTO ocupanteUltimaPlazaLibre = new PersonaDTO(3000L);
        PersonasPropuestaDTOPK ppDTOPK = new PersonasPropuestaDTOPK();
        ppDTOPK.setPersonaId(ocupanteUltimaPlazaLibre.getId());
        ppDTOPK.setPropuestaId(propuestaInscrita.getId());

        PersonasPropuestaDTO ppDTO = new PersonasPropuestaDTO();
        ppDTO.setAceptada(new BigDecimal(1));
        ppDTO.setTfgExtPersona(ocupanteUltimaPlazaLibre);
        ppDTO.setTfgPropuesta(propuestaInscrita);
        ppDTO.setId(ppDTOPK);

        List<PersonasPropuestaDTO> asignaciones = new ArrayList<PersonasPropuestaDTO>();

        asignaciones.add(propuestaInscrita.getTfgPersonasPropuestas().get(0));
        asignaciones.add(ppDTO);
        propuestaInscrita.setTfgPersonasPropuestas(asignaciones);

        propuestasService.comprobarDisponibilidadDePropuesta(propuestaNueva, buildAsignaciones(Arrays.asList(propuestaInscrita), 1000L));
    }

    @Test(expected = PropuestaException.class)
    public void testNoHayPlazas() throws PropuestaException, AsignaturaNoConfiguradaException {
        PropuestaDTO propuestaNueva = buildPropuestaSinPlazas(1L);

        propuestasService.comprobarDisponibilidadDePropuesta(propuestaNueva, null);
    }
    
    @Test(expected = PropuestaException.class)
    public void testYaInscritoEnDosPropuestas() throws PropuestaException, AsignaturaNoConfiguradaException {
        Long yo = 1100L;
        PropuestaDTO propuestaNueva = buildPropuestaConPlazas(1L);
        //Primera Propuesta: Está en espera
        PropuestaDTO propuestaInscrita = buildPropuestaConPlazas(2L);
        //Segun propuesta: Está aceptado
        PropuestaDTO propuestaEnEspera = buildPropuestaEnEspera(3L, yo);
        PersonaDTO yoDTO = new PersonaDTO(yo);
        PersonasPropuestaDTOPK ppDTOPK = new PersonasPropuestaDTOPK();
        ppDTOPK.setPersonaId(yoDTO.getId());
        ppDTOPK.setPropuestaId(propuestaInscrita.getId());

        PersonasPropuestaDTO ppDTO = new PersonasPropuestaDTO();
        //Lo añadimos en espera
        ppDTO.setAceptada(new BigDecimal(0));
        ppDTO.setTfgExtPersona(yoDTO);
        ppDTO.setTfgPropuesta(propuestaInscrita);
        ppDTO.setId(ppDTOPK);

        List<PersonasPropuestaDTO> asignaciones = new ArrayList<PersonasPropuestaDTO>();

        asignaciones.add(propuestaInscrita.getTfgPersonasPropuestas().get(0));
        asignaciones.add(ppDTO);
        propuestaInscrita.setTfgPersonasPropuestas(asignaciones);
        
        propuestasService.comprobarDisponibilidadDePropuesta(propuestaNueva, buildAsignaciones(Arrays.asList(propuestaInscrita, propuestaEnEspera), yo));
    }
}
