Ext.define('TFG.store.Propuestas',
{
    extend : 'Ext.data.Store',
    model : 'TFG.model.Propuesta',

    autoLoad : true,
    autoSync: true,

    proxy :
    {
        type : 'rest',
        url : '/tfg/rest/propuestas',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }
});