Ext.define('TFG.store.Profesores',
{
    extend : 'Ext.data.Store',
    model : 'TFG.model.Usuario',

    autoLoad : true,
    autoSync: true,

    proxy :
    {
        type : 'rest',
        url : '/tfg/rest/profesores',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }
});