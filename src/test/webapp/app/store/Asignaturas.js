Ext.define('TFG.store.Asignaturas',
{
    extend : 'Ext.data.Store',
    model : 'TFG.model.Asignatura',

    autoLoad : true,
    autoSync: true,

    proxy :
    {
        type : 'rest',
        url : '/tfg/rest/asignaturas',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }
    // ,
    // filter: function(filters, value) {
    //     Ext.data.Store.prototype.filter.apply(this, [
    //         filters,
    //         value ? new RegExp(Ext.String.escapeRegex(value), 'i') : value
    //     ]);
    // }    
}); 