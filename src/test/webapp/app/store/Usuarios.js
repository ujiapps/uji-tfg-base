Ext.define('TFG.store.Usuarios',
{
    extend : 'Ext.data.Store',
    model : 'TFG.model.Usuario',

    autoLoad : true,
    autoSync: true,

    proxy :
    {
        type : 'rest',
        url : '/tfg/rest/usuarios',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }
});