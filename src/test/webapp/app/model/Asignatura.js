Ext.define('TFG.model.Asignatura',
{
    extend: 'Ext.data.Model',

    fields: [{
        name: 'id',
        type: 'int'
    },{
        name: 'nombre',
        type: 'string'
    },{
        name: 'codigo',
        type: 'string'
    },{
        name: 'codinom',
        type: 'string'
    }]
});
