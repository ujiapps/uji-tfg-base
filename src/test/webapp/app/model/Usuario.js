Ext.define('TFG.model.Usuario',
{
    extend: 'Ext.data.Model',

    fields: [{
        name: 'id',
        type: 'int',
        useNull: true
    },{
        name: 'nombre',
        type: 'string'
    }]
});
