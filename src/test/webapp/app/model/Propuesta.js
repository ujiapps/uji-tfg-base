Ext.define('TFG.model.Propuesta',
    {
        extend: 'Ext.data.Model',

        fields: [
            {
                name: 'id',
                type: 'int',
                useNull: true
            },{
                name: 'autor',
                type: 'string'
            },{
                name: 'autor_id',
                type: 'int'
            },{
                name: 'fechaCreacion',
                type: 'date'
            },{
                name: 'asignatura',
                type: 'string'                
            },{
                name: 'asignatura_id',
                type: 'int'
            },{
                name: 'nombre',
                type: 'string'
            },{
                name: 'objetivos',
                type: 'string'
            },{
                name: 'bibliografia',
                type: 'string'
            },{
                name: 'descripcion',
                type: 'string'
            },{
                name: 'observaciones',
                type: 'string'
            },{
                name: 'disponibilidad',
                type: 'int'
            },{
                name: 'espera',
                type: 'int'
            },{
                name: 'plazas',
                type: 'int'
            },{
                name: 'gente_esperando',
                type: 'int'
            },{
                name: 'plazas_disponibles',
                type: 'int'
            }
        ]
    });
