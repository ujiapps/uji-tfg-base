Ext.define('Ext.ux.uji.Application',
{
    extend : 'Ext.app.Application',

    init : function()
    {
        this.addForeignColumnsToModels();

        var me = this;
        Ext.define('Ext.ux.uji.AppConfig',
        {
            alias : 'widget.AppConfig',
            singleton : true,
            appName : me.name
        });
    },

    addForeignColumnsToModels : function()
    {
        var me = this;
        Ext.each(this.models, function(modelName)
        {
            var model = Ext.ModelManager.getModel(me.name + '.model.' + modelName), fields = [];
            if (model)
            {
                Ext.each(model.getFields(), function(field)
                {
                    if (field.foreign)
                    {
                        fields.push(me.createMappedField(field));
                        field.defaulValue = field.defaulValue || {};
                    }
                    fields.push(field);
                });
                model.setFields(fields);
            }
        });
    },

    createMappedField : function(field)
    {
        return Ext.create('Ext.data.Field',
        {
            name : field.name + 'Id',
            type : 'int',
            mapping : field.name + '.id',
            useNull : field.useNull
        });
    }

});;Ext.require('Ext.container.Viewport');
Ext.require('Ext.layout.container.Border');

Ext.define('Ext.ux.uji.ApplicationViewport',
{
    extend : 'Ext.Viewport',
    alias : 'widget.applicationViewport',

    layout : 'border',
    treeWidth : 235,
    aplicacionCompleta : true,
    tabPanel : {},

    initComponent : function()
    {
        this.callParent(arguments);

        if (this.aplicacionCompleta)
        {
            this.buildLogoPanel();
            this.buildNavigationTree();
            this.buildWorkTabPanel();
        }

        this.initGlobalAjaxEvents();
        this.buildLoadingIndicator();

        this.i18n();
    },

    buildLogoPanel : function()
    {
        var logoPanel = new Ext.Panel(
            {
                region: 'north',
                layout: 'border',
                height: 70,
                items: [
                    {
                        region: 'center',
                        border: 0,
                        html: '<div style="background: url(http://e-ujier.uji.es/img/portal2/imagenes/cabecera_1px.png) repeat-x scroll left top transparent; height: 70px;">' + 
                            '<img src="http://e-ujier.uji.es/img/portal2/imagenes/logo_uji_horizontal.png" style="float: left;margin: 10px 16px;" />' + 
                            '<div style="float:left; margin-top:11px;">' + 
                            '<span style="color: rgb(255,255, 255); font-family: Helvetica,Arial,sans-serif;font-size:1.2em;">E-UJIER@</span><br/>' + 
                            '<span style="color: #CDCCE5; font-family: Helvetica,Arial,sans-serif; font-size:2.3em;">' + this.tituloAplicacion + '</span></div></div>'
                    },
                    {
                        region: 'east',
                        border: 0,
                        width: 120,
                        html: '<div style="background: url(http://e-ujier.uji.es/img/portal2/imagenes/cabecera_1px.png) repeat-x scroll left top transparent; height: 70px; padding-top: 35px">' + 
                            '<span style="color: #CDCCE5; font-family: Helvetica,Arial,sans-serif;"><img src="http://static.uji.es/js/extjs/uji-commons-extjs/img/lock.png"/>' + 
                            '<a style="color:inherit" href="http://xmlrpc.uji.es/lsm/logout_sso.php">desconnecteu-vos</a></span></div>'
                    }
                ]
            }
        );

        this.add(logoPanel);
    },

    buildNavigationTree : function()
    {
        var me = this;

        var navigationTree = Ext.create('Ext.tree.Panel',
        {
            title : 'Conectat com ' + login + '@',
            region : 'west',
            lines : false,
            width : this.treeWidth,
            split : true,
            collapsible : true,
            autoScroll : true,
            rootVisible : false,
            bodyStyle : 'padding-bottom:20px;',
            store : Ext.create('Ext.data.TreeStore',
            {
                autoLoad : true,

                root :
                {
                    expanded : true
                },

                proxy :
                {
                    type : 'ajax',
                    url : '/' + me.codigoAplicacion.toLowerCase() + '/rest/navigation/class?codigoAplicacion=' + this.codigoAplicacion,
                    reader :
                    {
                        type : 'json',
                        root : 'row'
                    }
                }
            })
        });

        this.add(navigationTree);
    },

    buildWorkTabPanel : function()
    {
        this.tabPanel = Ext.create('Ext.ux.uji.TabPanel',
        {
            deferredRender : false,
            region : 'center'
        });

        if (this.dashboard)
        {
            eval('this.tabPanel.addTab(Ext.create("' + this.codigoAplicacion + '.view.dashboard.PanelDashboard", {}));'); // jshint ignore:line
        }

        this.add(this.tabPanel);
    },

    addNewTab : function(id, config)
    {
        var params = Ext.encode(config || {});
        var newPanel = eval("Ext.create('" + id + "', " + params + ")"); // jshint ignore:line
        this.tabPanel.addTab(newPanel);

        newPanel.on('newtab', function(config)
        {
            if (config && config.pantalla)
            {
                var params = Ext.util.JSON.encode(config || {});
                var newPanel = eval("Ext.create('" + config.pantalla + "', " + params + ")"); // jshint ignore:line
                this.tabPanel.addTab(newPanel);
            }
            else
            {
                alert('[ApplicationViewport.js] ¡Atención!.' + 
                      'El parámetro "pantalla" (newtab) con' + 
                      'el nombre del componente que se quiere' + 
                      'instanciar debe estar definido. Por ejemplo:' + 
                      '"pantalla : \'UJI.XX.GestionXXXPanel\'"');
            }
        }, this);
    },

    i18n : function()
    {
        Ext.grid.RowEditor.prototype.cancelBtnText = "Cancel·lar";
        Ext.grid.RowEditor.prototype.saveBtnText = "Actualitzar";

        Ext.MessageBox.buttonText.yes = "Sí";
        Ext.MessageBox.buttonText.ok = "Acceptar";
        Ext.MessageBox.buttonText.cancel = "Cancel·lar";
    },

    initGlobalAjaxEvents : function()
    {
        Ext.Ajax.on('beforerequest', function()
        {
            Ext.getCmp('_$loadingIndicator').show();
        });

        Ext.Ajax.on('requestcomplete', function(conn, response, options)
        {
            Ext.getCmp('_$loadingIndicator').hide();

            if (options.isUpload)
            {
                var responseJSON = Ext.decode(response.responseText);
                var msgList = responseJSON.msg || responseJSON.message;

                if (msgList)
                {
                    Ext.MessageBox.show(
                    {
                        title : 'Error',
                        msg : msgList[0].firstChild.nodeValue,
                        buttons : Ext.MessageBox.OK,
                        icon : Ext.MessageBox.ERROR
                    });
                }
            }
        });

        Ext.Ajax.on('requestexception', function(conn, response, options)
        {
            Ext.getCmp('_$loadingIndicator').hide();

            if (response.responseText)
            {
                var responseJSON = Ext.decode(response.responseText);
                var msgList = responseJSON.msg || responseJSON.message;

                if (msgList)
                {
                    alert(msgList);
                }
            }
        });
    },

    buildLoadingIndicator : function()
    {
        new Ext.Panel(
        {
            xtype : 'panel',
            id : '_$loadingIndicator',
            frame : false,
            border : false,
            html : '<div style="font:normal 11px tahoma,arial,helvetica,sans-serif;border:1px solid gray;padding:8px;background-color:#fff;">' +
                   '<img style="margin-right:4px;" align="left" src="http://static.uji.es/img/commons/loading.gif" /> Carregant...</div>',
            hidden : true,
            style : 'z-index: 80000; position:absolute; top:5px; right:5px;',
            renderTo : document.body,
            width : 120
        });

    }
});
;Ext.define('Ext.ux.uji.TabPanel',
{
    extend : 'Ext.TabPanel',
    alias : 'widget.tabPanel',

    activeTab : 0,
    margins : '0 5 5 0',
    tabWidth : 150,
    minTabWidth : 120,
    enableTabScroll : true,
    deferredRender : false,
    autoShow : true,
    frame : false,
    plain : true,
    border : true,
    plugins : [
    {
        ptype : 'tabclosemenu'
    } ],

    initComponent : function()
    {
        this.callParent(arguments);
    },

    addTab : function(panel)
    {
        var tabs = this.down('panel[title="' + panel.title + '"]');

        if (tabs)
        {
            this.remove(tabs, true);
        }

        this.add(panel);
        this.setActiveTab(panel.id);

        this.doLayout();
    }
});
;function syncStoreLoad(storeList, baseParams, onComplete)
{
    if (storeList && storeList.length > 0)
    {
        var currentStore = storeList.shift();

        currentStore.on("load", function(store, records, options)
        {
            syncStoreLoad(storeList, baseParams, onComplete);
            store.un("load");
        });

        currentStore.load(
        {
            params : baseParams
        });
    }
    else
    {
        if (onComplete)
        {
            onComplete();
        }
    }
}

function initCap(str)
{
    return str.substring(0, 1).toUpperCase() + str.substring(1, str.length).toLowerCase();
}

function isEmptyDictionary(dic)
{
    for ( var prop in dic)
    {
        if (dic.hasOwnProperty(prop))
        {
            return false;
        }
    }
    
    return true;
}

function padLeft(text, char, size)
{
    var pattern = char;
    
    for (var i = 1; i <= size; i++)
    {
        pattern = pattern + char;
    }
    
    return (pattern + text).slice(-size);
}
;Ext.define('Ext.ux.uji.form.LookupComboBox',
{
    extend : 'Ext.form.ComboBox',
    alias : 'widget.lookupcombobox',

    mode : 'local',
    triggerAction : 'all',
    editable : false,
    valueField : 'id',
    displayField : 'nombre',
    store : {},

    extraFields : [],
    appPrefix : '',
    bean : 'base',
    windowTitle : 'Cercar registres',
    windowLayout : 'border',
    windowModal : true,
    windowHidden : true,
    windowWidth : 400,
    windowHeight : 400,
    windowCloseAction : 'hide',
    windowClearAfterSearch : false,

    listConfig : 
    {
        maxHeight: 0
    },

    initComponent : function()
    {
        this.store = Ext.create('Ext.data.ArrayStore',
        {
            fields : [ 'id', 'nombre' ].concat(this.extraFields),
            data : [ [ '', '' ] ]
        });

        this.on('expand', function(combo)
        {
            if (!this.lookupWindow)
            {
                this.lookupWindow = this.getLookupWindow(combo);
            }

            this.lookupWindow.show();
        });

        this.oldWidth = this.width;

        this.callParent(arguments);
    },

    executeSearch : function(grid, query)
    {
        grid.store.load(
        {
            query : query,
            bean : this.bean
        });
    },

    removeStore : function()
    {
        this.store.removeAll();
        this.clearValue();
        this.store.loadData([ [ '', '' ] ]);
    },

    loadRecord : function(id, nombre)
    {
        var Record = Ext.data.Record.create([ 'id', 'nombre' ].concat(this.extraFields));

        var record = new Record(
        {
            id : id,
            nombre : nombre

        }, id);

        this.store.removeAll();
        this.clearValue();

        this.store.add([ record ]);
        this.store.commitChanges();

        this.setValue(record.data.id);
    },

    getLookupWindow : function(combo)
    {
        var ref = this;
        var window = Ext.create('Ext.ux.uji.form.LookupWindow',
        {
            bean : ref.bean,
            appPrefix : ref.appPrefix,
            extraFields : ref.extraFields,
            width : ref.windowWidth,
            height : ref.windowHeight,
            modal : ref.windowModal,
            title : ref.windowTitle,
            layout : ref.windowLayout,
            hidden : ref.windowHidden,
            closeAction : ref.windowCloseAction,
            clearAfterSearch : ref.windowClearAfterSearch
        });

        window.on('LookoupWindowClickSeleccion', function(record)
        {
            combo.store.removeAll();
            combo.clearValue();

            combo.store.add([ record.data ]);
            combo.store.commitChanges();

            combo.setValue(record.data.id);
        });

        this.relayEvents(window, [ 'LookoupWindowClickSeleccion' ]);

        window.show();

        return window;
    }
});
;Ext.define('Ext.ux.uji.form.LookupWindow',
{
    extend : 'Ext.Window',

    alias : 'widget.lookupWindow',
    appPrefix : '',
    bean : 'base',
    lastQuery : '',
    queryField : '',
    formularioBusqueda : '',
    gridBusqueda : '',
    botonBuscar : '',
    botonCancelar : '',
    extraFields : [],
    title : 'Cercar registres',
    layout : 'border',
    modal : true,
    hidden : true,
    width : 400,
    height : 400,
    closeAction : 'hide',
    clearAfterSearch : true,

    initComponent : function()
    {
        this.callParent(arguments);
        this.initUI();
        this.add(this.formularioBusqueda);
        this.add(this.gridBusqueda);

        this.addEvents('LookoupWindowClickSeleccion');
    },

    initUI : function()
    {
        this.buildQueryField();
        this.buildBotonBuscar();
        this.buildFormularioBusqueda();
        this.buildBotonCancelar();
        this.buildBotonSeleccionar();
        this.buildGridBusqueda();
    },

    executeSearch : function(query)
    {
        this.gridBusqueda.store.load(
        {
            params :
            {
                query : query,
                bean : this.bean
            }
        });
    },

    buildQueryField : function()
    {
        var ref = this;
        this.queryField = Ext.create('Ext.form.TextField',
        {
            name : 'query',
            value : '',
            listeners :
            {
                specialkey : function(field, e)
                {
                    if (e.getKey() == e.ENTER)
                    {
                        ref.botonBuscar.handler.call(ref.botonBuscar.scope);
                    }
                }
            }
        });
    },

    buildBotonBuscar : function()
    {
        var ref = this;
        this.botonBuscar = Ext.create('Ext.Button',
        {
            text : 'Cerca',
            handler : function(boton, event)
            {
                if (ref.queryField.getValue().length < 3)
                {
                    Ext.Msg.alert("Error", "Per fer una cerca cal introduir al menys 3 caracters.");
                }
                else
                {
                    ref.lastQuery = ref.queryField.getValue();
                    ref.executeSearch(ref.queryField.getValue());
                }
            }
        });
    },

    buildFormularioBusqueda : function()
    {
        this.formularioBusqueda = Ext.create('Ext.Panel',
        {
            layout : 'hbox',
            region : 'north',
            height : 40,
            frame : true,
            items : [
            {
                xtype : 'label',
                text : 'Expressió: ',
                width : 100
            }, this.queryField, this.botonBuscar ]
        });
    },

    buildBotonCancelar : function()
    {
        var ref = this;

        this.botonCancelar = Ext.create('Ext.Button',
        {
            text : 'Cancel.lar',
            handler : function(e)
            {
                ref.hide();
            }
        });
    },

    buildBotonSeleccionar : function()
    {
        var ref = this;

        this.botonSeleccionar = Ext.create('Ext.Button',
        {
            text : 'Seleccionar',
            handler : function(e)
            {
                var record = ref.gridBusqueda.getSelectionModel().getSelection()[0];

                if (record)
                {
                    ref.fireEvent('LookoupWindowClickSeleccion', record);

                    if (ref.clearAfterSearch)
                    {
                        var query = ref.queryField;
                        query.setValue('');
                        ref.gridBusqueda.store.removeAll(true);
                        ref.gridBusqueda.getView().refresh();
                    }
                    ref.hide();
                }
            }
        });
    },

    buildGridBusqueda : function()
    {
        var ref = this;

        var resultColumnList = [
        {
            header : 'Codi',
            width : 50,
            dataIndex : 'id'
        },
        {
            header : 'Nom',
            width : 200,
            dataIndex : 'nombre'
        } ];

        var renderer = function(value, metaData, record, rowIndex, colIndex)
        {
            if (Ext.isDefined(value[(colIndex - 2)].value))
            {
                return value[(colIndex - 2)].value;
            }
        };

        for ( var extraField in this.extraFields)
        {
            if (this.extraFields.hasOwnProperty(extraField))
            {
                resultColumnList.push(
                {
                    header : this.extraFields[extraField],
                    width : 200,
                    dataIndex : 'extraParam',
                    renderer : renderer
                });
            }
        }

        this.gridBusqueda = Ext.create('Ext.grid.Panel',
        {
            region : 'center',
            flex : 1,
            frame : true,
            loadMask : true,
            store : Ext.create('Ext.data.Store',
            {
                model : 'Ext.ux.uji.form.model.Lookup',
                autoSync : false,

                proxy :
                {
                    type : 'ajax',
                    url : '/' + ref.appPrefix + '/rest/lookup',

                    reader :
                    {
                        type : 'json',
                        root : 'data'
                    }
                },
                listeners :
                {
                    load : function(store, records, successful, eOpts)
                    {
                        if (ref.gridBusqueda.store.data.length === 0)
                        {
                            Ext.Msg.alert("Aviso", "La búsqueda realitzada no ha produït cap resultat");
                        }
                    }
                }
            }),

            columns : resultColumnList,
            forceFit : true,
            listeners :
            {
                celldblclick : function(grid, td, cellindex, record)
                {
                    ref.botonSeleccionar.handler.call(ref.botonSeleccionar.scope);
                }
            },
            buttons : [ this.botonSeleccionar, this.botonCancelar ]

        });
    },

    onEsc : function()
    {
        this.botonCancelar.handler.call(this.botonCancelar.scope);
    },

    listeners :
    {
        'show' : function(window)
        {
            window.queryField.focus(true, 200);
        }
    }
});;Ext.define('Ext.ux.uji.form.TwinComboBox',
{
    extend : 'Ext.form.ComboBox',
    requires : [ 'Ext.form.field.Trigger' ],

    getTrigger : function()
    {
        var fieldTrigger = Ext.create('Ext.form.TwinTriggerField');
        return fieldTrigger.getTrigger();
    },

    initTrigger : function()
    {
        var fieldTrigger = Ext.create('Ext.form.TwinTriggerField');
        fieldTrigger.initTrigger();
    },

    trigger1Class : 'x-form-clear-trigger',
    hideTrigger1 : true,

    initComponent : function()
    {
        this.triggerConfig =
        {
            tag : 'span',
            cls : 'x-form-twin-triggers',
            cn : [
            {
                tag : 'img',
                src : Ext.BLANK_IMAGE_URL,
                cls : 'x-form-trigger ' + this.trigger1Class
            },
            {
                tag : 'img',
                src : Ext.BLANK_IMAGE_URL,
                cls : 'x-form-trigger ' + this.trigger2Class
            } ]
        };

        this.callParent(arguments);
    },

    onTrigger2Click : function()
    {
        this.onTriggerClick();
    },

    onTrigger1Click : function()
    {
        this.clearValue();
    }
});
;Ext.define('Ext.ux.uji.form.model.ExtraParam',
{
    extend : 'Ext.data.Model',

    fields : [ 'key', 'value' ]
});
;Ext.define('Ext.ux.uji.form.model.Lookup',
{
    extend : 'Ext.data.Model',
    fields : [ 'id', 'nombre',
    {
        model : 'Ext.ux.uji.form.model.ExtraParam',
        name : 'extraParam'
    } ]
});
;Ext.define('Ext.ux.uji.grid.ComboColumn',
{
    extend : 'Ext.grid.column.Column',
    alias : 'widget.combocolumn',

    config :
    {
        header : '',
        dataIndex : '',
        combo : {}
    },

    initComponent : function()
    {
        this.editor = this.combo;
        this.renderer = this.comboRenderer(this.combo);

        this.callParent(arguments);
    },

    comboRenderer : function(combo)
    {
        return function(value)
        {
            var record = combo.store.findRecord(combo.valueField, value);
            return record ? record.get(combo.displayField) : combo.valueNotFoundText;
        };
    }
});
;Ext.define('Ext.ux.uji.grid.ForeignColumn',
{
    extend : 'Ext.grid.column.Column',
    alias : 'widget.foreigncolumn',

    config :
    {
        header : '',
        property : null,
        dataIndex : null,
        model : null,
        store : null,
        editable : false,
        allowBlank : true,
        valueField : 'id',
        displayField : 'nombre',
        valueNotFoundText : 'No se encuentra la propiedad'
    },

    initComponent : function()
    {
        this.initializeConfigValuesByConvetion();

        if (this.editable)
        {
            this.setupEditor();
        }

        this.setupRenderer();

        this.callParent(arguments);
    },

    initializeConfigValuesByConvetion : function()
    {
        this.property = this.dataIndex;
        this.dataIndex = this.dataIndex + 'Id';

        if (!this.model)
        {
            this.model = this.getModelName(Ext.ux.uji.AppConfig.appName, this.property);
        }
    },

    getModelName : function(appName, modelBaseName)
    {
        return appName + '.model.' + Ext.String.capitalize(modelBaseName);
    },

    setupEditor : function()
    {
        var store = this.getStore();

        this.editor =
        {
            xtype : 'combobox',
            store : store,
            displayField : this.displayField,
            valueField : this.valueField,
            allowBlank : this.allowBlank,
            editable : true,
            mode : 'local'
        };
    },

    getStore : function()
    {
        var store = this.store || Ext.create('Ext.data.Store',
        {
            model : this.model,
            autoLoad : true
        });
        return store;
    },

    setupRenderer : function()
    {
        var me = this;
        this.renderer = function(value, metadata, record)
        {
            var relatedModel = record.get(me.property);
            if (typeof relatedModel[me.displayField] === 'undefined')
            {
                return me.valueNotFoundText;
            }
            return relatedModel[me.displayField];
        };
    }

});;Ext.define('Ext.ux.uji.grid.Panel',
{
    extend : 'Ext.grid.Panel',
    alias : 'widget.ujigridpanel',

    requires : [ 'Ext.grid.plugin.RowEditing' ],

    plugins : [
    {
        ptype : 'rowediting',
        clicksToEdit : 2
    } ],

    tbar : [],
    bbar : [],

    config :
    {
        allowEdit : true,
        showSearchField : false,
        showAddButton : true,
        showRemoveButton : true,
        showReloadButton : true,
        showExportbutton : true,
        showTopToolbar : true,
        showBottomToolbar : true,
        handlerAddButton : function() 
        {
            var ref = this.up('grid');

            var rowEditor = ref.getPlugin();
            var record = Ext.create(ref.store.model);

            rowEditor.cancelEdit();
            ref.store.insert(0, record);
            rowEditor.startEdit(0, 0);
        },
        handlerRemoveButton : function() 
        {
            var ref = this.up('grid');
            var records = ref.getSelectionModel().getSelection();

            if (records.length > 0) 
            {
                Ext.Msg.confirm('Esborrar', 'Esteu segur/a de voler esborrar el registre ?', function(btn, text)
                {
                    if (btn == 'yes')
                    {
                        var rowEditor = ref.getPlugin();
                        
                        rowEditor.cancelEdit();
                        ref.store.remove(records);
                    }
                });
            }
        }
    },

    initComponent : function()
    {
        var ref = this;

        this.callParent(arguments);

        ref.store.on('write', function(store, operation) 
        {
            if (operation.action === 'create') 
            {
                ref.fireEvent('select', ref, operation.records[0]);  
            }
        });

        var tbar = this.getDockedItems('toolbar[dock="top"]')[0];
        var bbar = this.getDockedItems('toolbar[dock="bottom"]')[0];

        tbar.insert(0,
        {
            xtype : 'button',
            text : 'Esborrar',
            action : 'remove',
            iconCls : 'application-delete',
            hidden : !this.showRemoveButton,
            handler : this.handlerRemoveButton
        });

        tbar.insert(0,
        {
            xtype : 'button',
            text : 'Afegir',
            action : 'add',
            iconCls : 'application-add',
            hidden : !this.showAddButton,
            handler : this.handlerAddButton
        });

        if (this.showSearchField)
        {
            tbar.insert(0, [
            {
                xtype : searchfield,
                emptyText : 'Recerca...',
                store : this.store,
                width : 180
            }, ' ', '-' ]);
        }

        if (this.showReloadButton)
        {
            bbar.add([ '->',
            {
                xtype : 'button',
                text : 'Recarregar',
                action : 'reload',
                iconCls : 'arrow-refresh',
                handler : function(button, event, opts)
                {
                    ref.store.reload();
                }
            } ]);
        }

        if (this.showExportButton)
        {
        }

        if (!this.showTopToolbar) 
        {
            tbar.hide();
        } 

        if (!this.showBottomToolbar) 
        {
            bbar.hide();
        } 

        this.store.getProxy().on('exception', function()
        {
            ref.store.rejectChanges();
        });
    },

    listeners :
    {
        'beforeedit' : function(editor, context, eOpts)
        {
            if (!this.allowEdit && !context.record.phantom)
            {
                return false;
            }
        },

        'canceledit' : function(editor, context, eOpts)
        {
            var record = context.record;

            if ((record.phantom))
            {
                this.store.remove(context.record);
            }
        }
    }
});