var lang = 'es';
if (Ext.util.Cookies.get('uji-lang')) {
    lang = Ext.util.Cookies.get('uji-lang');
}
Ext.Loader.setConfig(
{
    enable : true,
    paths :
    {
        'Ext.ux' : 'http://static.uji.es/js/extjs/extjs-4.2.0/examples/ux/',
        'Ext.i18n': 'app/locale/'+lang+'.js'
    }
});

Ext.require('Ext.ux.TabCloseMenu');
Ext.require('Ext.grid.plugin.RowEditing');


Ext.define('TFG.Application',
{
    extend : 'Ext.ux.uji.Application',

    name : 'TFG',
    appFolder : 'app',
    autoCreateViewport : false,

    models : [ 'Asignatura', 'Propuesta', 'Usuario' ],

    views : [  'propuesta.PanelPropuestas', 'asignatura.PanelAsignaturas'],

    controllers : [ 'ControllerAsignaturas', 'ControllerPropuestas', 'ControllerUsuarios'],

    launch : function()
    {
        
        Ext.create('Ext.ux.uji.ApplicationViewport',
        {
            codigoAplicacion : 'TFG',
            tituloAplicacion : UI.i18n.application.title,
            dashboard : false
        }); 
    }
});
var UI = Ext.create('Ext.i18n');
