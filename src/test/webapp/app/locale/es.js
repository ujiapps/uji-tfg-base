Ext.define('Ext.i18n', {
	'i18n': {
		'application': {
			'title': 'Trabajos fin de Grado'
		},
		'title': {
			'windowPropuesta': 'Editar proposta'
		},
		'button': {
			'save': 'Guardar',
			'cancel': 'Cancelar',
			'saveProposal': 'Guardar propuesta',
			'searchMatter': 'Buscar asignatura..'
		},
		'field': {
			'code': 'Código',
			'name': 'Nombre',
			'title': 'Título',
			'author': 'Profesor',
			'matter': 'Asignatura',
			'pax': 'Plazas',
			'description': 'Descripción',
			'goals': 'Objetivos',
			'bibliography': 'Bibliografía',
			'notes': 'Observaciones'				
		},
		'column': {
			'code': 'Código',
			'name': 'Nombre',
			'title': 'Título',
			'author': 'Profesor',
			'authorCode': 'Código de profesor',
			'creationDate': 'Fecha de creación',
			'matter': 'Asignatura',
			'matterCode': 'Codigo de asignatura',
			'goals': 'Objectivos',
			'bibliography': 'Bibliografía',
			'description': 'Descripción',
			'notes': 'Observaciones',
			'status': 'Estado',
			'pax': 'Plazas',
			'awaiting': 'Espera',
			'people_awaiting': 'Alumnos en espera',
			'pax_available': 'Plazas disponibles'
		},
		'message': {
			'saving': 'Guardando',
			'undefined': 'Sin definir',
			'availablePax': 'Con plazas',
			'bookedVacancies':'Lista de espera',
			'noPax': 'Completo'
		},
		'error': {
			'formSave': 'Error guardando el formulario',
			'form': 'Error en el formulario',
			'emptyFields': 'Los campos destacados en rojo son obligatorios'
		},
		'format': {
			'date': 'd/m/Y G:i'
		}

	}
});