Ext.define('Ext.i18n', {
	'i18n': {
		'application': {
			'title': 'Treballs fi de grau'
		},
		'title': {
			'windowPropuesta': 'Editar proposta'
		},
		'button': {
			'save': 'Desar',
			'cancel': 'Cancel&#183;lar',
			'saveProposal': 'Desar proposta',
			'searchMatter': 'Cercar asignatura..'
		},
		'field': {
			'code': 'Codi',
			'name': 'Nom',
			'title': 'Títol',
			'author': 'Profesor',
			'matter': 'Asignatura',
			'pax': 'Places',
			'description': 'Descripció',
			'goals': 'Objectius',
			'bibliography': 'Bibliografía',
			'notes': 'Observacions'
		},
		'column': {
			'code': 'Codi',
			'name': 'Nom',
			'title': 'Títol',
			'author': 'Profesor',
			'authorCode': 'Codi de profesor',
			'creationDate': 'Data de creació',
			'matter': 'Asignatura',
			'matterCode': 'Codi d\'asignatura',
			'goals': 'Objectius',
			'bibliography': 'Bibliografía',
			'description': 'Descripció',
			'notes': 'Observacions',
			'status': 'Estat',
			'pax': 'Places',
			'awaiting': 'Espera',
			'people_awaiting': 'Alumnes en espera',
			'pax_available': 'Places disponibles'
		},
		'message': {
			'saving': 'Desant',
			'undefined': 'Sense definir',
			'availablePax': 'Amb places',
			'bookedVacancies':'Llista d\'espera',
			'noPax': 'Complet'
		},
		'error': {
			'formSave': 'Error desant el formulari',
			'form': 'Error en el formulari',
			'emptyFields': 'Els camps destacats en roig son obligatoris'
		},
		'format': {
			'date': 'd/m/Y',
			'dateFull': 'd/m/Y G:i',
		}

	}
});