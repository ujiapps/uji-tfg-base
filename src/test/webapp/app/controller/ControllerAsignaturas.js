Ext.define('TFG.controller.ControllerAsignaturas',
    {
        extend: 'Ext.app.Controller',

        stores: [ 'Asignaturas' ],
        model: [ 'Asignatura' ],
        views: ['asignatura.GridAsignaturas', 'asignatura.PanelAsignaturas'],
        refs: [
            {
                selector: 'gridAsignaturas',
                ref: 'gridAsignaturas'
            }
        ],
        init: function () {
            this.control(
            {
                'gridPropuestas' :
                {
                    click : this.resumenAsignatura
                },
                'viewport > treepanel': {
                    itemclick: this.onTreeItemSelected
                }                
            });
        },
        onTreeItemSelected: function (grid, record, item, index, event, opts) {
            grid.up("viewport").addNewTab(record.data.id);            
        },        
        resumenAsignatura: function (grid, record, item, index, event, eOpts) {
            alert('resumen asignatura');
        }
    });