Ext.define('TFG.controller.ControllerProfesores',
    {
        extend: 'TFG.controller.BaseController',

        stores: [ 'Profesores' ],
        model: [ 'Usuario' ],
        views: ['profesor.GridProfesores','profesor.PanelProfesores', 'profesor.ResumenProfesor'],
        refs: [{
            selector: 'gridProfesores',
            ref: 'gridProfesores'
        },{
            selector: 'panelProfesores',
            ref: 'panelProfesores'
        }],
        init: function () {
            this.control(
            {
                'gridProfesores' :
                {
                    rowclick : this.resumenProfesor
                },
                'viewport > treepanel': {
                    itemclick: this.onTreeItemSelected
                }
            });
        },
        onTreeItemSelected: function (grid, record, item, index, event, opts) {
            grid.up("viewport").addNewTab(record.data.id);            
        }    
    });