Ext.define('TFG.controller.ControllerUsuarios',
{
    extend: 'Ext.app.Controller',

    stores: [ 'Usuarios', 'Profesores' ],
    model: [ 'Usuario' ],
});