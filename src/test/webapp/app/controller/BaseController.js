Ext.define('TFG.controller.BaseController',
	{
		extend: 'Ext.app.Controller',
		parseResponse: function (response) {
			if (response && response.response && response.response.responseText) {
				return Ext.JSON.decode(response.response.responseText);
			}
			else 
				return '';
		},
		filterStore: function(filter, store) {  
            store.clearFilter(true);
            if (filter == null) {
                store.load();
            } 
            else if (typeof filter == 'string') {
                store.filterBy(function(item, itemId) {return item.data.filter.toLowerCase().indexOf(filter.toLowerCase()) != -1;});
            }
            else {
                store.filterBy(function(item, itemId) {return item.data.filter_id == filter});
            }
            return false;
        }
	});