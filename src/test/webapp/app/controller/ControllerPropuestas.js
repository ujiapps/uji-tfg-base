Ext.define('TFG.controller.ControllerPropuestas',
    {
        extend: 'TFG.controller.BaseController',

        views:  [ 'propuesta.FormPropuestas', 'propuesta.GridPropuestas', 'propuesta.WindowPropuestas'],
        stores: ['Propuestas', 'Asignaturas', 'Profesores'],
        models: ['Propuesta', 'Asignatura', 'Usuario'],

        refs:   [{
            ref: 'panelPropuestas',
            selector: 'panelPropuestas'
        },{
            ref: 'formPropuestas',
            selector: 'formPropuestas'       
        },{
            ref: 'savePropuestaButton',
            selector: 'formPropuestas button[action=open]'
        },{
            ref: 'resetPropuestaButton',
            selector: 'formPropuestas button[action=del]'
        },{            
            ref: 'gridPropuestas',
            selector: 'gridPropuestas'
        },{
            ref: 'viewport',
            selector: 'viewport'
        }],

        init: function () {
            this.control(
            {
                'gridPropuestas button[action=add]' :
                {
                    click : this.mostrarModalFormulario
                },

                'gridPropuestas' :
                {
                    itemdblclick : this.editarPropuesta
                },

                'gridPropuestas combo[name=asignatura_filter]':
                {
                    change: this.filtrarPropuestas
                },

                'windowPropuestas button[action=save]' :
                {
                    click : this.guardarPropuesta
                },
                'windowPropuestas button[action=close]' :
                {
                    click : this.closeWindowPropuestas
                },
                'formPropuestas combo[name=autor_id]':
                {
                    render: this.deshabilitarProfesores
                },
                'viewport > treepanel': {
                    itemclick: this.onTreeItemSelected
                }
            });
            this.window = null;//Ext.create('TFG.view.propuesta.WindowPropuestas');            
        },
        onTreeItemSelected: function (grid, record, item, index, event, opts) {
            grid.up("viewport").addNewTab(record.data.id);            
        },
        editarPropuesta : function(grid, record, item, index, e, eOpts)
        {
            if (this.window == null) {
                this.window = Ext.create('TFG.view.propuesta.WindowPropuestas');            
            }
            this.window.load(record);
            this.window.show();
        },        
        guardarPropuesta: function () {
            var url = 'rest/propuestas';
            var grid = this.getGridPropuestas();
            var me = this;
            me.getFormPropuestas().saveFormData(grid, url, false, {
                success: function(response, request) {
                    // var data = me.parseResponse(response);
                    grid.getStore().reload();  
                    me.closeWindowPropuestas();                  
                },
                failure: function(response, request) {
                    console.log(response, request);
                }
            } );
        },
        closeWindowPropuestas: function () {
            if (this.window != null)    
                    this.window.clearAndHide();
        },
        mostrarModalFormulario: function () {
            if (this.window == null) {
                this.window = Ext.create('TFG.view.propuesta.WindowPropuestas');            
            }            
            this.window.show();
            return false;
        },
        filtrarPropuestas: function(combo, newValue, oldValue, eOpts) {              
            return this.filterStore(newValue, this.getGridPropuestas().getStore());
        },
        deshabilitarProfesores: function(combo, eOpts) {
            if (combo.store.data.length == 1) {  
                combo.setValue(combo.store.data.keys[0])           
                combo.disable();
            }
        }
    });