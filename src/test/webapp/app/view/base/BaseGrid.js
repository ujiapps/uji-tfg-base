Ext.define('TFG.view.base.BaseGrid',
	{
		extend: 'Ext.ux.uji.grid.Panel',
		showAsignaturaFilter: false,
        initComponent: function() {
            this.callParent(arguments);
            
            if (this.showAsignaturaFilter === true) {            
	            var tbar = this.getDockedItems('toolbar[dock="top"]')[0];
	            if (tbar) {	            	
		            tbar.insert(tbar.items.length,
		            {
		                xtype : 'combobox',
		                name: 'asignatura_filter',
		                emptyText : UI.i18n.button.searchMatter,
		                store: 'Asignaturas',
		                multiSelect: false,
		                valueField: 'codigo',
		                displayField: 'codinom',
		                width: 400,
		                queryMode: 'local'
		            });
	            }           
            }
        }

});