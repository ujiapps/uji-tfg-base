Ext.form.TwinTriggerField = function(config) {
    Ext.form.TwinTriggerField.superclass.constructor.apply(this, arguments);        
};
Ext.extend(Ext.form.TwinTriggerField, Ext.form.ComboBox, {

    trigger1Class: 'x-form-search-trigger',            
    trigger2Class: 'x-form-select-trigger',

    initComponent : function(){
        Ext.form.TwinTriggerField.superclass.initComponent.call(this);
        this.record = new Object();
        this.triggerConfig = {
            tag:'span', cls:'x-form-twin-triggers', cn:[
            {tag: "img", src: Ext.BLANK_IMAGE_URL, cls: "x-form-trigger " + this.trigger1Class},
            {tag: "img", src: Ext.BLANK_IMAGE_URL, cls: "x-form-trigger " + this.trigger2Class}
        ]};
    },    
    getTrigger : function(index){
        return this.triggers[index];
    },
    initTrigger : function(){
        var ts = this.trigger.select('.x-form-trigger', true);
        var triggerField = this;
        ts.each(function(t, all, index){
            t.hide = function(){
                var w = triggerField.wrap.getWidth();
                this.dom.style.display = 'none';
                triggerField.el.setWidth(w-triggerField.trigger.getWidth());
            };
            t.show = function(){
                var w = triggerField.wrap.getWidth();
                this.dom.style.display = '';
                triggerField.el.setWidth(w-triggerField.trigger.getWidth());
            };
            var triggerIndex = 'Trigger'+(index+1);

            if(this['hide'+triggerIndex]){
                t.dom.style.display = 'none';
            }
            t.on("click", this['on'+triggerIndex+'Click'], this, {preventDefault:true});
            t.addClassOnOver('x-form-trigger-over');
            t.addClassOnClick('x-form-trigger-click');
        }, this);
        this.triggers = ts.elements;
    },
    onTrigger1Click : function() {
        this.onTriggerClick();
    },
    onTrigger2Click : function() {
        this.onTrigger2Click();
    }
});
Ext.define('TFG.view.base.BaseTwinTrigger',{
    extend: 'Ext.form.TwinTriggerField'
});