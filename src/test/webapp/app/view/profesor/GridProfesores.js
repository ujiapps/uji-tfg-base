Ext.define('TFG.view.profesor.GridProfesores',
    {
        extend: 'Ext.ux.uji.grid.Panel',
        alias: 'widget.gridProfesores',

        store: 'Profesores',

        title: 'Profesores',
        closable: true,
        allowEdit: false,
        showSearchField : false,
        showAddButton : false,
        showRemoveButton : false,
        showReloadButton : false,
        showExportbutton : false,
        showTopToolbar : false,
        showBottomToolbar : false,
        columns: [{
                header: 'id',
                dataIndex: 'id',
                hidden: true
            },{
                header: 'Nom',
                dataIndex: 'nombre',
                flex: 60
            }
        ],
        listeners : {
            itemdblclick: function(dv, record, item, index, e) {
                this.resumenProfesor();
                return false;
            }
        },
        resumenProfesor: function (grid, record, item, index, event, eOpts) {
            
        }        
    });