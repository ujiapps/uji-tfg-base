Ext.define('TFG.view.profesor.PanelProfesores',
    {
        extend: 'Ext.Panel',
        alias: 'widget.panelProfesores',

        title: 'Profesores',

        requires: [ 'TFG.view.profesor.GridProfesores' ],

        closable: true,

        layout: {
            type: 'vbox',
            align: 'stretch'
        },

        items: [
            {
                xtype: 'gridProfesores',
                flex: 1
            }
        ]
    });
