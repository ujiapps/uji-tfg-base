Ext.define('TFG.view.profesor.GridResumenProfesores',
    {
        extend: 'Ext.ux.uji.grid.Panel',
        alias: 'widget.gridResumenProfesores',

        store: 'Asignaturas',

        title: 'Asignaturas',
        closable: true,
        allowEdit: false,
        showSearchField : false,
        showAddButton : false,
        showRemoveButton : false,
        showReloadButton : false,
        showExportbutton : false,
        showTopToolbar : false,
        showBottomToolbar : false,
        columns: [{
                header: 'id',
                dataIndex: 'id',
                hidden: true
            },{
                header: 'Codi',
                dataIndex: 'codigo',
                flex: 50,   
                editor: {
                    xtype: 'textfield',
                    allowBlank: false
                }
            },{
                header: 'Nom',
                dataIndex: 'nombre',
                flex: 60,
                editor: {
                    xtype: 'textfield',
                    allowBlank: false
                }
            }
        ]
    });