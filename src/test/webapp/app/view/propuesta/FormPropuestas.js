Ext.define('TFG.view.propuesta.FormPropuestas',
{
    extend: 'TFG.view.base.BaseForm',
    alias: 'widget.formPropuestas',
    border : false,
    store: 'Propuestas',
    // url : '/tfg/rest/propuestas', 
    layout :
    {
        type : 'form',
        align : 'stretch'
    },
    defaults: {
        allowBlank: false,
        alignTo: 'right',
        width: '100%',
        labelAlign: 'right'
    },
    autoScroll: true,
    items: [{
        fieldLabel: UI.i18n.field.author,
        name: 'autor_id',
        xtype: 'combo',
        store: 'Profesores',
        multiSelect: false,
        valueField: 'id',
        displayField: 'nombre'
    },{
        fieldLabel: UI.i18n.field.matter,
        name: 'asignatura_id',
        xtype: 'combo',
        store: 'Asignaturas',
        multiSelect: false,
        valueField: 'id',
        displayField: 'codinom'
    },{
        fieldLabel: UI.i18n.field.code,
        name: 'id',
        allowBlank: true,
        hidden: true,
        xtype: 'textfield'
    },{
        fieldLabel: UI.i18n.field.title,
        name: 'nombre',
        xtype: 'textfield'
    },{
        fieldLabel: UI.i18n.field.pax,
        xtype: 'numberfield',
        name: 'plazas',
        minValue: 1
    },{
        fieldLabel: UI.i18n.field.goals,
        allowBlank: true,
        name: 'objetivos',
        xtype: 'htmleditor',
        height: 115
    },{
        fieldLabel: UI.i18n.field.bibliography,
        allowBlank: true,
        name: 'bibliografia',
        xtype: 'htmleditor',
        height: 115
    },{
        fieldLabel: UI.i18n.field.description,
        allowBlank: true,
        name: 'descripcion',
        xtype: 'htmleditor',
        height: 115
    },{
        fieldLabel: UI.i18n.field.notes,
        allowBlank: true,
        name: 'observaciones',
        xtype: 'htmleditor',        
        height: 115
    }],
    buttons:[]
});
    