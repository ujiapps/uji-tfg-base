Ext.define('TFG.view.propuesta.WindowPropuestas',
{
    extend : 'Ext.window.Window',
    alias : 'widget.windowPropuestas',
    title : UI.i18n.title.windowPropuesta,
    reference : 'windowPropuestas',
    width : 800,
    height : 700,
    frame : false,
    bodyStyle : 'padding:1em; background-color:white;',
    modal : true,
    closable : false,
    layout : 'fit',
    items : [{
        xtype : 'formPropuestas'        
    }],

    buttons : [
    {
        text : UI.i18n.button.saveProposal,
        action : 'save'
    },
    {
        text : UI.i18n.button.cancel,
        action : 'close'
    }],

    clearForm : function()
    {
        this.down('form').getForm().reset();
    },

    clearAndHide : function()
    {
        this.clearForm();
        this.hide();
    },

    save : function(callback)
    {
        var form = this.down('form').getForm();

        if (form.isValid())
        {
            form.submit(
            {
                success : function(form, action)
                {
                    callback();
                }
            });
        }
    },

    load : function(record)
    {
        var form = this.down('form').getForm();
        form.loadRecord(record);        
    }    
});
