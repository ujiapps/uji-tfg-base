Ext.define('TFG.view.propuesta.PanelPropuestas',
    {
        extend: 'Ext.Panel',
        alias: 'widget.panelPropuestas',

        title: 'Propuestas',

        requires: [ 'TFG.view.propuesta.FormPropuestas' ],

        closable: true,

        layout: {
            type: 'vbox',
            align: 'stretch'
        },

        items: [{
                xtype: 'gridPropuestas',
                flex: 1

            }]
    });
