Ext.define('TFG.view.propuesta.GridPropuestas',
    {
        extend: 'TFG.view.base.BaseGrid',

        alias: 'widget.gridPropuestas',

        store: 'Propuestas',

        title: 'Propuestas',

        allowEdit: false,
        showSearchField : false,
        showAddButton : true,
        showRemoveButton : true,
        showReloadButton : true,
        showExportbutton : false,
        showTopToolbar : true,
        showBottomToolbar : true,
        ShowAsignaturaFilter : true,
        columns: [{
                header: UI.i18n.column.code,
                dataIndex: 'id',
                hidden: true                
            },{
                header: UI.i18n.column.title,
                dataIndex: 'nombre',
                flex: 60
            },{
                header: UI.i18n.column.author,
                dataIndex: 'autor',
                flex: 60
            },{
                header: UI.i18n.column.authorCode,
                hidden: true,
                dataIndex: 'autor_id'
            },{
                header: UI.i18n.column.matter,
                dataIndex: 'asignatura',
                hidden: true
            },{
                header: UI.i18n.column.matterCode,
                dataIndex: 'asignatura_id',
                hidden: true

            },{
                header: UI.i18n.column.goals,
                dataIndex: 'objetivos',
                hidden: true
            },{
                header: UI.i18n.column.bibliography,
                dataIndex: 'bibliografia',
                hidden: true
            },{
                header: UI.i18n.column.description,
                dataIndex: 'descripcion',
                hidden: true
            },{
                header: UI.i18n.column.notes,
                dataIndex: 'observaciones',
                hidden: true
            },{
                header: UI.i18n.column.pax_available,
                dataIndex: 'plazas_disponibles',
                flex: 65
            },{
                header: UI.i18n.column.people_awaiting,
                dataIndex: 'gente_esperando',
                flex: 65
            }, {
                header: UI.i18n.column.status,
                dataIndex: 'disponibilidad',
                flex: 55,
                renderer: function(value, p, record) {
                    var salida = UI.i18n.message.undefined;
                    switch (value) {
                        case 0:
                            salida = UI.i18n.message.availablePax;
                            salida = '<span style="color: green">' + salida + '</span>';
                            break;
                        case 1:
                            salida = UI.i18n.message.bookedVacancies;
                            salida = '<span style="color: yellow">' + salida + '</span>';
                            break;
                        case -1:
                            salida = UI.i18n.message.noPax;
                            salida = '<span style="color: red">' + salida + '</span>';
                            break;
                    }
                    return salida;
                }
            }, {
                header: UI.i18n.column.pax,
                dataIndex: 'plazas',
                hidden: true
            }, {
                header: UI.i18n.column.awaiting,
                dataIndex: 'espera',
                hidden: true
            }
        ],    
        buttons: []
    });