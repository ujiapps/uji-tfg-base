Ext.define('TFG.view.asignatura.PanelAsignaturas',
    {
        extend: 'Ext.Panel',
        alias: 'widget.panelAsignaturas',

        title: 'Asignaturas',

        requires: [ 'TFG.view.asignatura.GridAsignaturas' ],

        closable: true,

        layout: {
            type: 'vbox',
            align: 'stretch'
        },

        items: [
            {
                xtype: 'gridAsignaturas',
                flex: 1
            }
        ]
    });
