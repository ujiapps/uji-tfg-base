grant select on xpfdm.XPF_VMC_PERFILES_PER to uji_trabajofingrado;
create or replace view TFG_VW_ADMINISTRATIVOS_ACCESO_SPI as
select p.id, p.nombre, p.apellido1, p.apellido2, busca_cuenta(p.id) email
from per_personas p
         join XPF_VMC_PERFILES_PER pp
              on p.id = pp.per_id
where perf_id in ('9003002', '9003030');

CREATE TABLE TFG_ADMINISTRATIVOS_ACCESO_SPI
(
    ID     NUMBER NOT NULL,
    PER_ID NUMBER NOT NULL,
    CONSTRAINT TFG_ADMINISTRATIVOS_PK PRIMARY KEY
        (
         ID
            )
        ENABLE
);
ALTER TABLE TFG_ADMINISTRATIVOS_ACCESO_SPI
    ADD (CODIGO_ASIGNATURA VARCHAR2(20) NOT NULL);
