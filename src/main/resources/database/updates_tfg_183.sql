grant select on xpfdm.XPF_VMC_PERFILES_PER to uji_trabajofingrado;
grant select on gra_pod.pod_fechas_examenes to uji_trabajofingrado;
grant select on gra_pod.pod_convocatorias to uji_trabajofingrado;

grant select on gra_pop.POP_FECHAS_EXAMENES to uji_trabajofingrado;
grant select on gra_pop.pop_oferta to uji_trabajofingrado;

create table TFG_CONVOCATORIAS_FECHAS
(
    ID              number not null,
    CONVOCATORIA_ID number,
    ASIGNATURA_ID   varchar2(10 byte),
    CURSO_ACA       number,
    FECHA_INICIO    date,
    FECHA_FIN       date,
    constraint TFG_CONVOCATORIAS_FECHAS_PK primary key
        (
         ID
            )
        enable
);

CREATE OR REPLACE VIEW "UJI_TRABAJOFINGRADO"."TFG_VW_CONVOCATORIAS_FECHAS"
            ("ID", "ASIGNATURA_ID", "CURSO_ACA", "CONVOCATORIA_ID", "NOMBRE", "FECHA_INICIO", "FECHA_FIN") AS
select cf.id,
       c.asignatura_id,
       c.curso_aca,
       c.convocatoria_id,
       c.nombre,
       cf.fecha_inicio,
       cf.fecha_fin
from TFG_CONVOCATORIAS_FECHAS cf
         right join (select fe.curso_aca,
                            fe.asi_id asignatura_id,
                            fe.con_id convocatoria_id,
                            c.nombre
                     from pod_fechas_examenes fe
                              join pod_asi_cursos ac on fe.asi_id = ac.asi_id
                              join pod_convocatorias c on fe.con_id = c.id
                     where fe.parcial = 'N'
                       and ac.caracter = 'PF'
                       and ac.cur_id <= 6
                       and ac.visible = 'S'
                       and ac.cur_tit_id != 100
                     union
                     select pfe.CURSO_ACA,
                            pfe.ASI_ID asignatura_id,
                            pfe.con_id convocatoria_id,
                            c.nombre
                     from GRA_POP.POP_FECHAS_EXAMENES pfe
                              join gra_pop.pop_oferta o
                                   on o.pasi_id = pfe.asi_id
                                       and o.curso_aca = pfe.curso_aca
                              join pod_convocatorias c
                                   on pfe.con_id = c.id
                     where pfe.parcial = 'N'
                       and o.pmas_id != 42100) c
                    on cf.convocatoria_id = c.convocatoria_id and cf.curso_aca = c.curso_aca and
                       cf.asignatura_id = c.asignatura_id
;
