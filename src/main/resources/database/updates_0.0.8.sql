alter table tfg_ext_personas add (apellido1 varchar2(150), apellido2 varchar2(150));
update tfg_ext_personas set apellido1 = concat(concat('apellido1', '-'), id);
update tfg_ext_personas set apellido2 = concat(concat('apellido2', '-'), id);
alter table tfg_propuestas modify nombre varchar2(4000);