
ALTER TABLE tfg_ext_asignaturas MODIFY codigo VARCHAR(20) NOT NULL;

DELETE FROM tfg_asignaturas_config;
ALTER TABLE UJI_TRABAJOFINGRADO.tfg_asignaturas_config ADD (codigo_asignatura VARCHAR2(20) DEFAULT '');
ALTER TABLE tfg_asignaturas_config DROP CONSTRAINT TFG_ASIGNATURAS_CONFIG_PK;
ALTER TABLE tfg_asignaturas_config ADD CONSTRAINT TFG_ASIGNATURAS_CONFIG_PK PRIMARY KEY (codigo_asignatura);
ALTER TABLE tfg_asignaturas_config DROP CONSTRAINT TFG_ASIGNATURAS_CONFIG_FK1;
ALTER TABLE tfg_asignaturas_config DROP COLUMN asignatura_id;

DELETE FROM tfg_carga_profesores;
ALTER TABLE UJI_TRABAJOFINGRADO.tfg_carga_profesores ADD (codigo_asignatura VARCHAR2(20) DEFAULT '');
ALTER TABLE tfg_carga_profesores DROP CONSTRAINT TFG_CARGA_PROFESORES_PK;
ALTER TABLE tfg_carga_profesores ADD CONSTRAINT TFG_CARGA_PROFESORES_PK PRIMARY KEY (persona_id, codigo_asignatura);
ALTER TABLE tfg_carga_profesores DROP CONSTRAINT TFG_CARGA_PROFESORES_FK3;
ALTER TABLE tfg_carga_profesores DROP COLUMN asignatura_id;

--PARRAFO NO NECESARIO EN PRODUCCION SI EN DEVEL
DELETE FROM tfg_ext_asignaturas_personas;
ALTER TABLE UJI_TRABAJOFINGRADO.tfg_ext_asignaturas_personas ADD (codigo_asignatura VARCHAR2(20) DEFAULT '');
ALTER TABLE tfg_ext_asignaturas_personas DROP CONSTRAINT TFG_EXT_ASIGNATURAS_USUAR_PK;
ALTER TABLE tfg_ext_asignaturas_personas ADD CONSTRAINT TFG_EXT_ASIGNATURAS_USUAR_PK PRIMARY KEY (persona_id, codigo_asignatura, tipo_relacion);
ALTER TABLE tfg_ext_asignaturas_personas DROP CONSTRAINT TFG_EXT_ASIGNATURAS_USUAR_FK1;
ALTER TABLE tfg_ext_asignaturas_personas DROP COLUMN asignatura_id;
-- FIN PARRAFO

DELETE FROM tfg_propuestas;
ALTER TABLE UJI_TRABAJOFINGRADO.tfg_propuestas ADD (codigo_asignatura VARCHAR2(20) DEFAULT '');
ALTER TABLE tfg_propuestas DROP CONSTRAINT TFG_PROPUESTAS_ASIGNATURAS_FK1;
ALTER TABLE tfg_propuestas DROP COLUMN asignatura_id;

DELETE FROM tfg_turnos;
ALTER TABLE UJI_TRABAJOFINGRADO.tfg_turnos ADD (codigo_asignatura VARCHAR2(20) DEFAULT '');
ALTER TABLE tfg_turnos DROP CONSTRAINT TFG_TURNOS_PK;
ALTER TABLE tfg_turnos ADD CONSTRAINT TFG_TURNOS_PK PRIMARY KEY (persona_id, codigo_asignatura);
ALTER TABLE tfg_turnos DROP CONSTRAINT TFG_TURNOS_FK2;
ALTER TABLE tfg_turnos DROP COLUMN asignatura_id;


--PARRAFO NO NECESARIO EN PRODUCCION SI EN DEVEL
ALTER TABLE tfg_ext_asignaturas DROP CONSTRAINT TFG_EXT_ASIGNATURAS_PK;
ALTER TABLE tfg_ext_asignaturas DROP COLUMN id;
ALTER TABLE tfg_ext_asignaturas ADD CONSTRAINT TFG_EXT_ASIGNATURAS_PK PRIMARY KEY (codigo);
ALTER TABLE tfg_ext_asignaturas_personas ADD CONSTRAINT TFG_EXT_ASIGNATURAS_USUAR_FK1 FOREIGN KEY ("CODIGO_ASIGNATURA") REFERENCES "UJI_TRABAJOFINGRADO"."TFG_EXT_ASIGNATURAS" ("CODIGO") ON DELETE CASCADE;
-- FIN PARRAFO

ALTER TABLE tfg_asignaturas_config ADD CONSTRAINT TFG_ASIGNATURAS_CONFIG_FK1 FOREIGN KEY ("CODIGO_ASIGNATURA") REFERENCES "UJI_TRABAJOFINGRADO"."TFG_EXT_ASIGNATURAS" ("CODIGO") ON DELETE CASCADE;
ALTER TABLE tfg_carga_profesores ADD CONSTRAINT TFG_CARGA_PROFESORES_FK3 FOREIGN KEY ("CODIGO_ASIGNATURA") REFERENCES "UJI_TRABAJOFINGRADO"."TFG_EXT_ASIGNATURAS" ("CODIGO") ON DELETE CASCADE;
ALTER TABLE tfg_propuestas ADD CONSTRAINT TFG_PROPUESTAS_ASIGNATURAS_FK1 FOREIGN KEY ("CODIGO_ASIGNATURA") REFERENCES "UJI_TRABAJOFINGRADO"."TFG_EXT_ASIGNATURAS" ("CODIGO") ON DELETE CASCADE;
ALTER TABLE tfg_turnos ADD CONSTRAINT TFG_TURNOS_FK2 FOREIGN KEY ("CODIGO_ASIGNATURA") REFERENCES "UJI_TRABAJOFINGRADO"."TFG_EXT_ASIGNATURAS" ("CODIGO") ON DELETE CASCADE;


CREATE OR REPLACE FORCE VIEW UJI_TRABAJOFINGRADO.TFG_EXT_ASIGNATURAS_ (
   CODIGO,
   NOMBRE,
   CREDITOS,
   MATRICULAS,
   ESTUDIO_ID
   ) AS
   select distinct id codigo, nombre, creditos, (select count ( * )
                                                 from exp_asi_cursadas
                                                 where     mat_exp_tit_id = ac.cur_tit_id
                                                       and mat_curso_aca = ac.curso_aca
                                                       and asi_id = a.id)
                                                   matriculas, ac.cur_tit_id estudio_id
   from pod_asignaturas a,
        pod_asi_cursos ac
   where     a.id = ac.asi_id
         and ac.caracter = 'PF'
         and ac.cur_id < 6
         and ac.visible = 'S'
         and ac.curso_aca = euji_util.euji_curso_aca ()
   union
   select distinct id codigo, nombre, creditos, (select count ( * )
                                                 from pop_asi_cursadas
                                                 where     pmat_pexp_pmas_id = ac.pmas_id
                                                       and pmat_curso_aca = ac.curso_aca
                                                       and asi_id = a.id)
                                                   matriculas, ac.pmas_id estudio_id
   from pop_asignaturas a,
        pop_oferta ac
   where     a.id = ac.pasi_id
         and (a.pf = 'S'
              or a.pf_investigacion = 'S')
         and ac.curso_aca = euji_util.euji_curso_aca ();

CREATE OR REPLACE FORCE VIEW UJI_TRABAJOFINGRADO.TFG_EXT_ASIGNATURAS_PERSONAS_ (
   PERSONA_ID,
   TIPO_RELACION,
   CREDITOS,
   CODIGO_ASIGNATURA
   ) AS
   select "PERSONA_ID", "TIPO_RELACION", "CREDITOS", "CODIGO_ASIGNATURA"
   from tfg_vw_estudiantes_grados
   union
   select "PERSONA_ID", "TIPO_RELACION", "CREDITOS", "CODIGO_ASIGNATURA"
   from tfg_vw_estudiantes_masters
   union
   select "PERSONA_ID", "TIPO_RELACION", "CREDITOS", "CODIGO_ASIGNATURA"
   from tfg_vw_coordinadores_grados
   union
   select "PERSONA_ID", "TIPO_RELACION", "CREDITOS", "CODIGO_ASIGNATURA"
   from tfg_vw_coordinadores_masters
   union
   select "PERSONA_ID", "TIPO_RELACION", "CREDITOS", "CODIGO_ASIGNATURA"
   from tfg_vw_profesores_grados
   union
   select "PERSONA_ID", "TIPO_RELACION", "CRD_IMPARTIDOS", "CODIGO_ASIGNATURA"
   from tfg_vw_profesores_masters;

CREATE OR REPLACE FORCE VIEW UJI_TRABAJOFINGRADO.TFG_EXT_FESTIVOS_ (
   FECHA
   ) AS
   select fecha_completa fecha
   from grh_calendario
   where tipo_lab != 'L'
         and año = euji_util.euji_curso_aca ();

CREATE OR REPLACE FORCE VIEW UJI_TRABAJOFINGRADO.TFG_EXT_NOTAS_ALUMNOS_ (
   PERSONA_ID,
   ESTUDIO_ID,
   MEDIA
   ) AS
   select distinct
          mat_exp_per_id persona_id,
          mat_exp_tit_id estudio_id,
          pack_exp.media_exp (mat_exp_per_id, mat_exp_tit_id, 'S') media
   from exp_asi_cursadas
   where mat_curso_aca = euji_util.euji_curso_aca ();

CREATE OR REPLACE FORCE VIEW UJI_TRABAJOFINGRADO.TFG_EXT_PERSONAS_ (
   ID,
   NOMBRE,
   EMAIL
   ) AS
   select distinct
   cdo_per_id id, (p.nombre || ' ' || p.apellido1 || ' ' || p.apellido2) nombre, 'borillo@gmail.com' email --busca_cuenta (cdo_per_id) email
   from pod_subgrupo_pdi sp,
   per_personas p,
   pod_asi_cursos ac
   where     p.id = sp.cdo_per_id
   and cdo_curso_aca = euji_util.euji_curso_aca ()
   and sp.sgr_grp_asi_id = ac.asi_id
   and ac.curso_aca = sp.cdo_curso_aca
   and ac.caracter = 'PF'
   and ac.cur_id < 6
   and ac.visible = 'S'
   union
   select distinct
   mat_exp_per_id id, (p.nombre || ' ' || p.apellido1 || ' ' || p.apellido2) nombre, 'borillo@gmail.com' email --busca_cuenta (cdo_per_id) email
   from exp_asi_cursadas sp,
   per_personas p,
   pod_asi_cursos ac
   where     p.id = sp.mat_exp_per_id
   and sp.mat_curso_aca = euji_util.euji_curso_aca ()
   and sp.mat_curso_aca = ac.curso_aca
   and sp.asi_id = ac.asi_id
   and ac.cur_tit_id = sp.mat_exp_tit_id
   and ac.caracter = 'PF'
   and ac.cur_id < 6
   and ac.visible = 'S'
   union
   select distinct pmat_pexp_per_id id, (p.nombre || p.apellido1 || p.apellido2) nombre, 'borillo@gmail.com' email --busca_cuenta (cdo_per_id) email
   from pop_asi_cursadas sp,
   per_personas p,
   pop_asignaturas a,
   pop_oferta o
   where     p.id = sp.pmat_pexp_per_id
   and pmat_curso_aca = euji_util.euji_curso_aca ()
   and a.id = o.pasi_id
   and a.id = sp.asi_id
   and o.pasi_id = sp.asi_id
   and sp.pmat_pexp_pmas_id = o.pmas_id
   and sp.pmat_curso_aca = o.curso_aca
   and (a.pf = 'S'
   or a.pf_investigacion = 'S');

CREATE OR REPLACE FORCE VIEW UJI_TRABAJOFINGRADO.TFG_VW_COORDINADORES (
   PERSONA_ID
   ) AS
   select persona_id
   from tfg_vw_coordinadores_grados
   union
   select persona_id
   from tfg_vw_coordinadores_masters;

CREATE OR REPLACE FORCE VIEW UJI_TRABAJOFINGRADO.TFG_VW_COORDINADORES_GRADOS (
   PERSONA_ID,
   TIPO_RELACION,
   CREDITOS,
   CODIGO_ASIGNATURA
   ) AS
   select per_id persona_id, 'COORDINADOR' tipo_relacion, 0 creditos, ac.asi_id codigo_asignatura
   from grh_cargos_per cp,
        pod_asi_cursos ac
   where     nvl (f_fin, sysdate) >= sysdate
         and crg_id = 314
         and ac.cur_tit_id = cp.tit_id
         and ac.caracter = 'PF'
         and ac.cur_id < 6
         and ac.visible = 'S'
         and ac.curso_aca = euji_util.euji_curso_aca ();

CREATE OR REPLACE FORCE VIEW UJI_TRABAJOFINGRADO.TFG_VW_COORDINADORES_MASTERS (
   PERSONA_ID,
   TIPO_RELACION,
   CREDITOS,
   CODIGO_ASIGNATURA
   ) AS
   select per_id persona_id, 'COORDINADOR' tipo_relacion, 0 creditos, a.id codigo_asignatura
   from grh_cargos_per cp,
        pop_oferta ac,
        pop_asignaturas a
   where     nvl (f_fin, sysdate) >= sysdate
         and crg_id = 292
         and a.id = ac.pasi_id
         and ac.pmas_id = cp.tit_id
         and ac.curso_aca = euji_util.euji_curso_aca ()
         and (a.pf = 'S'
              or a.pf_investigacion = 'S');

CREATE OR REPLACE FORCE VIEW UJI_TRABAJOFINGRADO.TFG_VW_ESTUDIANTES_GRADOS (
   PERSONA_ID,
   TIPO_RELACION,
   CREDITOS,
   CODIGO_ASIGNATURA
   ) AS
   select p.id persona_id, 'ESTUDIANTE' tipo_relacion, 0 creditos, sp.asi_id codigo_asignatura
   from exp_asi_cursadas sp,
        per_personas p,
        pod_asi_cursos ac
   where     p.id = sp.mat_exp_per_id
         and mat_curso_aca = euji_util.euji_curso_aca ()
         and sp.asi_id = ac.asi_id
         and ac.caracter = 'PF'
         and ac.cur_id < 6
         and ac.visible = 'S'
         and ac.curso_aca = mat_curso_aca
         and ac.cur_tit_id = sp.mat_exp_tit_id;

CREATE OR REPLACE FORCE VIEW UJI_TRABAJOFINGRADO.TFG_VW_ESTUDIANTES_MASTERS (
   PERSONA_ID,
   TIPO_RELACION,
   CREDITOS,
   CODIGO_ASIGNATURA
   ) AS
   select sp.pmat_pexp_per_id persona_id, 'ESTUDIANTE' tipo_relacion, 0 creditos, sp.asi_id codigo_asignatura
   from pop_asi_cursadas sp,
        pop_oferta ac,
        pop_asignaturas a
   where     sp.pmat_curso_aca = euji_util.euji_curso_aca ()
         and sp.pmat_curso_aca = ac.curso_aca
         and sp.pmat_pexp_pmas_id = ac.pmas_id
         and sp.asi_id = ac.pasi_id
         and a.id = ac.pasi_id
         and a.id = sp.asi_id
         and ac.curso_aca = euji_util.euji_curso_aca ()
         and (a.pf = 'S'
              or a.pf_investigacion = 'S');

CREATE OR REPLACE FORCE VIEW UJI_TRABAJOFINGRADO.TFG_VW_PROFESORES_GRADOS (
   PERSONA_ID,
   TIPO_RELACION,
   CREDITOS,
   CODIGO_ASIGNATURA
   ) AS
   select pdi.cdo_per_id persona_id, 'PROFESOR' tipo_relacion, sum (pdi.creditos), ac.asi_id codigo_asignatura
   from pod_subgrupo_pdi pdi,
        pod_asi_cursos ac
   where     cdo_curso_aca = euji_util.euji_curso_aca ()
         and pdi.sgr_grp_asi_id = ac.asi_id
         and ac.caracter = 'PF'
         and ac.cur_id < 6
         and ac.visible = 'S'
         and ac.curso_aca = pdi.cdo_curso_aca
   group by pdi.cdo_per_id,
            'PROFESOR',
            ac.asi_id;

CREATE OR REPLACE FORCE VIEW UJI_TRABAJOFINGRADO.TFG_VW_PROFESORES_MASTERS (
   PERSONA_ID,
   TIPO_RELACION,
   CRD_IMPARTIDOS,
   CODIGO_ASIGNATURA
   ) AS
   select pdi.per_id persona_id, 'PROFESOR' tipo_relacion, pdi.crd_impartidos, a.id codigo_asignatura
   from pop_grupos_pdi pdi,
        pop_oferta ac,
        pop_asignaturas a
   where     pdi.pgr_pof_curso_aca = euji_util.euji_curso_aca ()
         and pdi.pgr_pof_pasi_id = a.id
         and pdi.pgr_pof_pasi_id = ac.pasi_id
         and pdi.pgr_pof_pmas_id = ac.pmas_id
         and ac.curso_aca = pdi.pgr_pof_curso_aca
         and ac.curso_aca = pdi.pgr_pof_curso_aca
         and (a.pf = 'S'
              or a.pf_investigacion = 'S');
