alter table tfg_ext_asignaturas add (ejercicio number);
alter table tfg_asignaturas_config add (ejercicio number);
alter table tfg_propuestas add (ejercicio number);
alter table tfg_turnos add (ejercicio number);
alter table tfg_carga_profesores add (ejercicio number);
alter table tfg_ext_asignaturas_personas add (ejercicio number);

update tfg_asignaturas_config set ejercicio = 2014;
update tfg_propuestas set ejercicio = 2014;
update tfg_turnos set ejercicio = 2014;
update tfg_carga_profesores set ejercicio = 2014;

alter table tfg_propuestas modify(ejercicio number not null);
alter table tfg_asignaturas_config modify(ejercicio number not null);
alter table tfg_turnos modify(ejercicio number not null);
alter table tfg_carga_profesores modify(ejercicio number not null);


CREATE TABLE tfg_asignaturas_config_new (
    id NUMBER NOT NULL,
    fecha_limite DATE,
    ordenacion VARCHAR2(100 BYTE) DEFAULT 'NOTA_MEDIA',
    codigo_asignatura VARCHAR2(20 BYTE) DEFAULT '',
    habilitar_espera NUMBER(1,0) DEFAULT 1 NOT NULL,
    ejercicio NUMBER,
    CONSTRAINT "TFG_ASIGNATURAS_CONFIG_PK" PRIMARY KEY ("ID"),
    CONSTRAINT "TFG_ASIGNATURAS_CONFIG_UNIQUE" UNIQUE ("CODIGO_ASIGNATURA", "EJERCICIO")
);

INSERT INTO tfg_asignaturas_config_new (id, fecha_limite, ordenacion, codigo_asignatura, habilitar_espera, ejercicio)
SELECT HIBERNATE_SEQUENCE.nextval, fecha_limite, ordenacion, codigo_asignatura, habilitar_espera, ejercicio FROM tfg_asignaturas_config;

DROP TABLE tfg_asignaturas_config;

alter table "UJI_TRABAJOFINGRADO"."TFG_ASIGNATURAS_CONFIG_NEW" rename to TFG_ASIGNATURAS_CONFIG


alter table tfg_ext_asignaturas add constraint tfg_asignaturas_unique unique (codigo, ejercicio);
alter table tfg_asignaturas_config add constraint tfg_asignaturas_config_unique unique (codigo_asignatura, ejercicio);
alter table tfg_carga_profesores add constraint tfg_carga_profesores_unique unique (codigo_asignatura, persona_id, ejercicio);
alter table tfg_turnos add constraint tfg_turnos_unique unique (codigo_asignatura, ejercicio);
alter table tfg_asignaturas_config add CONSTRAINT TFG_ASIGNATURAS_CONFIG_FK1 FOREIGN KEY ("CODIGO_ASIGNATURA", "EJERCICIO")  REFERENCES "UJI_TRABAJOFINGRADO"."TFG_EXT_ASIGNATURAS" ("CODIGO", "EJERCICIO") ON DELETE CASCADE
