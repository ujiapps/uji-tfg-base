ALTER TABLE TFG_PROPUESTAS
    ADD (PER_ID_CREACION NUMBER );

CREATE TABLE "UJI_TRABAJOFINGRADO"."TFG_PROPUESTAS_TUTORES"
(
    "ID"           NUMBER NOT NULL ENABLE,
    "TUTOR_ID"     NUMBER NOT NULL ENABLE,
    "PROPUESTA_ID" NUMBER NOT NULL ENABLE
);
CREATE UNIQUE INDEX "UJI_TRABAJOFINGRADO"."TFG_PROPUESTAS_TUTORES_PK" ON "UJI_TRABAJOFINGRADO"."TFG_PROPUESTAS_TUTORES" ("ID");
ALTER TABLE "UJI_TRABAJOFINGRADO"."TFG_PROPUESTAS_TUTORES"
    ADD CONSTRAINT "TFG_PROPUESTAS_TUTORES_PK" PRIMARY KEY ("ID") ENABLE;


CREATE
OR REPLACE FORCE EDITIONABLE VIEW "UJI_TRABAJOFINGRADO"."TFG_VW_PROPUESTAS" ("ID", "NOMBRE", "PLAZAS", "ESPERA", "CODIGO_ASIGNATURA", "FECHA_CREACION", "TUTOR_ID", "TUTORES", "TUTORES_NOMBRE", "ACEPTADOS", "EN_ESPERA", "EJERCICIO") AS
select p.id,
       nombre,
       plazas,
       espera,
       CODIGO_ASIGNATURA,
       fecha_creacion,
       pt.tutor_id,
       (select LISTAGG(t.tutor_id, ',') WITHIN
GROUP ( ORDER BY t.tutor_id )
from TFG_PROPUESTAS_TUTORES t
where p.id=t.propuesta_id ) tutores
    , (
select LISTAGG( per.nombre || ' '|| per.apellido1|| ' ' || per.apellido2, ',' ) WITHIN
GROUP ( ORDER BY t.tutor_id )
from TFG_PROPUESTAS_TUTORES t join TFG_EXT_PERSONAS per
on t.tutor_id=per.id
where p.id=t.propuesta_id ) tutores_nombre
    , (
select count (*)
from TFG_PERSONAS_PROPUESTAS pp
where pp.propuesta_id=p.id
  and aceptada=1 ) aceptados
    , (
select count (*)
from TFG_PERSONAS_PROPUESTAS pp
where pp.propuesta_id=p.id
  and aceptada=0 ) en_espera
    , p.ejercicio
from tfg_propuestas p left join TFG_PROPUESTAS_TUTORES pt
on p.id = pt.propuesta_id;

CREATE
OR REPLACE VIEW "UJI_TRABAJOFINGRADO"."TGF_VW_ASIGNACION_PROPUESTAS" ("PERSONA_ID", "NOMBRE", "APELLIDO1", "APELLIDO2", "PROPUESTA_ID", "PROPUESTA_NOMBRE", "CODIGO_ASIGNATURA", "EJERCICIO", "ACEPTADA", "COMENTARIOS", "FECHA_EDICION", "TUTOR_ID", "TUTORES_NOMBRE") AS
select ap.persona_id,
       pe.nombre,
       pe.apellido1,
       pe.apellido2,
       p.id        propuesta_id,
       p.nombre    propuesta_nombre,
       ap.codigo_asignatura,
       ap.ejercicio,
       pp.aceptada aceptada,
       pp.comentarios,
       pp.fecha_edicion,
       pt.tutor_id,
       (select LISTAGG(per.nombre || ' ' || per.apellido1 || ' ' || per.apellido2, ',') WITHIN
GROUP ( ORDER BY t.tutor_id )
from TFG_PROPUESTAS_TUTORES t join TFG_EXT_PERSONAS per
on t.tutor_id=per.id
where p.id=t.propuesta_id ) tutores_nombre

from TFG_EXT_ASIGNATURAS_PERSONAS ap
    join TFG_EXT_PERSONAS pe
on ap.PERSONA_ID = pe.id
    left join TFG_PERSONAS_PROPUESTAS pp on pp.persona_id=pe.id
    join TFG_PROPUESTAS p on p.id=pp.propuesta_id and p.codigo_asignatura=ap.codigo_asignatura and p.ejercicio=ap.ejercicio
    join TFG_PROPUESTAS_TUTORES pt on p.id=pt.propuesta_id
where ap.TIPO_RELACION = 'ESTUDIANTE'
union
select ap.persona_id,
       pe.nombre,
       pe.apellido1,
       pe.apellido2,
       null propuesta_id,
       null propuesta_nombre,
       ap.codigo_asignatura,
       ap.ejercicio,
       null aceptada,
       null comentarios,
       null fecha_edicion,
       null tutor_id,
       null tutores_nombre
from TFG_EXT_ASIGNATURAS_PERSONAS ap
         join TFG_EXT_PERSONAS pe on ap.PERSONA_ID = pe.id
         left join TFG_PERSONAS_PROPUESTAS pp on pp.persona_id = pe.id
where pp.propuesta_id is null
  and ap.TIPO_RELACION = 'ESTUDIANTE';
