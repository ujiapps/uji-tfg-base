-- SQL Unicamente válido para Producción (en Devel ya está ejecutado)
INSERT INTO TFG_ASIGNATURAS_CONFIG (FECHA_LIMITE, ORDENACION, CODIGO_ASIGNATURA)
SELECT NULL, 'NOTA_MEDIA', CODIGO FROM TFG_EXT_ASIGNATURAS
-- SQL PARA AÑADIR LA COLUMNA DE NTOA DE ACCESO
ALTER TABLE TFG_EXT_NOTAS_ALUMNOS ADD (NOTA_ACCESO NUMBER DEFAULT 0);
ALTER TABLE TFG_EXT_NOTAS_ALUMNOS MODIFY NOTA_ACCESO NOT NULL;
UPDATE TFG_EXT_NOTAS_ALUMNOS SET NOTA_ACCESO = replace(to_char(dbms_random.value(4,10), '99.99'), '.',',');

ALTER TABLE TFG_PERSONAS_PROPUESTAS ADD (comentarios VARCHAR2(2000) DEFAULT '');

-- VARCHAR TO CLOB
ALTER TABLE tfg_propuestas ADD (descripcion_temp CLOB);
UPDATE tfg_propuestas SET descripcion_temp = descripcion;
ALTER TABLE tfg_propuestas DROP COLUMN descripcion;
ALTER TABLE tfg_propuestas RENAME COLUMN descripcion_temp TO descripcion;

ALTER TABLE tfg_propuestas ADD (bibliografia_temp CLOB);
UPDATE tfg_propuestas SET bibliografia_temp = bibliografia;
ALTER TABLE tfg_propuestas DROP COLUMN bibliografia;
ALTER TABLE tfg_propuestas RENAME COLUMN bibliografia_temp TO bibliografia;

ALTER TABLE tfg_propuestas ADD (objetivos_temp CLOB);
UPDATE tfg_propuestas SET objetivos_temp = objetivos;
ALTER TABLE tfg_propuestas DROP COLUMN objetivos;
ALTER TABLE tfg_propuestas RENAME COLUMN objetivos_temp TO objetivos;

ALTER TABLE tfg_propuestas ADD (observaciones_temp CLOB);
UPDATE tfg_propuestas SET observaciones_temp = observaciones;
ALTER TABLE tfg_propuestas DROP COLUMN observaciones;
ALTER TABLE tfg_propuestas RENAME COLUMN observaciones_temp TO observaciones;


