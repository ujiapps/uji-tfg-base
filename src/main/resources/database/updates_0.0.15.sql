ALTER TABLE TFG_ASIGNATURAS_CONFIG
    ADD (PERMITIR_SOLICITUDES DATE);


grant select on pod_convocatorias to UJI_TRABAJOFINGRADO;
grant select on pop_notas to UJI_TRABAJOFINGRADO;
grant select on exp_notas to UJI_TRABAJOFINGRADO;
grant select on exp_calificaciones to UJI_TRABAJOFINGRADO;