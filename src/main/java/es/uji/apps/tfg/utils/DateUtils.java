package es.uji.apps.tfg.utils;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import es.uji.apps.tfg.i18n.ResourceProperties;

public class DateUtils
{   
    public static Date getCurrent() {
        Calendar cal = Calendar.getInstance();
        return cal.getTime();
    }
    
    public static Date getDate(String date) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/mm/YYYY h:m:s");
        return sdf.parse(date);
    }

    public static Timestamp getFechaFinTurno(Timestamp fechaTurno, Integer plazoMatricula)
    {
        return new Timestamp(fechaTurno.getTime() + plazoMatricula * 24 * 3600 * 1000);
    }

    public static String getMensajeEstadoTurno(String idioma, Timestamp fechaTurno, Timestamp finplazo)
    {
        String fechaTurnoMensaje;
        if (esFechaFutura(finplazo)) {                            
            SimpleDateFormat sdf  = new SimpleDateFormat("dd/MM/yyyy (HH:mm 'h'.)");
            fechaTurnoMensaje = ResourceProperties.getMessage(new Locale(idioma), "tfg.message.turno.alert") + " " + sdf.format(fechaTurno);
        }
        else {
            fechaTurnoMensaje = ResourceProperties.getMessage(new Locale(idioma), "tfg.message.turno.alertTimeOut");
        }
        return fechaTurnoMensaje;
    }

    public static boolean esFechaFutura(Timestamp finplazo)
    {
        Timestamp now = new Timestamp(DateUtils.getCurrent().getTime());
        return now.before(finplazo);
    }

    public static boolean tieneTurnoHabilitado(Timestamp fechaTurno, Timestamp finplazo)
    {
        if (fechaTurno == null) {
            return false;
        }
        Timestamp now = new Timestamp(DateUtils.getCurrent().getTime());
        return now.after(fechaTurno) && now.before(finplazo);
    }
    
}
