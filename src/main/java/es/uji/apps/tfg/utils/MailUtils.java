package es.uji.apps.tfg.utils;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.MediaType;

import es.uji.commons.messaging.client.MessageNotSentException;
import es.uji.commons.messaging.client.MessagingClient;
import es.uji.commons.messaging.client.model.MailMessage;

public class MailUtils
{
    private List<String> to;
    private String from;
    private String subject;
    private String body;
    private String type;
    private String error;
    private String nombrePersona;

    public MailUtils()
    {

    }

    public MailUtils(String to, String subject, String body, String type, String nombrePersona)
    {
        setTo(to);
        setFrom("e-ujier@uji.es");
        setSubject(subject);
        setBody(body);
        setType(type);
        setNombrePersona(nombrePersona);
    }

    public List<String> getTo()
    {
        return to;
    }

    public void setTo(List<String> to)
    {
        this.to = to;
    }

    public void setTo(String to)
    {
        if (this.to == null)
        {
            this.to = new ArrayList<String>();
        }
        this.to.add(to);
    }

    public String getFrom()
    {
        return from;
    }

    public void setFrom(String from)
    {
        this.from = from;
    }

    public String getSubject()
    {
        return subject;
    }

    public void setSubject(String subject)
    {
        this.subject = subject;
    }

    public String getBody()
    {
        return body;
    }

    public void setBody(String body)
    {
        this.body = body;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public String getError()
    {
        return error;
    }

    public void setError(String error)
    {
        this.error = error;
    }

    public void send() throws MessageNotSentException
    {                
        MailMessage mensaje = new MailMessage("TFG");
        mensaje.setTitle(subject.toString());
        if (type != null && type.equals("html")) {
            mensaje.setContentType(MediaType.TEXT_HTML_VALUE);
        }
        else {            
            mensaje.setContentType(MediaType.TEXT_PLAIN_VALUE);
        }
        mensaje.setContent(body);

        for (String mailPersonal : to)
        {
            mensaje.addToRecipient(mailPersonal);
        }
        mensaje.setSender(getFrom());
        MessagingClient client = new MessagingClient();
        
        client.send(mensaje);
    }

    public String getNombrePersona()
    {
        return nombrePersona;
    }

    public void setNombrePersona(String nombrePersona)
    {
        this.nombrePersona = nombrePersona;
    }
}