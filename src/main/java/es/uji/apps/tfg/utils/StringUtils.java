package es.uji.apps.tfg.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.nio.charset.Charset;

import org.w3c.tidy.Tidy;

public class StringUtils
{
    public final static String BR_PARSE =  "(?i)<br[^>]*>";
    
    public static String cleanHTML(String html)
    {
        if (html == null)
            return "";
        
        String limpio = html.replaceAll(BR_PARSE, "<br/>");
        return limpio;
    }
    
    public static String tidyClean(String html) {
        if (html == null)
            return "";

        html = html.replaceAll("<o:", "<");
        html = html.replaceAll("</o:", "</");

        Tidy tidy = new Tidy();
        tidy.setPrintBodyOnly(true);
        tidy.setInputEncoding("UTF-8");
        tidy.setOutputEncoding("UTF-8");
        tidy.setWraplen(Integer.MAX_VALUE);
        tidy.setXmlOut(false);
        tidy.setXmlPIs(false);
        tidy.setXmlTags(false);
        tidy.setXHTML(true);
        tidy.setBreakBeforeBR(false);
        tidy.setIndentAttributes(false);
        tidy.setIndentContent(false);
        ByteArrayInputStream inputStream = new ByteArrayInputStream(html.getBytes(Charset.forName("UTF-8")));
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        tidy.parse(inputStream, outputStream);

        return outputStream.toString();
    }

    public static void main(String[] args)
    {
        String texto = "<p style=\"line-height:15.6pt\"><span style=\"font-size:10.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#333333\">- Adquirir los conocimientos y competencias necesarios para atender un encargo real de traducción jurídica profesional<o:p></o:p></span></p>";
        texto = texto.replaceAll("<o:", "<");
        texto = texto.replaceAll("</o:", "</");
        System.out.println("*" + StringUtils.tidyClean(texto));
    }
}
