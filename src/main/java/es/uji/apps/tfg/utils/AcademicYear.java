package es.uji.apps.tfg.utils;


import java.util.Calendar;

public class AcademicYear {
    public static int getCurrent() {
        Calendar now = Calendar.getInstance();
        Calendar academicYearStart = Calendar.getInstance();
        academicYearStart.set(Calendar.YEAR, now.getWeekYear());
        academicYearStart.set(Calendar.MONTH, Calendar.SEPTEMBER);
        academicYearStart.set(Calendar.DAY_OF_MONTH, 1);
        if (now.after(academicYearStart)) {
            return now.getWeekYear();
        } else {
            return now.getWeekYear() - 1;
        }
    }
}
