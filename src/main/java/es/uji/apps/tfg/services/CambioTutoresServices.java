package es.uji.apps.tfg.services;

import es.uji.apps.tfg.dao.CambioTutoresDAO;
import es.uji.apps.tfg.dao.PropuestasDAO;
import es.uji.apps.tfg.dto.PropuestaDTO;
import es.uji.apps.tfg.model.Asignatura;
import es.uji.apps.tfg.model.PropuestaCambio;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class CambioTutoresServices {

    public final static Logger log = LoggerFactory.getLogger(CambioTutoresServices.class);


    private CambioTutoresDAO cambioTutoresDAO;
    private UsuariosService usuariosService;
    private AsignaturasService asignaturasService;
    private PropuestasDAO propuestasDAO;

    @Autowired
    public CambioTutoresServices(CambioTutoresDAO cambioTutoresDAO, UsuariosService usuariosService, AsignaturasService asignaturasService, PropuestasDAO propuestasDAO) {
        this.cambioTutoresDAO = cambioTutoresDAO;
        this.usuariosService = usuariosService;
        this.asignaturasService = asignaturasService;
        this.propuestasDAO = propuestasDAO;
    }


    public List<PropuestaCambio> getPropuestasCambio(Long connectedUserId, String codigoAsignatura, Integer cursoAca) {
        Boolean perfilesValido = usuariosService.hasPerfiles(new String[]{"ADMIN", "COORDINADOR"}, connectedUserId);
        List<PropuestaCambio> resultado;

        if (perfilesValido || (usuariosService.coordinaAsignatura(connectedUserId, codigoAsignatura)))
            resultado = agrupaAlumnos(cambioTutoresDAO.getPropuestasCambio(codigoAsignatura, cursoAca));
        else
            resultado = agrupaAlumnos(cambioTutoresDAO.getPropuestasCambioTutor(connectedUserId, codigoAsignatura, cursoAca));

        return resultado;
    }

    private List<PropuestaCambio> agrupaAlumnos(List<PropuestaCambio> propuestas) {
        if (propuestas.isEmpty()) return propuestas;

        return propuestas.stream()
                .collect(Collectors.groupingBy(PropuestaCambio::getId))
                .values().stream()
                .map(grupo -> {
                    PropuestaCambio base = grupo.get(0);
                    base.setAlumnoNombre(
                            grupo.stream()
                                    .map(PropuestaCambio::getAlumnoNombre)
                                    .filter(Objects::nonNull)
                                    .distinct()
                                    .collect(Collectors.joining(", "))
                    );
                    return base;
                }).collect(Collectors.toList());
    }

    public List<Asignatura> getAsignaturasCambio(Long connectedUserId) {
        List<Asignatura> asignaturaList = new ArrayList<>();
        Boolean isAdmin = usuariosService.hasPerfil("ADMIN", connectedUserId);
        Boolean isCoord = usuariosService.hasPerfil("COORDINADOR", connectedUserId);
        if (isAdmin) {
            asignaturaList = asignaturasService.getAsignaturas();
        } else if (isCoord) {
            asignaturaList = asignaturasService.getAsignaturasByCoordinador(connectedUserId, true);
        }
        return asignaturaList.stream().sorted(Comparator.comparing(o -> o.getCodigo())).collect(Collectors.toList());
    }

    public List<Asignatura> getAsignaturasCambioByAnyo(Long connectedUserId, Long anyo) {
        List<Asignatura> asignaturaList = new ArrayList<>();
        Boolean isAdmin = usuariosService.hasPerfil("ADMIN", connectedUserId);
        Boolean isCoord = usuariosService.hasPerfil("COORDINADOR", connectedUserId);
        if (isAdmin) {
            asignaturaList = asignaturasService.getAsignaturasByAnyo(anyo);
        } else if (isCoord) {
            asignaturaList = asignaturasService.getAsignaturasByCoordinadorByAnyo(connectedUserId, true, anyo);
        }
        return asignaturaList.stream().sorted(Comparator.comparing(o -> o.getCodigo())).collect(Collectors.toList());
    }

    public PropuestaDTO getPropuestaById(long id) {
        return cambioTutoresDAO.getPropuesta(id);
    }

    public void addUpdateTutores(Long propuestaId, List<Long> tutores, String nombre) {
        propuestasDAO.addUpdateTutoresCambio(propuestaId, tutores, nombre);
    }
}
