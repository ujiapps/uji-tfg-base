package es.uji.apps.tfg.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.tfg.ResponseMessage;
import es.uji.apps.tfg.model.Convocatoria;
import es.uji.apps.tfg.services.ConvocatoriaService;
import es.uji.apps.tfg.services.UsuariosService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Path("convocatorias")
public class ConvocatoriasResource extends CoreBaseService {

    public final static Logger log = LoggerFactory.getLogger(ConvocatoriasResource.class);
    private final DateFormat format = new SimpleDateFormat("MM/dd/yyyy mm:ss");


    @InjectParam
    UsuariosService usuariosService;

    @InjectParam
    ConvocatoriaService convocatoriaService;

    @GET
    @Path("{codigoAsignatura}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getConvocatoriasFechas(@PathParam("codigoAsignatura") String codigoAsignatura) {
        Long userId = AccessManager.getConnectedUserId(request);
        boolean hasPerfiles = usuariosService.hasPerfiles(new String[]{"ADMIN", "COORDINADOR"}, userId);
        if (hasPerfiles || (usuariosService.coordinaAsignatura(userId, codigoAsignatura))) {
            return UIEntity.toUI(convocatoriaService.getConvocatoriasFechas(codigoAsignatura));
        }
        return new ArrayList<>();
    }

    @POST
    @Path("{codigoAsignatura}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity createConvocatoria(@PathParam("codigoAsignatura") String codigoAsignatura,
                                       UIEntity uiEntity) throws ParseException {
        Date fechaIncio = uiEntity.getDate("fechaInicio");
        Date fechaFin = uiEntity.getDate("fechaFin");
        ParamUtils.checkNotNull(codigoAsignatura, fechaIncio, fechaFin);
        Long userId = AccessManager.getConnectedUserId(request);
        Convocatoria convocatoria = new Convocatoria(codigoAsignatura, fechaIncio, fechaFin);
        boolean hasPerfiles = usuariosService.hasPerfiles(new String[]{"ADMIN", "COORDINADOR"}, userId);
        if (hasPerfiles || (usuariosService.coordinaAsignatura(userId, convocatoria.getAsignaturaId()))) {
            return UIEntity.toUI(convocatoriaService.createOrUpdateConvocatoria(convocatoria));
        }
        return new UIEntity();
    }

    @PUT
    @Path("{codigoAsignatura}/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity updateConvocatoria(@PathParam("codigoAsignatura") String codigoAsignatura, UIEntity uiEntity) {
        Long userId = AccessManager.getConnectedUserId(request);
        Convocatoria convocatoria = new Convocatoria(uiEntity);
        boolean hasPerfiles = usuariosService.hasPerfiles(new String[]{"ADMIN", "COORDINADOR"}, userId);
        if (hasPerfiles || (usuariosService.coordinaAsignatura(userId, convocatoria.getAsignaturaId()))) {
            return UIEntity.toUI(convocatoriaService.createOrUpdateConvocatoria(convocatoria));
        }
        return new UIEntity();
    }

    @DELETE
    @Path("{codigoAsignatura}/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public ResponseMessage deleteConvocatoria(@PathParam("codigoAsignatura") String codigoAsignatura, UIEntity uiEntity) {
        Long userId = AccessManager.getConnectedUserId(request);
        Convocatoria convocatoria = new Convocatoria(uiEntity);
        boolean hasPerfiles = usuariosService.hasPerfiles(new String[]{"ADMIN", "COORDINADOR"}, userId);
        if (hasPerfiles || (usuariosService.coordinaAsignatura(userId, convocatoria.getAsignaturaId()))) {
            convocatoriaService.deleteConvocatoria(convocatoria);
            return new ResponseMessage(true, "Convocatoria borrada");
        }
        return new ResponseMessage(false, "Error al esborrar convocatoria");
    }


}
