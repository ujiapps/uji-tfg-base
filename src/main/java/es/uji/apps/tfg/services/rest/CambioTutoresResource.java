package es.uji.apps.tfg.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.tfg.dto.PropuestaDTO;
import es.uji.apps.tfg.model.Profesor;
import es.uji.apps.tfg.model.Propuesta;
import es.uji.apps.tfg.model.PropuestaCambio;
import es.uji.apps.tfg.services.CambioTutoresServices;
import es.uji.apps.tfg.services.UsuariosService;
import es.uji.apps.tfg.utils.AcademicYear;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;
import es.uji.commons.sso.User;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

public class CambioTutoresResource extends CoreBaseService {

    @InjectParam
    CambioTutoresServices cambioTutoresServices;

    @InjectParam
    UsuariosService usuariosService;

    @GET
    @Path("{codigoAsignatura}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<PropuestaCambio> getPropuestasCambio(@PathParam("codigoAsignatura") String codigoAsignatura,
                                                     @QueryParam("cursoAca") Integer cursoAca) {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        return cambioTutoresServices.getPropuestasCambio(connectedUserId, codigoAsignatura, cursoAca);
    }

    @GET
    @Path("asignaturas")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getAsignaturasCambio() {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        return UIEntity.toUI(cambioTutoresServices.getAsignaturasCambio(connectedUserId));
    }

    @GET
    @Path("asignaturas/{anyo}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getAsignaturasCambioByAnyo(@PathParam("anyo") Long anyo) {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        return UIEntity.toUI(cambioTutoresServices.getAsignaturasCambioByAnyo(connectedUserId, anyo));
    }

    @GET
    @Path("/{codigoAsignatura}/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Propuesta getPropuesta(@PathParam("id") long id) {

        PropuestaDTO response = cambioTutoresServices.getPropuestaById(id);
        if (response != null) {
            return new Propuesta(response, false);
        }
        return null;
    }

    @GET
    @Path("{codigo_asignatura}/profesores")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Profesor> getProfesores(@PathParam("codigo_asignatura") @DefaultValue("0") String codigo_asignatura) {
        User user = AccessManager.getConnectedUser(request);
        boolean isAdmin = usuariosService.hasPerfil("ADMIN", user.getId());
        boolean isCoord = usuariosService.hasPerfil("COORDINADOR", user.getId());

        List<Profesor> salida = new ArrayList<>();
        if (isAdmin || (isCoord && codigo_asignatura.compareTo("0") != 0 && usuariosService.coordinaAsignatura(user.getId(), codigo_asignatura))) {
            salida = usuariosService.getProfesoresByAsignaturaCursoPasado(codigo_asignatura);
        }

        if (salida.size() == 0) {
            salida.add(new Profesor(usuariosService.getUsuario(user.getId())));
        }
        return salida;
    }

    @PUT
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateTutores(@FormParam("tutores") List<Long> tutores, @FormParam("id") Long propuestaId, @FormParam("nombre") String nombre) {

        ParamUtils.checkNotNull(tutores, propuestaId);
        cambioTutoresServices.addUpdateTutores(propuestaId, tutores, nombre);
        return Response.ok().build();
    }

    @GET
    @Path("previousYear")
    @Produces(MediaType.APPLICATION_JSON)
    public int getPreviousYear() {
       return AcademicYear.getCurrent()-1;
    }
}
