package es.uji.apps.tfg.services.rest;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.tfg.dto.PersonaDTO;
import es.uji.apps.tfg.model.Asignatura;
import es.uji.apps.tfg.model.Profesor;
import es.uji.apps.tfg.model.ResultadoOperacion;
import es.uji.apps.tfg.services.AsignaturasService;
import es.uji.apps.tfg.services.UsuariosService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.sso.AccessManager;
import es.uji.commons.sso.User;

@Path("profesores")
public class ProfesoresResource extends CoreBaseService
{
    @InjectParam
    private UsuariosService usuariosService;
    @InjectParam
    private AsignaturasService asignaturasService;

    @GET
    @Path("{codigoAsignatura}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Profesor> getUsuarios(@PathParam("codigoAsignatura") String codigo_asignatura)
    {
        User user = AccessManager.getConnectedUser(request);
        return usuariosService.getProfesores(codigo_asignatura, user);
    }

    @PUT
    @Path("{codigoAsignatura}/{profesorId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateCarga(@PathParam("codigoAsignatura") String codigo_asignatura, @PathParam("profesorId") Long persona_id, Profesor profesor)
    {
        User user = AccessManager.getConnectedUser(request);
        boolean isAdmin = usuariosService.hasPerfil("ADMIN", user.getId());
        boolean isCoord = usuariosService.hasPerfil("COORDINADOR", user.getId());
        if (isCoord || isAdmin)
        {
            List<Profesor> profesores;
            if (isAdmin)
            {
                profesores = usuariosService.getProfesoresByAsignatura(codigo_asignatura);
            }
            else
            {
                profesores = usuariosService.getProfesoresByCoordinador(user.getId());
            }
            boolean contenido = false;
            for (Profesor prof : profesores)
            {
                if (prof != null && prof.getId() == persona_id.longValue() && persona_id.longValue() == profesor.getId().longValue())
                {
                    contenido = true;
                    break;
                }
            }
            if (contenido && usuariosService.setCargaProfesor(persona_id, codigo_asignatura, profesor.getCarga(), user.getId()) > 0)
            {
                return Response.ok(true).build();
            }
        }
        return Response.status(500).build();
    }

    @GET
    @Path("loot/{codigoAsignatura}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response distribuirCargaProfesores(@PathParam("codigoAsignatura") String codigo_asignatura)
    {
        User user = AccessManager.getConnectedUser(request);
        boolean isAdmin = usuariosService.hasPerfil("ADMIN", user.getId());
        boolean isCoord = usuariosService.hasPerfil("COORDINADOR", user.getId());
        if (isAdmin || isCoord)
        {
            if (!isAdmin)
            {
                Long editor_id = user.getId();
                List<Asignatura> asignaturas = asignaturasService.getAsignaturas(editor_id, isAdmin, isCoord, true);
                for (Asignatura asignatura : asignaturas)
                {
                    if (asignatura.getCodigo().compareTo(codigo_asignatura) == 0)
                    {
                        if (usuariosService.distribuirCargaProfesores(codigo_asignatura, user.getId()))
                        {
                            return Response.ok().build();
                        }
                    }
                }
            }
            else
            {
                if (usuariosService.distribuirCargaProfesores(codigo_asignatura, user.getId()))
                {
                    return Response.ok().build();
                }
            }
        }
        return Response.status(500).build();
    }

    @GET
    @Path("cargaRestante/{codigo_asignatura}")
    @Produces(MediaType.APPLICATION_JSON)
    public ResultadoOperacion cargaRestante(@PathParam("codigo_asignatura") String codigo_asignatura)
    {
        User user = AccessManager.getConnectedUser(request);
        boolean isAdmin = usuariosService.hasPerfil("ADMIN", user.getId());
        boolean isCoord = usuariosService.hasPerfil("COORDINADOR", user.getId());
        if (isAdmin || (isCoord && usuariosService.coordinaAsignatura(user.getId(), codigo_asignatura)))
        {
            return new ResultadoOperacion(true, "ADMIN");
        }
        Integer acumulada = usuariosService.getCargaAcumulada(codigo_asignatura, user.getId());
        Integer requerida = usuariosService.getCargaProfesor(user.getId(), codigo_asignatura);
        if (acumulada != null && requerida != null)
        {
            return new ResultadoOperacion(true, requerida - acumulada);
        }
        return new ResultadoOperacion(false, 0);
    }


    @GET
    @Path("{codigoAsignatura}/descargar")
    @Produces("application/vnd.ms-excel")
    public Response getCargaExcel(@PathParam("codigoAsignatura") String codigo_asignatura) throws IOException
    {

        User user = AccessManager.getConnectedUser(request);
        List<Profesor> profesorList = usuariosService.getProfesores(codigo_asignatura, user);

        ByteArrayOutputStream ostream = usuariosService.getExcel(profesorList);
        return Response.ok(ostream.toByteArray(), MediaType.APPLICATION_OCTET_STREAM).header("content-disposition", "attachment; filename = Carrega-" + codigo_asignatura + ".xls").build();
    }
}
