package es.uji.apps.tfg.services;

import es.uji.apps.tfg.dao.EvidenciasDAO;
import es.uji.apps.tfg.dao.PersonasDAO;
import es.uji.apps.tfg.dto.EvidenciaDTO;
import es.uji.apps.tfg.model.TipoEstudio;
import es.uji.apps.tfg.services.rest.BaseResource;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.Role;
import org.apache.poi.ss.usermodel.Row;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

@Service
public class EvidenciasService extends BaseResource {
    public final static Logger log = LoggerFactory.getLogger(EvidenciasService.class);

    @Autowired
    private PersonasDAO personasDAO;
    @Autowired
    private EvidenciasDAO evidenciasDAO;

    @Role({"AUDITOR","ADMIN"})
    public ByteArrayOutputStream generateEvidencias(Long connectedUserId, String codigoAsignatura, Integer ejercicio) throws IOException {
        List<EvidenciaDTO> evidencias = evidenciasDAO.getEvidencias(codigoAsignatura, ejercicio);

        if (ParamUtils.isNotNull(evidencias) && evidencias.size() > 0) {
            WriterExcelService writerExcelService = new WriterExcelService();
            cabeceraEvidencias(writerExcelService, evidencias.get(0));
            cuerpoEvidencias(writerExcelService, evidencias);
            writerExcelService.autoSizeColumns(7);
            return writerExcelService.getExcel();
        } else
            return null;
    }

    private void cuerpoEvidencias(WriterExcelService writerExcelService, List<EvidenciaDTO> evidencias) {
        Integer rownum = 5;
        for (EvidenciaDTO evidenciaDTO : evidencias) {
            int cellnum = 0;
            Row row = writerExcelService.getNewRow(++rownum);
            writerExcelService.addCell(cellnum++, evidenciaDTO.getPersonaId().toString(), null, row);
            writerExcelService.addCell(cellnum++, evidenciaDTO.getNombreAlumno(), null, row);
            writerExcelService.addCell(cellnum++, evidenciaDTO.getNombreTrabajo(), null, row);
            writerExcelService.addCell(cellnum++, evidenciaDTO.getTutoresNombre(), null, row);
            writerExcelService.addCell(cellnum++, evidenciaDTO.getFechaLectura() != null ? evidenciaDTO.getFechaLectura().toString() : null, null, row);
            writerExcelService.addCell(cellnum++, "", null, row);
            writerExcelService.addCell(cellnum++, evidenciaDTO.getNota(), null, row);
        }
    }

    private void cabeceraEvidencias(WriterExcelService writerExcelService, EvidenciaDTO evidencia) {
        Integer rownum = 0;
        TipoEstudio tipoEstudio = TipoEstudio.get(evidencia.getTipoEstudio());
        writerExcelService.addFulla(tipoEstudio.getCodigo());

        Row resumen = writerExcelService.getNewRow(rownum);
        writerExcelService.addCell(0, "EVIDENCIA E15: TRABAJOS DE FINAL DE ".concat(tipoEstudio.getValue()), writerExcelService.getEstilNegreta(), resumen);
        writerExcelService.addMergedRegion(rownum, rownum, 0, 6);
        rownum = 2;
        resumen = writerExcelService.getNewRow(rownum);
        String texto = "E15: Listado de "
                .concat(tipoEstudio.getCodigo())
                .concat(" del Grado en ")
                .concat(evidencia.getNombreTitulacion())
                .concat(", asignatura ")
                .concat(evidencia.getCodigoAsignatura() + " - " + evidencia.getNombreAsignatura());
        writerExcelService.addCell(0, texto, null, resumen);
        writerExcelService.addMergedRegion(rownum, rownum, 0, 12);

        rownum = 4;
        resumen = writerExcelService.getNewRow(rownum);
        writerExcelService.addCell(0, "CURSO ".concat(String.valueOf(evidencia.getEjercicio())), null, resumen);
        writerExcelService.addMergedRegion(rownum, rownum, 0, 2);

        String[] columnas = new String[]{"Id", "Estudiante", "Título", "Tutor/a", "Fecha defensa", "Tribunal", "Calificación"};
        writerExcelService.generaCeldes(writerExcelService.getEstilNegreta(), ++rownum, columnas);

    }
}
