package es.uji.apps.tfg.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.tfg.model.Asignatura;
import es.uji.apps.tfg.model.ResultadoOperacion;
import es.uji.apps.tfg.services.AsignaturasService;
import es.uji.apps.tfg.services.UsuariosService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.json.lookup.LookupItem;
import es.uji.commons.sso.AccessManager;
import es.uji.commons.sso.User;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.Map;

@Path("asignaturas")
public class AsignaturasResource extends CoreBaseService {
    @InjectParam
    private AsignaturasService asignaturasService;
    @InjectParam
    private UsuariosService usuariosService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Asignatura> getAsignaturas()
    {
        User user = AccessManager.getConnectedUser(request);
        boolean isAdmin = usuariosService.hasPerfil("ADMIN", user.getId());
        boolean isCoord = usuariosService.hasPerfil("COORDINADOR", user.getId());
        return asignaturasService.getAsignaturas(user.getId(), isAdmin, isCoord, false);
    }

    @GET
    @Path("coord")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Asignatura> getAsignaturasCoordinadas()
    {
        User user = AccessManager.getConnectedUser(request);
        boolean isAdmin = usuariosService.hasPerfil("ADMIN", user.getId());
        boolean isCoord = usuariosService.hasPerfil("COORDINADOR", user.getId());
        return asignaturasService.getAsignaturas(user.getId(), isAdmin, isCoord, true);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("search")
    public List<LookupItem> getAsignaturasSearch(@QueryParam("query") String query)
    {
        User user = AccessManager.getConnectedUser(request);
        boolean isAdmin = usuariosService.hasPerfil("ADMIN", user.getId());
        boolean isCoord = usuariosService.hasPerfil("COORDINADOR", user.getId());
        return asignaturasService.getAsignaturasSearch(user.getId(), isAdmin, isCoord, false,query);
    }

    @GET
    @Path("coord/search")
    @Produces(MediaType.APPLICATION_JSON)
    public List<LookupItem> getAsignaturasCoordinadasSearch(@QueryParam("query") String query)
    {
        User user = AccessManager.getConnectedUser(request);
        boolean isAdmin = usuariosService.hasPerfil("ADMIN", user.getId());
        boolean isCoord = usuariosService.hasPerfil("COORDINADOR", user.getId());
        return asignaturasService.getAsignaturasSearch(user.getId(), isAdmin, isCoord, true,query);
    }


    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResultadoOperacion updateConfig(Asignatura asignatura)
    {
        long resultado = asignaturasService.createOrUpdateConfigAsignatura(asignatura);
        if (resultado == 0)
        {
            return new ResultadoOperacion(false, "SinCambios");
        }
        return new ResultadoOperacion(true, asignatura);
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("coord")
    public ResultadoOperacion updateConfigCoord(Asignatura asignatura)
    {
        long resultado = asignaturasService.createOrUpdateConfigAsignatura(asignatura);
        if (resultado == 0)
        {
            return new ResultadoOperacion(false, "SinCambios");
        }
        return new ResultadoOperacion(true, new Asignatura(asignaturasService.getAsignaturaById(asignatura.getCodigo())));
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("coord/{codigo}")
    public ResultadoOperacion updateConfigCoordConfig(Asignatura asignatura) {
        long resultado = asignaturasService.createOrUpdateConfigAsignatura(asignatura);
        if (resultado == 0) {
            return new ResultadoOperacion(false, "SinCambios");
        }
        return new ResultadoOperacion(true, new Asignatura(asignaturasService.getAsignaturaById(asignatura.getCodigo())));
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("anyos")
    public List<Map<String, Integer>> anyosConAsignaturas() {
        User user = AccessManager.getConnectedUser(request);
        return asignaturasService.anyosConAsignaturas(user.getId());

    }

    @GET
    @Path("combo")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getAsignaturasCombo(@QueryParam("cursoAca") Long cursoAca) {
        ResponseMessage responseMessage = new ResponseMessage(true);
        User user = AccessManager.getConnectedUser(request);
        boolean isAdmin = usuariosService.hasPerfil("ADMIN", user.getId());
        boolean isCoord = usuariosService.hasPerfil("COORDINADOR", user.getId());
        try {
            responseMessage.setData(UIEntity.toUI(asignaturasService.getAsignaturasCombo(user.getId(), isAdmin, isCoord, true, cursoAca)));
            responseMessage.setSuccess(true);
        } catch (Exception e) {
            responseMessage.setSuccess(false);
        }
        return responseMessage;
    }
}
