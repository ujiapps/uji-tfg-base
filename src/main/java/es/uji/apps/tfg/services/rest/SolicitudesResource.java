package es.uji.apps.tfg.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.tfg.config.Configuration;
import es.uji.apps.tfg.dto.PersonasPropuestaDTO;
import es.uji.apps.tfg.dto.PropuestaDTO;
import es.uji.apps.tfg.dto.TurnoDTO;
import es.uji.apps.tfg.excepciones.AsignaturaNoConfiguradaException;
import es.uji.apps.tfg.excepciones.PropuestaException;
import es.uji.apps.tfg.i18n.ResourceProperties;
import es.uji.apps.tfg.model.Asignatura;
import es.uji.apps.tfg.model.Propuesta;
import es.uji.apps.tfg.model.ResultadoOperacion;
import es.uji.apps.tfg.model.Solicitud;
import es.uji.apps.tfg.services.AsignaturasService;
import es.uji.apps.tfg.services.NotificacionesService;
import es.uji.apps.tfg.services.PropuestasService;
import es.uji.apps.tfg.services.UsuariosService;
import es.uji.apps.tfg.utils.DateUtils;
import es.uji.commons.sso.AccessManager;
import es.uji.commons.sso.User;
import es.uji.commons.web.template.HTMLTemplate;
import es.uji.commons.web.template.Template;
import es.uji.commons.web.template.model.Pagina;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

@Path("solicitudes")
public class SolicitudesResource extends BaseResource
{

    public final static Logger log = LoggerFactory.getLogger(SolicitudesResource.class);

    @InjectParam
    private PropuestasService propuestasService;
    @InjectParam
    private AsignaturasService asignaturasService;
    @InjectParam
    private UsuariosService usuariosService;
    @InjectParam
    private NotificacionesService notificacionesService;

    @Context
    private HttpServletRequest request;

    final private String PLANTILLAS_DIR = "tfg/";
    final private String APP = "tfg";
    protected static final String LANG = "language";

    private boolean estaMatriculado(Long persona_id, String codigo_asignatura)
    {
        boolean esta_matriculado = false;
        for (Asignatura asig : getAsignaturasByEstudiante(persona_id))
        {
            if (codigo_asignatura.compareTo(asig.getCodigo()) == 0)
            {
                esta_matriculado = true;
                break;
            }
        }
        return esta_matriculado;
    }

    private List<Solicitud> getPropuestasByAsignatura(Long persona_id, String codigo_asignatura)
    {
        List<Solicitud> propuestaAlu = new ArrayList<Solicitud>();

        if (!estaMatriculado(persona_id, codigo_asignatura))
        {
            return propuestaAlu;
        }
        propuestaAlu = Solicitud.transformarListaDTO(propuestasService.getPropuestasByAsignatura(codigo_asignatura));
        return propuestaAlu;
    }

    private List<Asignatura> getAsignaturasByEstudiante(Long persona_id)
    {
        return asignaturasService.getAsignaturasByEstudiante(persona_id);
    }

    @POST
    @Path("solicitud/{propuesta_id}/remove")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResultadoOperacion removeInscripcion(@CookieParam("uji-lang") @DefaultValue("ca") String idioma, @PathParam("propuesta_id") long propuesta_id)
    {
        User user = AccessManager.getConnectedUser(request);
        PropuestaDTO propuesta = propuestasService.getPropuestaById(propuesta_id);

        List<Propuesta> propuestasInscritas = propuestasService.getPropuestasAsignadasByAsignatura(user.getId(), false, false, propuesta
                .getTfgExtAsignatura().getCodigo());
        if (propuestasInscritas == null || propuestasInscritas.size() == 0)
        {
            return new ResultadoOperacion(false, ResourceProperties.getMessage(new Locale(idioma), "tfg.message.error.noPreguntasInscritas"));
        }
        if (!estaMatriculado(user.getId(), propuesta.getTfgExtAsignatura().getCodigo()))
        {
            return new ResultadoOperacion(false, ResourceProperties.getMessage(new Locale(idioma), "tfg.message.error.noMatriculado"));
        }

        PersonasPropuestaDTO inscripcion = propuestasService.getInscripcionDePersonaEnAsignatura((long) user.getId(), propuesta_id);
        boolean estaInscrito = false;
        boolean enEspera = false;
        if (inscripcion.getAceptada().intValue() == 0)
        {
            enEspera = true;
        }
        else
        {
            enEspera = false;
        }
        for (Propuesta inscrita : propuestasInscritas) {
            if (inscrita.getId() == propuesta_id) {
                estaInscrito = true;
                break;
            }
        }
        if (!estaInscrito)
        {
            return new ResultadoOperacion(true, ResourceProperties.getMessage(new Locale(idioma), "tfg.message.error.noEstaInscrito"));
        }
        try
        {
            propuestasService.removeSolicitud(user.getId(), propuesta_id);
            // Si se ha eliminado correctamente intentamos repartir su plaza en caso de no estar
            // únicamente en espera
            if (!enEspera)
            {
                String errores = "";
                errores = propuestasService.redistribuirPlazas(propuesta_id);
                if (errores.compareTo("") != 0)
                {
                    throw new MessagingException(errores);
                }
            }
        }
        catch (MessagingException e)
        {
            //Se elimina las notificaciones a coordinadores
//            notificacionesService.notificarErrorACoordinadores(propuesta.getTfgExtAsignatura().getCodigo(), e.getMessage(),
//                    NotificacionesService.getNotificadorPorDefecto());
            log.error(e.getMessage(),e);
            return new ResultadoOperacion(true, "");
        }
        catch (Exception e)
        {
            return new ResultadoOperacion(false, e.getMessage());
        }
        return new ResultadoOperacion(true, "");
    }

    @PUT
    @Path("solicitud/{propuesta_id}")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public ResultadoOperacion inscripcionEstudiante(@CookieParam("uji-lang") @DefaultValue("ca") String idioma, @PathParam("propuesta_id") long propuesta_id, @FormParam("comentarios") String comentarios)
    {
        User user = AccessManager.getConnectedUser(request);
        PropuestaDTO propuestaDTO = propuestasService.getPropuestaById(propuesta_id);
        Propuesta propuesta = new Propuesta(propuestaDTO, true);
        boolean notificar = (propuesta).getDisponibilidad() == 0;
        if (propuestaDTO == null)
        {
            return new ResultadoOperacion(false, ResourceProperties.getMessage(new Locale(idioma), "tfg.message.error.errorPropuesta"));
        }
        if (!estaMatriculado(user.getId(), propuestaDTO.getTfgExtAsignatura().getCodigo()))
        {
            return new ResultadoOperacion(false, ResourceProperties.getMessage(new Locale(idioma), "tfg.message.error.noMatriculado"));
        }
        try
        {
            propuestasService.comprobarDisponibilidadDePropuesta(propuestaDTO,
            propuestasService.getAsignacionesDePersonaByAsignatura(user.getId(), propuestaDTO.getTfgExtAsignatura().getCodigo()));
            propuestasService.addSolicitud(propuestaDTO, user.getId(), propuestaDTO.getTfgExtAsignatura().getCodigo(), comentarios);
        }
        /* No debe suceder en la parte pública
        catch (AsignaturaNoConfiguradaException ance) {
            return new ResultadoOperacion(false, ResourceProperties.getMessage(new Locale(idioma), ance.getMessage()));
        }
        */
        catch (PropuestaException e)
        {
            return new ResultadoOperacion(false, ResourceProperties.getMessage(new Locale(idioma), e.getMessage()));
        }
        catch (Exception e)
        {
            return new ResultadoOperacion(false, e.getMessage());
        }
        if (notificar) {            
            try {
                propuestasService.notificarAsignacion(propuestaDTO.getTfgExtAsignatura(), propuestaDTO, usuariosService.getUsuario(user.getId()),
                        propuestaDTO.getPropuestaTutores(), comentarios);
            }
            catch (Exception e)
            {
                //Se elimina las notificaciones a coordinadores
//                notificacionesService.notificarErrorACoordinadores(propuestaDTO.getTfgExtAsignatura().getCodigo(), e.getMessage(),
//                        NotificacionesService.getNotificadorPorDefecto());
                log.error(e.getMessage(),e);

            }
        }
        return new ResultadoOperacion(true, null);
    }

    @GET
    @Produces(MediaType.TEXT_HTML)
    @Path("inicio")
    public Template getInicio(@CookieParam("uji-lang") @DefaultValue("ca") String idioma) throws ParseException, IOException
    {
        User user = AccessManager.getConnectedUser(request);

        List<Asignatura> asignaturas = getAsignaturasByEstudiante(user.getId());
        HTMLTemplate template = new HTMLTemplate(PLANTILLAS_DIR + "inicio", new Locale(idioma), APP);
        String url = request.getRequestURL().toString();

        Pagina pagina = null;
        try
        {
            pagina = buildPublicPageInfo(getBaseUrlPublic(), url, idioma);
        }
        catch (Exception e)
        {
            throw e;
        }

        template.put("server", "https://ujiapps.uji.es");
        template.put("baseUrl", getBaseUrlPublic());
        template.put("urlBase", "/tfg");
        template.put("asignaturas", asignaturas);
        template.put("user_id", user.getId());
        template.put("propuestas", new ArrayList<Solicitud>());
        template.put("idioma", idioma);
        template.put("pagina", pagina);
        template.put("millis", DateUtils.getCurrent().getTime());

        template.process();
        return template;
    }

    @GET
    @Produces(MediaType.TEXT_HTML)
    @Path("inicio/{codigoAsignatura}/filter")
    public Template getInicioByAsignaturaFilter(@CookieParam("uji-lang") @DefaultValue("ca") String idioma,
            @PathParam("codigoAsignatura") String codigo_asignatura) throws ParseException, AsignaturaNoConfiguradaException {
        User user = AccessManager.getConnectedUser(request);
        List<Solicitud> propuestaAlu = new ArrayList<Solicitud>();

        String mensajeEstadoTurno = null;
        boolean activa = false;
        HTMLTemplate template = new HTMLTemplate(PLANTILLAS_DIR + "listaPropuestasNew", new Locale(idioma), APP);
        String url = request.getRequestURL().toString();

        if (!codigo_asignatura.equals("0")) {
            propuestaAlu = getPropuestasByAsignatura(user.getId(), codigo_asignatura);

            Integer plazo_matricula = new Integer(Configuration.getPlazoSolicitud());
            TurnoDTO turnoDTO = usuariosService.getTurno(user.getId(), codigo_asignatura);
            template.put("espera_habilitada", asignaturasService.isEsperaHabilitada(codigo_asignatura));

            if (turnoDTO != null) {
                if (turnoDTO.getFecha() != null) {
                    Timestamp finplazo = DateUtils.getFechaFinTurno(turnoDTO.getFecha(), plazo_matricula);
                    activa = DateUtils.tieneTurnoHabilitado(turnoDTO.getFecha(), finplazo);
                    if (!activa)
                        mensajeEstadoTurno = DateUtils.getMensajeEstadoTurno(idioma, turnoDTO.getFecha(), finplazo);
                }
            }

            template.put("inscripciones", propuestasService.getMensajeEstadoPropuestas(idioma, codigo_asignatura, user.getId()));
            template.put("fechaTurno", mensajeEstadoTurno);
            template.put("activa", activa);


            HashMap<Long, String> comentarios = new HashMap<>();
            HashMap<Long, Boolean> estaInscritoList = new HashMap<>();

            propuestaAlu.forEach(solicitud -> {
                Propuesta propuesta = new Propuesta(propuestasService.getPropuestaById(solicitud.getId()), false);
                String comentarios_usuario = propuesta.getComentario(user.getId());

                if (comentarios_usuario == null) comentarios_usuario = "";
                comentarios.put(solicitud.getId(), comentarios_usuario);

                boolean estaInscrito = propuestasService.getInscripcionDePersonaEnAsignatura(user.getId(), solicitud.getId()) != null;
                estaInscritoList.put(solicitud.getId(), estaInscrito);
            });

            template.put("estaInscrito", estaInscritoList);
            template.put("comentarios", comentarios);
        }

        Pagina pagina = null;
        try {
            pagina = buildPublicPageInfo(getBaseUrlPublic(), url, idioma);
        } catch (Exception e) {
            throw e;
        }

        template.put("server", "https://ujiapps.uji.es");
        template.put("baseUrl", getBaseUrlPublic());
        template.put("urlBase", "/tfg");
        template.put("asignatura_filter_id", codigo_asignatura);
        template.put("user_id", user.getId());
        template.put("millis", DateUtils.getCurrent().getTime());
        template.put("idioma", idioma);
        template.put("pagina", pagina);
        template.put("propuestas", propuestaAlu);

        template.process();

        return template;
    }


    @GET
    @Produces(MediaType.TEXT_HTML)
    @Path("detalle/{propuesta_id}")
    public Template getDetallePropuesta(@CookieParam("uji-lang") @DefaultValue("ca") String idioma, @PathParam("propuesta_id") long propuesta_id)
            throws ParseException, IOException
    {
        Propuesta propuesta = new Propuesta(propuestasService.getPropuestaById(propuesta_id), false);
        HTMLTemplate template = new HTMLTemplate(PLANTILLAS_DIR + "detallePropuesta", new Locale(idioma), APP);
        User user = AccessManager.getConnectedUser(request);
        boolean estaInscrito = propuestasService.getInscripcionDePersonaEnAsignatura(user.getId(), propuesta_id) != null;

        String url = request.getRequestURL().toString();
        Pagina pagina = null;
        try
        {
            pagina = buildPublicPageInfo(getBaseUrlPublic(), url, idioma);
        }
        catch (Exception e)
        {
            throw e;
        }
        String comentarios_usuario = propuesta.getComentario(user.getId());
        if (comentarios_usuario == null)
        {
            comentarios_usuario = "";
        }
        template.put("estaInscrito", estaInscrito);
        template.put("comentarios", comentarios_usuario);
        template.put("propuesta", propuesta);
        template.put("idioma", idioma);
        template.put("server", "https://ujiapps.uji.es");
        template.put("baseUrl", getBaseUrlPublic());
        template.put("urlBase", "/tfg");

        template.put("pagina", pagina);

        template.process();
        return template;
    }

}
