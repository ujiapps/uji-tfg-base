package es.uji.apps.tfg.services;

import es.uji.apps.tfg.dao.AccesosSPIDAO;
import es.uji.apps.tfg.dto.AdministrativosDTO;
import es.uji.apps.tfg.model.Administrativo;
import es.uji.commons.rest.json.lookup.LookupItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class AccesosSPIService {

    AccesosSPIDAO accesosSPIDAO;

    @Autowired
    public AccesosSPIService(AccesosSPIDAO accesosSPIDAO) {
        this.accesosSPIDAO = accesosSPIDAO;
    }

    public List<Administrativo> getAccesosSPI(String codigoAsignatura) {
        return accesosSPIDAO.getAccesosSPI(codigoAsignatura);
    }

    public List<LookupItem> searchAdministrativos(String query) {
        return accesosSPIDAO.searchAdministrativos(query);
    }

    public AdministrativosDTO addAdministrativo(Administrativo administrativo) {
        AdministrativosDTO administrativosDTO = new AdministrativosDTO(administrativo);
        return accesosSPIDAO.insert(administrativosDTO);
    }

    @Transactional
    public Long deleteAdministrativo(Long id) {
        return accesosSPIDAO.deleteAdministrativo(id);
    }
}
