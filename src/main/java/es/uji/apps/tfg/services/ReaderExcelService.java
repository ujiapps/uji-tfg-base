package es.uji.apps.tfg.services;


import es.uji.apps.tfg.model.PropuestaAlumno;
import es.uji.commons.rest.ParamUtils;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;

public class ReaderExcelService {

    public final static Logger log = LoggerFactory.getLogger(ReaderExcelService.class);

    private HSSFWorkbook workbook;

    public ReaderExcelService(InputStream datos) {
        try {
            this.workbook = new HSSFWorkbook(datos);
        } catch (IOException e) {
            log.error("Error en fichero xls", e);
        }
    }

    public Map<String, List<PropuestaAlumno>> extract() throws IOException {
        List<PropuestaAlumno> propuestaAlumnos = new ArrayList<>();

        HSSFSheet sheet = this.workbook.getSheetAt(0);

        Iterator<Row> rowIterator = sheet.iterator();
        Row codigoAsignaturaRow = rowIterator.next();
        String codigoAsignatura = "";
        if (codigoAsignaturaRow.getCell(1) != null) {
            codigoAsignatura = codigoAsignaturaRow.getCell(1).getStringCellValue();
        } else {
            return null;
        }
        rowIterator.next();
        while (rowIterator.hasNext()) {
            Row row = rowIterator.next();
            if (row.getCell(0) != null && row.getCell(0).getCellType() != CellType.BLANK && !row.getCell(0).getStringCellValue().trim().isEmpty()) {
                String titulo = "", tutorEmail = "", alumnoEmail = "";
                if (!isCellEmpty(row.getCell(0))) {
                    titulo = row.getCell(0).getStringCellValue();
                }
                if (!isCellEmpty(row.getCell(1))) {
                    tutorEmail = row.getCell(1).getStringCellValue();
                }
                if (!isCellEmpty(row.getCell(2))) {
                    alumnoEmail = row.getCell(2).getStringCellValue();
                }
                if (ParamUtils.isNotNull(titulo, tutorEmail, alumnoEmail)) {
                    PropuestaAlumno propuestaAlumno = new PropuestaAlumno(titulo, tutorEmail.trim(), alumnoEmail.trim());
                    propuestaAlumnos.add(propuestaAlumno);
                }
            }
        }
        return Collections.singletonMap(codigoAsignatura, propuestaAlumnos);
    }

    private boolean isCellEmpty(Cell cell) {
        return cell.getStringCellValue().isEmpty() || cell.getStringCellValue().trim().isEmpty();
    }


}
