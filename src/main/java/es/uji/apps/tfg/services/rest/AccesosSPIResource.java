package es.uji.apps.tfg.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.tfg.ResponseMessage;
import es.uji.apps.tfg.excepciones.AccesoSPIException;
import es.uji.apps.tfg.model.Administrativo;
import es.uji.apps.tfg.services.AccesosSPIService;
import es.uji.apps.tfg.services.UsuariosService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.json.lookup.LookupResult;
import es.uji.commons.sso.AccessManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;

@Path("acceso-spi")
public class AccesosSPIResource extends CoreBaseService {

    public final static Logger log = LoggerFactory.getLogger(AccesosSPIResource.class);

    @InjectParam
    UsuariosService usuariosService;

    @InjectParam
    AccesosSPIService accesosSPIService;

    @GET
    @Path("{codigoAsignatura}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getAccesosSPI(@PathParam("codigoAsignatura") String codigoAsignatura) {
        Long userId = AccessManager.getConnectedUserId(request);
        boolean hasPerfiles = usuariosService.hasPerfiles(new String[]{"ADMIN", "COORDINADOR"}, userId);
        if (hasPerfiles || (usuariosService.coordinaAsignatura(userId, codigoAsignatura))) {
            return UIEntity.toUI(accesosSPIService.getAccesosSPI(codigoAsignatura));
        }
        return new ArrayList<>();
    }

    @GET
    @Path("administrativos")
    @Produces(MediaType.APPLICATION_JSON)
    public LookupResult searchAdministrativos(@QueryParam("query") String query) {
        LookupResult result = new LookupResult();
        result.setData(accesosSPIService.searchAdministrativos(query));
        return result;
    }

    @POST
    @Path("{codigoAsignatura}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity addAdministrativo(@PathParam("codigoAsignatura") String codigoAsignatura, UIEntity uiEntity) throws AccesoSPIException {
        Long userId = AccessManager.getConnectedUserId(request);
        Administrativo administrativo = new Administrativo(uiEntity.getLong("personaId"), codigoAsignatura);
        boolean hasPerfiles = usuariosService.hasPerfiles(new String[]{"ADMIN", "COORDINADOR"}, userId);
        if (hasPerfiles || (usuariosService.coordinaAsignatura(userId, codigoAsignatura))) {
            return UIEntity.toUI(accesosSPIService.addAdministrativo(administrativo));
        } else {
            throw new AccesoSPIException("Acceso no permitido");
        }
    }


    @DELETE
    @Path("{codigoAsignatura}/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public ResponseMessage deleteAdministrativo(@PathParam("codigoAsignatura") String codigoAsignatura, @PathParam("id") Long id) {
        Long userId = AccessManager.getConnectedUserId(request);
        boolean hasPerfiles = usuariosService.hasPerfiles(new String[]{"ADMIN", "COORDINADOR"}, userId);
        if (hasPerfiles || (usuariosService.coordinaAsignatura(userId, codigoAsignatura))) {
            accesosSPIService.deleteAdministrativo(id);
            return new ResponseMessage(true, "Convocatoria borrada");
        }
        return new ResponseMessage(false, "Error al esborrar convocatoria");
    }

}
