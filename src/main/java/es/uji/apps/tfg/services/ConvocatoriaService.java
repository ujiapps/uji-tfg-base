package es.uji.apps.tfg.services;

import es.uji.apps.tfg.dao.ConvocatoriaDAO;
import es.uji.apps.tfg.model.Convocatoria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ConvocatoriaService {

    ConvocatoriaDAO convocatoriaDAO;

    @Autowired
    public ConvocatoriaService(ConvocatoriaDAO convocatoriaDAO) {
        this.convocatoriaDAO = convocatoriaDAO;
    }

    public List<Convocatoria> getConvocatoriasFechas(String codigoAsignatura) {
        return Convocatoria.transformaListaDTO(convocatoriaDAO.getConvocatoriasFechas(codigoAsignatura));
    }

    public Convocatoria createOrUpdateConvocatoria(Convocatoria convocatoria) {
        return convocatoriaDAO.createOrUpdateConvocatoria(convocatoria);
    }

    public void deleteConvocatoria(Convocatoria convocatoria) {
        convocatoriaDAO.deleteConvocatoria(convocatoria.getId());
    }
}
