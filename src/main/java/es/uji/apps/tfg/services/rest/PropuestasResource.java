package es.uji.apps.tfg.services.rest;

import com.sun.jersey.api.core.InjectParam;
import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataParam;
import es.uji.apps.tfg.ResponseMessage;
import es.uji.apps.tfg.dto.AsignaturaDTO;
import es.uji.apps.tfg.dto.PersonaDTO;
import es.uji.apps.tfg.dto.PropuestaDTO;
import es.uji.apps.tfg.excepciones.AsignaturaNoConfiguradaException;
import es.uji.apps.tfg.excepciones.PropuestaException;
import es.uji.apps.tfg.i18n.ResourceProperties;
import es.uji.apps.tfg.model.Asignacion;
import es.uji.apps.tfg.model.Propuesta;
import es.uji.apps.tfg.model.PropuestaAlumno;
import es.uji.apps.tfg.model.ResultadoOperacion;
import es.uji.apps.tfg.services.AsignaturasService;
import es.uji.apps.tfg.services.NotificacionesService;
import es.uji.apps.tfg.services.PropuestasService;
import es.uji.apps.tfg.services.UsuariosService;
import es.uji.apps.tfg.utils.DateUtils;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.sso.AccessManager;
import org.apache.batik.transcoder.TranscoderException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Path("propuestas")
public class PropuestasResource extends CoreBaseService {

    public final static Logger log = LoggerFactory.getLogger(PropuestasResource.class);

    @InjectParam
    private PropuestasService propuestasService;
    @InjectParam
    private UsuariosService usuariosService;
    @InjectParam
    private AsignaturasService asignaturasService;
    @InjectParam
    private NotificacionesService notificacionesService;


    @Path("cambiotutores")
    public CambioTutoresResource getPlatformItem(
            @InjectParam CambioTutoresResource cambioTutoresResource) {
        return cambioTutoresResource;
    }

    @GET
    @Path("listado/{codigoAsignatura}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Propuesta> getPropuestas(@PathParam("codigoAsignatura") String codigo_asignatura) {
        Long userId = AccessManager.getConnectedUserId(request);
        boolean isAdmin = usuariosService.hasPerfil("ADMIN", userId);
        boolean isCoord = usuariosService.hasPerfil("COORDINADOR", userId);
        if (isAdmin || (isCoord && usuariosService.coordinaAsignatura(userId, codigo_asignatura))) {
            return propuestasService.getPropuestasAsignadasByAsignatura(userId, isAdmin, isCoord, codigo_asignatura);
        } else {
            return propuestasService.getPropuestasDePersonaByAsignatura(userId, codigo_asignatura);
        }
    }

    @POST
    @Path("duplicar/{codigoAsignatura}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getDuplicarPropuestas(@PathParam("codigoAsignatura") String codigo_asignatura) {
        Long userId = AccessManager.getConnectedUserId(request);
        boolean isAdmin = usuariosService.hasPerfil("ADMIN", userId);
        boolean isCoord = usuariosService.hasPerfil("COORDINADOR", userId);

        List<PropuestaDTO> listado = new ArrayList<PropuestaDTO>();
        if (isAdmin || (isCoord && usuariosService.coordinaAsignatura(userId, codigo_asignatura))) {
            listado = propuestasService.getPropuestasADuplicar(codigo_asignatura);
        } else {
            ResultadoOperacion resultado = new ResultadoOperacion(false, "NoEsPropietario");
            return Response.ok(resultado).build();
        }

        try {
            propuestasService.duplicarPropuestas(userId, listado);
            ResultadoOperacion resultado = new ResultadoOperacion(true, listado.size());
            return Response.ok(resultado).build();
        } catch (Exception ance) {
            return Response.status(500).build();
        }
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response createPropuesta(@FormParam("id") int id, @FormParam("nombre") String name, @FormParam("tutores") List<Long> tutores,
                                    @FormParam("codigo_asignatura") String codigo_asignatura, @FormParam("objetivos") String objetivos, @FormParam("bibliografia") String bibliografia,
                                    @FormParam("descripcion") String descripcion, @FormParam("observaciones") String observaciones, @FormParam("plazas") int plazas) throws Exception {
        try {
            Date fecha_limite = asignaturasService.getFechaLimite(codigo_asignatura);
            Long userId = AccessManager.getConnectedUserId(request);
            boolean isAdmin = usuariosService.hasPerfil("ADMIN", userId);
            boolean isCoord = usuariosService.hasPerfil("COORDINADOR", userId);
            if (!ParamUtils.isNotNull(tutores) || tutores.size() == 0) {
                ResultadoOperacion resultado = new ResultadoOperacion(false, "AsginaturaIdIncorrecto");
                return Response.ok(resultado).build();
            }
            if (!isAdmin && !isCoord && fecha_limite != null && fecha_limite.compareTo(DateUtils.getCurrent()) < 0) {
                ResultadoOperacion resultado = new ResultadoOperacion(false, "AsignaturaOrigenExpirada");
                return Response.ok(resultado).build();
            }
            BigDecimal BDplazas = new BigDecimal(plazas);
            if (id == 0 && name.length() > 0 && plazas > 0 && codigo_asignatura.length() > 0) {
                long result = propuestasService.createPropuesta(name, tutores, codigo_asignatura, objetivos, bibliografia, descripcion, observaciones, BDplazas,
                        BDplazas, userId);
                ResultadoOperacion resultado = new ResultadoOperacion(true, result);
                return Response.ok(resultado).build();
            }
        } catch (AsignaturaNoConfiguradaException ance) {
            throw new Exception(ResourceProperties.getMessage(request.getLocale(), ance.getMessage()));
        }
        return Response.status(500).build();
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Propuesta getPropuesta(@PathParam("id") long id) {

        PropuestaDTO response = propuestasService.getPropuestaById(id);
        if (response != null) {
            return new Propuesta(response, false);
        }
        return null;
    }

    @GET
    @Path("importar-practicas/{codigoAsignatura}")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage importarPropuestas(@PathParam("codigoAsignatura") String codigoAsignatura) throws PropuestaException {

        Long connectedUserId = AccessManager.getConnectedUserId(request);
        Integer propuestasCreadas = propuestasService.importarPropuestas(connectedUserId, codigoAsignatura);
        return new ResponseMessage(true, propuestasCreadas + "  Proprostes importades");
    }

    @DELETE
    @Path("remove/{codigoAsignatura}/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deletePropuestaRemove(@PathParam("codigoAsignatura") String codigo_asignatura, @PathParam("id") int id) throws Exception {
        return deletePropuesta(codigo_asignatura, id);
    }

    @DELETE
    @Path("/{codigoAsignatura}/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deletePropuesta(@PathParam("codigoAsignatura") String codigo_asignatura, @PathParam("id") int id) throws Exception {
        try {
            Date fecha_limite = asignaturasService.getFechaLimite(codigo_asignatura);
            PropuestaDTO propuestaDTO = propuestasService.getPropuestaById(id);
            Long userId = AccessManager.getConnectedUserId(request);
            boolean isAdmin = usuariosService.hasPerfil("ADMIN", userId);
            boolean isCoord = usuariosService.hasPerfil("COORDINADOR", userId);

            if (propuestaDTO.getTfgExtAsignatura().getCodigo().compareTo(codigo_asignatura) != 0) {
                ResultadoOperacion resultado = new ResultadoOperacion(false, "AsginaturaIdIncorrecto");
                return Response.ok(resultado).build();
            }

            if (!isAdmin && !isCoord && fecha_limite != null && fecha_limite.compareTo(DateUtils.getCurrent()) < 0) {
                ResultadoOperacion resultado = new ResultadoOperacion(false, "AsignaturaOrigenExpirada");
                return Response.ok(resultado).build();
            }
            if (!isAdmin && !(isCoord && usuariosService.coordinaAsignatura(userId, codigo_asignatura)) && !propuestaDTO.compruebaTutor(userId)) {
                ResultadoOperacion resultado = new ResultadoOperacion(false, "NoEsPropietario");
                return Response.ok(resultado).build();
            }

            boolean result = propuestasService.deletePropuesta(id);
            ResultadoOperacion resultado = new ResultadoOperacion(true, result);
            return Response.ok(resultado).build();
        } catch (AsignaturaNoConfiguradaException ance) {
            throw new Exception(ResourceProperties.getMessage(request.getLocale(), ance.getMessage()));
        }
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response updatePropuesta(@PathParam("id") long id, @FormParam("id") long formId, @FormParam("nombre") String name,
                                    @FormParam("tutores") List<Long> tutores, @FormParam("codigo_asignatura") String codigo_asignatura, @FormParam("objetivos") String objetivos,
                                    @FormParam("bibliografia") String bibliografia, @FormParam("descripcion") String descripcion, @FormParam("observaciones") String observaciones,
                                    @FormParam("plazas") int plazas) throws Exception {
        if (id != formId || name.length() == 0 || plazas < 1 || codigo_asignatura.length() == 0 || codigo_asignatura.length() == 0 || !ParamUtils.isNotNull(tutores)) {
            ResultadoOperacion resultado = new ResultadoOperacion(false, "AsignaturaOrigenExpirada");
            return Response.ok(resultado).build();
        }
        Long userId = AccessManager.getConnectedUserId(request);
        boolean isAdmin = usuariosService.hasPerfil("ADMIN", userId);
        boolean isCoord = usuariosService.hasPerfil("COORDINADOR", userId);

        try {
            PropuestaDTO propuestaDTO = propuestasService.getPropuestaById(id);
            if (codigo_asignatura.compareTo(propuestaDTO.getTfgExtAsignatura().getCodigo()) == 0) {
                Date fecha_limite_origen = asignaturasService.getFechaLimite(propuestaDTO.getTfgExtAsignatura().getCodigo());

                if (!isAdmin && !isCoord && fecha_limite_origen != null && fecha_limite_origen.compareTo(DateUtils.getCurrent()) < 0) {
                    ResultadoOperacion resultado = new ResultadoOperacion(false, "AsignaturaOrigenExpirada");
                    return Response.ok(resultado).build();
                }
            }

            Date fecha_limite = asignaturasService.getFechaLimite(codigo_asignatura);
            if (!isAdmin && !isCoord && fecha_limite != null && fecha_limite.compareTo(DateUtils.getCurrent()) < 0) {
                ResultadoOperacion resultado = new ResultadoOperacion(false, "AsignaturaDestinoExpirada");
                return Response.ok(resultado).build();
            }

            if (!isAdmin
                    && !(isCoord && usuariosService.coordinaAsignatura(userId, codigo_asignatura))
                    && propuestaDTO.getPropuestaTutores() != null && !propuestaDTO.compruebaTutor(userId)) {
                ResultadoOperacion resultado = new ResultadoOperacion(false, "NoEsPropietario");
                return Response.ok(resultado).build();
            }
            BigDecimal BDplazas = new BigDecimal(plazas);
            propuestasService.updatePropuesta(id, name, tutores, codigo_asignatura, objetivos, bibliografia, descripcion, observaciones, BDplazas, BDplazas);
            PropuestaDTO result = propuestasService.getPropuestaById(id);
            ResultadoOperacion resultado = new ResultadoOperacion(true, new Propuesta(result, false));
            return Response.ok(resultado).build();
        } catch (AsignaturaNoConfiguradaException ance) {
            throw new Exception(ResourceProperties.getMessage(request.getLocale(), ance.getMessage()));
        }
    }

    @GET
    @Path("asignaciones/{codigoAsignatura}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Asignacion> getAsignacionesByAsignatura(@PathParam("codigoAsignatura") String codigo_asignatura) {
        return propuestasService.getAsignacionesByAsignatura(codigo_asignatura, -1L);
    }

    @GET
    @Path("asignacionesprofesor/{codigoAsignatura}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Asignacion> getAsignacionesDeProfesorByAsignatura(@PathParam("codigoAsignatura") String codigo_asignatura) {
        Long userId = AccessManager.getConnectedUserId(request);
        return propuestasService.getAsignacionesByAsignatura(codigo_asignatura, userId);
    }

    // @GET
    // @Path("asignaciones/{codigoAsignatura}/{personaId}/{propuestaId}")
    // @Produces(MediaType.APPLICATION_JSON)
    // public ResultadoOperacion getAsignacionByAsignatura(@PathParam("codigoAsignatura") String
    // codigo_asignatura, @PathParam("personaId") Long persona_id,
    // @PathParam("propuestaId") Long propuesta_id)
    // {
    // Long userId = AccessManager.getConnectedUserId(request);
    // boolean isAdmin = usuariosService.hasPerfil("ADMIN", userId);
    //
    // if (!isAdmin && !usuariosService.coordinaAsignatura(userId, codigo_asignatura) &&
    // !usuariosService.profesorDeAsignatura(userId, codigo_asignatura))
    // {
    // return new ResultadoOperacion(false, null);
    // }
    // PersonasPropuestaDTO asignacion = usuariosService.getAsignacion(persona_id, propuesta_id);
    // return new ResultadoOperacion(true, new Asignacion(asignacion,
    // asignaturasService.getAsignaturaById(codigo_asignatura)));
    // }

    @PUT
    @Path("asignaciones/{codigoAsignatura}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResultadoOperacion updateAsignaciones(@PathParam("codigoAsignatura") String codigo_asignatura, Asignacion asignacion) {
        return getResultadoOperacion(codigo_asignatura, asignacion);
    }

    private ResultadoOperacion getResultadoOperacion(String codigo_asignatura, Asignacion asignacion) {
        Long userId = AccessManager.getConnectedUserId(request);
        boolean isAdmin = usuariosService.hasPerfil("ADMIN", userId);
        boolean isCoord = usuariosService.hasPerfil("COORDINADOR", userId);

        if (!isAdmin && !(isCoord && usuariosService.coordinaAsignatura(userId, codigo_asignatura))) {
            return new ResultadoOperacion(false, null);
        }
        try {
            propuestasService.updateAsignaciones(asignacion.getPersona_id(), codigo_asignatura, asignacion.getPropuesta_id(), userId,
                    asignacion.getComentarios(), DateUtils.getCurrent());
        } catch (AsignaturaNoConfiguradaException ance) {
            return new ResultadoOperacion(false, ResourceProperties.getMessage(request.getLocale(), ance.getMessage()));
        } catch (PropuestaException e) {
            return new ResultadoOperacion(false, ResourceProperties.getMessage(request.getLocale(), e.getMessage()));
        }
        PropuestaDTO propuesta = propuestasService.getPropuestaById(asignacion.getPropuesta_id());
        PersonaDTO usuario = usuariosService.getUsuario(userId);
        asignacion.setEditor_nombre(usuario.getNombre());
        asignacion.setEditor_apellido1(usuario.getApellido1());
        asignacion.setEditor_apellido2(usuario.getApellido2());
        asignacion.setEditor_persona_id(userId);
        asignacion.setPropuesta_nombre(propuesta.getNombre());
        asignacion.setComentarios(asignacion.getComentarios());
        PersonaDTO alumno = usuariosService.getUsuario(asignacion.getPersona_id());
        AsignaturaDTO asignaturaDTO = asignaturasService.getAsignaturaById(codigo_asignatura);
        try {
            propuestasService.notificarAsignacion(asignaturaDTO, propuesta, alumno, propuesta.getPropuestaTutores(), asignacion.getComentarios());
        } catch (Exception e) {
            //Se elimina las notificaciones a coordinadores
//            notificacionesService.notificarErrorACoordinadores(codigo_asignatura, e.getMessage(), NotificacionesService.getNotificadorPorDefecto());
            log.error(e.getMessage(), e);
        }
        return new ResultadoOperacion(true, asignacion);
    }

    //TODO añadido por compa..
    @PUT
    @Path("asignaciones/{codigoAsignatura}/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResultadoOperacion updateAsignacionesById(@PathParam("codigoAsignatura") String codigo_asignatura, Asignacion asignacion) {
        return getResultadoOperacion(codigo_asignatura, asignacion);
    }

    @POST
    @Path("asignaciones/{codigoAsignatura}")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public ResultadoOperacion updateComentariosAsignacion(@PathParam("codigoAsignatura") String codigo_asignatura, @FormParam("persona_id") Long persona_id,
                                                          @FormParam("propuesta_id") Long propuesta_id, @FormParam("comentarios") String comentarios) {
        Long userId = AccessManager.getConnectedUserId(request);
        boolean isAdmin = usuariosService.hasPerfil("ADMIN", userId);
        boolean isCoord = usuariosService.hasPerfil("COORDINADOR", userId);

        if (!isAdmin && !(isCoord && usuariosService.coordinaAsignatura(userId, codigo_asignatura))) {
            return new ResultadoOperacion(false, null);
        }
        try {
            propuestasService.updateComentariosAsignacion(persona_id, propuesta_id, comentarios, userId, DateUtils.getCurrent());
        } catch (Exception e) {
            return new ResultadoOperacion(false, ResourceProperties.getMessage(request.getLocale(), e.getMessage()));
        }

        return new ResultadoOperacion(true, "ok");
    }

    @POST
    @Path("asignaciones/activar/{codigoAsignatura}")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public ResultadoOperacion confirmarAsignacion(@PathParam("codigoAsignatura") String codigo_asignatura, @FormParam("persona_id") Long persona_id,
                                                  @FormParam("propuesta_id") Long propuesta_id, @FormParam("comentarios") String comentarios) {
        Long userId = AccessManager.getConnectedUserId(request);
        boolean isAdmin = usuariosService.hasPerfil("ADMIN", userId);
        boolean isCoord = usuariosService.hasPerfil("COORDINADOR", userId);

        if (!isAdmin && !(isCoord && usuariosService.coordinaAsignatura(userId, codigo_asignatura))) {
            return new ResultadoOperacion(false, null);
        }
        try {
            propuestasService.confirmarAsignacion(persona_id, propuesta_id, userId, DateUtils.getCurrent());
        } catch (Exception e) {
            return new ResultadoOperacion(false, ResourceProperties.getMessage(request.getLocale(), e.getMessage()));
        }

        return new ResultadoOperacion(true, "ok");
    }

    @DELETE
    @Path("asignaciones/{codigoAsignatura}/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResultadoOperacion deleteAsignacionEnEspera(@PathParam("codigoAsignatura") String codigo_asignatura, Asignacion asignacion) {
        Long userId = AccessManager.getConnectedUserId(request);
        boolean isAdmin = usuariosService.hasPerfil("ADMIN", userId);
        boolean isCoord = usuariosService.hasPerfil("COORDINADOR", userId);

        if (!isAdmin && !(isCoord && usuariosService.coordinaAsignatura(userId, codigo_asignatura))) {
            return new ResultadoOperacion(false, null);
        }
        try {
            propuestasService.deleteAsignacion(asignacion);
        } catch (PropuestaException e) {
            return new ResultadoOperacion(false, ResourceProperties.getMessage(request.getLocale(), e.getMessage()));
        } catch (Exception e2) {
            return new ResultadoOperacion(false, "");
        }

        return new ResultadoOperacion(true, "ok");
    }

    @GET
    @Path("asignaciones/excel/{codigoAsignatura}")
    @Produces("application/vnd.ms-excel")
    public Response gridToExcel(@PathParam("codigoAsignatura") String codigo_asignatura) throws TranscoderException, IOException {
        ByteArrayOutputStream ostream = propuestasService.getExcel(codigo_asignatura);
        return Response.ok(ostream.toByteArray(), MediaType.APPLICATION_OCTET_STREAM).header("content-disposition", "attachment; filename = assignacions" + codigo_asignatura + ".xls").build();
    }

    @GET
    @Path("excel/{codigoAsignatura}")
    @Produces("application/vnd.ms-excel")
    public Response propuestasToExcel(@PathParam("codigoAsignatura") String codigo_asignatura) throws TranscoderException, IOException {
        ByteArrayOutputStream ostream = propuestasService.getPropuetasExcel(codigo_asignatura);
        return Response.ok(ostream.toByteArray(), MediaType.APPLICATION_OCTET_STREAM).header("content-disposition", "attachment; filename = propuestas" + codigo_asignatura + ".xls").build();
    }


    @GET
    @Path("excel/{codigoAsignatura}/plantilla")
    @Produces("application/vnd.ms-excel")
    public Response generarPlantilla(@PathParam("codigoAsignatura") String codigo_asignatura) throws TranscoderException, IOException {
        ByteArrayOutputStream ostream = propuestasService.generarPlantilla(codigo_asignatura);
        return Response.ok(ostream.toByteArray(), MediaType.APPLICATION_OCTET_STREAM).header("content-disposition", "attachment; filename = Template - " + codigo_asignatura + ".xls").build();
    }

    @POST
    @Path("excel/{codigoAsignatura}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response anyadirPropuestasDesdeArchivo(@PathParam("codigoAsignatura") String codigo_asignatura, @FormDataParam("file") InputStream uploadedInputStream,
                                                  @FormDataParam("file") FormDataContentDisposition fileDetail) throws Exception {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        try {
            Date fecha_limite = asignaturasService.getFechaLimite(codigo_asignatura);
            Long userId = AccessManager.getConnectedUserId(request);
            Boolean perfilValido = usuariosService.hasPerfiles(new String[]{"ADMIN", "COORDINADOR"}, userId);
            if (!perfilValido && fecha_limite != null && fecha_limite.compareTo(DateUtils.getCurrent()) < 0) {
                ResultadoOperacion resultado = new ResultadoOperacion(false, "AsignaturaOrigenExpirada");
                return Response.ok(resultado).build();
            }
            List<PropuestaAlumno> resp = propuestasService.subirPropuestas(connectedUserId, codigo_asignatura, uploadedInputStream)
                    .get(codigo_asignatura).stream().filter(p -> !p.getCreada()).collect(Collectors.toList());
            return Response.ok(new ResultadoOperacion(resp.isEmpty(), resp)).build();

        } catch (AsignaturaNoConfiguradaException ance) {
            throw new Exception(ResourceProperties.getMessage(request.getLocale(), ance.getMessage()));
        }

    }

}
