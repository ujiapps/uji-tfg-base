package es.uji.apps.tfg.services;

import es.uji.apps.tfg.dao.AsignaturasDAO;
import es.uji.apps.tfg.dto.AsignaturaDTO;
import es.uji.apps.tfg.dto.AsignaturasConfigDTO;
import es.uji.apps.tfg.dto.PersonaDTO;
import es.uji.apps.tfg.excepciones.AsignaturaNoConfiguradaException;
import es.uji.apps.tfg.model.Asignatura;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.json.lookup.LookupItem;
import es.uji.commons.sso.dao.ApaDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class AsignaturasService
{
    private AsignaturasDAO asignaturasDAO;

    @Autowired
    private ApaDAO apaDAO;
    @Autowired
    public AsignaturasService(AsignaturasDAO asignaturasDAO)
    {
        this.asignaturasDAO = asignaturasDAO;
    }

    public List<Asignatura> getAsignaturasByEstudiante(Long persona_id)
    {
        List<AsignaturaDTO> listado = asignaturasDAO.getAsignaturasByEstudiante(persona_id);
        return Asignatura.transformaListaDTO(listado);
    }

    public Date getFechaLimite(String codigo_asignatura) throws AsignaturaNoConfiguradaException
    {
        AsignaturasConfigDTO config = asignaturasDAO.getAsignatura(codigo_asignatura).getTfgAsignaturasConfig();
        if (config != null)
        {
            return asignaturasDAO.getFechaLimite(codigo_asignatura);
        }
        throw new AsignaturaNoConfiguradaException();
    }

    public long setFechaLimite(String codigo_asignatura, Date fechaLimite)
    {
        return asignaturasDAO.setFechaLimite(codigo_asignatura, fechaLimite);
    }

    public long createOrUpdateConfigAsignatura(Asignatura asignatura)
    {
        if (asignaturasDAO.getAsignatura(asignatura.getCodigo()).getTfgAsignaturasConfig() == null)
        {
            AsignaturasConfigDTO nueva = asignaturasDAO.createAsignaturaConfig(asignatura.getCodigo(), asignatura.getFechaLimite(), asignatura.getOrdenacion(), asignatura.isEspera_habilitada(), asignatura.getPermitirSolicitudes(), asignatura.isNotificarCoordinador());
            if (nueva != null)
            {
                return 1L;
            }
        }
        return asignaturasDAO.updateAsignaturaConfig(asignatura.getCodigo(), asignatura.getFechaLimite(), asignatura.getOrdenacion(), asignatura.isEspera_habilitada(), asignatura.getPermitirSolicitudes(), asignatura.isNotificarCoordinador());
    }

    public List<PersonaDTO> getCoordinadoresByAsignatura(String codigo_asignatura)
    {
        return asignaturasDAO.getCoordinadoresByAsignatura(codigo_asignatura);
    }

    public boolean isEsperaHabilitada(String codigo_asignatura) throws AsignaturaNoConfiguradaException
    {
        if (getAsignaturaById(codigo_asignatura).getTfgAsignaturasConfig() != null)
        {
            return getAsignaturaById(codigo_asignatura).getTfgAsignaturasConfig().isHabilitar_espera();
        }
        else
        {
            throw new AsignaturaNoConfiguradaException();
        }
    }

    public AsignaturaDTO getAsignaturaById(String codigo_asignatura)
    {
        return asignaturasDAO.getAsignatura(codigo_asignatura);
    }

    public List<LookupItem> getAsignaturasSearch(Long id, boolean isAdmin, boolean isCoord, boolean solo_coordinadas, String query)
    {
        List<Asignatura> list = getAsignaturas(id, isAdmin, isCoord, solo_coordinadas);
        if (ParamUtils.isNotNull(query))
        {
            return list.stream().filter(asi -> asi.getCodinom().toUpperCase().contains(query.toUpperCase())).map(asignatura -> {
                        LookupItem lookupItem = new LookupItem();
                        lookupItem.setId(asignatura.getCodigo());
                        lookupItem.setNombre(asignatura.getCodinom());
                        return lookupItem;
                    }
            ).collect(Collectors.toList());
        }
        else
        {
            return list.stream().map(asignatura -> {
                        LookupItem lookupItem = new LookupItem();
                        lookupItem.setId(asignatura.getCodigo());
                        lookupItem.setNombre(asignatura.getCodinom());
                        return lookupItem;
                    }
            ).collect(Collectors.toList());
        }
    }

    public List<Asignatura> getAsignaturas(Long persona_id, boolean isAdmin, boolean isCoord, boolean solo_coordinadas)
    {
        List<Asignatura> asignaturaList;
        if (isAdmin)
        {
            asignaturaList = getAsignaturas();
        }
        else if (isCoord)
        {
            asignaturaList = getAsignaturasByCoordinador(persona_id, solo_coordinadas);
        }
        else
        {
            asignaturaList = getAsignaturasByProfesor(persona_id);
        }
        return asignaturaList.stream().sorted(Comparator.comparing(o -> o.getCodigo())).collect(Collectors.toList());
    }

    public List<Asignatura> getAsignaturas()
    {
        return Asignatura.transformaListaDTO(asignaturasDAO.getAsignaturas());
    }

    public List<Asignatura> getAsignaturasByAnyo(Long anyo)
    {
        return Asignatura.transformaListaDTO(asignaturasDAO.getAsignaturasByAnyo(anyo));
    }

    public List<Asignatura> getAsignaturasByCoordinador(Long persona_id, boolean solo_coordinadas)
    {
        List<AsignaturaDTO> listado = null;
        if (solo_coordinadas)
        {
            listado = asignaturasDAO.getAsignaturasByCoordinador(persona_id);
        }
        else
        {
            listado = asignaturasDAO.getAsignaturasByCoordinadorOrProfesor(persona_id);
        }
        return Asignatura.transformaListaDTO(listado);
    }

    public List<Asignatura> getAsignaturasByCoordinadorByAnyo(Long persona_id, boolean solo_coordinadas, Long anyo) {
        List<AsignaturaDTO> listado;

        if (solo_coordinadas)
            listado = asignaturasDAO.getAsignaturasByCoordinadorByAnyo(persona_id, anyo);
        else
            listado = asignaturasDAO.getAsignaturasByCoordinadorOrProfesor(persona_id);

        return Asignatura.transformaListaDTO(listado);
    }

    public List<Asignatura> getAsignaturasByProfesor(Long persona_id) {
        List<AsignaturaDTO> listado = asignaturasDAO.getAsignaturasByProfesor(persona_id);
        return Asignatura.transformaListaDTO(listado);
    }

    public List<Map<String, Integer>> anyosConAsignaturas(Long userId) {
        List<Integer> listado;

        if (apaDAO.hasPerfil("TFG", "ADMIN", userId))
            listado = asignaturasDAO.anyosConAsignaturasAdmin();
         else
            listado = asignaturasDAO.anyosConAsignaturas(userId);

        return listado.stream().map(i -> Collections.singletonMap("id", i)).collect(Collectors.toList());
    }

    public List<Asignatura> getAsignaturasCombo(Long id, boolean isAdmin, boolean isCoord, boolean solo_coordinadas, Long cursoAca) {
        List<Asignatura> asignaturaList;
        if (isAdmin)
            asignaturaList = asignaturasDAO.getAsignaturasCombo(cursoAca);
        else if (isCoord)
            asignaturaList = getAsignaturasComboByCoordinador(id, solo_coordinadas, cursoAca);
        else
            asignaturaList = asignaturasDAO.getAsignaturasComboByProfesor(id, cursoAca);
        return asignaturaList.stream().sorted(Comparator.comparing(o -> o.getCodigo())).collect(Collectors.toList());
    }

    private List<Asignatura> getAsignaturasComboByCoordinador(Long id, boolean soloCoordinadas, Long cursoAca) {
        if (soloCoordinadas)
            return asignaturasDAO.getAsignaturasComboByCoordinador(id, cursoAca);
        else
            return asignaturasDAO.getAsignaturasComboByCoordinadorOrProfesor(id, cursoAca);
    }
}
