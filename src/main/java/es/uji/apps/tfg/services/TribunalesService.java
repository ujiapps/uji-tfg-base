package es.uji.apps.tfg.services;

import es.uji.apps.tfg.dao.ConvocatoriaTribunalWVDAO;
import es.uji.apps.tfg.dao.TribunalesDAO;
import es.uji.apps.tfg.dto.ConvocatoriaTribunalVWDTO;
import es.uji.apps.tfg.dto.TribunalAlumnoDTO;
import es.uji.apps.tfg.dto.TribunalDTO;
import es.uji.apps.tfg.model.Tribunal;
import es.uji.apps.tfg.model.TribunalAlumno;
import es.uji.apps.tfg.utils.AcademicYear;
import es.uji.commons.rest.json.lookup.LookupItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class TribunalesService {
    private TribunalesDAO tribunalesDAO;
    private ConvocatoriaTribunalWVDAO convocatoriaTribunalWVDAO;

    @Autowired
    public TribunalesService(TribunalesDAO tribunalesDAO, ConvocatoriaTribunalWVDAO convocatoriaTribunalWVDAO) {
        this.tribunalesDAO = tribunalesDAO;
        this.convocatoriaTribunalWVDAO = convocatoriaTribunalWVDAO;
    }

    public List<Tribunal> getTribunales(String codigoAsignatura, Long cursoAca) {
        return tribunalesDAO.getTribunales(codigoAsignatura, cursoAca);
    }

    @Transactional
    public void deleteTribunal(Long id) {
        tribunalesDAO.deleteTribunal(id);
    }

    public TribunalDTO addTribunal(Tribunal tribunal, Long cursoAca) {
        tribunal.setCursoAca(cursoAca.intValue());
        TribunalDTO tribunalDTO = new TribunalDTO(tribunal);
        return tribunalesDAO.insert(tribunalDTO);
    }

    @Transactional
    public Long updateTribunal(Tribunal tribunal) {
        return tribunalesDAO.updateTribunal(tribunal);
    }


    public List<TribunalAlumno> getAlumnosTribunal(String codigoAsignatura, Long id) {
        return tribunalesDAO.getAlumnosTribunal(codigoAsignatura, id);
    }

    @Transactional
    public void deleteAlumnoTribunal(Long alumnoId) {
        tribunalesDAO.deleteAlumnoTribunal(alumnoId);
    }

    @Transactional
    public TribunalAlumnoDTO addAlumnoTribunal(Long alumnoId, Long tribunalId) {
        TribunalAlumnoDTO tribunal = new TribunalAlumnoDTO();
        tribunal.setTribunalId(tribunalId);
        tribunal.setAlumnoId(alumnoId);

        return tribunalesDAO.insert(tribunal);
    }

    public List<LookupItem> buscarAlumnos(String codigoAsignatura, String query, Long cursoAca) {
        return tribunalesDAO.buscarAlumnos(codigoAsignatura, query, cursoAca);
    }

    public List<ConvocatoriaTribunalVWDTO> getConvocatoriasTribunales(String codigoAsignatura, Long cursoAca) {
        return convocatoriaTribunalWVDAO.getListado(codigoAsignatura, cursoAca);
    }
}
