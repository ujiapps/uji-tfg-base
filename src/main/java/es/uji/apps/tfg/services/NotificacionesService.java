package es.uji.apps.tfg.services;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import org.springframework.stereotype.Service;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.tfg.config.Configuration;
import es.uji.apps.tfg.dto.PersonaDTO;
import es.uji.apps.tfg.i18n.ResourceProperties;
import es.uji.apps.tfg.utils.MailUtils;
import es.uji.commons.messaging.client.MessageNotSentException;

@Service
public class NotificacionesService
{
    @InjectParam
    private AsignaturasService asignaturasService;

    public static String getTextoParseado(String plantilla, HashMap<String, String> reemplazos)
    {
        for (String key : reemplazos.keySet())
        {
            plantilla = plantilla.replaceAll("#" + key.toUpperCase() + "#", reemplazos.get(key));
        }
        return plantilla;
    }

    public void notificarErrorACoordinadores(String codigoAsignatura, String mensajeError, PersonaDTO notificadorPorDefecto)
    {
        List<PersonaDTO> destinatarios = asignaturasService.getCoordinadoresByAsignatura(codigoAsignatura);

        if (destinatarios == null || destinatarios.size() == 0)
            destinatarios = Arrays.asList(notificadorPorDefecto);

        HashMap<String, String> mapa = new HashMap<String, String>();
        mapa.put("EXCEPTION", mensajeError);

        try
        {
            enviarMailAPersonas("tfg.message.errorNotificacionBody", mapa, "tfg.message.errorNotificacionSubject", destinatarios);
        }
        catch (Exception e2)
        {
            // TODO Registrar el error
        }
    }

    public void enviarMailAPersonas(String bodyKey, HashMap<String, String> reemplazos, String subjectKey, List<PersonaDTO> destinatarios) throws MessageNotSentException
    {
        String lang = "ca";
        enviarMailAPersonasLang(bodyKey, reemplazos, subjectKey, destinatarios, lang);
    }

    public void enviarMailAPersonasLang(String bodyKey, HashMap<String, String> reemplazos, String subjectKey, List<PersonaDTO> destinatarios, String idioma) throws MessageNotSentException
    {
        String body = getTextoParseado(ResourceProperties.getMessage(new Locale(idioma), bodyKey), reemplazos);
        for (PersonaDTO destinatario : destinatarios)
        {
            MailUtils mailList = new MailUtils(destinatario.getEmail(), ResourceProperties.getMessage(new Locale(idioma), subjectKey), body, "html",
                    destinatario.getNombreCompleto());
            mailList.send();
        }
    }

    public static PersonaDTO getNotificadorPorDefecto()
    {
        PersonaDTO avisar = new PersonaDTO();
        String nombreavisos = Configuration.getNombreAvisos();
        String[] nombreSplit = nombreavisos.split(",");
        avisar.setNombre(nombreSplit[1]);
        avisar.setApellido1(nombreSplit[0]);
        avisar.setApellido2("");
        avisar.setEmail(Configuration.getEmailAvisos());
        return avisar;
    }
}
