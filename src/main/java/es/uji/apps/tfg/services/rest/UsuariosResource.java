package es.uji.apps.tfg.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.tfg.dto.PersonaDTO;
import es.uji.apps.tfg.excepciones.AsignaturaNoConfiguradaException;
import es.uji.apps.tfg.excepciones.PropuestaException;
import es.uji.apps.tfg.i18n.ResourceProperties;
import es.uji.apps.tfg.model.*;
import es.uji.apps.tfg.services.UsuariosService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.sso.AccessManager;
import es.uji.commons.sso.User;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.*;

@Path("usuarios")
public class UsuariosResource extends CoreBaseService {
    @InjectParam
    private UsuariosService usuariosService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Persona> getUsuarios() {
        List<PersonaDTO> listado = usuariosService.getUsuarios();
        List<Persona> salida = new ArrayList<Persona>();
        for (PersonaDTO personaDTO : listado) {
            Persona usuario = new Persona(personaDTO);
            salida.add(usuario);
        }
        return salida;
    }

    @GET
    @Path("profesores")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Profesor> getProfesores(@QueryParam("codigo_asignatura") @DefaultValue("0") String codigo_asignatura)
    {
        User user = AccessManager.getConnectedUser(request);
        boolean isAdmin = usuariosService.hasPerfil("ADMIN", user.getId());
        boolean isCoord = usuariosService.hasPerfil("COORDINADOR", user.getId());

        List<Profesor> salida = new ArrayList<Profesor>();
//        List<PersonaDTO> listado = new ArrayList<PersonaDTO>();
        if (isAdmin)
        {
            salida = usuariosService.getProfesoresByAsignatura(codigo_asignatura);
        }
        else if (isCoord && codigo_asignatura.compareTo("0") != 0 && usuariosService.coordinaAsignatura( user.getId(),  codigo_asignatura))
        {
            salida = usuariosService.getProfesoresByAsignatura(codigo_asignatura);
        }
        if (salida.size() == 0)
        {// No hay ningún profesor asociado a sus asignaturas o no hay ninguna asignatura asociada a él
            salida.add(new Profesor(usuariosService.getUsuario(user.getId())));
        }
//        for (PersonaDTO personaDTO : listado)
//        {
//            Persona persona = new Persona(personaDTO);
//            salida.add(persona);
//        }
        return salida;
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response createUsuario(@FormParam("name") String name, @FormParam("autor_id") Long autor_id,
            @FormParam("codigo_asignatura") String codigo_asignatura, @FormParam("texto") String texto)
    {

        return Response.ok().build();
    }

    @GET
    @Path("estudiantes/{codigoAsignatura}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Persona> getEstudiantesByAsignatura(@PathParam("codigoAsignatura") String codigo_asignatura)
    {
        List<PersonaDTO> estudiantes = usuariosService.getEstudiantesByAsignatura(codigo_asignatura);
        List<Persona> salida = Persona.transformarLista(estudiantes);
        return salida;
    }

    @GET
    @Path("plan/{codigoAsignatura}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<AsignacionPlan> getPlanAsignacionesByAsignatura(@PathParam("codigoAsignatura") String codigo_asignatura) throws Exception {
        Long userId = AccessManager.getConnectedUserId(request);
        boolean isAdmin = usuariosService.hasPerfil("ADMIN", userId);
        boolean isCoord = usuariosService.hasPerfil("COORDINADOR", userId);
        List<AsignacionPlan> salida = new ArrayList<AsignacionPlan>();
        if (!(isAdmin || isCoord))
        {
            return salida;
        }
        try {
            salida = usuariosService.getPlanAsignacionesByAsignatura(codigo_asignatura);
        }
        catch (AsignaturaNoConfiguradaException ance) {
            throw new Exception(ResourceProperties.getMessage(request.getLocale(), ance.getMessage()));
        }

        return salida;
    }

    @GET
    @Path("asignados/{codigoAsignatura}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<AlumnosAsignados> getAlumnosasignadosByAsignatura(@PathParam("codigoAsignatura") String codigo_asignatura, @QueryParam("anyo") Integer anyo) {
        Long userId = AccessManager.getConnectedUserId(request);
        return usuariosService.getAlumnosasignadosByAsignatura(codigo_asignatura, userId, anyo);
    }

    @GET
    @Path("getExcludedDays")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Date> getExcludedDays()
    {
        List<Date> salida = new ArrayList<Date>();
        salida = usuariosService.getExcludedDays();
        return salida;
    }

    @PUT
    @Path("plan/{codigoAsignatura}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResultadoOperacion establecerTurnos(@PathParam("codigoAsignatura") String codigo_asignatura, List<AsignacionPlan> asignacionPlanes)
    {
        Long userId = AccessManager.getConnectedUserId(request);
        boolean isAdmin = usuariosService.hasPerfil("ADMIN", userId);
        boolean isCoord = usuariosService.hasPerfil("COORDINADOR", userId);
        if (!(isAdmin || isCoord))
        {
            return new ResultadoOperacion(false, 405);
        }
        boolean resultado = usuariosService.updateAsignacionPlan(asignacionPlanes, codigo_asignatura);

        return new ResultadoOperacion(resultado, null);
    }

    @PUT
    @Path("plan/{codigoAsignatura}/guardarPlan")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResultadoOperacion guardarPlanSinEnviarCorreo(@PathParam("codigoAsignatura") String codigo_asignatura, List<AsignacionPlan> asignacionPlanes) {
        Long userId = AccessManager.getConnectedUserId(request);
        boolean isAdmin = usuariosService.hasPerfil("ADMIN", userId);
        boolean isCoord = usuariosService.hasPerfil("COORDINADOR", userId);

        if (!(isAdmin || isCoord))
            return new ResultadoOperacion(false, 405);

        return new ResultadoOperacion(usuariosService.guardarPlanSinEnviarCorreo(asignacionPlanes, codigo_asignatura), null);
    }

    @PUT
    @Path("plan/{codigoAsignatura}/{personaId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResultadoOperacion establecerTurnosPersona(@PathParam("codigoAsignatura") String codigo_asignatura, List<AsignacionPlan> asignacionPlanes)
    {
        Long userId = AccessManager.getConnectedUserId(request);
        boolean isAdmin = usuariosService.hasPerfil("ADMIN", userId);
        boolean isCoord = usuariosService.hasPerfil("COORDINADOR", userId);
        if (!(isAdmin || isCoord))
        {
            return new ResultadoOperacion(false, 405);
        }
        boolean resultado = usuariosService.updateAsignacionPlan(asignacionPlanes, codigo_asignatura);

        return new ResultadoOperacion(resultado, null);
    }

    @GET
    @Path("corregirBajas")
    @Produces(MediaType.TEXT_HTML)
    public ResultadoOperacion corregirBajas() {
        Long userId = AccessManager.getConnectedUserId(request);
        boolean isAdmin = usuariosService.hasPerfil("ADMIN", userId);
        boolean isCoord = usuariosService.hasPerfil("COORDINADOR", userId);
        if (!(isAdmin || isCoord))
        {
            return new ResultadoOperacion(false, 405);
        }
        boolean resultado = usuariosService.corregirBajas();
        return new ResultadoOperacion(resultado, null);
    }

    @GET
    @Path("perfil")
    @Produces(MediaType.APPLICATION_JSON)
    public ResultadoOperacion corregirBajasConfirmado() {
        Long userId = AccessManager.getConnectedUserId(request);
        Map<String, Boolean> ui = new HashMap<>();
        ui.put("admin", usuariosService.hasPerfil("ADMIN", userId));
        ui.put("coordinador", usuariosService.hasPerfil("COORDINADOR", userId));

        return new ResultadoOperacion(true, ui);
    }


    @GET
    @Path("asignados/{codigoAsignatura}/{ejercicio}")
    @Produces("application/vnd.ms-excel")
    public Response getExcelAlumnosAsignados(@PathParam("codigoAsignatura") String codigoAsignatura, @PathParam("ejercicio") Integer ejercicio) throws PropuestaException, IOException {
        ParamUtils.checkNotNull(codigoAsignatura, ejercicio);
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        ByteArrayOutputStream ostream = usuariosService.getExcelAlumnosAsignados(connectedUserId, codigoAsignatura, ejercicio);

        if (ostream != null) {
            return Response.ok(ostream.toByteArray(), MediaType.APPLICATION_OCTET_STREAM).header("content-disposition", "attachment; filename = Propuestas-Alumnos" + codigoAsignatura + ".xls").build();
        }
        throw new PropuestaException("Sense dades");
    }
}
