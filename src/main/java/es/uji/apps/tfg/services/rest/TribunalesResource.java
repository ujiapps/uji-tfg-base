package es.uji.apps.tfg.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.tfg.ResponseMessage;
import es.uji.apps.tfg.excepciones.AccesoSPIException;
import es.uji.apps.tfg.model.Tribunal;
import es.uji.apps.tfg.services.TribunalesService;
import es.uji.apps.tfg.services.UsuariosService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.json.lookup.LookupItem;
import es.uji.commons.sso.AccessManager;
import es.uji.commons.web.template.PDFTemplate;
import es.uji.commons.web.template.Template;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Path("tribunales")
public class TribunalesResource extends CoreBaseService {
    public final static Logger log = LoggerFactory.getLogger(TribunalesResource.class);
    @InjectParam
    UsuariosService usuariosService;
    @InjectParam
    private TribunalesService tribunalesService;

    @GET
    @Path("{codigoAsignatura}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getTribunales(@PathParam("codigoAsignatura") String codigoAsignatura,
                                        @QueryParam("cursoAca") Long cursoAca) {
        Long userId = AccessManager.getConnectedUserId(request);
        boolean hasPerfiles = usuariosService.hasPerfiles(new String[]{"ADMIN", "COORDINADOR"}, userId);
        if (hasPerfiles || (usuariosService.coordinaAsignatura(userId, codigoAsignatura, cursoAca))) {
            return UIEntity.toUI(tribunalesService.getTribunales(codigoAsignatura, cursoAca));
        }
        return new ArrayList<>();
    }

    @POST
    @Path("{codigoAsignatura}/{cursoAcademico}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity addTribunal(@PathParam("codigoAsignatura") String codigoAsignatura, UIEntity uiEntity,
                                @PathParam("cursoAcademico") Long cursoAca) throws AccesoSPIException {
        Long userId = AccessManager.getConnectedUserId(request);
        Tribunal tribunal = new Tribunal(uiEntity);
        tribunal.setCodigoAsignatura(codigoAsignatura);
        boolean hasPerfiles = usuariosService.hasPerfiles(new String[]{"ADMIN", "COORDINADOR"}, userId);
        if (hasPerfiles || (usuariosService.coordinaAsignatura(userId, codigoAsignatura, cursoAca))) {
            return UIEntity.toUI(tribunalesService.addTribunal(tribunal, cursoAca));
        } else {
            throw new AccesoSPIException("Acceso no permitido");
        }
    }

    @PUT
    @Path("{codigoAsignatura}/{cursoAcademico}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity upadteTribunal(@PathParam("codigoAsignatura") String codigoAsignatura, UIEntity uiEntity,
                                   @PathParam("cursoAcademico") Long cursoAca) throws AccesoSPIException {
        Long userId = AccessManager.getConnectedUserId(request);
        Tribunal tribunal = new Tribunal(uiEntity);
        tribunal.setCodigoAsignatura(codigoAsignatura);
        boolean hasPerfiles = usuariosService.hasPerfiles(new String[]{"ADMIN", "COORDINADOR"}, userId);
        if (hasPerfiles || (usuariosService.coordinaAsignatura(userId, codigoAsignatura, cursoAca))) {
            return UIEntity.toUI(tribunalesService.updateTribunal(tribunal));
        } else {
            throw new AccesoSPIException("Acceso no permitido");
        }
    }

    @DELETE
    @Path("{codigoAsignatura}/{id}/{cursoAcademico}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public ResponseMessage deleteAdministrativo(@PathParam("codigoAsignatura") String codigoAsignatura,
                                                @PathParam("id") Long id,
                                                @PathParam("cursoAcademico") Long cursoAca) {
        Long userId = AccessManager.getConnectedUserId(request);
        boolean hasPerfiles = usuariosService.hasPerfiles(new String[]{"ADMIN", "COORDINADOR"}, userId);
        if (hasPerfiles || (usuariosService.coordinaAsignatura(userId, codigoAsignatura, cursoAca))) {
            tribunalesService.deleteTribunal(id);
            return new ResponseMessage(true, "Tribunal borrada");
        }
        return new ResponseMessage(false, "Error al esborrar Tribunal");
    }

    @GET
    @Path("{codigoAsignatura}/{id}/alumnos")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getAlumnoTribunal(@PathParam("codigoAsignatura") String codigoAsignatura, @PathParam("id") Long id,
                                            @QueryParam("cursoAca") Long cursoAca) {
        Long userId = AccessManager.getConnectedUserId(request);
        boolean hasPerfiles = usuariosService.hasPerfiles(new String[]{"ADMIN", "COORDINADOR"}, userId);
        if (hasPerfiles || (usuariosService.coordinaAsignatura(userId, codigoAsignatura, cursoAca))) {
            return UIEntity.toUI(tribunalesService.getAlumnosTribunal(codigoAsignatura, id));
        }
        return new ArrayList<>();
    }

    @GET
    @Path("{codigoAsignatura}/{id}/alumnos/busqueda")
    @Produces(MediaType.APPLICATION_JSON)
    public List<LookupItem> buscarAlumnos(@PathParam("codigoAsignatura") String codigoAsignatura,
                                          @PathParam("id") Long id,
                                          @QueryParam("query") String query,
                                          @QueryParam("cursoAca") Long cursoAca) {
        Long userId = AccessManager.getConnectedUserId(request);
        boolean hasPerfiles = usuariosService.hasPerfiles(new String[]{"ADMIN", "COORDINADOR"}, userId);
        if (hasPerfiles || (usuariosService.coordinaAsignatura(userId, codigoAsignatura, cursoAca))) {
            return tribunalesService.buscarAlumnos(codigoAsignatura, query, cursoAca);
        }
        return new ArrayList<>();
    }

    @POST
    @Path("{codigoAsignatura}/{tribunalId}/alumnos")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity addAlumnoTribunal(@PathParam("codigoAsignatura") String codigoAsignatura, @PathParam("tribunalId") Long tribunalId, UIEntity uiEntity) throws AccesoSPIException {
        Long userId = AccessManager.getConnectedUserId(request);
        Long alumnoId = uiEntity.getLong("alumnoId");
        Long cursoAca = uiEntity.getLong("cursoAca");
        boolean hasPerfiles = usuariosService.hasPerfiles(new String[]{"ADMIN", "COORDINADOR"}, userId);
        if (hasPerfiles || (usuariosService.coordinaAsignatura(userId, codigoAsignatura, cursoAca))) {
            return UIEntity.toUI(tribunalesService.addAlumnoTribunal(alumnoId, tribunalId));
        } else {
            throw new AccesoSPIException("Acceso no permitido");
        }
    }

    @DELETE
    @Path("{codigoAsignatura}/{tribunalId}/alumnos/{alumnoId}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public ResponseMessage deleteAlumnoTribunal(@PathParam("codigoAsignatura") String codigoAsignatura, @PathParam("alumnoId") Long alumnoId,
                                                @QueryParam("cursoAca") Long cursoAca) {
        Long userId = AccessManager.getConnectedUserId(request);
        boolean hasPerfiles = usuariosService.hasPerfiles(new String[]{"ADMIN", "COORDINADOR"}, userId);
        if (hasPerfiles || (usuariosService.coordinaAsignatura(userId, codigoAsignatura, cursoAca))) {
            tribunalesService.deleteAlumnoTribunal(alumnoId);
            return new ResponseMessage(true, "Alumne de Tribunal esborrat");
        }
        return new ResponseMessage(false, "Error al esborrar Tribunal");
    }

    @GET
    @Path("{codigoAsignatura}/pdf/{cursoAcademico}")
    @Produces("application/pdf")
    public Response exportListados(@PathParam("codigoAsignatura") String codigoAsignatura,
                                   @PathParam("cursoAcademico") Long cursoAca) throws IOException {
        Long userId = AccessManager.getConnectedUserId(request);
        boolean hasPerfiles = usuariosService.hasPerfiles(new String[]{"ADMIN", "COORDINADOR"}, userId);
        if (hasPerfiles || (usuariosService.coordinaAsignatura(userId, codigoAsignatura, cursoAca))) {
            Template template = new PDFTemplate("tfg/pdf/listado-convocatoria", new Locale("ca"), "tfg");
            template.put("titulo", "Llistat convocatoria tribunals - ".concat(codigoAsignatura));
            template.put("contenido", tribunalesService.getConvocatoriasTribunales(codigoAsignatura, cursoAca));

            return Response.ok(template).type("application/pdf").header("Content-Disposition", "attachment; filename = listado-" + codigoAsignatura + ".pdf").build();
        }
        return Response.status(Response.Status.UNAUTHORIZED).build();
    }

}
