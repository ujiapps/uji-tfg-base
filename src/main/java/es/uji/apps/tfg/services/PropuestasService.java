package es.uji.apps.tfg.services;

import es.uji.apps.tfg.config.Configuration;
import es.uji.apps.tfg.dao.PersonasDAO;
import es.uji.apps.tfg.dao.PropuestasDAO;
import es.uji.apps.tfg.dto.*;
import es.uji.apps.tfg.excepciones.*;
import es.uji.apps.tfg.i18n.ResourceProperties;
import es.uji.apps.tfg.model.Asignacion;
import es.uji.apps.tfg.model.Propuesta;
import es.uji.apps.tfg.model.PropuestaAlumno;
import es.uji.apps.tfg.utils.AcademicYear;
import es.uji.apps.tfg.utils.StringUtils;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.content.ContentCleaner;
import es.uji.commons.rest.content.PlainTextHTMLCleaner;
import org.apache.poi.ss.usermodel.Row;
import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.util.HtmlUtils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class PropuestasService {

    public final static Logger log = LoggerFactory.getLogger(PropuestasService.class);

    @Autowired
    private PropuestasDAO propuestasDAO;
    @Autowired
    private PersonasDAO personasDAO;
    @Autowired
    private NotificacionesService notificacionesService;
    @Autowired
    private AsignaturasService asignaturasService;

    public PropuestaDTO getPropuestaById(long id) {
        return propuestasDAO.getPropuesta(id);
    }

    public long createPropuesta(String name, List<Long> tutores, String codigo_asignatura, String objetivos, String bibliografia, String descripcion,
                                String observaciones, BigDecimal plazas, BigDecimal espera, Long perIdCreador) {
        objetivos = StringUtils.tidyClean(objetivos);
        bibliografia = StringUtils.tidyClean(bibliografia);
        descripcion = StringUtils.tidyClean(descripcion);
        observaciones = StringUtils.tidyClean(observaciones);

        Long propuestaId = propuestasDAO.create(name, tutores, codigo_asignatura, objetivos, bibliografia, descripcion, observaciones, plazas, espera, perIdCreador);

        if (propuestaId > 0) {
            addUpdateTutores(propuestaId, tutores);
        }
        return propuestaId;
    }

    private void addUpdateTutores(Long propuestaId, List<Long> tutores) {
        propuestasDAO.addUpdateTutores(propuestaId, tutores);
    }

    public PropuestaDTO updatePropuesta(long id, String name, List<Long> tutores, String codigo_asignatura, String objetivos, String bibliografia,
                                        String descripcion, String observaciones, BigDecimal plazas, BigDecimal espera) {
        objetivos = StringUtils.tidyClean(objetivos);
        bibliografia = StringUtils.tidyClean(bibliografia);
        descripcion = StringUtils.tidyClean(descripcion);
        observaciones = StringUtils.tidyClean(observaciones);

        PropuestaDTO propuestaDTO = propuestasDAO.update(id, name, tutores, codigo_asignatura, objetivos, bibliografia, descripcion, observaciones, plazas, espera);
        if (propuestaDTO != null && propuestaDTO.getId() > 0) {
            addUpdateTutores(propuestaDTO.getId(), tutores);
        }
        return propuestaDTO;
    }

    public boolean deletePropuesta(int id) {
        PropuestaDTO p = this.getPropuestaById(id);
        return propuestasDAO.delete(p);
    }

    public List<PropuestaDTO> getPropuestas(Long persona_id, boolean isAdmin, boolean isCoord) {
        return propuestasDAO.getPropuestasByAutor(persona_id, isAdmin, isCoord);
    }

    public List<Asignacion> getAsignacionesByAsignatura(String codigo_asignatura, Long profesor_id) {
        if (profesor_id > 0) {
            return propuestasDAO.getAsignacionesDeProfesorByAsignatura(codigo_asignatura, profesor_id);
        } else {
            return propuestasDAO.getAsignacionesByAsignatura(codigo_asignatura);
        }

    }

    public List<PersonasPropuestaDTO> getAsignacionesByPropuesta(long propuesta_id) {
        return propuestasDAO.getAsignacionesByPropuesta(propuesta_id);
    }

    public void updateAsignaciones(Long persona_id, String codigo_asignatura, Long propuesta_id, Long editor_persona_id, String comentarios, Date current)
            throws AsignaturaNoConfiguradaException, PropuestaException {
        PropuestaDTO liberada = propuestasDAO.updateAsignaciones(persona_id, propuesta_id, editor_persona_id, comentarios, current);
        propuestasDAO.ampliarPlazasSiLimiteSuperado(propuesta_id);
        if (liberada != null) {
            redistribuirPlazas(liberada.getId());
        }
    }

    public List<Propuesta> getPropuestasAsignadasByAsignatura(Long userId, boolean isAdmin, boolean isCoord, String codigo_asignatura) {
        return propuestasDAO.getPropuestasAsignadasByAsignatura(userId, isAdmin, isCoord, codigo_asignatura, null);
    }

    public List<Propuesta> getPropuestasAsignadasByAsignatura(Long userId, boolean isAdmin, boolean isCoord, String codigo_asignatura, Integer ejercicio) {
        return propuestasDAO.getPropuestasAsignadasByAsignatura(userId, isAdmin, isCoord, codigo_asignatura, ejercicio);
    }

    public List<PropuestaDTO> getPropuestasByAsignatura(String codigo_asignatura) {
        return propuestasDAO.getPropuestasByAsignatura(codigo_asignatura);
    }

    public List<Propuesta> getPropuestasDePersonaByAsignatura(Long persona_id, String codigo_asignatura) {
        return propuestasDAO.getPropuestasDePersonaByAsignatura(persona_id, codigo_asignatura, null);
    }

    public List<Propuesta> getPropuestasDePersonaByAsignatura(Long persona_id, String codigo_asignatura, Integer ejercicio) {
        return propuestasDAO.getPropuestasDePersonaByAsignatura(persona_id, codigo_asignatura, ejercicio);
    }

    public void addSolicitud(PropuestaDTO propuestaDTO, long persona_id, String codigo_asignatura, String comentarios) throws Exception {
        Propuesta propuesta = new Propuesta(propuestaDTO, true);
        // PersonaDTO personaDTO = personasDAO.getUsuario(persona_id);
        TurnoDTO turno = personasDAO.getTurno(persona_id, codigo_asignatura);
        if (turno == null) {
            throw new Exception("tfg.message.exception.sinTurnoAsignado");
        }

        Integer plazo_matricula = new Integer(Configuration.getPlazoSolicitud());
        Timestamp finplazo = new Timestamp(turno.getFecha().getTime() + plazo_matricula * 24 * 3600 * 1000);

        if (turno.getFecha().getTime() > finplazo.getTime()) {
            throw new Exception("tfg.message.exception.turnoCerrado");
        }

        if (propuesta.getDisponibilidad() == -1) {
            throw new Exception("tfg.message.exception.noPax");
        } else {
            try {
                propuestasDAO.addSolicitud(propuestaDTO, persona_id, propuesta.getDisponibilidad() == 0, comentarios);
            } catch (ConstraintViolationException e) {
                throw new Exception("tfg.message.exception.yaInscrito");
            } catch (Exception ex) {
                throw new Exception("tfg.message.exception.errorGeneral");
            }

        }
    }

    public void removeSolicitud(long persona_id, long propuesta_id) {
        propuestasDAO.removeSolicitud(persona_id, propuesta_id);
    }

    public String redistribuirPlazas(long propuesta_id) {
        String errores = "";
        PropuestaDTO propuestaDTO = propuestasDAO.getPropuesta(propuesta_id);
        List<PersonasPropuestaDTO> inscritos = propuestaDTO.getTfgPersonasPropuestas();
        if (inscritos != null && inscritos.size() > 0) {
            PersonasPropuestaDTO enEsperaPrimerSolicitante = null;
            for (PersonasPropuestaDTO inscrito : inscritos) {
                if (inscrito.getAceptada().intValue() == 0) {
                    if (enEsperaPrimerSolicitante == null) {
                        enEsperaPrimerSolicitante = inscrito;
                    } else {
                        if (inscrito.getFechaEdicion().before(enEsperaPrimerSolicitante.getFechaEdicion())) {
                            enEsperaPrimerSolicitante = inscrito;
                        }
                    }
                }
            }
            if (enEsperaPrimerSolicitante != null) {
                PersonaDTO solicitante = enEsperaPrimerSolicitante.getTfgExtPersona();
                PropuestaDTO liberada = null;
                List<PersonasPropuestaDTO> solicitudes = propuestasDAO.getAsignacionesDePersonaByAsignatura(solicitante.getId(), propuestaDTO
                        .getTfgExtAsignatura().getCodigo());
                for (PersonasPropuestaDTO solicitud : solicitudes) {
                    if (solicitud.getAceptada().intValue() == 1) {
                        liberada = solicitud.getTfgPropuesta();
                        removeSolicitud(solicitud.getTfgExtPersona().getId(), solicitud.getTfgPropuesta().getId());
                    } else {
                        // Actualizamos la solicitud
                        propuestasDAO.aceptarSolicitud(solicitud);
                        // Notificamos al solicitante
                        String nom_asignatura = propuestaDTO.getTfgExtAsignatura().getCodigo() + " - " + propuestaDTO.getTfgExtAsignatura().getNombre();
                        HashMap<String, String> mapa = new HashMap<String, String>();
                        mapa.put("PROPUESTA_NOMBRE", propuestaDTO.getNombre());
                        mapa.put("ASIGNATURA_NOMBRE", nom_asignatura);

                        try {
                            notificacionesService
                                    .enviarMailAPersonas("tfg.message.espera.body", mapa, "tfg.message.espera.subject", Arrays.asList(solicitante));
                        } catch (Exception e) {
                            errores += "\n" + solicitante.getNombreCompleto() + "<" + solicitante.getEmail() + ">";
                        }
                    }
                }
                if (liberada != null) {
                    errores += redistribuirPlazas(liberada.getId());
                }
            }
        }
        return errores;
    }

    public PersonasPropuestaDTO getInscripcionDePersonaEnAsignatura(long persona_id, long propuesta_id) {
        return propuestasDAO.getAsignacion(persona_id, propuesta_id);
    }

    public void notificarAsignacion(AsignaturaDTO asignaturaDTO, PropuestaDTO propuesta, PersonaDTO alumno, Set<PropuestaTutorDTO> propuestaTutores, String comentarios) {
        propuestaTutores.stream().forEach(propuestaTutorDTO -> {
            try {
                notificarAsignacion(asignaturaDTO, propuesta, alumno, propuestaTutorDTO.getTutor(), comentarios);
            } catch (Exception e) {
                log.error("Error al notificar inscripcion", e);
            }
        });
    }


    public void notificarAsignacion(AsignaturaDTO asignaturaDTO, PropuestaDTO propuesta, PersonaDTO alumno, PersonaDTO tutor, String comentarios) throws Exception {
        String nom_asignatura = asignaturaDTO.getCodigo() + " - " + asignaturaDTO.getNombre();

        HashMap<String, String> mapa = new HashMap<String, String>();

        mapa.put("PROPUESTA_NOMBRE", propuesta.getNombre());
        mapa.put("ALUMNO_NOMBRE", alumno.getNombreCompleto());
        mapa.put("ASIGNATURA_NOMBRE", nom_asignatura);
        mapa.put("COMENTARIOS", ParamUtils.isNotNull(comentarios) ? comentarios : "");

        String errores = "";
        try {
            notificacionesService.enviarMailAPersonas("tfg.message.asignacion.body_tutor", mapa, "tfg.message.asignacion.subject_tutor", Arrays.asList(tutor));
        } catch (Exception e) {
            errores += "\n" + tutor.getNombreCompleto() + "<" + tutor.getEmail() + "> (Tutor)";
        }

        try {
            notificacionesService.enviarMailAPersonas("tfg.message.asignacion.body", mapa, "tfg.message.asignacion.subject", Arrays.asList(alumno));
        } catch (Exception e) {
            errores += "\n" + alumno.getNombreCompleto() + "<" + alumno.getEmail() + "> (Estudiante)";
        }
        if (errores.length() > 0) {
            throw new Exception(errores);
        }
    }

    public void comprobarDisponibilidadDePropuesta(PropuestaDTO propuesta, List<PersonasPropuestaDTO> asignaciones) throws PropuestaException, AsignaturaNoConfiguradaException {
        int disponibilidadNueva = (new Propuesta(propuesta, true)).getDisponibilidad();
        // Durante la petición se han llenado las plazas de la propuesta
        // o se ha deshabilitado la lista de espera y sólo quedan plazas en espera
        /* Una persona no puede tener turno para inscribirse en una propuesta si la propuesta no tiene configuración previa establecida
        if (propuesta.getTfgExtAsignatura().getTfgAsignaturasConfig() == null)
            throw new AsignaturaNoConfiguradaException();
          */
        if (disponibilidadNueva == -1
                || (disponibilidadNueva == 1 && !propuesta.getTfgExtAsignatura().getTfgAsignaturasConfig().isHabilitar_espera())) {
            throw new PropuestaException("tfg.message.error.noPaxMustRefresh");
        }

        if (asignaciones != null) {
            if (asignaciones.size() >= 2) {
                throw new PropuestaException("tfg.message.error.maxInscricipiones");
            } else {
                for (PersonasPropuestaDTO asignacion : asignaciones) {
                    if (asignacion.getTfgPropuesta().getId() == propuesta.getId()) {
                        throw new PropuestaException("tfg.message.error.yaInscrito");
                    }

                    // Ya tiene una asignada y al tener plazas se le asignaría ésta también
                    if (disponibilidadNueva == 0L && asignacion.getAceptada().intValue() == 1) {
                        throw new PropuestaException("tfg.message.error.yaEstaInscrito");
                    }
                    // Ya tiene una en espera y no puede solicitar entrar en espera otra vez
                    if (disponibilidadNueva == 1L && asignacion.getAceptada().intValue() == 0) {
                        throw new PropuestaException("tfg.message.error.yaEstaEnEspera");
                    }
                }
            }
        }
    }

    public List<PersonasPropuestaDTO> getAsignacionesDePersonaByAsignatura(Long personaId, String codigoAsignatura) {
        return propuestasDAO.getAsignacionesDePersonaByAsignatura(personaId, codigoAsignatura);
    }

    public String getMensajeEstadoPropuestas(String idioma, String codigo_asignatura, Long persona_id) {
        if (codigo_asignatura == null || codigo_asignatura.length() == 0) {
            return null;
        }
        List<PersonasPropuestaDTO> solicitudes = getAsignacionesDePersonaByAsignatura(persona_id, codigo_asignatura);
        Locale locale = new Locale(idioma);
        String mensajeInscripciones = ResourceProperties.getMessage(locale, "tfg.message.inscripciones.status");
        if (solicitudes == null || solicitudes.size() == 0) {
            // mensajeInscripciones += "<br/>" + ResourceProperties.getMessage(locale,
            // "tfg.message.inscripciones.ninguna");
            return null;
        } else {
            for (PersonasPropuestaDTO solicitud : solicitudes) {
                if (solicitud.getAceptada().intValue() == 0) {
                    mensajeInscripciones += "<br/><span data-propuesta=\"" + solicitud.getTfgPropuesta().getId() + "\">"
                            + ResourceProperties.getMessage(locale, "tfg.message.inscripciones.espera") + ": " + HtmlUtils.htmlEscape(solicitud.getTfgPropuesta().getNombre())
                            + "</span>";
                }
                if (solicitud.getAceptada().intValue() == 1) {
                    mensajeInscripciones += "<br/><span data-propuesta=\"" + solicitud.getTfgPropuesta().getId() + "\">"
                            + ResourceProperties.getMessage(locale, "tfg.message.inscripciones.inscrito") + ": " + HtmlUtils.htmlEscape(solicitud.getTfgPropuesta().getNombre())
                            + "</span>";
                }
            }
        }
        return mensajeInscripciones;
    }

    public Long updateComentariosAsignacion(Long persona_id, Long propuesta_id, String comentarios, Long userId, Date current) {
        return propuestasDAO.updateComentariosAsignacion(persona_id, propuesta_id, comentarios, userId, current);

    }

    public void deleteAsignacionPropuesta(Asignacion asignacion)
            throws AsignaturaNoConfiguradaException, PropuestaException {
        PropuestaDTO liberada = propuestasDAO.liberaAsignaciones(asignacion.getPersona_id(), asignacion.getPropuesta_id());
        propuestasDAO.ampliarPlazasSiLimiteSuperado(asignacion.getPropuesta_id());
        if (liberada != null) {
            redistribuirPlazas(liberada.getId());
        }
    }

    @Transactional
    public void deleteAsignacion(Asignacion asignacion) throws Exception {
        if (asignacion.getAceptada() == 1) {
            deleteAsignacionPropuesta(asignacion);
        }
        propuestasDAO.deleteAsignacion(asignacion);
    }

    public ByteArrayOutputStream getExcel(String codigo_asignatura) throws IOException {

        List<Asignacion> listado = propuestasDAO.getAsignacionesByAsignatura(codigo_asignatura);

        WriterExcelService writerExcelService = new WriterExcelService();


        if (listado != null && listado.size() > 0) {
            List<Asignacion> asignadas = new ArrayList<Asignacion>();
            List<Asignacion> en_espera = new ArrayList<Asignacion>();
            List<Asignacion> sin_asign = new ArrayList<Asignacion>();

            for (Asignacion item : listado) {
                if (item.getPropuesta_id() != null) {
                    if (item.getAceptada() == 1) {
                        asignadas.add(item);
                    } else {
                        en_espera.add(item);
                    }
                } else {
                    sin_asign.add(item);
                }
            }
            int rownum = 0;
            writerExcelService.addFulla("Assignades");
            writerExcelService.generaCeldes(writerExcelService.getEstilNegreta(), 0, "Nom", "1r. Cognom", "2n. Cognom", "Proposta ja acceptada", "Proposada per", "Data assignació", "Comentaris");
            for (Asignacion asignacion : asignadas) {
                int cellnum = 0;
                Row row = writerExcelService.getNewRow(++rownum);
                writerExcelService.addCell(cellnum++, asignacion.getNombre(), null, row);
                writerExcelService.addCell(cellnum++, asignacion.getApellido1(), null, row);
                writerExcelService.addCell(cellnum++, asignacion.getApellido2(), null, row);
                writerExcelService.addCell(cellnum++, asignacion.getPropuestaNombre(), null, row);
                writerExcelService.addCell(cellnum++, asignacion.getPropuestaPersonaNombre(), null, row);
                writerExcelService.addCell(cellnum++, asignacion.getFecha_edicion(), null, row);
                writerExcelService.addCell(cellnum++, asignacion.getComentarios(), null, row);
            }
            rownum = 0;
            writerExcelService.addFulla("En espera");
            writerExcelService.generaCeldes(writerExcelService.getEstilNegreta(), 0, "Nom", "1r. Cognom", "2n. Cognom", "Proposta en espera", "Proposta ja acceptada", "Proposada per", "Data assignació", "Comentaris");
            for (Asignacion asignacion : en_espera) {
                int cellnum = 0;
                Row row = writerExcelService.getNewRow(++rownum);
                writerExcelService.addCell(cellnum++, asignacion.getNombre(), null, row);
                writerExcelService.addCell(cellnum++, asignacion.getApellido1(), null, row);
                writerExcelService.addCell(cellnum++, asignacion.getApellido2(), null, row);
                writerExcelService.addCell(cellnum++, asignacion.getPropuestaNombre(), null, row);
                writerExcelService.addCell(cellnum++, asignacion.getPropuestaPersonaNombre(), null, row);
                writerExcelService.addCell(cellnum++, asignacion.getFecha_edicion(), null, row);
                writerExcelService.addCell(cellnum++, asignacion.getComentarios(), null, row);
            }
            rownum = 0;
            writerExcelService.addFulla("Sense assignar");
            writerExcelService.generaCeldes(writerExcelService.getEstilNegreta(), 0, "Nom", "1r. Cognom", "2n. Cognom", "Proposta ja acceptada", "Proposada per", "Data assignació", "Comentaris");
            for (Asignacion asignacion : sin_asign) {
                int cellnum = 0;
                Row row = writerExcelService.getNewRow(++rownum);
                writerExcelService.addCell(cellnum++, asignacion.getNombre(), null, row);
                writerExcelService.addCell(cellnum++, asignacion.getApellido1(), null, row);
                writerExcelService.addCell(cellnum++, asignacion.getApellido2(), null, row);
                writerExcelService.addCell(cellnum++, "Sense assignar", null, row);
                writerExcelService.addCell(cellnum++, "", null, row);
                writerExcelService.addCell(cellnum++, asignacion.getFecha_edicion(), null, row);
                writerExcelService.addCell(cellnum++, asignacion.getComentarios(), null, row);
            }
        }
        return writerExcelService.getExcel();
    }

    public void confirmarAsignacion(Long persona_id, Long propuesta_id, Long userId, Date current) {
        propuestasDAO.aceptarAsignacion(persona_id, propuesta_id, userId, current);
    }


    @Transactional
    public void duplicarPropuestas(Long userId, List<PropuestaDTO> listado) {
        int year = AcademicYear.getCurrent();
        listado.stream().forEach(propuestaDTO -> {
            propuestaDTO.setId(null);
            propuestaDTO.setEjercicio(year);
            propuestaDTO.setTfgPersonasPropuestas(null);
            propuestaDTO.setFechaCreacion(new Date());
            propuestaDTO.setPerIdCreacion(userId);
            propuestaDTO.setPropuestaTutores(null);
            propuestasDAO.update(propuestaDTO);
        });
    }

    public List<PropuestaDTO> getPropuestasADuplicar(String codigo_asignatura) {
        List<PropuestaDTO> propuestasList = propuestasDAO.getPropuestasADuplicar(codigo_asignatura);
        Map<Long, List<PropuestaDTO>> propuestasTutor = new HashMap<>();
        propuestasList.forEach(propuestaDTO -> {
            propuestaDTO.getPropuestaTutores().forEach(pt -> {
                if (propuestasTutor.containsKey(pt.getTutor().getId())) {
                    propuestasTutor.get(pt.getTutor().getId()).add(pt.getPropuesta());
                } else {
                    List<PropuestaDTO> p = new ArrayList<>();
                    p.add(pt.getPropuesta());
                    propuestasTutor.put(pt.getTutor().getId(), p);
                }
            });
        });
        List<PropuestaDTO> result = new ArrayList<>();
        propuestasTutor.entrySet().forEach(e -> {
            List<PropuestaDTO> b = e.getValue().stream().collect(Collectors.groupingBy(
                            PropuestaDTO::getEjercicio,
                            TreeMap::new,
                            Collectors.toList()))
                    .lastEntry()
                    .getValue();
            result.addAll(b);
        });

        return result;
    }

    public String limpiaHtml(String contenido) {
        Integer MAX_CELL_CHARACTERS = 32700;
        ContentCleaner contentCleaner = new PlainTextHTMLCleaner();
        try {
            String resultado = "";
            if (ParamUtils.isNotNull(contenido)) {
                resultado = contentCleaner.clean(contenido);
                return resultado.substring(0, Math.min(resultado.length(), MAX_CELL_CHARACTERS));
            }
            return resultado;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return "";
        }
    }

    public ByteArrayOutputStream getPropuetasExcel(String codigoAsignatura) throws IOException {
        List<PropuestaVWDTO> listado = propuestasDAO.getPropuestas(codigoAsignatura);
        WriterExcelService excelService = new WriterExcelService();

        if (listado != null && listado.size() > 0) {
            int rownum = 0;
            excelService.addFulla("Propostes");
            excelService.generaCeldes(excelService.getEstilNegreta(), 0, "Id", "Nom", "Plaçes", "Espera", "Codi d'assignatura", "Data creació", "Tutors",
                    "Acceptats", "En Espera", "Exercici", "Objetius", "Observacions", "Descripció", "Biografia");
            for (PropuestaVWDTO propuesta : listado) {
                int cellnum = 0;
                Row row = excelService.getNewRow(++rownum);
                excelService.addCell(cellnum++, propuesta.getId(), row);
                excelService.addCell(cellnum++, propuesta.getNombre(), null, row);
                excelService.addCell(cellnum++, propuesta.getPlazas(), null, row);
                excelService.addCell(cellnum++, propuesta.getEspera(), null, row);
                excelService.addCell(cellnum++, propuesta.getCodigoAsignatura(), null, row);
                excelService.addCell(cellnum++, propuesta.getFechaCreacion().toString(), null, row);
                excelService.addCell(cellnum++, propuesta.getTutoresNombre(), null, row);
                excelService.addCell(cellnum++, propuesta.getAceptados(), null, row);
                excelService.addCell(cellnum++, propuesta.getEnEspera(), null, row);
                excelService.addCell(cellnum++, propuesta.getEjercicio(), null, row);
                excelService.addCell(cellnum++, limpiaHtml(propuesta.getObjetivos()), null, row);
                excelService.addCell(cellnum++, limpiaHtml(propuesta.getObservaciones()), null, row);
                excelService.addCell(cellnum++, limpiaHtml(propuesta.getDescripcion()), null, row);
                excelService.addCell(cellnum++, limpiaHtml(propuesta.getBibliografia()), null, row);
            }
            excelService.autoSizeColumns(10);
        }
        return excelService.getExcel();
    }

    public ByteArrayOutputStream generarPlantilla(String codigoAsignatura) throws IOException {
        WriterExcelService writerExcelService = new WriterExcelService();
        writerExcelService.addFulla(codigoAsignatura);


        Row resumen = writerExcelService.getNewRow(0);
        writerExcelService.addCell(0, "Asignatura: ", writerExcelService.getEstilNegreta(), resumen);
        writerExcelService.addCell(1, codigoAsignatura, null, resumen);
        resumen = writerExcelService.getNewRow(1);
        writerExcelService.addCell(0, "Titol", writerExcelService.getEstilNegreta(), resumen);
        writerExcelService.addCell(1, "Tutor correo", writerExcelService.getEstilNegreta(), resumen);
        writerExcelService.addCell(2, "Alumne correo", writerExcelService.getEstilNegreta(), resumen);
        writerExcelService.autoSizeColumns(3);


        return writerExcelService.getExcel();
    }

    public List<String> extraeCorreos(List<PropuestaAlumno> propuestaAlumnos) {
        return propuestaAlumnos.stream()
                .map(propuestaAlumno -> Arrays.asList(propuestaAlumno.getAlumnoEmail(), propuestaAlumno.getTutorEmail()))
                .flatMap(List::stream).distinct().
                collect(Collectors.toList());
    }

    public Map<String, List<PropuestaAlumno>> subirPropuestas(Long userId, String codigoAsignatura, InputStream documento) throws IOException, PropuestaException {
        ReaderExcelService readerExcelService = new ReaderExcelService(documento);
        Map<String, List<PropuestaAlumno>> listado = readerExcelService.extract();
        if (listado.get(codigoAsignatura).isEmpty()) {
            throw new PropuestaException("Plantilla incorrecta para " + codigoAsignatura);
        }
        List<String> correos = extraeCorreos(listado.get(codigoAsignatura));
        Map<String, PersonaDTO> personas = personasDAO.getPersonasAsignaturaByMails(codigoAsignatura, correos);
        crearMultiplesPropuestas(userId, codigoAsignatura, listado, personas);
        return listado;
    }

    public void crearMultiplesPropuestas(Long userId, String codigoAsignatura, Map<String, List<PropuestaAlumno>> listado, Map<String, PersonaDTO> personas) {
        listado.get(codigoAsignatura).stream().forEach(propuestaAlumno -> creaAsignaPropuesta(userId, codigoAsignatura, personas, propuestaAlumno));
    }

    public void creaAsignaPropuesta(Long userId, String codigoAsignatura, Map<String, PersonaDTO> personas, PropuestaAlumno propuestaAlumno) {
        PropuestaDTO propuesta = null;
        try {
            List<Long> tutores = getTutor(personas, propuestaAlumno);
            Long alumnoId = getAlumno(personas, propuestaAlumno);
            tienePropuestasAceptadas(alumnoId, codigoAsignatura);
            propuesta = createPropuestaMinima(propuestaAlumno.getTitulo(), tutores, codigoAsignatura, userId);
            creaAsignaciones(userId, alumnoId, propuesta);
            propuestaAlumno.setCreada(true);
        } catch (PropuestaTutoresException | PropuestaAlumnoException | PropuestaException e) {
            propuestaAlumno.setCreada(false);
            propuestaAlumno.setMensaje(e.getMessage());
        } catch (PropuestaAsignacionesException e) {
            propuestaAlumno.setCreada(false);
            propuestaAlumno.setMensaje(e.getMessage());
            deletePropuesta((int) propuesta.getId());
        }

    }

    private List<Long> getTutor(Map<String, PersonaDTO> personas, PropuestaAlumno propuestaAlumno) throws PropuestaTutoresException {
        try {
            return Collections.singletonList(personas.get(propuestaAlumno.getTutorEmail()).getId());
        } catch (Exception e) {
            throw new PropuestaTutoresException("Tutor incorrecto");
        }
    }

    private Long getAlumno(Map<String, PersonaDTO> personas, PropuestaAlumno propuestaAlumno) throws PropuestaAlumnoException {
        try {
            return personas.get(propuestaAlumno.getAlumnoEmail()).getId();
        } catch (Exception e) {
            throw new PropuestaAlumnoException("Alumno incorrecto");
        }
    }

    public void tienePropuestasAceptadas(Long alumnoId, String codigoAsignatura) throws PropuestaAlumnoException {
        if (propuestasDAO.tienePropuestasAceptadas(alumnoId, codigoAsignatura)) {
            throw new PropuestaAlumnoException("Propuestas aceptadas");
        }
    }

    public PropuestaDTO createPropuestaMinima(String name, List<Long> tutores, String codigoAsignatura, Long perIdCreador) throws PropuestaException {
        try {
            return propuestasDAO.createPropuestaMinima(name, tutores, codigoAsignatura, perIdCreador);
        } catch (Exception e) {
            throw new PropuestaException("Error al crear propuesta");
        }
    }

    private void creaAsignaciones(Long userId, Long alumnoId, PropuestaDTO propuesta) throws PropuestaAsignacionesException {
        try {
            propuestasDAO.creaAsignaciones(alumnoId, propuesta.getId(), userId, "", new Date());
        } catch (Exception e) {
            throw new PropuestaAsignacionesException("Error al crear propuesta");
        }
    }

    public Integer importarPropuestas(Long connectedUserId, String codigoAsignatura) throws PropuestaException {
        List<EstanciasPracticasVWDTO> estanciasPracticas = propuestasDAO.propuestaEstanciaPracticas(codigoAsignatura);
        try {
            estanciasPracticas.forEach(estancia -> {
                Long propuestaId = createPropuesta(estancia.getTitulo(), Collections.singletonList(estancia.getTutorId()), codigoAsignatura, estancia.getTareas(), estancia.getConocimientos(), null,
                        estancia.getObservaciones(), new BigDecimal(1), new BigDecimal(0), connectedUserId);
                propuestasDAO.creaAsignaciones(estancia.getAlumnoId(), propuestaId, connectedUserId, "", new Date());
            });
            return estanciasPracticas.size();
        } catch (Exception e) {
            throw new PropuestaException("Error al crear propuesta");
        }


    }
}
