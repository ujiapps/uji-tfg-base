package es.uji.apps.tfg.services;

import es.uji.apps.tfg.dao.AlumnosAsignadosDAO;
import es.uji.apps.tfg.dao.AsignaturasDAO;
import es.uji.apps.tfg.dao.PersonasDAO;
import es.uji.apps.tfg.dao.PropuestasDAO;
import es.uji.apps.tfg.dto.*;
import es.uji.apps.tfg.excepciones.AsignaturaNoConfiguradaException;
import es.uji.apps.tfg.model.AlumnosAsignados;
import es.uji.apps.tfg.model.AsignacionPlan;
import es.uji.apps.tfg.model.Profesor;
import es.uji.apps.tfg.services.rest.BaseResource;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.sso.User;
import es.uji.commons.sso.dao.ApaDAO;
import org.apache.poi.ss.usermodel.Row;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class UsuariosService extends BaseResource
{
    public final static Logger log = LoggerFactory.getLogger(UsuariosService.class);

    @Autowired
    private PersonasDAO personasDAO;
    @Autowired
    private ApaDAO apaDAO;
    @Autowired
    private AsignaturasDAO asignaturasDAO;

    @Autowired
    private PropuestasDAO propuestasDAO;

    @Autowired
    private AlumnosAsignadosDAO alumnosAsignadosDAO;


    @Autowired
    private NotificacionesService notificacionesService;

    public List<PersonaDTO> getUsuarios()
    {
        return personasDAO.getUsuarios();
    }

    public List<Profesor> getProfesoresByCoordinador(Long persona_id)
    {
        return personasDAO.getProfesoresByCoordinador(persona_id).stream().map(persona -> new Profesor(persona)).collect(Collectors.toList());
    }

    public Integer getCargaTotalProfesor(Long id)
    {
        return personasDAO.getCargaTotalProfesor(id);
    }

    public Integer getCargaProfesor(Long id, String codigo_asignatura)
    {
        return personasDAO.getCargaProfesor(id, codigo_asignatura);
    }

    public long setCargaProfesor(Long persona_id, String codigo_asignatura, int carga, long editor_id)
    {
        if (codigo_asignatura == null)
        {
            return 0;
        }
        PersonaDTO personaDTO = getUsuario(persona_id);
        if (personaDTO != null)
        {
            if (personasDAO.createCargaProfesor(persona_id, codigo_asignatura, carga, editor_id) != null)
            {
                return 1L;
            }
        }
        return 0;
    }

    public PersonaDTO getUsuario(Long userId)
    {
        return personasDAO.getUsuario(userId);
    }

    public boolean distribuirCargaProfesores(String codigo_asignatura, long editor_id)
    {
        return personasDAO.distribuirCargaProfesores(codigo_asignatura, editor_id);
    }

    public List<PersonaDTO> getEstudiantesByAsignatura(String codigo_asignatura)
    {
        return personasDAO.getEstudiantesByAsignatura(codigo_asignatura);
    }

    public List<AsignacionPlan> getPlanAsignacionesByAsignatura(String codigo_asignatura) throws AsignaturaNoConfiguradaException
    {
        AsignaturaDTO asignaturaDTO = asignaturasDAO.getAsignatura(codigo_asignatura);
        List<AsignacionPlan> salida = new ArrayList<AsignacionPlan>();
        if (asignaturaDTO.getTfgAsignaturasConfig() == null || asignaturaDTO.getTfgAsignaturasConfig().getOrdenacion() == null)
        {
            throw new AsignaturaNoConfiguradaException();
        }
        switch (asignaturaDTO.getTfgAsignaturasConfig().getOrdenacion())
        {
            case "NOTA_MEDIA":
                salida = AsignacionPlan.transformarLista(personasDAO.getPlanAsignacionesByNotaMedia(codigo_asignatura));
                break;
            case "NOTA_ACCESO":
                salida = AsignacionPlan.transformarLista(personasDAO.getPlanAsignacionesByNotaAcceso(codigo_asignatura));
                break;
            case "MANUAL":
                salida = AsignacionPlan.transformarLista(personasDAO.getPlanAsignacionesByNotaMedia(codigo_asignatura))
                        .stream().map(asignacionPlan -> {
                            asignacionPlan.setCriterio(null);
                            return asignacionPlan;
                        }).collect(Collectors.toList());

                break;

        }
        return salida;
    }

    public List<Date> getExcludedDays()
    {
        return personasDAO.getExcludedDays();
    }

    public boolean guardarPlanSinEnviarCorreo(List<AsignacionPlan> asignacionPlanes, String codigo_asignatura) {
        List<TurnoDTO> turnos = new ArrayList<TurnoDTO>();

        for (AsignacionPlan asignacionPlan : asignacionPlanes) {
            TurnoDTOPK turnoPK = new TurnoDTOPK();
            turnoPK.setCodigoAsignatura(codigo_asignatura);
            turnoPK.setPersonaId(asignacionPlan.getPersona_id());
            TurnoDTO turno = new TurnoDTO();
            turno.setId(turnoPK);
            turno.setFecha(getFechaConHoraAñadida(asignacionPlan.getFecha_apertura(), asignacionPlan.getHora_apertura()));
            turnos.add(turno);
        }

        try {
            personasDAO.updateAsignacionPlan(turnos, codigo_asignatura);
        } catch (Exception e) {
            log.error(e.getMessage(),e);
            return false;
        }
        return true;
    }

    public boolean updateAsignacionPlan(List<AsignacionPlan> asignacionPlanes, String codigo_asignatura)
    {
        List<TurnoDTO> turnos = new ArrayList<TurnoDTO>();

        HashMap<Long, PersonaDTO> personas = personasDAO.getMailEstudiantesByAsignatura(codigo_asignatura);
        AsignaturaDTO asignaturaDTO = asignaturasDAO.getAsignatura(codigo_asignatura);

        for (AsignacionPlan asignacionPlan : asignacionPlanes)
        {
            TurnoDTOPK turnoPK = new TurnoDTOPK();
            turnoPK.setCodigoAsignatura(codigo_asignatura);
            turnoPK.setPersonaId(asignacionPlan.getPersona_id());
            TurnoDTO turno = new TurnoDTO();
            turno.setId(turnoPK);
            turno.setFecha(getFechaConHoraAñadida(asignacionPlan.getFecha_apertura(), asignacionPlan.getHora_apertura()));
            turnos.add(turno);
        }
        try
        {
            String errores = "";
            if (personasDAO.updateAsignacionPlan(turnos, codigo_asignatura))
            {
                SimpleDateFormat fechaDateFormat = new SimpleDateFormat("dd-MM-yyyy' a les 'HH:mm");
                HashMap<String, String> mapa = new HashMap<String, String>();
                mapa.put("ASIGNATURA_NOMBRE", asignaturaDTO.getCodigo() + " - " + asignaturaDTO.getNombre());
                for (AsignacionPlan asignacionPlan : asignacionPlanes)
                {
                    PersonaDTO destinatario = personas.get(asignacionPlan.getPersona_id());
                    try
                    {
                        mapa.put("FECHA_TURNO",
                                fechaDateFormat.format(getFechaConHoraAñadida(asignacionPlan.getFecha_apertura(), asignacionPlan.getHora_apertura())));
                        notificacionesService.enviarMailAPersonas("tfg.message.turno.body", mapa, "tfg.message.turno.subject", Arrays.asList(destinatario));
                    }
                    catch (Exception e)
                    {
                        errores += "\n" + destinatario.getNombreCompleto() + "<" + destinatario.getEmail() + ">";
                    }
                }
                //Se elimina las notificaciones a coordinadores.
//                if (errores.length() > 0)
//                {
//                    try
//                    {
//                        notificacionesService.notificarErrorACoordinadores(codigo_asignatura, errores, NotificacionesService.getNotificadorPorDefecto());
//                    }
//                    catch (Exception e)
//                    {
//                        //esto suena fatal, pero como la actualizacion ha sido correcta, devolvemos que se ha hecho bien y ya registramos el error
//                        //del envio de mail por otra parte
//                        return true;
//                    }
//                }
            }

        }
        catch (Exception e)
        {
            log.error(e.getMessage(),e);
            return false;
        }
        return true;
    }

    private Timestamp getFechaConHoraAñadida(Timestamp fecha, Timestamp hora)
    {
        Long total = fecha.getTime();
        SimpleDateFormat minutos = new SimpleDateFormat("mm");
        SimpleDateFormat horas = new SimpleDateFormat("HH");
        String horasStr = horas.format(hora.getTime());
        String minutoStr = minutos.format(hora.getTime());
        total += (new Long(minutoStr) * 60 * 1000);
        total += (new Long(horasStr) * 3600 * 1000);
        return new Timestamp(total);
    }

    public TurnoDTO getTurno(long persona_id, String codigo_asignatura)
    {
        return personasDAO.getTurno(persona_id, codigo_asignatura);
    }

    public CargaProfesorDTO getCargaProfesorDTO(Long id, String codigo_asignatura)
    {
        return personasDAO.getCargaProfesorDTO(id, codigo_asignatura);
    }

    public Integer getCargaAcumulada(String codigo_asignatura, Long persona_id)
    {
        return propuestasDAO.getCargaAcumulada(codigo_asignatura, persona_id);
    }

    public boolean corregirBajas()
    {
        return true;
        // return personasDAO.corregirBajas();
    }

    public List<Profesor> getProfesores(String codigo_asignatura, User user)
    {
        boolean isAdmin = this.hasPerfil("ADMIN", user.getId());
        boolean isCoord = this.hasPerfil("COORDINADOR", user.getId());
//        List<Profesor> salida = new ArrayList<Profesor>();
        List<Profesor> listado = new ArrayList<>();

        if (isAdmin || (isCoord && this.coordinaAsignatura(user.getId(), codigo_asignatura)))
        {
            listado = this.getProfesoresByAsignatura(codigo_asignatura);
        }
        else
        {
            listado.add(this.getUsuarioProfesor(user.getId()));
        }
//        for (PersonaDTO personaDTO : listado)
//        {
//            salida.add(Profesor.fromDTO(personaDTO, codigo_asignatura));
//        }
        return listado;
    }

    public List<Profesor> getProfesoresByAsignatura(String codigo_asignatura) {
        return personasDAO.getProfesoresByAsignatura(codigo_asignatura);
    }

    public List<Profesor> getProfesoresByAsignaturaCursoPasado(String codigo_asignatura) {
        return personasDAO.getProfesoresByAsignaturaPasado(codigo_asignatura);
    }

    public boolean hasPerfil(String perfil, Long personaId) {
        boolean hasPerfil = apaDAO.hasPerfil("TFG", perfil, personaId);

        return hasPerfil;
    }

    public boolean hasPerfiles(String[] perfiles, Long personaId) {
        boolean hasPerfil = apaDAO.hasPerfil("TFG", perfiles, personaId);

        return hasPerfil;
    }

    public boolean coordinaAsignatura(long persona_id, String codigo_asignatura) {
        return personasDAO.coordinaAsignatura(persona_id, codigo_asignatura);
    }

    public Profesor getUsuarioProfesor(Long id) {
        return personasDAO.getUsuarioProfesor(id);
    }

    public ByteArrayOutputStream getExcel(List<Profesor> profesores) throws IOException {


        WriterExcelService writerExcelService = new WriterExcelService();

        int rownum = 0;
        writerExcelService.addFulla("Carrega de treball");
        writerExcelService.generaCabecera("Nom", "1r.Cognom", "2n.Cognom", "Càrrega", "Crèdits");
        for (Profesor profesor : profesores) {
            int cellnum = 0;
            Row row = writerExcelService.getNewRow(++rownum);
            writerExcelService.addCell(cellnum++, profesor.getNombre(), null, row);
            writerExcelService.addCell(cellnum++, profesor.getApellido1(), null, row);
            writerExcelService.addCell(cellnum++, profesor.getApellido2(), null, row);
            writerExcelService.addCell(cellnum++, (profesor.getCarga() != null) ? profesor.getCarga().toString() : "", null, row);
            writerExcelService.addCell(cellnum++, profesor.getCreditos(), row);
        }
        return writerExcelService.getExcel();
    }

    public List<AlumnosAsignados> getAlumnosasignadosByAsignatura(String codigo_asignatura, Long userId, Integer anyo) {
        return alumnosAsignadosDAO.getAlumnosasignadosByAsignatura(codigo_asignatura, userId, anyo).stream().map(a -> new AlumnosAsignados(a)).collect(Collectors.toList());
    }

    public ByteArrayOutputStream getExcelAlumnosAsignados(Long userId, String codigoAsignatura, Integer ejercicio) throws IOException {
        List<AlumnosAsignadosDTO> alumnosAsignados = alumnosAsignadosDAO.getAlumnosasignadosByAsignatura(codigoAsignatura, userId, ejercicio);

        if (ParamUtils.isNotNull(alumnosAsignados)) {
            WriterExcelService writerExcelService = new WriterExcelService();
            cabeceraAlumnosAsignados(writerExcelService, alumnosAsignados.get(0));
            cuerpoAlumnosAsignados(writerExcelService, alumnosAsignados);
            writerExcelService.autoSizeColumns(5);
            return writerExcelService.getExcel();
        } else
            return null;
    }

    private void cuerpoAlumnosAsignados(WriterExcelService writerExcelService, List<AlumnosAsignadosDTO> alumnosAsignados) {
        Integer rownum = 2;
        for (AlumnosAsignadosDTO alumnosAsignado : alumnosAsignados) {
            int cellnum = 0;
            Row row = writerExcelService.getNewRow(++rownum);
            writerExcelService.addCell(cellnum++, alumnosAsignado.getNombre(), null, row);
            writerExcelService.addCell(cellnum++, alumnosAsignado.getTutoreNombre(), null, row);
            writerExcelService.addCell(cellnum++, alumnosAsignado.getNombreCompleto(), null, row);
            writerExcelService.addCell(cellnum++, alumnosAsignado.getEmail(), null, row);
            writerExcelService.addCell(cellnum++, alumnosAsignado.getNota(), null, row);
        }
    }

    private void cabeceraAlumnosAsignados(WriterExcelService writerExcelService, AlumnosAsignadosDTO asignadosDTO) {
        Integer rownum = 0;
        writerExcelService.addFulla(asignadosDTO.getCodigoAsignatura());

        Row resumen = writerExcelService.getNewRow(rownum);
        writerExcelService.addCell(0, "Propuestes assignades a alumnes de ".concat(asignadosDTO.getCodigoAsignatura()).concat(" CURS ".concat(String.valueOf(asignadosDTO.getEjercicio()))), writerExcelService.getEstilNegreta(), resumen);
        writerExcelService.addMergedRegion(rownum, rownum, 0, 6);

        String[] columnas = new String[]{"Titol", "Tutors", "Nombre", "Correu", "Nota"};
        writerExcelService.generaCeldes(writerExcelService.getEstilNegreta(), 2, columnas);

    }

    public boolean coordinaAsignatura(Long userId, String codigoAsignatura, Long cursoAca) {
        return personasDAO.coordinaAsignatura(userId, codigoAsignatura, cursoAca);
    }
}
