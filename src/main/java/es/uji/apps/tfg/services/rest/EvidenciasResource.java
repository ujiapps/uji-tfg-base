package es.uji.apps.tfg.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.tfg.excepciones.EvidenciasException;
import es.uji.apps.tfg.services.EvidenciasService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.sso.AccessManager;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

@Path("evidencias")
public class EvidenciasResource extends CoreBaseService {
    @InjectParam
    private EvidenciasService evidenciasService;

    @GET
    @Path("{codigoAsignatura}/{ejercicio}")
    @Produces("application/vnd.ms-excel")
    public Response generarEvidencias(@PathParam("codigoAsignatura") String codigoAsignatura, @PathParam("ejercicio") Integer ejercicio) throws IOException, EvidenciasException {
        ParamUtils.checkNotNull(codigoAsignatura, ejercicio);
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        ByteArrayOutputStream ostream = evidenciasService.generateEvidencias(connectedUserId, codigoAsignatura, ejercicio);

        if (ostream != null) {
            return Response.ok(ostream.toByteArray(), MediaType.APPLICATION_OCTET_STREAM).header("content-disposition", "attachment; filename = Evidencias-" + codigoAsignatura + ".xls").build();
        }
        throw new EvidenciasException("Sense dades");
    }
}
