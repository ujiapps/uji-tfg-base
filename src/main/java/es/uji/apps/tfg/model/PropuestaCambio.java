package es.uji.apps.tfg.model;

import com.mysema.query.annotations.QueryProjection;
import es.uji.commons.rest.ParamUtils;

import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class PropuestaCambio {
    private long id;
    private String fechaCreacion;
    private String nombre;
    private String asignatura;
    private String codigo_asignatura;
    private String objetivos;
    private String bibliografia;
    private String descripcion;
    private String observaciones;
    private HashMap<Long, String> comentarios;
    private int plazas;
    private int espera;
    private int disponibilidad;// -1 Completa 0 Disponible 1 Solo disponible en espera
    private int gente_esperando;
    private int plazas_disponibles;
    private int ejercicio;
    private List<Long> tutores;
    private String tutoresNombre;
    private Integer aceptados;

    private Long alumnoId;

    private String alumnoNombre;

    public PropuestaCambio() {
    }

    @QueryProjection
    public PropuestaCambio(Long id,
                           Date fechaCreacion,
                           String nombre,
                           String codigoAsignatura,
                           Integer plazas,
                           Integer espera,
                           Integer ejercicio,
                           String tutores,
                           String tutoresNombre,
                           Integer enEspera,
                           Integer aceptados,
                           Long alumnoId,
                           String alumnoNombre) {
        this.id = id;
        this.fechaCreacion = fechaCreacion.toString();
        this.nombre = nombre;
        this.codigo_asignatura = codigoAsignatura;
        this.plazas = plazas;
        this.espera = espera;
        this.ejercicio = ejercicio;
        this.tutores = ParamUtils.isNotNull(tutores) ? Arrays.stream(tutores.split(",")).map(Long::parseLong).collect(Collectors.toList()) : null;
        this.tutoresNombre = tutoresNombre;
        this.gente_esperando = enEspera;
        this.aceptados = aceptados;
        this.plazas_disponibles = plazas - aceptados;
        this.disponibilidad = calculaDisponibilidad();
        this.alumnoId = alumnoId;
        this.alumnoNombre = alumnoNombre;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(String fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getAsignatura() {
        return asignatura;
    }

    public void setAsignatura(String asignatura) {
        this.asignatura = asignatura;
    }

    public int getPlazas() {
        return plazas;
    }

    public void setPlazas(int plazas) {
        this.plazas = plazas;
    }

    public int getEspera() {
        return espera;
    }

    public void setEspera(int espera) {
        this.espera = espera;
    }

    public int getDisponibilidad() {
        return disponibilidad;
    }

    public void setDisponibilidad(int disponibilidad) {
        this.disponibilidad = disponibilidad;
    }

    public String getObjetivos() {
        return objetivos;
    }

    public void setObjetivos(String objetivos) {
        this.objetivos = objetivos;
    }

    public String getBibliografia() {
        return bibliografia;
    }

    public void setBibliografia(String bibliografia) {
        this.bibliografia = bibliografia;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public int getGente_esperando() {
        return gente_esperando;
    }

    public void setGente_esperando(int gente_esperando) {
        this.gente_esperando = gente_esperando;
    }

    public int getPlazas_disponibles() {
        return plazas_disponibles;
    }

    public void setPlazas_disponibles(int plazas_disponibles) {
        this.plazas_disponibles = plazas_disponibles;
    }

    public String getCodigo_asignatura() {
        return codigo_asignatura;
    }

    public void setCodigo_asignatura(String codigo_asignatura) {
        this.codigo_asignatura = codigo_asignatura;
    }

    public void addComentario(Long persona_id, String comentarios) {
        if (this.comentarios == null || this.comentarios.size() == 0) {
            this.comentarios = new HashMap<Long, String>();
        }
        this.comentarios.put(persona_id, comentarios);
    }

    public String getComentario(Long persona_id) {
        if (this.comentarios != null && this.comentarios.size() > 0) {
            return this.comentarios.get(persona_id);
        }
        return null;
    }

    public HashMap<Long, String> getComentarios() {
        return comentarios;
    }

    public void setComentarios(HashMap<Long, String> comentarios) {
        this.comentarios = comentarios;
    }

    public int getEjercicio() {
        return ejercicio;
    }

    public void setEjercicio(int ejercicio) {
        this.ejercicio = ejercicio;
    }

    public List<Long> getTutores() {
        return tutores;
    }

    public void setTutores(List<Long> tutores) {
        this.tutores = tutores;
    }

    public String getTutoresNombre() {
        return tutoresNombre;
    }

    public void setTutoresNombre(String tutoresNombre) {
        this.tutoresNombre = tutoresNombre;
    }

    public Integer getAceptados() {
        return aceptados;
    }

    public void setAceptados(Integer aceptados) {
        this.aceptados = aceptados;
    }

    public Long getAlumnoId() {
        return alumnoId;
    }

    public void setAlumnoId(Long alumnoId) {
        this.alumnoId = alumnoId;
    }

    public String getAlumnoNombre() {
        return alumnoNombre;
    }

    public void setAlumnoNombre(String alumnoNombre) {
        this.alumnoNombre = alumnoNombre;
    }

    private int calculaDisponibilidad() {
        if (this.aceptados >= this.plazas) {
            if (this.gente_esperando >= this.espera) {
                return -1;// Completo
            } else {
                return 1;// Disponible en espera
            }
        } else if (this.plazas > 0) {
            return 0;
        } else {
            return this.espera > 0 ? 1 : -1;
        }
    }
}
