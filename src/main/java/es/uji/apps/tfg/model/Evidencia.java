package es.uji.apps.tfg.model;

import java.util.ArrayList;
import java.util.List;

public class Evidencia {

    private String tipoEstudio;
    private String nombreEstudio;
    private List<Long> evindencias;
    private String asignatura;

    public String getTipoEstudio() {
        return tipoEstudio;
    }

    public void setTipoEstudio(String tipoEstudio) {
        this.tipoEstudio = tipoEstudio;
    }

    public String getNombreEstudio() {
        return nombreEstudio;
    }

    public void setNombreEstudio(String nombreEstudio) {
        this.nombreEstudio = nombreEstudio;
    }

    public List<Long> getEvindencias() {
        return evindencias;
    }

    public void setEvindencias(ArrayList<Long> evindencias) {
        this.evindencias = evindencias;
    }

    public void setAsignatura(String asignatura) {
        this.asignatura = asignatura;
    }

    public String getAsignatura() {
        return asignatura;
    }
}
