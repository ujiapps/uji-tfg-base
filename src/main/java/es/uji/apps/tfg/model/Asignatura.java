package es.uji.apps.tfg.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import es.uji.apps.tfg.dto.AsignaturaDTO;
import es.uji.apps.tfg.dto.AsignaturasConfigDTO;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Asignatura {
    private String codigo;
    private String nombre;
    private String codinom;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy HH:mm:ss", timezone = "Europe/Madrid")
    private Date fechaLimite;
    private String ordenacion;
    private Boolean espera_habilitada;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy HH:mm:ss", timezone = "Europe/Madrid")
    private Date permitirSolicitudes;

    private Boolean notificarCoordinador;

    public Asignatura() {

    }

    public Asignatura(AsignaturaDTO asig) {
        setNombre(asig.getNombre());
        setCodigo(asig.getCodigo());
        setCodinom(asig.getCodigo() + " - " + asig.getNombre());
        if (asig.getTfgAsignaturasConfig() != null) {
            if (asig.getTfgAsignaturasConfig().getFechaLimite() != null) {
                setFechaLimite(asig.getTfgAsignaturasConfig().getFechaLimite());
            }
            if (asig.getTfgAsignaturasConfig().getOrdenacion() != null) {
                setOrdenacion(asig.getTfgAsignaturasConfig().getOrdenacion());
            } else {
                setOrdenacion("NOTA_MEDIA");//DEFAULT
            }
            if (asig.getTfgAsignaturasConfig().isHabilitar_espera() != null) {
                setEspera_habilitada(asig.getTfgAsignaturasConfig().isHabilitar_espera());
            } else {
                setEspera_habilitada(true);//DEFAULT
            }
            if (asig.getTfgAsignaturasConfig().getPermitirSolicitudes() != null) {
                setPermitirSolicitudes(asig.getTfgAsignaturasConfig().getPermitirSolicitudes());
            }
            setNotificarCoordinador(asig.getTfgAsignaturasConfig().isNotificarCoordinador());
        }
        else {
            asig.setTfgAsignaturasConfig(new AsignaturasConfigDTO(asig.getCodigo(), asig.getEjercicio()));
        }
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getCodigo()
    {
        return codigo;
    }

    public void setCodigo(String codigo)
    {
        this.codigo = codigo;
    }

    public String getCodinom()
    {
        return codinom;
    }

    public void setCodinom(String codinom)
    {
        this.codinom = codinom;
    }

    public static List<Asignatura> transformaListaDTO(List<AsignaturaDTO> asignaturas)
    {
        List<Asignatura> resultado = new ArrayList<Asignatura>();

        for (AsignaturaDTO asig : asignaturas)
        {
            resultado.add(new Asignatura(asig));
        }
        return resultado;
    }

    public Date getFechaLimite()
    {
        return fechaLimite;
    }

    public void setFechaLimite(Date fecha_limite)
    {
        this.fechaLimite = fecha_limite;
    }

    public String getOrdenacion()
    {
        return ordenacion;
    }

    public void setOrdenacion(String ordenacion)
    {
        this.ordenacion = ordenacion;
    }

    public Boolean isEspera_habilitada() {
        return espera_habilitada;
    }

    public void setEspera_habilitada(boolean espera_habilitada) {
        this.espera_habilitada = espera_habilitada;
    }

    public Date getPermitirSolicitudes() {
        return permitirSolicitudes;
    }

    public void setPermitirSolicitudes(Date permitirSolicitudes) {
        this.permitirSolicitudes = permitirSolicitudes;
    }

    public void setNotificarCoordinador(Boolean notificarCoordinador) {
        this.notificarCoordinador = notificarCoordinador;
    }

    public Boolean isNotificarCoordinador() {
        return notificarCoordinador;
    }
}
