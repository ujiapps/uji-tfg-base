package es.uji.apps.tfg.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class AsignacionPlan
{
    private Long persona_id;
    private String nombre;
    private String apellido1;
    private String apellido2;    
    private String email;
    private String codigo_asignatura;
    private String criterio;
    private String titulado;
    private Timestamp fecha_apertura;
    private Timestamp hora_apertura;

    public AsignacionPlan()
    {

    }
    
    public AsignacionPlan(Object[] ob)
    {
        setPersona_id(new BigDecimal(ob[0].toString()).longValue());
        setNombre((String) ob[1]);
        setApellido1((String) ob[2]);
        setApellido2((String) ob[3]);
        setEmail((String) ob[4]);
        setCodigo_asignatura(ob[5].toString());
        setCriterio(ob[6].toString());
        setTitulado(ob[7].toString());
        Timestamp milisegundos = (Timestamp) ob[8];
        if (milisegundos != null) {            
            setFecha_apertura(new Timestamp(milisegundos.getTime()));   
            setHora_apertura(new Timestamp(milisegundos.getTime()));
        }
    }

    public Long getPersona_id()
    {
        return persona_id;
    }

    public void setPersona_id(Long persona_id)
    {
        this.persona_id = persona_id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getCriterio()
    {
        return criterio;
    }

    public void setCriterio(String criterio)
    {
        this.criterio = criterio;
    }

    public String getTitulado() {
        return titulado;
    }

    public void setTitulado(String titulado) {
        this.titulado = titulado;
    }

    public Timestamp getFecha_apertura()
    {
        return fecha_apertura;
    }

    public void setFecha_apertura(Timestamp fecha_apertura)
    {
        this.fecha_apertura = fecha_apertura;
    }

    public static List<AsignacionPlan> transformarLista(List<Object[]> planAsignacionesByCriterio)
    {
        List<AsignacionPlan> lista = new ArrayList<AsignacionPlan>();
        for (Object[] ob : planAsignacionesByCriterio)
        {
            if (ob != null)
            {
                lista.add(new AsignacionPlan(ob));
            }
        }
        return lista;
    }

    public String getCodigo_asignatura()
    {
        return codigo_asignatura;
    }

    public void setCodigo_asignatura(String codigo_asignatura)
    {
        this.codigo_asignatura = codigo_asignatura;
    }

    public String getApellido1()
    {
        return apellido1;
    }

    public void setApellido1(String apellido1)
    {
        this.apellido1 = apellido1;
    }

    public String getApellido2()
    {
        return apellido2;
    }

    public void setApellido2(String apellido2)
    {
        this.apellido2 = apellido2;
    }

    public Timestamp getHora_apertura()
    {
        return hora_apertura;
    }

    public void setHora_apertura(Timestamp hora_apertura)
    {
        this.hora_apertura = hora_apertura;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }
}