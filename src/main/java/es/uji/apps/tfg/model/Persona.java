package es.uji.apps.tfg.model;

import java.util.ArrayList;
import java.util.List;

import es.uji.apps.tfg.dto.PersonaDTO;

public class Persona
{
    private Long id;
    private String nombre;
    private String apellido1;
    private String apellido2;

    public Persona()
    {

    }

    public Persona(PersonaDTO personaDTO)
    {
        this.setId(personaDTO.getId());
        this.setNombre(personaDTO.getNombre());
        this.setApellido1(personaDTO.getApellido1());
        this.setApellido2(personaDTO.getApellido2());
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public static List<Persona> transformarLista(List<PersonaDTO> personasDTO)
    {
        List<Persona> salida = new ArrayList<Persona>();
        for (PersonaDTO personaDTO : personasDTO)
        {
            Persona persona = new Persona(personaDTO);
            salida.add(persona);
        }
        return salida;
    }

    public String getApellido1()
    {
        return apellido1;
    }

    public void setApellido1(String apellido1)
    {
        this.apellido1 = apellido1;
    }

    public String getApellido2()
    {
        return apellido2;
    }

    public void setApellido2(String apellido2)
    {
        this.apellido2 = apellido2;
    }

}
