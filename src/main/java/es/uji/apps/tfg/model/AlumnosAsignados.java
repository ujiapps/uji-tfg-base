package es.uji.apps.tfg.model;

import es.uji.apps.tfg.dto.AlumnosAsignadosDTO;

public class AlumnosAsignados
{
    private Long id;
    private String nombre;
    private Long personaId;
    private String nombreCompleto;
    private String email;
    private Integer ejercicio;
    private String codigoAsignatura;
    private String tutoresNombre;
    private String nota;


    public AlumnosAsignados(){

    }

    public AlumnosAsignados(AlumnosAsignadosDTO alumnosAsignadosDTO)
    {
        this.id = alumnosAsignadosDTO.getId();
        this.nombre = alumnosAsignadosDTO.getNombre();
        this.personaId = alumnosAsignadosDTO.getPersonaId();
        this.nombreCompleto = alumnosAsignadosDTO.getNombreCompleto();
        this.email = alumnosAsignadosDTO.getEmail();
        this.ejercicio = alumnosAsignadosDTO.getEjercicio();
        this.codigoAsignatura = alumnosAsignadosDTO.getCodigoAsignatura();
        this.tutoresNombre = alumnosAsignadosDTO.getTutoreNombre();
        this.nota = alumnosAsignadosDTO.getNota();
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public Long getPersonaId()
    {
        return personaId;
    }

    public void setPersonaId(Long personaId)
    {
        this.personaId = personaId;
    }

    public String getNombreCompleto()
    {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto)
    {
        this.nombreCompleto = nombreCompleto;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public Integer getEjercicio()
    {
        return ejercicio;
    }

    public void setEjercicio(Integer ejercicio)
    {
        this.ejercicio = ejercicio;
    }

    public String getCodigoAsignatura() {
        return codigoAsignatura;
    }

    public void setCodigoAsignatura(String codigoAsignatura) {
        this.codigoAsignatura = codigoAsignatura;
    }

    public String getTutoresNombre() {
        return tutoresNombre;
    }

    public void setTutoresNombre(String tutoresNombre) {
        this.tutoresNombre = tutoresNombre;
    }

    public String getNota() {
        return nota;
    }

    public void setNota(String nota) {
        this.nota = nota;
    }
}