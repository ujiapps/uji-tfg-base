package es.uji.apps.tfg.model;

import com.mysema.query.annotations.QueryProjection;
import es.uji.commons.rest.ParamUtils;

public class TribunalAlumno {

    private Long id;
    private Long tribunalId;
    private Long alumnoId;
    private String alumnoNombreCompleto;

    public TribunalAlumno() {
    }

    @QueryProjection
    public TribunalAlumno(Long id, Long tribunalId, Long alumnoId, String nombre, String apellido1) {
        this.id = id;
        this.tribunalId = tribunalId;
        this.alumnoId = alumnoId;
        this.alumnoNombreCompleto = ParamUtils.isNotNull(nombre) ? nombre.concat(" ").concat(apellido1) : "";
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTribunalId() {
        return tribunalId;
    }

    public void setTribunalId(Long tribunalId) {
        this.tribunalId = tribunalId;
    }

    public Long getAlumnoId() {
        return alumnoId;
    }

    public void setAlumnoId(Long alumnoId) {
        this.alumnoId = alumnoId;
    }

    public String getAlumnoNombreCompleto() {
        return alumnoNombreCompleto;
    }

    public void setAlumnoNombreCompleto(String alumnoNombreCompleto) {
        this.alumnoNombreCompleto = alumnoNombreCompleto;
    }
}
