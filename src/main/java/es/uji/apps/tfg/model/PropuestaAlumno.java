package es.uji.apps.tfg.model;

public class PropuestaAlumno {
    private String titulo;
    private String tutorEmail;
    private String alumnoEmail;
    private Boolean creada;
    private String mensaje;

    public PropuestaAlumno(String titulo, String tutorEmail, String alumnoEmail) {
        this.titulo = titulo;
        this.tutorEmail = tutorEmail;
        this.alumnoEmail = alumnoEmail;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getTutorEmail() {
        return tutorEmail;
    }

    public void setTutorEmail(String tutorEmail) {
        this.tutorEmail = tutorEmail;
    }

    public String getAlumnoEmail() {
        return alumnoEmail;
    }

    public void setAlumnoEmail(String alumnoEmail) {
        this.alumnoEmail = alumnoEmail;
    }

    public Boolean getCreada() {
        return creada;
    }

    public void setCreada(Boolean creada) {
        this.creada = creada;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }
}
