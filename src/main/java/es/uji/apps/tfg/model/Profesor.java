package es.uji.apps.tfg.model;

import com.mysema.query.annotations.QueryProjection;
import es.uji.apps.tfg.dto.PersonaDTO;

import java.math.BigDecimal;

public class Profesor
{
    private Long id;
    private String nombre;
    private String apellido1;
    private String apellido2;
    private Float creditos;
    private Integer carga;
    private Integer oferta;

    public Profesor()
    {

    }

//    public Profesor(PersonaDTO personaDTO, Integer carga, Float creditos)
//    {
//        this.setId(personaDTO.getId());
//        this.setNombre(personaDTO.getNombre());
//        this.setApellido1(personaDTO.getApellido1());
//        this.setApellido2(personaDTO.getApellido2());
//        this.setCarga(carga);
//        this.setCreditos(creditos);
//    }
//
    public Profesor(PersonaDTO personaDTO)
    {
        this.setId(personaDTO.getId());
        this.setNombre(personaDTO.getNombre());
        this.setApellido1(personaDTO.getApellido1());
        this.setApellido2(personaDTO.getApellido2());
        this.setCarga(0);
        this.setCreditos(0F);
    }

    @QueryProjection
    public Profesor(Long id, String nombre, String apellido1, String apellido2, BigDecimal carga, BigDecimal creditos)
    {
        this.setId(id);
        this.setNombre(nombre);
        this.setApellido1(apellido1);
        this.setApellido2(apellido2);
        if(carga!=null) {
            this.setCarga(carga.intValue());
        } else {
            this.setCarga(0);
        }
        if (creditos != null) {
            this.setCreditos(creditos.floatValue());
        } else {
            this.setCreditos(0F);
        }
    }

    @QueryProjection
    public Profesor(Long id, String nombre, String apellido1, String apellido2, BigDecimal carga, BigDecimal creditos, BigDecimal oferta) {
        this.setId(id);
        this.setNombre(nombre);
        this.setApellido1(apellido1);
        this.setApellido2(apellido2);
        if (carga != null) {
            this.setCarga(carga.intValue());
        } else {
            this.setCarga(0);
        }
        if (creditos != null) {
            this.setCreditos(creditos.floatValue());
        } else {
            this.setCreditos(0F);
        }
        this.oferta = oferta.intValue();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido1() {
        return apellido1;
    }

    public void setApellido1(String apellido1) {
        this.apellido1 = apellido1;
    }

    public String getApellido2() {
        return apellido2;
    }

    public void setApellido2(String apellido2) {
        this.apellido2 = apellido2;
    }

    public Integer getCarga()
    {
        return carga;
    }

    public void setCarga(Integer carga)
    {
        this.carga = carga;
    }

    public Float getCreditos()
    {
        return creditos;
    }

    public void setCreditos(Float creditos)
    {
        this.creditos = creditos;
    }

    public Integer getOferta()
    {
        return oferta;
    }

    public void setOferta(Integer oferta)
    {
        this.oferta = oferta;
    }

}
