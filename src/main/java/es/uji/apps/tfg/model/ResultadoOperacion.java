package es.uji.apps.tfg.model;

public class ResultadoOperacion
{

    private boolean success;
    private Object datos;
    
    public ResultadoOperacion(boolean resultado, Object datos)
    {
        this.setSuccess(resultado);
        this.setDatos(datos);
    }

    public boolean isSuccess()
    {
        return success;
    }

    public void setSuccess(boolean success)
    {
        this.success = success;
    }

    public Object getDatos()
    {
        return datos;
    }

    public void setDatos(Object datos)
    {
        this.datos = datos;
    }

}
