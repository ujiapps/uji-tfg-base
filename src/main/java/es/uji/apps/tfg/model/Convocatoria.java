package es.uji.apps.tfg.model;

import es.uji.apps.tfg.dto.ConvocatoriaFechaDTO;
import es.uji.apps.tfg.utils.AcademicYear;
import es.uji.commons.rest.UIEntity;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class Convocatoria {

    private Long id;
    private String asignaturaId;
    private Integer cursoAcademico;
    private Date fechaInicio;
    private Date fechaFin;

    public Convocatoria(ConvocatoriaFechaDTO convocatoriaFechas) {
        this.id = convocatoriaFechas.getId();
        this.asignaturaId = convocatoriaFechas.getAsignaturaId();
        this.cursoAcademico = convocatoriaFechas.getCursoAcademico();
        this.fechaInicio = convocatoriaFechas.getFechaInicio();
        this.fechaFin = convocatoriaFechas.getFechaFin();
    }

    public Convocatoria() {

    }

    public Convocatoria(UIEntity uiEntity) {
        this.id = uiEntity.getLong("id");
        this.asignaturaId = uiEntity.get("asignaturaId");
        this.cursoAcademico = Integer.valueOf(uiEntity.get("cursoAcademico"));
        this.fechaInicio = uiEntity.getDate("fechaInicio");
        this.fechaFin = uiEntity.getDate("fechaFin");
    }

    public Convocatoria(String codigoAsignatura, Date fechaInicio, Date fechaFin) {
        this.cursoAcademico = AcademicYear.getCurrent();
        this.asignaturaId = codigoAsignatura;
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public String getAsignaturaId() {
        return asignaturaId;
    }

    public void setAsignaturaId(String asignaturaId) {
        this.asignaturaId = asignaturaId;
    }

    public Integer getCursoAcademico() {
        return cursoAcademico;
    }

    public void setCursoAcademico(Integer cursoAcademico) {
        this.cursoAcademico = cursoAcademico;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public static List<Convocatoria> transformaListaDTO(List<ConvocatoriaFechaDTO> convocatoriaFechas) {
        return convocatoriaFechas.stream().map(convocatoriaFechaDTO -> new Convocatoria(convocatoriaFechaDTO)).collect(Collectors.toList());
    }


}
