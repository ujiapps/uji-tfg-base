package es.uji.apps.tfg.model;

public enum TipoEstudio {

    G("GRADO", "TFG"), M("MASTER", "TFM");

    String value;
    String codigo;

    TipoEstudio(String value, String codigo) {
        this.value = value;
        this.codigo = codigo;
    }

    public String getValue() {
        return value;
    }

    public String getCodigo() {
        return codigo;
    }

    public static TipoEstudio get(String tipo) {
        if (tipo.equals("M")) {
            return TipoEstudio.M;

        } else
            return TipoEstudio.G;
    }
}
