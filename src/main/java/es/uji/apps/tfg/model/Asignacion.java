package es.uji.apps.tfg.model;

import com.mysema.query.annotations.QueryProjection;
import es.uji.apps.tfg.dto.AsignaturaDTO;
import es.uji.apps.tfg.dto.PersonasPropuestaDTO;
import es.uji.apps.tfg.dto.PropuestaDTO;
import es.uji.apps.tfg.utils.DateUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Asignacion {
    private Long persona_id;
    private String nombre;
    private String apellido1;
    private String apellido2;
    private String codigo_asignatura;
    private String asignatura;
    private Long propuesta_id;
    private String propuesta_nombre;
    private String PropuestaNombre;
    private String PropuestaPersonaNombre;
    private Long editor_persona_id;
    private String editor_nombre;
    private String editor_apellido1;
    private String editor_apellido2;
    private Date fecha_edicion;
    private Integer aceptada;
    private String comentarios;
    private String propuesta_aceptada;

    public Asignacion() {

    }

    @QueryProjection
    public Asignacion(Long personaId,
                      String nombre,
                      String apellido1,
                      String apellido2,
                      Long propuestaId,
                      String propuestaNombre,
                      String codigoAsignatura,
                      String tutoresNombre,
                      Integer aceptada,
                      String comentarios,
                      Date fechaEdicion
    ) {

        this.persona_id = personaId;
        this.nombre = nombre;
        this.apellido1 = apellido1;
        this.apellido2 = apellido2;
        this.codigo_asignatura = codigoAsignatura;
        this.asignatura = "";
        this.propuesta_id = propuestaId;
        this.PropuestaPersonaNombre = tutoresNombre;
        this.PropuestaNombre = propuestaNombre;
        this.propuesta_nombre = propuestaNombre;
        this.propuesta_aceptada = propuestaNombre;
        this.fecha_edicion = fechaEdicion;
        this.aceptada = aceptada;
        this.comentarios = comentarios;

    }


    public Asignacion(PersonasPropuestaDTO personasPropuestaDTO, AsignaturaDTO asignaturaDTO) {
        setPersona_id(personasPropuestaDTO.getTfgExtPersona().getId());
        setNombre(personasPropuestaDTO.getTfgExtPersona().getNombre());
        setApellido1(personasPropuestaDTO.getTfgExtPersona().getApellido1());
        setApellido2(personasPropuestaDTO.getTfgExtPersona().getApellido2());
        setCodigo_asignatura(asignaturaDTO.getCodigo());
        setAsignatura(asignaturaDTO.getNombre());
        if (personasPropuestaDTO.getTfgPropuesta() != null) {
            PropuestaDTO propuestaDTO = personasPropuestaDTO.getTfgPropuesta();
            setPropuesta_id(propuestaDTO.getId());
            //TODO Muchas personas que hacemos con esto?
//            if(propuestaDTO.getTfgExtPersona() != null)
//            {
//                setPropuesta_nombre(propuestaDTO.getNombre() + " (" + propuestaDTO.getTfgExtPersona().getNombreCompleto() + ")");
//                setPropuestaNombre(propuestaDTO.getNombre());
//                setPropuestaPersonaNombre(propuestaDTO.getTfgExtPersona().getNombreCompleto());
//            }else{
            setPropuesta_nombre(propuestaDTO.getNombre() + " (Proposta sense autor)");
            setPropuestaNombre(propuestaDTO.getNombre());
//            }

        }
        if (personasPropuestaDTO.getTfgExtEditor() != null) {
            setEditor_persona_id(personasPropuestaDTO.getTfgExtEditor().getId());
//            setEditor_nombre(personasPropuestaDTO.getTfgExtEditor().getNombre());
//            setEditor_apellido1(personasPropuestaDTO.getTfgExtEditor().getApellido1());
//            setEditor_apellido2(personasPropuestaDTO.getTfgExtEditor().getApellido2());
        }
        if (personasPropuestaDTO.getFechaEdicion() != null) {
            setFecha_edicion(personasPropuestaDTO.getFechaEdicion());
        }
        if (personasPropuestaDTO.getAceptada() != null) {
            setAceptada(personasPropuestaDTO.getAceptada().intValue());
        }
        setComentarios(personasPropuestaDTO.getComentarios());
    }

    public Long getPersona_id() {
        return persona_id;
    }

    public void setPersona_id(Long persona_id) {
        this.persona_id = persona_id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getAsignatura() {
        return asignatura;
    }

    public void setAsignatura(String asignatura) {
        this.asignatura = asignatura;
    }

    public Long getPropuesta_id() {
        return propuesta_id;
    }

    public void setPropuesta_id(Long propuesta_id) {
        this.propuesta_id = propuesta_id;
    }

    public String getPropuesta_nombre() {
        return propuesta_nombre;
    }

    public void setPropuesta_nombre(String propuesta_nombre) {
        this.propuesta_nombre = propuesta_nombre;
    }

    public Long getEditor_persona_id() {
        return editor_persona_id;
    }

    public void setEditor_persona_id(Long editor_persona_id) {
        if (editor_persona_id != null)
            this.editor_persona_id = editor_persona_id;
    }

    public String getEditor_nombre() {
        return editor_nombre;
    }

    public void setEditor_nombre(String editor_nombre) {
        this.editor_nombre = editor_nombre;
    }

    public Date getFecha_edicion() {
        return fecha_edicion;
    }

    public void setFecha_edicion(Date fecha_edicion) {
        if (fecha_edicion == null) {
            fecha_edicion = DateUtils.getCurrent();
        }
        this.fecha_edicion = fecha_edicion;
    }

    public static List<Asignacion> transformarLista(List<PersonasPropuestaDTO> personasPropuestasDTO, AsignaturaDTO asignaturaDTO) {
        List<Asignacion> salida = new ArrayList<Asignacion>();
        for (PersonasPropuestaDTO personasPropuestaDTO : personasPropuestasDTO) {
            Asignacion asignacion = new Asignacion(personasPropuestaDTO, asignaturaDTO);
            salida.add(asignacion);
        }
        return salida;
    }

    public String getCodigo_asignatura() {
        return codigo_asignatura;
    }

    public void setCodigo_asignatura(String codigo_asignatura) {
        this.codigo_asignatura = codigo_asignatura;
    }

    public Integer getAceptada() {
        return aceptada;
    }

    public void setAceptada(Integer aceptada) {
        this.aceptada = aceptada;
    }

    public String getComentarios() {
        return comentarios;
    }

    public void setComentarios(String comentarios) {
        this.comentarios = comentarios;
    }

    public String getApellido1() {
        return apellido1;
    }

    public void setApellido1(String apellido1) {
        this.apellido1 = apellido1;
    }

    public String getApellido2() {
        return apellido2;
    }

    public void setApellido2(String apellido2) {
        this.apellido2 = apellido2;
    }

    public String getEditor_apellido1() {
        return editor_apellido1;
    }

    public void setEditor_apellido1(String editor_apellido1) {
        this.editor_apellido1 = editor_apellido1;
    }

    public String getEditor_apellido2() {
        return editor_apellido2;
    }

    public void setEditor_apellido2(String editor_apellido2) {
        this.editor_apellido2 = editor_apellido2;
    }

    public String getPropuesta_aceptada() {
        return propuesta_aceptada;
    }

    public void setPropuesta_aceptada(String propuesta_aceptada) {
        this.propuesta_aceptada = propuesta_aceptada;
    }

    public String getPropuestaNombre() {
        return PropuestaNombre;
    }

    public void setPropuestaNombre(String propuestaNombre) {
        PropuestaNombre = propuestaNombre;
    }

    public String getPropuestaPersonaNombre() {
        return PropuestaPersonaNombre;
    }

    public void setPropuestaPersonaNombre(String propuestaPersonaNombre) {
        PropuestaPersonaNombre = propuestaPersonaNombre;
    }
}