package es.uji.apps.tfg.model;


import com.mysema.query.annotations.QueryProjection;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;

public class Tribunal {

    private Long id;
    private String nombre;
    private Integer cursoAca;
    private String codigoAsignatura;
    private Long presidenteId;
    private Long miembro1Id;
    private Long miembro2Id;

    private Boolean presidenteUJI;
    private String nombrePresidente;
    private String correoPresidente;

    private Boolean miembro1UJI;
    private String nombreMiembro1;
    private String correoMiembro1;

    private Boolean miembro2UJI;
    private String nombreMiembro2;
    private String correoMiembro2;

    @QueryProjection
    public Tribunal(Long id, String nombre, Integer cursoAca, String codigoAsignatura, Long presidenteId,
                    String presidenteNombre, String presidenteApellido, Long miembro1Id, String miembro1Nombre, String miembro1Apellido,
                    Long miembro2Id, String miembro2Nombre, String miembro2Apellido,
                    Boolean presidenteUJI, String nombrePresidenteNoUJI, String correoPresidenteNoUJI,
                    Boolean miembro1UJI, String nombreMiembro1NoUJI, String correoMiembro1NoUJI,
                    Boolean miembro2UJI, String nombreMiembro2NoUJI, String correoMiembro2NoUJI) {
        this.id = id;
        this.nombre = nombre;
        this.cursoAca = cursoAca;
        this.codigoAsignatura = codigoAsignatura;
        this.presidenteId = presidenteId;
        this.miembro1Id = miembro1Id;
        this.miembro2Id = miembro2Id;
        this.presidenteUJI = presidenteUJI;
        this.miembro1UJI = miembro1UJI;
        this.miembro2UJI = miembro2UJI;

        if (this.presidenteUJI) {
            this.nombrePresidente = ParamUtils.isNotNull(presidenteNombre) ? presidenteNombre.concat(" ").concat(presidenteApellido) : "";
        } else {
            this.nombrePresidente = nombrePresidenteNoUJI;
            this.correoPresidente = correoPresidenteNoUJI;
        }
        if (this.miembro1UJI) {
            this.nombreMiembro1 = ParamUtils.isNotNull(miembro1Nombre) ? miembro1Nombre.concat(" ").concat(miembro1Apellido) : "";
        } else {
            this.nombreMiembro1 = nombreMiembro1NoUJI;
            this.correoMiembro1 = correoMiembro1NoUJI;
        }

        if (this.miembro2UJI) {
            this.nombreMiembro2 = ParamUtils.isNotNull(miembro2Nombre) ? miembro2Nombre.concat(" ").concat(miembro2Apellido) : "";
        } else {
            this.nombreMiembro2 = nombreMiembro2NoUJI;
            this.correoMiembro2 = correoMiembro2NoUJI;
        }
    }

    public Tribunal(UIEntity uiEntity) {
        this.id = ParamUtils.isNotNull(uiEntity.get("id")) ? uiEntity.getLong("id") : null;
        this.nombre = uiEntity.get("nombre");

        this.presidenteUJI = uiEntity.getBoolean("presidenteUJI");
        if (this.presidenteUJI) {
            this.presidenteId = uiEntity.getLong("presidenteId");
        } else {
            this.nombrePresidente = uiEntity.get("nombrePresidente");
            this.correoPresidente = uiEntity.get("correoPresidente");
        }

        this.miembro1UJI = uiEntity.getBoolean("miembro1UJI");
        if (this.miembro1UJI) {
            this.miembro1Id = uiEntity.getLong("miembro1Id");
        } else {
            this.nombreMiembro1 = uiEntity.get("nombreMiembro1");
            this.correoMiembro1 = uiEntity.get("correoMiembro1");
        }

        this.miembro2UJI = uiEntity.getBoolean("miembro2UJI");
        if (this.miembro2UJI) {
            this.miembro2Id = uiEntity.getLong("miembro2Id");
        } else {
            this.nombreMiembro2 = uiEntity.get("nombreMiembro2");
            this.correoMiembro2 = uiEntity.get("correoMiembro2");
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getCursoAca() {
        return cursoAca;
    }

    public void setCursoAca(Integer cursoAca) {
        this.cursoAca = cursoAca;
    }

    public String getCodigoAsignatura() {
        return codigoAsignatura;
    }

    public void setCodigoAsignatura(String codigoAsignatura) {
        this.codigoAsignatura = codigoAsignatura;
    }

    public Long getPresidenteId() {
        return presidenteId;
    }

    public void setPresidenteId(Long presidenteId) {
        this.presidenteId = presidenteId;
    }

    public Long getMiembro1Id() {
        return miembro1Id;
    }

    public void setMiembro1Id(Long miembro1Id) {
        this.miembro1Id = miembro1Id;
    }

    public Long getMiembro2Id() {
        return miembro2Id;
    }

    public void setMiembro2Id(Long miembro2Id) {
        this.miembro2Id = miembro2Id;
    }

    public Boolean getPresidenteUJI() {
        return presidenteUJI;
    }

    public Boolean isPresidenteUJI() {
        return presidenteUJI;
    }

    public void setPresidenteUJI(Boolean presidenteUJI) {
        this.presidenteUJI = presidenteUJI;
    }

    public String getNombrePresidente() {
        return nombrePresidente;
    }

    public void setNombrePresidente(String nombrePresidente) {
        this.nombrePresidente = nombrePresidente;
    }

    public String getCorreoPresidente() {
        return correoPresidente;
    }

    public void setCorreoPresidente(String correoPresidente) {
        this.correoPresidente = correoPresidente;
    }

    public Boolean getMiembro1UJI() {
        return miembro1UJI;
    }

    public void setMiembro1UJI(Boolean miembro1UJI) {
        this.miembro1UJI = miembro1UJI;
    }

    public Boolean isMiembro1UJI() {
        return miembro1UJI;
    }

    public String getNombreMiembro1() {
        return nombreMiembro1;
    }

    public void setNombreMiembro1(String nombreMiembro1) {
        this.nombreMiembro1 = nombreMiembro1;
    }

    public String getCorreoMiembro1() {
        return correoMiembro1;
    }

    public void setCorreoMiembro1(String correoMiembro1) {
        this.correoMiembro1 = correoMiembro1;
    }

    public Boolean getMiembro2UJI() {
        return miembro2UJI;
    }

    public Boolean isMiembro2UJI() {
        return miembro2UJI;
    }

    public void setMiembro2UJI(Boolean miembro2UJI) {
        this.miembro2UJI = miembro2UJI;
    }

    public String getNombreMiembro2() {
        return nombreMiembro2;
    }

    public void setNombreMiembro2(String nombreMiembro2) {
        this.nombreMiembro2 = nombreMiembro2;
    }

    public String getCorreoMiembro2() {
        return correoMiembro2;
    }

    public void setCorreoMiembro2(String correoMiembro2) {
        this.correoMiembro2 = correoMiembro2;
    }
}
