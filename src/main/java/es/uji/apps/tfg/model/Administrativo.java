package es.uji.apps.tfg.model;

import com.mysema.query.annotations.QueryProjection;

public class Administrativo {

    private Long id;
    private Long personaId;
    private String email;
    private String nombre;
    private String apellido1;
    private String apellido2;
    private String codigoAsignatura;

    @QueryProjection
    public Administrativo(Long id, Long personaId, String email, String nombre, String apellido1, String apellido2, String codigoAsignatura) {
        this.id = id;
        this.personaId = personaId;
        this.email = email;
        this.nombre = nombre;
        this.apellido1 = apellido1;
        this.apellido2 = apellido2;
        this.codigoAsignatura = codigoAsignatura;
    }

    public Administrativo(Long personaId, String codigoAsignatura) {
        this.personaId = personaId;
        this.codigoAsignatura = codigoAsignatura;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPersonaId() {
        return personaId;
    }

    public void setPersonaId(Long personaId) {
        this.personaId = personaId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido1() {
        return apellido1;
    }

    public void setApellido1(String apellido1) {
        this.apellido1 = apellido1;
    }

    public String getApellido2() {
        return apellido2;
    }

    public void setApellido2(String apellido2) {
        this.apellido2 = apellido2;
    }

    public String getCodigoAsignatura() {
        return codigoAsignatura;
    }

    public void setCodigoAsignatura(String codigoAsignatura) {
        this.codigoAsignatura = codigoAsignatura;
    }
}
