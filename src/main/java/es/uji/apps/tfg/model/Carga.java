package es.uji.apps.tfg.model;

import java.math.BigDecimal;

public class Carga {
    private Long profesorId;
    private Integer carga;
    private BigDecimal cargaOriginal;

    public Carga(Long profesorId, Integer carga, BigDecimal cargaOriginal) {
        this.profesorId = profesorId;
        this.carga = carga;
        this.cargaOriginal = cargaOriginal;
    }

    public Long getProfesorId() {
        return profesorId;
    }

    public void setProfesorId(Long profesorId) {
        this.profesorId = profesorId;
    }

    public Integer getCarga() {
        return carga;
    }

    public void setCarga(Integer carga) {
        this.carga = carga;
    }

    public BigDecimal getCargaOriginal() {
        return cargaOriginal;
    }

    public void setCargaOriginal(BigDecimal cargaOriginal) {
        this.cargaOriginal = cargaOriginal;
    }
}
