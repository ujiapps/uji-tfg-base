package es.uji.apps.tfg.model;

import es.uji.apps.tfg.dao.PropuestasDAO;
import es.uji.apps.tfg.dto.PersonaDTO;
import es.uji.apps.tfg.dto.PersonasPropuestaDTO;
import es.uji.apps.tfg.dto.PropuestaDTO;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Solicitud
{
    @Autowired
    PropuestasDAO propuestasDAO = new PropuestasDAO();

    private long id;
    private String fechaCreacion;
    private String nombre;
    private String asignatura;
    private String codigo_asignatura;
    private String objetivos;
    private String bibliografia;
    private String descripcion;
    private String observaciones;
    private int plazas;
    private int espera;
    private int disponibilidad;// -1 Completa 0 Disponible 1 Solo disponible en espera
    private int gente_esperando;
    private int plazas_disponibles;
    private List<Long> inscritos;
    private List<Long> esperando;
    private String tutoresNombre;

    public Solicitud()
    {
    }

    public Solicitud(PropuestaDTO propuestaDTO)
    {
        List<PersonasPropuestaDTO> solicitudes = propuestaDTO.getTfgPersonasPropuestas();
        int plazas_ocupadas = 0;
        int espera_ocupadas = 0;
        int estado_plazas = 0;// Hay plazas
        inscritos = new ArrayList<Long>();
        esperando = new ArrayList<Long>();
        setId(propuestaDTO.getId());
        setNombre(propuestaDTO.getNombre());
        setFechaCreacion(propuestaDTO.getFechaCreacion().toString());

        Asignatura asig = new Asignatura(propuestaDTO.getTfgExtAsignatura());
        setAsignatura(asig.getCodinom());
        setCodigo_asignatura(asig.getCodigo());

        setObjetivos(propuestaDTO.getObjetivos());
        setBibliografia(propuestaDTO.getBibliografia());
        setDescripcion(propuestaDTO.getDescripcion());
        setObservaciones(propuestaDTO.getObservaciones());
        setPlazas(propuestaDTO.getPlazas().intValue());
        setEspera(propuestaDTO.getEspera().intValue());

        for (PersonasPropuestaDTO personasPropuestaDTO : solicitudes)
        {
            PersonaDTO solicitante = personasPropuestaDTO.getTfgExtPersona();
            if (solicitante == null) {
                continue;
            }
            if (personasPropuestaDTO.getAceptada().intValue() == 1)
            {
                Long id = solicitante.getId();
                inscritos.add(id);
            }
            else
            {
                esperando.add(solicitante.getId());
            }
        }

        // Inicializamos estado_plazas
        if (getPlazas() > 0)
        {
            estado_plazas = 0;
        }
        else
        {// Si tiene 0 plazas ya está completo
            estado_plazas = getEspera() > 0 ? 1 : -1;
        }
        for (PersonasPropuestaDTO personasPropuestaDTO : solicitudes)
        {
            if (personasPropuestaDTO.getAceptada().intValue() > 0)
            {
                plazas_ocupadas++;
            }
            else
            {
                espera_ocupadas++;
            }
        }
        if (plazas_ocupadas >= getPlazas())
        {
            if (espera_ocupadas >= propuestaDTO.getEspera().intValue()) {
                estado_plazas = -1;// Completo
            } else {
                estado_plazas = 1;// Disponible en espera
            }
        }
        setDisponibilidad(estado_plazas);
        setGente_esperando(espera_ocupadas);
        setPlazas_disponibles(getPlazas() - plazas_ocupadas);
        this.tutoresNombre = propuestaDTO.getPropuestaTutores().stream().map(p -> {
            if (p.getTutor() != null) {
                return p.getTutor().getNombreCompleto();
            }
            return "";
        }).collect(Collectors.joining(","));

    }

    public static List<Solicitud> transformarListaDTO(List<PropuestaDTO> propuestasByAsignatura)
    {
        List<Solicitud> salida = new ArrayList<Solicitud>();
        if (propuestasByAsignatura.size() > 0)
        {
            for (PropuestaDTO propuestaDTO : propuestasByAsignatura)
            {
                salida.add(new Solicitud(propuestaDTO));
            }
        }
        return salida;
    }

    public long getId()
    {
        return id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public String getFechaCreacion()
    {
        return fechaCreacion;
    }

    public void setFechaCreacion(String fechaCreacion)
    {
        this.fechaCreacion = fechaCreacion;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getAsignatura()
    {
        return asignatura;
    }

    public void setAsignatura(String asignatura)
    {
        this.asignatura = asignatura;
    }

    public String getObjetivos()
    {
        return objetivos;
    }

    public void setObjetivos(String objetivos)
    {
        this.objetivos = objetivos;
    }

    public String getBibliografia()
    {
        return bibliografia;
    }

    public void setBibliografia(String bibliografia)
    {
        this.bibliografia = bibliografia;
    }

    public String getDescripcion()
    {
        return descripcion;
    }

    public void setDescripcion(String descripcion)
    {
        this.descripcion = descripcion;
    }

    public String getObservaciones()
    {
        return observaciones;
    }

    public void setObservaciones(String observaciones)
    {
        this.observaciones = observaciones;
    }

    public int getPlazas()
    {
        return plazas;
    }

    public void setPlazas(int plazas)
    {
        this.plazas = plazas;
    }

    public int getEspera()
    {
        return espera;
    }

    public void setEspera(int espera)
    {
        this.espera = espera;
    }

    public int getDisponibilidad()
    {
        return disponibilidad;
    }

    public void setDisponibilidad(int disponibilidad)
    {
        this.disponibilidad = disponibilidad;
    }

    public int getGente_esperando()
    {
        return gente_esperando;
    }

    public void setGente_esperando(int gente_esperando)
    {
        this.gente_esperando = gente_esperando;
    }

    public int getPlazas_disponibles()
    {
        return plazas_disponibles;
    }

    public void setPlazas_disponibles(int plazas_disponibles)
    {
        this.plazas_disponibles = plazas_disponibles;
    }

    public List<Long> getInscritos()
    {
        return inscritos;
    }

    public void setInscritos(List<Long> inscritos)
    {
        this.inscritos = inscritos;
    }

    public List<Long> getEsperando()
    {
        return esperando;
    }

    public void setEsperando(List<Long> esperando)
    {
        this.esperando = esperando;
    }

    public String getCodigo_asignatura() {
        return codigo_asignatura;
    }

    public void setCodigo_asignatura(String codigo_asignatura) {
        this.codigo_asignatura = codigo_asignatura;
    }

    public String getTutoresNombre() {
        return tutoresNombre;
    }

    public void setTutoresNombre(String tutoresNombre) {
        this.tutoresNombre = tutoresNombre;
    }
}
