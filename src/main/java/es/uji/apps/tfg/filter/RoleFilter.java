package es.uji.apps.tfg.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Context;

import es.uji.commons.sso.exceptions.UnauthorizedUserException;

public class RoleFilter implements Filter
{

    // private Lsm lsm;
    private FilterConfig filterConfig;
    private String returnScheme;
    private String returnHost;
    private String returnPort;
    private String defaultUserId;
    private String defaultUserName;

    @Context
    ServletContext ctx;
    @Override
    public void init(FilterConfig config) throws ServletException
    {

        filterConfig = config;
        ctx = config.getServletContext();
        
        returnScheme = filterConfig.getInitParameter("returnScheme");
        returnHost = filterConfig.getInitParameter("returnHost");
        returnPort = filterConfig.getInitParameter("returnPort");
        defaultUserId = filterConfig.getInitParameter("defaultUserId");
        defaultUserName = filterConfig.getInitParameter("defaultUserName");
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException
    {
        HttpServletRequest httpRequest = (HttpServletRequest) servletRequest;
        String url = httpRequest.getRequestURL().toString();
        if (!url.matches("http.*/tfg/(rest/)?solicitudes.*"))
        {
            try
            {
                checkPerfil("ADMIN");
                checkPerfil("COORDINADOR");
            }
            catch (UnauthorizedUserException e)
            {
                ((HttpServletResponse) servletResponse).sendRedirect("http://www.uji.es");
                return;
            }
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy()
    {

    }

    private void checkPerfil(String perfil) throws UnauthorizedUserException
    {       
//        if (perfil.compareTo("ESTUDIANTE") != 0) {
//            throw new UnauthorizedUserException("Secció no permesa a usuaris del teu perfil");
//        }
    }
}
