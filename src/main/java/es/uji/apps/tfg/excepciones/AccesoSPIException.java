package es.uji.apps.tfg.excepciones;

@SuppressWarnings("serial")
public class AccesoSPIException extends Exception {

    public AccesoSPIException(String mensaje) {
        super(mensaje);
    }

}
