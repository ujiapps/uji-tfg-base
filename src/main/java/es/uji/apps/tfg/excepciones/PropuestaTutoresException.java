package es.uji.apps.tfg.excepciones;

@SuppressWarnings("serial")
public class PropuestaTutoresException extends Exception {

    public PropuestaTutoresException(String mensaje) {
        super(mensaje);
    }

}
