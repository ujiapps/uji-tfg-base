package es.uji.apps.tfg.excepciones;

public class AsignaturaNoConfiguradaException  extends Exception {

    public AsignaturaNoConfiguradaException() {
        super("tfg.message.error.asignaturaNoConfigurada");
    }
}
