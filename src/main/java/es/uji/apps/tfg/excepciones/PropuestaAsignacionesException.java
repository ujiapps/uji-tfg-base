package es.uji.apps.tfg.excepciones;

public class PropuestaAsignacionesException extends Exception {

    public PropuestaAsignacionesException(String mensaje) {
        super(mensaje);
    }

}