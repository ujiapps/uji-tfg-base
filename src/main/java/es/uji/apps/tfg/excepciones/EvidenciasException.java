package es.uji.apps.tfg.excepciones;

@SuppressWarnings("serial")
public class EvidenciasException extends Exception {

    public EvidenciasException(String mensaje) {
        super(mensaje);
    }

}
