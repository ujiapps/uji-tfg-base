package es.uji.apps.tfg.excepciones;

public class PropuestaAlumnoException extends Exception {

    public PropuestaAlumnoException(String mensaje) {
        super(mensaje);
    }

}