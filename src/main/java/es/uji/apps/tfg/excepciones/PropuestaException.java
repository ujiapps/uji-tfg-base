package es.uji.apps.tfg.excepciones;

@SuppressWarnings("serial")
public class PropuestaException extends Exception
{

    public PropuestaException(String mensaje)
    {
        super(mensaje);
    }

}
