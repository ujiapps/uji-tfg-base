package es.uji.apps.tfg.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Properties;

import org.apache.log4j.Logger;

public class Configuration
{
    private static final String URL_ADMIN = "uji.tfg.urlAdmin";
    private static final String URL_PUBLIC = "uji.tfg.urlPublic";
    private static final String HTML_TITLE = "uji.tfg.htmltitle";
    private static final String SECRET = "uji.tfg.secret";
    private static final String PROPERTIES_SEPARATOR = ",";
    private static final String MAIL_HOST = "uji.tfg.mail.host";
    private static final String MAIL_FROM = "uji.tfg.mail.from";
    private static final String ENVIAR_MAILS_ERROR = "uji.tfg.enviarMailsError";
    private static final String AUTH_CLASS = "uji.tfg.authClass";

    private static final String JDBC_URL = "uji.db.jdbcUrl";
    private static final String DB_USER = "uji.db.username";
    private static final String DB_PASS = "uji.db.password";
    private static final String SYNC_TIPO = "uji.sync.lugar";
    private static final String SYNC_URL_TIPO = "uji.sync.rss";
	private static final String API_KEY = "api.key";
	private static final String URL_PUBLIC_SIN_HTTPS = "uji.tfg.urlPublicSinHTTPS";
	private static final String URL_PUBLIC_LIMPIO = "uji.tfg.urlPublicLimpio";
	private static final String MAILING_CLASS = "uji.tfg.mail.class";
	private static final String TMP_FOLDER = "uji.tmp.folder";
	private static final String PGP_PASSPHRASE = "uji.pgp.key";
	private static final String CODIGO_BUZON = "uji.codigo.buzon";
	private static final String WSDL_URL = "uji.tpv.wsdlurl";
	private static final String ACTIVE_DIRECTORY_IP = "activedirectory.ip";
	private static final String ACTIVE_DIRECTORY_Port = "activedirectory.port";
	private static final String ACTIVE_DIRECTORY_DOMAIN = "activedirectory.domain";
	private static final String ACTIVE_DIRECTORY_DC = "activedirectory.dc";
	private static final String IMAGEN_SUSTITUTIVA = "uji.reports.imagenSustitutiva";
	private static final String IMAGEN_SUSTITUTIVA_CONTENT_TYPE = "uji.reports.imagenSustitutivaContentType";
	private static final String PLAZO_SOLICITUD = "uji.tfg.plazo_solicitud";	
    private static final String NOMBRE_AVISOS = "uji.tfg.avisos.nombre";
    private static final String EMAIL_AVISOS = "uji.tfg.avisos.email";
    		

    public static Logger log = Logger.getLogger(Configuration.class);

    private static String fileName = "/etc/uji/tfg/app.properties";
    private Properties properties;
    private static Configuration instance;

    static
    {
        try
        {
            instance = new Configuration();
        }
        catch (IOException e)
        {
            log.error(e.toString());
        }
    }

    private Configuration() throws IOException
    {
        String filePath = fileName;
        File f = new File(filePath);

        properties = new Properties();
        FileInputStream fis = new FileInputStream(f);
        properties.load(fis);
    }

    public static void reinitConfig() throws IOException
    {
        instance = null;
        instance = new Configuration();
    }
    
//    private static String getNoObligatoryProperty(String propertyName)
//    {
//        try
//        {
//            String property = getProperty(propertyName);
//            return property;
//        }
//        catch (Exception e)
//        {
//            return null;
//        }
//    }

    private static String getProperty(String propertyName)
    {
        String value = (String) instance.properties.getProperty(propertyName);
        try
        {
            value = new String(value.getBytes("ISO-8859-1"), "UTF-8");
        }
        catch (UnsupportedEncodingException e)
        {
            throw new RuntimeException(e);
        }
        
        return value.trim();
    }
    
    public static String getUrlPublic()
    {
        return getProperty(URL_PUBLIC);
    }
    
    public static String getUrlAdmin()
    {
        return getProperty(URL_ADMIN);
    }

    public static String getSecret()
    {
        return getProperty(SECRET);
    }

    public static String getMailHost()
    {
        return getProperty(MAIL_HOST);
    }

    public static String getMailFrom()
    {
        return getProperty(MAIL_FROM);
    }

    public static String getEnviarMailsError()
    {
        return getProperty(ENVIAR_MAILS_ERROR);
    }

    public static void desactivaLogGmail()
    {
        instance.properties.setProperty(ENVIAR_MAILS_ERROR, "false");
    }

    public static String getAuthClass()
    {
        return getProperty(AUTH_CLASS);
    }
    
    public static String getJdbUrl()
    {
        return getProperty(JDBC_URL);
    }

    public static String getSyncTipo()
    {
        return getProperty(SYNC_TIPO);
    }
    
    public static String[] getSyncUrlsRss()
    {
        return getProperty(SYNC_URL_TIPO).split(PROPERTIES_SEPARATOR);
    }
    
  	public static String getApiKey() {
		return getProperty(API_KEY);
	}

	public static String getHtmlTitle() {
		return getProperty(HTML_TITLE);
	}

	public static String getUrlPublicSinHTTPS() {
		return getProperty(URL_PUBLIC_SIN_HTTPS);
	}

	public static String getMailingClass() {
		return getProperty(MAILING_CLASS);
	}

	public static String getTmpFolder() {
		return getProperty(TMP_FOLDER);
	}

	public static String getPassphrase() {
		return getProperty(PGP_PASSPHRASE);
	}

	public static String getCodigoBuzon() {
		return getProperty(CODIGO_BUZON);
	}

	public static String getWSDLURL() {
		return getProperty(WSDL_URL);
	}
	
	public static String getDBUser() {
		return getProperty(DB_USER);
	}
	
	public static String getDBPassword() {
		return getProperty(DB_PASS);
	}

	public static String getUrlPublicLimpio() {
		return getProperty(URL_PUBLIC_LIMPIO);
	}
	
	public static String getActiveDirectoryIP() {
		return getProperty(ACTIVE_DIRECTORY_IP);
	}
	
	public static String getActiveDirectoryPort() {
		return getProperty(ACTIVE_DIRECTORY_Port);
	}
	
	public static String getActiveDirectoryDomain() {
		return getProperty(ACTIVE_DIRECTORY_DOMAIN);
	}
	
	public static String getActiveDirectoryDC() {
		return getProperty(ACTIVE_DIRECTORY_DC);
	}

	public static String getPathImagenSustitutiva() {
		return getProperty(IMAGEN_SUSTITUTIVA);
	}

	public static String getImagenSustitutivaContentType() {
		return getProperty(IMAGEN_SUSTITUTIVA_CONTENT_TYPE);
	}

    public static String getPlazoSolicitud()
    {
        return getProperty(PLAZO_SOLICITUD);
    }

    public static String getNombreAvisos()
    {
        return getProperty(NOMBRE_AVISOS);
    }
    
    public static String getEmailAvisos()
    {
        return getProperty(EMAIL_AVISOS);
    }       
}
