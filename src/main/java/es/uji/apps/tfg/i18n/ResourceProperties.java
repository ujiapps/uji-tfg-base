package es.uji.apps.tfg.i18n;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.PropertyResourceBundle;

public class ResourceProperties
{
    private static final String[] LENGUAJES = new String[]{"es", "ca"};

    private static Map<String,PropertyResourceBundle> properties;
    
    protected static final String LANG = "language";
    
    public static String getProperty(Locale locale, String propertyName, Object ... objects)
    {
    	String language = LENGUAJES[0];
        if (properties == null)
            initProperties();
        
        if (locale != null)
        	language = locale.getLanguage();
        
        if (objects.length == 0) 
            return properties.get(language).getString(propertyName);
        else 
            return String.format(properties.get(language).getString(propertyName), objects);
    }

    private static void initProperties()
    {
        properties = new HashMap<String, PropertyResourceBundle>();
        
        for (String lenguaje:LENGUAJES)
        {
            try
            {
            	String path = "/etc/uji/tfg/i18n/i18n_" + lenguaje.toLowerCase() + ".properties";
            	InputStream inputStream = new FileInputStream(path);
                properties.put(lenguaje, new PropertyResourceBundle(new InputStreamReader(inputStream)));
            }
            catch (IOException e)
            {
                throw new RuntimeException(e);
            }
        }
    }
    
    public static String getProperty(String messageProperty, Locale locale, Object... values)
    {
        return ResourceProperties.getProperty(locale, messageProperty, values);
    }
    
    public static String getMessage(Locale locale, String message) {
        if (message.indexOf("tfg.message") != -1) {
            return getProperty(locale, message);
        }        
        return message;       
    }
}
