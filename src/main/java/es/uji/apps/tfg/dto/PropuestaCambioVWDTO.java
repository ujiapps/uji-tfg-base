package es.uji.apps.tfg.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "TFG_VW_PROPUESTAS_CAMBIO")
public class PropuestaCambioVWDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    private Long id;

    private String nombre;
    private Integer plazas;
    private Integer espera;
    @Column(name = "TUTOR_ID")
    private Long tutorId;
    private String tutores;
    @Column(name = "TUTORES_NOMBRE")
    private String tutoresNombre;
    private Integer aceptados;
    @Column(name = "EN_ESPERA")
    private Integer enEspera;


    @Column(name = "CODIGO_ASIGNATURA")
    private String codigoAsignatura;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FECHA_CREACION")
    private Date fechaCreacion;

    private Integer ejercicio;

    @Lob
    @Column(name = "objetivos", columnDefinition = "CLOB")
    private String objetivos;

    @Lob
    @Column(name = "observaciones", columnDefinition = "CLOB")
    private String observaciones;

    @Lob
    @Column(name = "bibliografia", columnDefinition = "CLOB")
    private String bibliografia;

    @Lob
    @Column(name = "descripcion", columnDefinition = "CLOB")
    private String descripcion;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fecha_lectura")
    private Date fechaLectura;

    @Id
    @Column(name = "ALUMNO_ID")
    private Long alumnoId;

    @Column(name = "ALUMNO_NOMBRE")
    private String alumnoNombre;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getPlazas() {
        return plazas;
    }

    public void setPlazas(Integer plazas) {
        this.plazas = plazas;
    }

    public Integer getEspera() {
        return espera;
    }

    public void setEspera(Integer espera) {
        this.espera = espera;
    }

    public Long getTutorId() {
        return tutorId;
    }

    public void setTutorId(Long tutorId) {
        this.tutorId = tutorId;
    }

    public String getTutores() {
        return tutores;
    }

    public void setTutores(String tutores) {
        this.tutores = tutores;
    }

    public String getTutoresNombre() {
        return tutoresNombre;
    }

    public void setTutoresNombre(String tutoresNombre) {
        this.tutoresNombre = tutoresNombre;
    }

    public Integer getAceptados() {
        return aceptados;
    }

    public void setAceptados(Integer aceptados) {
        this.aceptados = aceptados;
    }

    public Integer getEnEspera() {
        return enEspera;
    }

    public void setEnEspera(Integer enEspera) {
        this.enEspera = enEspera;
    }

    public String getCodigoAsignatura() {
        return codigoAsignatura;
    }

    public void setCodigoAsignatura(String codigoAsignatura) {
        this.codigoAsignatura = codigoAsignatura;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Integer getEjercicio() {
        return ejercicio;
    }

    public void setEjercicio(Integer ejercicio) {
        this.ejercicio = ejercicio;
    }

    public String getObjetivos() {
        return objetivos;
    }

    public void setObjetivos(String objetivos) {
        this.objetivos = objetivos;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public String getBibliografia() {
        return bibliografia;
    }

    public void setBibliografia(String bibliografia) {
        this.bibliografia = bibliografia;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Date getFechaLectura() {
        return fechaLectura;
    }

    public void setFechaLectura(Date fechaLectura) {
        this.fechaLectura = fechaLectura;
    }

    public Long getAlumnoId() {
        return alumnoId;
    }

    public void setAlumnoId(Long alumnoId) {
        this.alumnoId = alumnoId;
    }

    public String getAlumnoNombre() {
        return alumnoNombre;
    }

    public void setAlumnoNombre(String alumnoNombre) {
        this.alumnoNombre = alumnoNombre;
    }
}
