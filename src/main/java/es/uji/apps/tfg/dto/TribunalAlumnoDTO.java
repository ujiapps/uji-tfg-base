package es.uji.apps.tfg.dto;

import javax.persistence.*;

@Entity
@Table(name = "TFG_TRIBUNALES_ALUMNOS")
public class TribunalAlumnoDTO {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "TRIBUNAL_ID")
    private Long tribunalId;

    @Column(name = "ALUMNO_ID")
    private Long alumnoId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTribunalId() {
        return tribunalId;
    }

    public void setTribunalId(Long tribunalId) {
        this.tribunalId = tribunalId;
    }

    public Long getAlumnoId() {
        return alumnoId;
    }

    public void setAlumnoId(Long alumnoId) {
        this.alumnoId = alumnoId;
    }
}
