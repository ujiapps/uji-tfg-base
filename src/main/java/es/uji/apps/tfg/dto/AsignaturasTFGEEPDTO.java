package es.uji.apps.tfg.dto;

import javax.persistence.*;

@Entity
@Table(name = "TFG_ASIGNATURAS_TFG_EEP")
public class AsignaturasTFGEEPDTO {

    @Id
    private String codigoAsignatura;

    public AsignaturasTFGEEPDTO() {
    }

    public String getCodigoAsignatura() {
        return codigoAsignatura;
    }

    public void setCodigoAsignatura(String codigoAsignatura) {
        this.codigoAsignatura = codigoAsignatura;
    }
}
