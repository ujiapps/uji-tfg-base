package es.uji.apps.tfg.dto;

import es.uji.apps.tfg.model.Tribunal;

import javax.persistence.*;

@Entity
@Table(name = "TFG_TRIBUNALES")
public class TribunalDTO {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String nombre;

    @Column(name = "CURSO_ACA")
    private Integer cursoAca;

    @Column(name = "CODIGO_ASIGNATURA")
    private String codigoAsignatura;

    @Column(name = "PRESIDENTE_ID")
    private Long presidenteId;

    @Column(name = "MIEMBRO1_ID")
    private Long miembro1Id;

    @Column(name = "MIEMBRO2_ID")
    private Long miembro2Id;

    @Column(name = "PRESIDENTE_UJI")
    private Boolean presidenteUJI;
    @Column(name = "NOMBRE_PRESIDENTE")
    private String nombrePresidente;
    @Column(name = "CORREO_PRESIDENTE")
    private String correoPresidente;

    @Column(name = "MIEMBRO1_UJI")
    private Boolean miembro1UJI;
    @Column(name = "NOMBRE_MIEMBRO1")
    private String nombreMiembro1;
    @Column(name = "CORREO_MIEMBRO1")
    private String correoMiembro1;

    @Column(name = "MIEMBRO2_UJI")
    private Boolean miembro2UJI;
    @Column(name = "NOMBRE_MIEMBRO2")
    private String nombreMiembro2;
    @Column(name = "CORREO_MIEMBRO2")
    private String correoMiembro2;



    public TribunalDTO(Tribunal tribunal) {
        this.id = tribunal.getId();
        this.nombre = tribunal.getNombre();
        this.cursoAca = tribunal.getCursoAca();
        this.codigoAsignatura = tribunal.getCodigoAsignatura();
        this.presidenteId = tribunal.getPresidenteId();
        this.miembro1Id = tribunal.getMiembro1Id();
        this.miembro2Id = tribunal.getMiembro2Id();

        this.presidenteUJI = tribunal.getPresidenteUJI();
        this.nombrePresidente = tribunal.getNombrePresidente();
        this.correoPresidente = tribunal.getCorreoPresidente();

        this.miembro1UJI = tribunal.getMiembro1UJI();
        this.nombreMiembro1 = tribunal.getNombreMiembro1();
        this.correoMiembro1 = tribunal.getCorreoMiembro1();

        this.miembro2UJI = tribunal.getMiembro2UJI();
        this.nombreMiembro2 = tribunal.getNombreMiembro2();
        this.correoMiembro2 = tribunal.getCorreoMiembro2();
    }

    public TribunalDTO() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getCursoAca() {
        return cursoAca;
    }

    public void setCursoAca(Integer cursoAca) {
        this.cursoAca = cursoAca;
    }

    public String getCodigoAsignatura() {
        return codigoAsignatura;
    }

    public void setCodigoAsignatura(String codigoAsignatura) {
        this.codigoAsignatura = codigoAsignatura;
    }

    public Long getPresidenteId() {
        return presidenteId;
    }

    public void setPresidenteId(Long presidenteId) {
        this.presidenteId = presidenteId;
    }

    public Long getMiembro1Id() {
        return miembro1Id;
    }

    public void setMiembro1Id(Long miembro1Id) {
        this.miembro1Id = miembro1Id;
    }

    public Long getMiembro2Id() {
        return miembro2Id;
    }

    public void setMiembro2Id(Long miembro2Id) {
        this.miembro2Id = miembro2Id;
    }

    public Boolean getPresidenteUJI() {
        return presidenteUJI;
    }

    public void setPresidenteUJI(Boolean presidenteUJI) {
        this.presidenteUJI = presidenteUJI;
    }

    public Boolean isPresidenteUJI() {
        return presidenteUJI;
    }

    public String getNombrePresidente() {
        return nombrePresidente;
    }

    public void setNombrePresidente(String nombrePresidente) {
        this.nombrePresidente = nombrePresidente;
    }


    public String getCorreoPresidente() {
        return correoPresidente;
    }

    public void setCorreoPresidente(String correoPresidente) {
        this.correoPresidente = correoPresidente;
    }

    public Boolean getMiembro1UJI() {
        return miembro1UJI;
    }

    public void setMiembro1UJI(Boolean miembro1UJI) {
        this.miembro1UJI = miembro1UJI;
    }

    public Boolean isMiembro1UJI() {
        return miembro1UJI;
    }


    public String getNombreMiembro1() {
        return nombreMiembro1;
    }

    public void setNombreMiembro1(String nombreMiembro1) {
        this.nombreMiembro1 = nombreMiembro1;
    }

    public String getCorreoMiembro1() {
        return correoMiembro1;
    }

    public void setCorreoMiembro1(String correoMiembro1) {
        this.correoMiembro1 = correoMiembro1;
    }

    public Boolean getMiembro2UJI() {
        return miembro2UJI;
    }

    public void setMiembro2UJI(Boolean miembro2UJI) {
        this.miembro2UJI = miembro2UJI;
    }

    public Boolean isMiembro2UJI() {
        return miembro2UJI;
    }


    public String getNombreMiembro2() {
        return nombreMiembro2;
    }

    public void setNombreMiembro2(String nombreMiembro2) {
        this.nombreMiembro2 = nombreMiembro2;
    }

    public String getCorreoMiembro2() {
        return correoMiembro2;
    }

    public void setCorreoMiembro2(String correoMiembro2) {
        this.correoMiembro2 = correoMiembro2;
    }
}
