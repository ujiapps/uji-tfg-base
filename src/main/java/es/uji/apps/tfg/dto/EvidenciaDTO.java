package es.uji.apps.tfg.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "TFG_VW_EVIDENCIAS")
public class EvidenciaDTO implements Serializable {

    @Id
    @Column(name = "PERSONA_ID")
    private Long personaId;
    @Id
    @Column(name = "PROPUESTA_ID")
    private Long propuestaId;
    @Column(name = "NOMBRE_ALUMNO")
    private String nombreAlumno;

    private Integer ejercicio;

    @Column(name = "FECHA_LECTURA")
    @Temporal(TemporalType.DATE)
    private Date fechaLectura;

    @Column(name = "NOMBRE_TRABAJO")
    private String nombreTrabajo;

    private String nota;
    @Column(name = "NOMBRE_TITULACION")
    private String nombreTitulacion;
    @Column(name = "NOMBRE_ASIGNATURA")
    private String nombreAsignatura;

    @Column(name = "CODIGO_ASIGNATURA")
    private String codigoAsignatura;

    @Column(name = "TUTORES_NOMBRE")
    private String tutoresNombre;

    @Column(name = "tipo")
    private String tipoEstudio;

    public Long getPersonaId() {
        return personaId;
    }

    public void setPersonaId(Long personaId) {
        this.personaId = personaId;
    }

    public Long getPropuestaId() {
        return propuestaId;
    }

    public void setPropuestaId(Long propuestaId) {
        this.propuestaId = propuestaId;
    }

    public String getNombreAlumno() {
        return nombreAlumno;
    }

    public void setNombreAlumno(String nombreAlumno) {
        this.nombreAlumno = nombreAlumno;
    }

    public Integer getEjercicio() {
        return ejercicio;
    }

    public void setEjercicio(Integer ejercicio) {
        this.ejercicio = ejercicio;
    }

    public Date getFechaLectura() {
        return fechaLectura;
    }

    public void setFechaLectura(Date fechaLectura) {
        this.fechaLectura = fechaLectura;
    }

    public String getNombreTrabajo() {
        return nombreTrabajo;
    }

    public void setNombreTrabajo(String nombreTrabajo) {
        this.nombreTrabajo = nombreTrabajo;
    }

    public String getNota() {
        return nota;
    }

    public void setNota(String nota) {
        this.nota = nota;
    }

    public String getNombreTitulacion() {
        return nombreTitulacion;
    }

    public void setNombreTitulacion(String nombreTitulacion) {
        this.nombreTitulacion = nombreTitulacion;
    }

    public String getNombreAsignatura() {
        return nombreAsignatura;
    }

    public void setNombreAsignatura(String nombreAsignatura) {
        this.nombreAsignatura = nombreAsignatura;
    }

    public String getCodigoAsignatura() {
        return codigoAsignatura;
    }

    public void setCodigoAsignatura(String codigoAsignatura) {
        this.codigoAsignatura = codigoAsignatura;
    }

    public String getTutoresNombre() {
        return tutoresNombre;
    }

    public void setTutoresNombre(String tutoresNombre) {
        this.tutoresNombre = tutoresNombre;
    }

    public String getTipoEstudio() {
        return tipoEstudio;
    }

    public void setTipoEstudio(String tipoEstudio) {
        this.tipoEstudio = tipoEstudio;
    }
}
