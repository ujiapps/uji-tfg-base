package es.uji.apps.tfg.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "TFG_VW_ALUMNOS_ASIGNADOS")
public class AlumnosAsignadosDTO implements Serializable
{
    @Id
    private Long id;
    private String nombre;

    @Id
    @Column(name = "PERSONA_ID")
    private Long personaId;

    @Column(name = "NOMBRE_COMPLETO")
    private String nombreCompleto;

    @Column(name = "EMAIL")
    private String email;

    @Id
    @Column(name = "EJERCICIO")
    private Integer ejercicio;

    @Column(name = "CODIGO_ASIGNATURA")
    private String codigoAsignatura;

    @Column(name = "TUTORES_NOMBRE")
    private String tutoreNombre;

    private String nota;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public Long getPersonaId()
    {
        return personaId;
    }

    public void setPersonaId(Long personaId)
    {
        this.personaId = personaId;
    }

    public String getNombreCompleto()
    {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto)
    {
        this.nombreCompleto = nombreCompleto;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public Integer getEjercicio()
    {
        return ejercicio;
    }

    public void setEjercicio(Integer ejercicio)
    {
        this.ejercicio = ejercicio;
    }

    public String getCodigoAsignatura() {
        return codigoAsignatura;
    }

    public void setCodigoAsignatura(String codigoAsignatura) {
        this.codigoAsignatura = codigoAsignatura;
    }

    public String getTutoreNombre() {
        return tutoreNombre;
    }

    public void setTutoreNombre(String tutoreNombre) {
        this.tutoreNombre = tutoreNombre;
    }

    public String getNota() {
        return nota;
    }

    public void setNota(String nota) {
        this.nota = nota;
    }
}
