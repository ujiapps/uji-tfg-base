package es.uji.apps.tfg.dto;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the TFG_TURNOS database table.
 * 
 */
@Embeddable
public class TurnoDTOPK implements Serializable
{
    // default serial version id, required for serializable classes.
    private static final long serialVersionUID = 1L;

    @Column(name = "PERSONA_ID")
    private long personaId;

    @Column(name = "CODIGO_ASIGNATURA")
    private String codigoAsignatura;

    public TurnoDTOPK()
    {
    }

    public long getPersonaId()
    {
        return this.personaId;
    }

    public void setPersonaId(long personaId)
    {
        this.personaId = personaId;
    }

    public String getCodigoAsignatura()
    {
        return this.codigoAsignatura;
    }

    public void setCodigoAsignatura(String codigoAsignatura)
    {
        this.codigoAsignatura = codigoAsignatura;
    }

    public boolean equals(Object other)
    {
        if (this == other)
        {
            return true;
        }
        if (!(other instanceof TurnoDTOPK))
        {
            return false;
        }
        TurnoDTOPK castOther = (TurnoDTOPK) other;
        return (this.personaId == castOther.personaId) && (this.codigoAsignatura == castOther.codigoAsignatura);
    }

    public int hashCode()
    {
        final int prime = 31;
        int hash = 17;
        hash = hash * prime + ((int) (this.personaId ^ (this.personaId >>> 32)));
        hash = hash * prime + this.codigoAsignatura.hashCode();

        return hash;
    }
}