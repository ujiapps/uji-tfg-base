package es.uji.apps.tfg.dto;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * The persistent class for the TFG_PROPUESTAS database table.
 * 
 */
@Entity
@Table(name = "TFG_PROPUESTAS")
public class PropuestaDTO implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Lob    
    @Column(name = "bibliografia", columnDefinition="CLOB")
    private String bibliografia;

    @Lob
    @Column(name = "descripcion", columnDefinition = "CLOB")
    private String descripcion;

    private BigDecimal espera;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FECHA_CREACION")
    private Date fechaCreacion;

    @Column(name = "PER_ID_CREACION")
    private Long perIdCreacion;

    private String nombre;

    @Lob
    @Column(name = "objetivos", columnDefinition = "CLOB")
    private String objetivos;

    @Lob
    @Column(name = "observaciones", columnDefinition = "CLOB")
    private String observaciones;

    private BigDecimal plazas;

    @Column(name = "ejercicio")
    private int ejercicio;

    @Column(name = "codigo_asignatura")
    private String codigo_asignatura;

    // bi-directional many-to-one association to PersonasPropuestaDTO
    @OneToMany(mappedBy = "tfgPropuesta", fetch = FetchType.EAGER)
    private List<PersonasPropuestaDTO> tfgPersonasPropuestas;

    @Autowired
    @ManyToOne
    @JoinColumns({
            @JoinColumn(name = "CODIGO_ASIGNATURA", referencedColumnName = "CODIGO", updatable = false, insertable = false),
            @JoinColumn(name = "EJERCICIO", referencedColumnName = "EJERCICIO", updatable = false, insertable = false)
    })
    @NotFound(action = NotFoundAction.IGNORE)
    private AsignaturaDTO tfgExtAsignatura;

    @OneToMany(mappedBy = "propuesta")
    @NotFound(action = NotFoundAction.IGNORE)
    private Set<PropuestaTutorDTO> propuestaTutores;

    public PropuestaDTO() {
    }

    public PropuestaDTO(Long propuesta_id) {
        this.id = propuesta_id;
    }

    public PropuestaDTO(Long propuesta_id, String propuesta_nombre)
    {
        this.id = propuesta_id;
        this.nombre = propuesta_nombre;
    }

    public long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }


    public String getBibliografia()
    {
        return this.bibliografia;
    }

    public void setBibliografia(String bibliografia)
    {
        this.bibliografia = bibliografia;
    }

    public String getDescripcion()
    {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion)
    {
        this.descripcion = descripcion;
    }

    public BigDecimal getEspera() {
        return this.espera;
    }

    public void setEspera(BigDecimal espera) {
        this.espera = espera;
    }

    public Long getPerIdCreacion() {
        return perIdCreacion;
    }

    public void setPerIdCreacion(Long perIdCreacion) {
        this.perIdCreacion = perIdCreacion;
    }

    public Date getFechaCreacion() {
        return this.fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getNombre()
    {
        return this.nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getObjetivos()
    {
        return this.objetivos;
    }

    public void setObjetivos(String objetivos)
    {
        this.objetivos = objetivos;
    }

    public String getObservaciones()
    {
        return this.observaciones;
    }

    public void setObservaciones(String observaciones)
    {
        this.observaciones = observaciones;
    }

    public BigDecimal getPlazas()
    {
        return this.plazas;
    }

    public void setPlazas(BigDecimal plazas)
    {
        this.plazas = plazas;
    }

    public int getEjercicio() {
        return ejercicio;
    }

    public void setEjercicio(int ejercicio) {
        this.ejercicio = ejercicio;
    }
    public List<PersonasPropuestaDTO> getTfgPersonasPropuestas()
    {
        return this.tfgPersonasPropuestas;
    }

    public void setTfgPersonasPropuestas(List<PersonasPropuestaDTO> tfgPersonasPropuestas)
    {
        this.tfgPersonasPropuestas = tfgPersonasPropuestas;
    }

    public PersonasPropuestaDTO addTfgPersonasPropuesta(PersonasPropuestaDTO tfgPersonasPropuesta)
    {
        getTfgPersonasPropuestas().add(tfgPersonasPropuesta);
        tfgPersonasPropuesta.setTfgPropuesta(this);

        return tfgPersonasPropuesta;
    }

    public Set<PropuestaTutorDTO> getPropuestaTutores() {
        return propuestaTutores;
    }

    public void setPropuestaTutores(Set<PropuestaTutorDTO> propuestaTutores) {
        this.propuestaTutores = propuestaTutores;
    }

    public PersonasPropuestaDTO removeTfgPersonasPropuesta(PersonasPropuestaDTO tfgPersonasPropuesta)
    {
        getTfgPersonasPropuestas().remove(tfgPersonasPropuesta);
        tfgPersonasPropuesta.setTfgPropuesta(null);

        return tfgPersonasPropuesta;
    }

    public AsignaturaDTO getTfgExtAsignatura()
    {
        return this.tfgExtAsignatura;
    }

    public void setTfgExtAsignatura(AsignaturaDTO tfgExtAsignatura)
    {
        this.tfgExtAsignatura = tfgExtAsignatura;
    }

    public String getCodigo_asignatura() {
        return codigo_asignatura;
    }

    public void setCodigo_asignatura(String codigo_asignatura) {
        this.codigo_asignatura = codigo_asignatura;
    }

    public Boolean compruebaTutor(Long tutorId){
        return this.propuestaTutores.stream().anyMatch(propuestaTutorDTO -> propuestaTutorDTO.getTutor().getId() == tutorId);
    }
}
