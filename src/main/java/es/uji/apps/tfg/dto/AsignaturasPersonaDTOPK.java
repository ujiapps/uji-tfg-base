package es.uji.apps.tfg.dto;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the TFG_EXT_ASIGNATURAS_PERSONAS database table.
 * 
 */
@Embeddable
public class AsignaturasPersonaDTOPK implements Serializable
{
    // default serial version id, required for serializable classes.
    private static final long serialVersionUID = 1L;

    @Column(name = "PERSONA_ID")
    private long personaId;

    @Column(name = "CODIGO_ASIGNATURA")
    private String codigoAsignatura;

    @Column(name = "TIPO_RELACION")
    private String tipoRelacion;

    public AsignaturasPersonaDTOPK()
    {
    }

    public long getPersonaId()
    {
        return this.personaId;
    }

    public void setPersonaId(long personaId)
    {
        this.personaId = personaId;
    }

    public String getCodigoAsignatura()
    {
        return this.codigoAsignatura;
    }

    public void setCodigoAsignatura(String codigoAsignatura)
    {
        this.codigoAsignatura = codigoAsignatura;
    }

    public String getTipoRelacion()
    {
        return this.tipoRelacion;
    }

    public void setTipoRelacion(String tipoRelacion)
    {
        this.tipoRelacion = tipoRelacion;
    }

    public boolean equals(Object other)
    {
        if (this == other)
        {
            return true;
        }
        if (!(other instanceof AsignaturasPersonaDTOPK))
        {
            return false;
        }
        AsignaturasPersonaDTOPK castOther = (AsignaturasPersonaDTOPK) other;
        return (this.personaId == castOther.personaId) && (this.codigoAsignatura == castOther.codigoAsignatura) && this.tipoRelacion.equals(castOther.tipoRelacion);
    }

    public int hashCode()
    {
        final int prime = 31;
        int hash = 17;
        hash = hash * prime + ((int) (this.personaId ^ (this.personaId >>> 32)));
        hash = hash * prime + this.codigoAsignatura.hashCode();
        hash = hash * prime + this.tipoRelacion.hashCode();

        return hash;
    }
}