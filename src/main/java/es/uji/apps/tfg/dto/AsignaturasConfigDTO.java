package es.uji.apps.tfg.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * The persistent class for the TFG_ASIGNATURAS_CONFIG database table.
 * 
 */
@Entity
@Table(name = "TFG_ASIGNATURAS_CONFIG", uniqueConstraints={@UniqueConstraint(columnNames={"ejercicio", "codigo_asignatura"})})
public class AsignaturasConfigDTO implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "CODIGO_ASIGNATURA")
    private String codigoAsignatura;

    @Column(name = "ejercicio")
    private int ejercicio;

//    @Temporal(TemporalType.DATE)
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FECHA_LIMITE")
    private Date fechaLimite;

    @Column(name = "ORDENACION")
    private String ordenacion;

    @Column(name = "HABILITAR_ESPERA")
    private Boolean habilitar_espera;

//    @Temporal(TemporalType.DATE)
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "PERMITIR_SOLICITUDES")
    private Date permitirSolicitudes;

    @Column(name = "notificar_coordinador")
    private Boolean notificarCoordinador;


    @OneToOne
    @JoinColumns({
            @JoinColumn(name = "ejercicio", referencedColumnName = "ejercicio", insertable = false, updatable = false),
            @JoinColumn(name = "codigo_asignatura", referencedColumnName = "codigo", insertable = false, updatable = false)
    })
    private AsignaturaDTO tfgExtAsignatura;



    public AsignaturasConfigDTO()
    {
    }

    public AsignaturasConfigDTO(String codigo_asignatura, int ejercicio) {
        setCodigoAsignatura(codigo_asignatura);
        setEjercicio(ejercicio);
    }

    public String getCodigoAsignatura() {
        return codigoAsignatura;
    }

    public void setCodigoAsignatura(String codigoAsignatura) {
        this.codigoAsignatura = codigoAsignatura;
    }

    public int getEjercicio() {
        return ejercicio;
    }

    public void setEjercicio(int ejercicio) {
        this.ejercicio = ejercicio;
    }

    public Date getFechaLimite()
    {
        return this.fechaLimite;
    }

    public void setFechaLimite(Date fechaLimite)
    {
        this.fechaLimite = fechaLimite;
    }

    public String getOrdenacion()
    {
        return this.ordenacion;
    }

    public void setOrdenacion(String ordenacion)
    {
        this.ordenacion = ordenacion;
    }

    @OneToOne
    public AsignaturaDTO getTfgExtAsignatura()
    {
        return this.tfgExtAsignatura;
    }

    public void setTfgExtAsignatura(AsignaturaDTO tfgExtAsignatura)
    {
        this.tfgExtAsignatura = tfgExtAsignatura;
    }

    public Boolean isHabilitar_espera() {
        return habilitar_espera;
    }

    public void setHabilitar_espera(Boolean habilitar_espera) {
        this.habilitar_espera = habilitar_espera;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getPermitirSolicitudes() {
        return permitirSolicitudes;
    }

    public void setPermitirSolicitudes(Date permitirSolicitud) {
        this.permitirSolicitudes = permitirSolicitud;
    }

    public void setNotificarCoordinador(Boolean notificarCoordinador) {
        this.notificarCoordinador = notificarCoordinador;
    }

    public Boolean isNotificarCoordinador() {
        return notificarCoordinador;
    }


}
