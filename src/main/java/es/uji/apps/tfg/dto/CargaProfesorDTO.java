package es.uji.apps.tfg.dto;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * The persistent class for the TFG_CARGA_PROFESORES database table.
 * 
 */
@Entity
@Table(name = "TFG_CARGA_PROFESORES")
public class CargaProfesorDTO implements Serializable
{
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private CargaProfesorDTOPK id;

    @Column(name = "CARGA")
    private BigDecimal carga;

    @Column(name = "CARGA_ORIGINAL")
    private BigDecimal cargaOriginal;

    @Column(name = "EJERCICIO")
    private int ejercicio;

    @Column(name = "CODIGO_ASIGNATURA", updatable = false, insertable = false)
    private String codigoAsignatura;

    @Autowired
    @ManyToOne
    @JoinColumns({
            @JoinColumn(name = "CODIGO_ASIGNATURA", referencedColumnName = "CODIGO", updatable = false, insertable = false),
            @JoinColumn(name = "EJERCICIO", referencedColumnName = "EJERCICIO", updatable = false, insertable = false)
    })
    @NotFound(action = NotFoundAction.IGNORE)
    private AsignaturaDTO tfgExtAsignatura;

    // bi-directional many-to-one association to PersonaDTO
    @ManyToOne
    @JoinColumn(name = "PERSONA_ID", referencedColumnName = "ID", updatable = false, insertable = false)
    @NotFound(action= NotFoundAction.IGNORE)
    private PersonaDTO tfgExtPersona;

    // uni-directional many-to-one association to PersonaDTO
    @ManyToOne
    @JoinColumn(name = "EDITOR_PERSONA_ID", referencedColumnName = "ID", updatable = false, insertable = false)
    private PersonaDTO tfgExtEditor;

    public CargaProfesorDTO()
    {
    }
    
    public CargaProfesorDTO(CargaProfesorDTOPK id) {
        setId(id);
    }

    public CargaProfesorDTO(long persona_id, String codigoAsignatura)
    {
        CargaProfesorDTOPK nuevoPK = new CargaProfesorDTOPK(persona_id, codigoAsignatura);
        nuevoPK.setCodigoAsignatura(codigoAsignatura);
        nuevoPK.setPersonaId(persona_id);        
        CargaProfesorDTO nuevo = new CargaProfesorDTO(nuevoPK);
        setCarga(nuevo.getCarga());
        setCargaOriginal(nuevo.getCargaOriginal());
        setTfgExtAsignatura(nuevo.getTfgExtAsignatura());
        setTfgExtEditor(nuevo.getTfgExtEditor());
        setTfgExtPersona(nuevo.getTfgExtPersona());
    }

    public CargaProfesorDTOPK getId()
    {
        return this.id;
    }

    public void setId(CargaProfesorDTOPK id)
    {
        this.id = id;
    }

    public BigDecimal getCarga()
    {
        return this.carga;
    }

    public void setCarga(BigDecimal carga)
    {
        this.carga = carga;
    }

    public BigDecimal getCargaOriginal()
    {
        return this.cargaOriginal;
    }

    public void setCargaOriginal(BigDecimal cargaOriginal)
    {
        this.cargaOriginal = cargaOriginal;
    }

    public AsignaturaDTO getTfgExtAsignatura()
    {
        return this.tfgExtAsignatura;
    }

    public void setTfgExtAsignatura(AsignaturaDTO tfgExtAsignatura)
    {
        this.tfgExtAsignatura = tfgExtAsignatura;
    }

    public PersonaDTO getTfgExtPersona()
    {
        return this.tfgExtPersona;
    }

    public void setTfgExtPersona(PersonaDTO tfgExtPersona)
    {
        this.tfgExtPersona = tfgExtPersona;
    }

    public PersonaDTO getTfgExtEditor()
    {
        return this.tfgExtEditor;
    }

    public void setTfgExtEditor(PersonaDTO tfgExtEditor)
    {
        this.tfgExtEditor = tfgExtEditor;
    }

    public int getEjercicio() {
        return ejercicio;
    }

    public void setEjercicio(int ejercicio) {
        this.ejercicio = ejercicio;
    }

    public String getCodigoAsignatura() {
        return codigoAsignatura;
    }

    public void setCodigoAsignatura(String codigoAsignatura) {
        this.codigoAsignatura = codigoAsignatura;
    }
}
