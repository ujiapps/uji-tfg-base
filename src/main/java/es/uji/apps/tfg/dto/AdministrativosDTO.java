package es.uji.apps.tfg.dto;


import es.uji.apps.tfg.model.Administrativo;

import javax.persistence.*;

@Entity
@Table(name = "TFG_ADMINISTRATIVOS_ACCESO_SPI")
public class AdministrativosDTO {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "PER_ID")
    private Long personaId;

    @Column(name = "CODIGO_ASIGNATURA")
    private String codigoAsignatura;

    public AdministrativosDTO() {
    }

    public AdministrativosDTO(Administrativo administrativo) {
        this.personaId = administrativo.getPersonaId();
        this.codigoAsignatura = administrativo.getCodigoAsignatura();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPersonaId() {
        return personaId;
    }

    public void setPersonaId(Long personaId) {
        this.personaId = personaId;
    }

    public String getCodigoAsignatura() {
        return codigoAsignatura;
    }

    public void setCodigoAsignatura(String codigoAsignatura) {
        this.codigoAsignatura = codigoAsignatura;
    }
}
