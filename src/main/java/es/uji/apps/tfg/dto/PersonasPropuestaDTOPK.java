package es.uji.apps.tfg.dto;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the TFG_PERSONAS_PROPUESTAS database table.
 * 
 */
@Embeddable
public class PersonasPropuestaDTOPK implements Serializable
{
    // default serial version id, required for serializable classes.
    private static final long serialVersionUID = 1L;

    @Column(name = "PERSONA_ID")
    private long personaId;

    @Column(name = "PROPUESTA_ID")
    private long propuestaId;

    public PersonasPropuestaDTOPK()
    {
    }

    public long getPersonaId()
    {
        return this.personaId;
    }

    public void setPersonaId(long personaId)
    {
        this.personaId = personaId;
    }

    public long getPropuestaId()
    {
        return this.propuestaId;
    }

    public void setPropuestaId(long propuestaId)
    {
        this.propuestaId = propuestaId;
    }

    public boolean equals(Object other)
    {
        if (this == other)
        {
            return true;
        }
        if (!(other instanceof PersonasPropuestaDTOPK))
        {
            return false;
        }
        PersonasPropuestaDTOPK castOther = (PersonasPropuestaDTOPK) other;
        return (this.personaId == castOther.personaId) && (this.propuestaId == castOther.propuestaId);
    }

    public int hashCode()
    {
        final int prime = 31;
        int hash = 17;
        hash = hash * prime + ((int) (this.personaId ^ (this.personaId >>> 32)));
        hash = hash * prime + ((int) (this.propuestaId ^ (this.propuestaId >>> 32)));

        return hash;
    }
}