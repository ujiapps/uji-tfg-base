package es.uji.apps.tfg.dto;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the TFG_EXT_NOTAS_ALUMNOS database table.
 * 
 */
@Embeddable
public class NotasAlumnoDTOPK implements Serializable
{
    // default serial version id, required for serializable classes.
    private static final long serialVersionUID = 1L;

    @Column(name = "PERSONA_ID")
    private long personaId;

    @Column(name = "ESTUDIO_ID")
    private long estudioId;

    public NotasAlumnoDTOPK()
    {
    }

    public long getPersonaId()
    {
        return this.personaId;
    }

    public void setPersonaId(long personaId)
    {
        this.personaId = personaId;
    }

    public long getEstudioId()
    {
        return this.estudioId;
    }

    public void setEstudioId(long estudioId)
    {
        this.estudioId = estudioId;
    }

    public boolean equals(Object other)
    {
        if (this == other)
        {
            return true;
        }
        if (!(other instanceof NotasAlumnoDTOPK))
        {
            return false;
        }
        NotasAlumnoDTOPK castOther = (NotasAlumnoDTOPK) other;
        return (this.personaId == castOther.personaId) && (this.estudioId == castOther.estudioId);
    }

    public int hashCode()
    {
        final int prime = 31;
        int hash = 17;
        hash = hash * prime + ((int) (this.personaId ^ (this.personaId >>> 32)));
        hash = hash * prime + ((int) (this.estudioId ^ (this.estudioId >>> 32)));

        return hash;
    }
}