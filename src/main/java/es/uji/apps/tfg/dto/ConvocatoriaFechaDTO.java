package es.uji.apps.tfg.dto;

import es.uji.apps.tfg.model.Convocatoria;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "TFG_CONVOCATORIAS_FECHAS")
public class ConvocatoriaFechaDTO implements Serializable {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Id
    @Column(name = "ASIGNATURA_ID")
    private String asignaturaId;
    @Id
    @Column(name = "CURSO_ACA")
    private Integer cursoAcademico;

    @Column(name = "FECHA_INICIO")
    @Temporal(TemporalType.DATE)
    private Date fechaInicio;

    @Column(name = "FECHA_FIN")
    @Temporal(TemporalType.DATE)
    private Date fechaFin;

    public ConvocatoriaFechaDTO(Convocatoria convocatoria) {
        this.id = convocatoria.getId();
        this.asignaturaId = convocatoria.getAsignaturaId();
        this.cursoAcademico = convocatoria.getCursoAcademico();
        this.fechaInicio = convocatoria.getFechaInicio();
        this.fechaFin = convocatoria.getFechaFin();
    }

    public ConvocatoriaFechaDTO() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAsignaturaId() {
        return asignaturaId;
    }

    public void setAsignaturaId(String asignaturaId) {
        this.asignaturaId = asignaturaId;
    }

    public Integer getCursoAcademico() {
        return cursoAcademico;
    }

    public void setCursoAcademico(Integer cursoAcademico) {
        this.cursoAcademico = cursoAcademico;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }
}
