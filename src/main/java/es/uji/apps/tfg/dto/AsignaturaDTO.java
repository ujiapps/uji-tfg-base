package es.uji.apps.tfg.dto;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

@Entity
@Table(name = "TFG_EXT_ASIGNATURAS")
@AssociationOverride(name ="tfgExtAsignatura.tfgAsignaturasConfig",
        joinColumns = {
                @JoinColumn(name ="ejercicio",referencedColumnName = "EJERCICIO"),
                @JoinColumn(name = "codigo", referencedColumnName = "CODIGO_ASIGNATURA")
        }
)

public class AsignaturaDTO implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "CODIGO")
    private String codigo;

    @Id
    @Column(name = "EJERCICIO")
    private int ejercicio;

    private BigDecimal creditos;

    @Column(name = "ESTUDIO_ID")
    private BigDecimal estudioId;

    private BigDecimal matriculas;

    private String nombre;

    // bi-directional one-to-one association to AsignaturasConfigDTO
    @OneToOne(mappedBy = "tfgExtAsignatura", fetch = FetchType.LAZY)
    @NotFound(action = NotFoundAction.IGNORE)
    private AsignaturasConfigDTO tfgAsignaturasConfig;

    // bi-directional many-to-one association to CargaProfesorDTO
    @OneToMany(mappedBy = "tfgExtAsignatura")
    private List<CargaProfesorDTO> tfgCargaProfesores;

    // bi-directional many-to-one association to AsignaturasPersonaDTO
    @OneToMany(mappedBy = "tfgExtAsignatura")
    private List<AsignaturasPersonaDTO> tfgExtAsignaturasPersonas;

    // bi-directional many-to-one association to PropuestaDTO
    @OneToMany(mappedBy = "tfgExtAsignatura")
    private List<PropuestaDTO> tfgPropuestas;

    // bi-directional many-to-one association to TurnoDTO
    @OneToMany(mappedBy = "tfgExtAsignatura")
    private List<TurnoDTO> tfgTurnos;

    public AsignaturaDTO()
    {
    }

    public AsignaturaDTO(String codigo, int ejercicio)
    {
        setCodigo(codigo);
        setEjercicio(ejercicio);
    }

    public String getCodigo() {
        return this.codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public int getEjercicio() {
        return this.ejercicio;
    }

    public void setEjercicio(int ejercicio) {
        this.ejercicio = ejercicio;
    }

    public BigDecimal getCreditos()
    {
        return this.creditos;
    }

    public void setCreditos(BigDecimal creditos)
    {
        this.creditos = creditos;
    }

    public BigDecimal getEstudioId()
    {
        return this.estudioId;
    }

    public void setEstudioId(BigDecimal estudioId)
    {
        this.estudioId = estudioId;
    }

    public BigDecimal getMatriculas()
    {
        return this.matriculas;
    }

    public void setMatriculas(BigDecimal matriculas)
    {
        this.matriculas = matriculas;
    }

    public String getNombre()
    {
        return this.nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public AsignaturasConfigDTO getTfgAsignaturasConfig()
    {
        return this.tfgAsignaturasConfig;
    }

    public void setTfgAsignaturasConfig(AsignaturasConfigDTO tfgAsignaturasConfig)
    {
        this.tfgAsignaturasConfig = tfgAsignaturasConfig;
    }

    public List<CargaProfesorDTO> getTfgCargaProfesores()
    {
        return this.tfgCargaProfesores;
    }

    public void setTfgCargaProfesores(List<CargaProfesorDTO> tfgCargaProfesores)
    {
        this.tfgCargaProfesores = tfgCargaProfesores;
    }

    public CargaProfesorDTO addTfgCargaProfesore(CargaProfesorDTO tfgCargaProfesore)
    {
        getTfgCargaProfesores().add(tfgCargaProfesore);
        tfgCargaProfesore.setTfgExtAsignatura(this);

        return tfgCargaProfesore;
    }

    public CargaProfesorDTO removeTfgCargaProfesore(CargaProfesorDTO tfgCargaProfesore)
    {
        getTfgCargaProfesores().remove(tfgCargaProfesore);
        tfgCargaProfesore.setTfgExtAsignatura(null);

        return tfgCargaProfesore;
    }

    public List<AsignaturasPersonaDTO> getTfgExtAsignaturasPersonas()
    {
        return this.tfgExtAsignaturasPersonas;
    }

    public void setTfgExtAsignaturasPersonas(List<AsignaturasPersonaDTO> tfgExtAsignaturasPersonas)
    {
        this.tfgExtAsignaturasPersonas = tfgExtAsignaturasPersonas;
    }

    public AsignaturasPersonaDTO addTfgExtAsignaturasPersona(AsignaturasPersonaDTO tfgExtAsignaturasPersona)
    {
        getTfgExtAsignaturasPersonas().add(tfgExtAsignaturasPersona);
        tfgExtAsignaturasPersona.setTfgExtAsignatura(this);

        return tfgExtAsignaturasPersona;
    }

    public AsignaturasPersonaDTO removeTfgExtAsignaturasPersona(AsignaturasPersonaDTO tfgExtAsignaturasPersona)
    {
        getTfgExtAsignaturasPersonas().remove(tfgExtAsignaturasPersona);
        tfgExtAsignaturasPersona.setTfgExtAsignatura(null);

        return tfgExtAsignaturasPersona;
    }

    public List<PropuestaDTO> getTfgPropuestas()
    {
        return this.tfgPropuestas;
    }

    public void setTfgPropuestas(List<PropuestaDTO> tfgPropuestas)
    {
        this.tfgPropuestas = tfgPropuestas;
    }

    public PropuestaDTO addTfgPropuesta(PropuestaDTO tfgPropuesta)
    {
        getTfgPropuestas().add(tfgPropuesta);
        tfgPropuesta.setTfgExtAsignatura(this);

        return tfgPropuesta;
    }

    public PropuestaDTO removeTfgPropuesta(PropuestaDTO tfgPropuesta)
    {
        getTfgPropuestas().remove(tfgPropuesta);
        tfgPropuesta.setTfgExtAsignatura(null);

        return tfgPropuesta;
    }

    public List<TurnoDTO> getTfgTurnos()
    {
        return this.tfgTurnos;
    }

    public void setTfgTurnos(List<TurnoDTO> tfgTurnos)
    {
        this.tfgTurnos = tfgTurnos;
    }

    public TurnoDTO addTfgTurno(TurnoDTO tfgTurno)
    {
        getTfgTurnos().add(tfgTurno);
        tfgTurno.setTfgExtAsignatura(this);

        return tfgTurno;
    }

    public TurnoDTO removeTfgTurno(TurnoDTO tfgTurno)
    {
        getTfgTurnos().remove(tfgTurno);
        tfgTurno.setTfgExtAsignatura(null);

        return tfgTurno;
    }
}
