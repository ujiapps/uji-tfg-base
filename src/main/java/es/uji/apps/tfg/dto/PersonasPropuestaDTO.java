package es.uji.apps.tfg.dto;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * The persistent class for the TFG_PERSONAS_PROPUESTAS database table.
 * 
 */
@Entity
@Table(name = "TFG_PERSONAS_PROPUESTAS")
public class PersonasPropuestaDTO implements Serializable
{
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private PersonasPropuestaDTOPK id;

    private BigDecimal aceptada;

    @Column(name = "FECHA_EDICION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaEdicion;

    // bi-directional many-to-one association to PersonaDTO
    @ManyToOne
    @JoinColumn(name = "PERSONA_ID", referencedColumnName = "ID", updatable = false, insertable = false)
    @NotFound(action= NotFoundAction.IGNORE)
    private PersonaDTO tfgExtPersona;

    // bi-directional many-to-one association to PersonaDTO
    @ManyToOne
    @JoinColumn(name = "EDITOR_PERSONA_ID", referencedColumnName = "ID", updatable = false, insertable = false)
    @NotFound(action= NotFoundAction.IGNORE)
    private PersonaDTO tfgExtEditor;

    // bi-directional many-to-one association to PropuestaDTO
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name = "PROPUESTA_ID", referencedColumnName = "ID", updatable = false, insertable = false)
    private PropuestaDTO tfgPropuesta;

    private String comentarios;
    
    public PersonasPropuestaDTO()
    {
    }

    public PersonasPropuestaDTOPK getId()
    {
        return this.id;
    }

    public void setId(PersonasPropuestaDTOPK id)
    {
        this.id = id;
    }

    public BigDecimal getAceptada()
    {
        return this.aceptada;
    }

    public void setAceptada(BigDecimal aceptada)
    {
        this.aceptada = aceptada;
    }

    public Date getFechaEdicion()
    {
        return this.fechaEdicion;
    }

    public void setFechaEdicion(Date fechaEdicion)
    {
        this.fechaEdicion = fechaEdicion;
    }

    public PersonaDTO getTfgExtPersona()
    {
        return this.tfgExtPersona;
    }

    public void setTfgExtPersona(PersonaDTO tfgExtPersona)
    {
        this.tfgExtPersona = tfgExtPersona;
    }

    public PersonaDTO getTfgExtEditor()
    {
        return this.tfgExtEditor;
    }

    public void setTfgExtEditor(PersonaDTO tfgExtEditor)
    {
        this.tfgExtEditor = tfgExtEditor;
    }

    public PropuestaDTO getTfgPropuesta()
    {
        return this.tfgPropuesta;
    }

    public void setTfgPropuesta(PropuestaDTO tfgPropuesta)
    {
        this.tfgPropuesta = tfgPropuesta;
    }

    public String getComentarios()
    {
        return comentarios;
    }

    public void setComentarios(String comentarios)
    {
        this.comentarios = comentarios;
    }

}
