package es.uji.apps.tfg.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

/**
 * The persistent class for the TFG_EXT_PERSONAS database table.
 * 
 */
@Entity
@Table(name = "TFG_EXT_PERSONAS")
public class PersonaDTO implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    private long id;

    @Column(name = "EMAIL")
    private String email;

    private String nombre;
    
    private String apellido1;
    
    private String apellido2;

    // bi-directional many-to-one association to CargaProfesorDTO
    @OneToMany(mappedBy = "tfgExtPersona")
    private Set<CargaProfesorDTO> tfgCargaProfesores;

    // bi-directional many-to-one association to AsignaturasPersonaDTO
    @OneToMany(mappedBy = "tfgExtPersona")
    private Set<AsignaturasPersonaDTO> tfgExtAsignaturasPersonas;

    // bi-directional many-to-one association to NotasAlumnoDTO
    @OneToMany(mappedBy = "tfgExtPersona")
    private Set<NotasAlumnoDTO> tfgExtNotasAlumnos;

    // bi-directional many-to-one association to PersonasPropuestaDTO
    @OneToMany(mappedBy = "tfgExtPersona")
    private Set<PersonasPropuestaDTO> tfgPersonasPropuestas;

    // bi-directional many-to-one association to PersonasPropuestaDTO
    @OneToMany(mappedBy = "tfgExtEditor")
    private Set<PersonasPropuestaDTO> tfgPropuestasEditadas;

    @OneToMany(mappedBy = "tutor")
    private Set<PropuestaTutorDTO> propuestaTutores;

    // bi-directional many-to-one association to TurnoDTO
    @OneToMany(mappedBy = "tfgExtPersona")
    private Set<TurnoDTO> tfgTurnos;

    public PersonaDTO()
    {
    }

    public PersonaDTO(Long persona_id)
    {
        setId(persona_id);
    }

    public long getId()
    {
        return this.id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public String getEmail()
    {
        return this.email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getNombre()
    {
        return this.nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public Set<CargaProfesorDTO> getTfgCargaProfesores()
    {
        return this.tfgCargaProfesores;
    }

    public void setTfgCargaProfesores(Set<CargaProfesorDTO> tfgCargaProfesores)
    {
        this.tfgCargaProfesores = tfgCargaProfesores;
    }

    public CargaProfesorDTO addTfgCargaProfesore(CargaProfesorDTO tfgCargaProfesore)
    {
        getTfgCargaProfesores().add(tfgCargaProfesore);
        tfgCargaProfesore.setTfgExtPersona(this);

        return tfgCargaProfesore;
    }

    public CargaProfesorDTO removeTfgCargaProfesore(CargaProfesorDTO tfgCargaProfesore)
    {
        getTfgCargaProfesores().remove(tfgCargaProfesore);
        tfgCargaProfesore.setTfgExtPersona(null);

        return tfgCargaProfesore;
    }

    public Set<AsignaturasPersonaDTO> getTfgExtAsignaturasPersonas()
    {
        return this.tfgExtAsignaturasPersonas;
    }

    public void setTfgExtAsignaturasPersonas(Set<AsignaturasPersonaDTO> tfgExtAsignaturasPersonas)
    {
        this.tfgExtAsignaturasPersonas = tfgExtAsignaturasPersonas;
    }

    public AsignaturasPersonaDTO addTfgExtAsignaturasPersona(AsignaturasPersonaDTO tfgExtAsignaturasPersona)
    {
        getTfgExtAsignaturasPersonas().add(tfgExtAsignaturasPersona);
        tfgExtAsignaturasPersona.setTfgExtPersona(this);

        return tfgExtAsignaturasPersona;
    }

    public AsignaturasPersonaDTO removeTfgExtAsignaturasPersona(AsignaturasPersonaDTO tfgExtAsignaturasPersona)
    {
        getTfgExtAsignaturasPersonas().remove(tfgExtAsignaturasPersona);
        tfgExtAsignaturasPersona.setTfgExtPersona(null);

        return tfgExtAsignaturasPersona;
    }

    public Set<NotasAlumnoDTO> getTfgExtNotasAlumnos()
    {
        return this.tfgExtNotasAlumnos;
    }

    public void setTfgExtNotasAlumnos(Set<NotasAlumnoDTO> tfgExtNotasAlumnos)
    {
        this.tfgExtNotasAlumnos = tfgExtNotasAlumnos;
    }

    public NotasAlumnoDTO addTfgExtNotasAlumno(NotasAlumnoDTO tfgExtNotasAlumno)
    {
        getTfgExtNotasAlumnos().add(tfgExtNotasAlumno);
        tfgExtNotasAlumno.setTfgExtPersona(this);

        return tfgExtNotasAlumno;
    }

    public NotasAlumnoDTO removeTfgExtNotasAlumno(NotasAlumnoDTO tfgExtNotasAlumno)
    {
        getTfgExtNotasAlumnos().remove(tfgExtNotasAlumno);
        tfgExtNotasAlumno.setTfgExtPersona(null);

        return tfgExtNotasAlumno;
    }

    public Set<PersonasPropuestaDTO> getTfgPersonasPropuestas()
    {
        return this.tfgPersonasPropuestas;
    }

    public void setTfgPersonasPropuestas(Set<PersonasPropuestaDTO> tfgPersonasPropuestas)
    {
        this.tfgPersonasPropuestas = tfgPersonasPropuestas;
    }

    public PersonasPropuestaDTO addTfgPersonasPropuesta(PersonasPropuestaDTO tfgPersonasPropuesta)
    {
        getTfgPersonasPropuestas().add(tfgPersonasPropuesta);
        tfgPersonasPropuesta.setTfgExtPersona(this);

        return tfgPersonasPropuesta;
    }

    public PersonasPropuestaDTO removeTfgPersonasPropuesta(PersonasPropuestaDTO tfgPersonasPropuesta)
    {
        getTfgPersonasPropuestas().remove(tfgPersonasPropuesta);
        tfgPersonasPropuesta.setTfgExtPersona(null);

        return tfgPersonasPropuesta;
    }

    public Set<PersonasPropuestaDTO> getTfgPropuestasEditadas()
    {
        return this.tfgPropuestasEditadas;
    }

    public void setTfgPropuestasEditadas(Set<PersonasPropuestaDTO> tfgPropuestasEditadas)
    {
        this.tfgPropuestasEditadas = tfgPropuestasEditadas;
    }

    public PersonasPropuestaDTO addTfgPropuestasEditada(PersonasPropuestaDTO tfgPropuestasEditada)
    {
        getTfgPropuestasEditadas().add(tfgPropuestasEditada);
        tfgPropuestasEditada.setTfgExtEditor(this);

        return tfgPropuestasEditada;
    }

    public PersonasPropuestaDTO removeTfgPropuestasEditada(PersonasPropuestaDTO tfgPropuestasEditada)
    {
        getTfgPropuestasEditadas().remove(tfgPropuestasEditada);
        tfgPropuestasEditada.setTfgExtEditor(null);

        return tfgPropuestasEditada;
    }

    public Set<PropuestaTutorDTO> getPropuestaTutores() {
        return propuestaTutores;
    }

    public void setPropuestaTutores(Set<PropuestaTutorDTO> propuestaTutores) {
        this.propuestaTutores = propuestaTutores;
    }

    public Set<TurnoDTO> getTfgTurnos()
    {
        return this.tfgTurnos;
    }

    public void setTfgTurnos(Set<TurnoDTO> tfgTurnos)
    {
        this.tfgTurnos = tfgTurnos;
    }

    public TurnoDTO addTfgTurno(TurnoDTO tfgTurno)
    {
        getTfgTurnos().add(tfgTurno);
        tfgTurno.setTfgExtPersona(this);

        return tfgTurno;
    }

    public TurnoDTO removeTfgTurno(TurnoDTO tfgTurno)
    {
        getTfgTurnos().remove(tfgTurno);
        tfgTurno.setTfgExtPersona(null);

        return tfgTurno;
    }

    public String getApellido1()
    {
        return apellido1;
    }

    public void setApellido1(String apellido1)
    {
        this.apellido1 = apellido1;
    }

    public String getApellido2()
    {
        return apellido2;
    }

    public void setApellido2(String apellido2)
    {
        this.apellido2 = apellido2;
    }
    
    public String getNombreCompleto() 
    {
        String salida = "";
        if (getNombre() != null && getNombre().length() > 0) {
            salida = getNombre();
        }
        if (getApellido1() != null && getApellido1().length() > 0) {
            salida += " "  + getApellido1();
            if (getApellido2() != null && getApellido2().length() > 0) {
                salida += " "  + getApellido2();
            }
        }
        return salida;
    }

}
