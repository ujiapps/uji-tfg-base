package es.uji.apps.tfg.dto;


import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "TFG_PROPUESTAS_TUTORES")
public class PropuestaTutorDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "TUTOR_ID")
    @NotFound(action = NotFoundAction.IGNORE)
    private PersonaDTO tutor;

    @ManyToOne
    @JoinColumn(name = "PROPUESTA_ID",referencedColumnName = "ID")
    private PropuestaDTO propuesta;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PersonaDTO getTutor() {
        return tutor;
    }

    public void setTutor(PersonaDTO tutor) {
        this.tutor = tutor;
    }

    public PropuestaDTO getPropuesta() {
        return propuesta;
    }

    public void setPropuesta(PropuestaDTO propuesta) {
        this.propuesta = propuesta;
    }
}
