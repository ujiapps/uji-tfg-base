package es.uji.apps.tfg.dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "TGF_VW_ASIGNACION_PROPUESTAS")
public class AsignacionesPropuestaVWDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "PERSONA_ID")
    private Long personaId;

    @Column(name = "NOMBRE")
    private String nombre;

    @Column(name = "APELLIDO1")
    private String apellido1;

    @Column(name = "APELLIDO2")
    private String apellido2;

    @Id
    @Column(name = "PROPUESTA_ID")
    private Long propuestaId;

    @Column(name = "PROPUESTA_NOMBRE")
    private String propuestaNombre;

    @Column(name = "CODIGO_ASIGNATURA")
    private String codigoAsignatura;

    @Id
    private Integer ejercicio;
    private Integer aceptada;
    private String comentarios;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FECHA_EDICION")
    private Date fechaEdicion;

    @Id
    @Column(name = "TUTOR_ID")
    private Long tutorId;

    @Column(name = "TUTORES_NOMBRE")
    private String tutoresNombre;

    public Long getPersonaId() {
        return personaId;
    }

    public void setPersonaId(Long personaId) {
        this.personaId = personaId;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido1() {
        return apellido1;
    }

    public void setApellido1(String apellido1) {
        this.apellido1 = apellido1;
    }

    public String getApellido2() {
        return apellido2;
    }

    public void setApellido2(String apellido2) {
        this.apellido2 = apellido2;
    }

    public Long getPropuestaId() {
        return propuestaId;
    }

    public void setPropuestaId(Long propuestaId) {
        this.propuestaId = propuestaId;
    }

    public String getPropuestaNombre() {
        return propuestaNombre;
    }

    public void setPropuestaNombre(String propuestaNombre) {
        this.propuestaNombre = propuestaNombre;
    }

    public String getCodigoAsignatura() {
        return codigoAsignatura;
    }

    public void setCodigoAsignatura(String codigoAsignatura) {
        this.codigoAsignatura = codigoAsignatura;
    }

    public Integer getEjercicio() {
        return ejercicio;
    }

    public void setEjercicio(Integer ejercicio) {
        this.ejercicio = ejercicio;
    }

    public Integer getAceptada() {
        return aceptada;
    }

    public void setAceptada(Integer aceptada) {
        this.aceptada = aceptada;
    }

    public String getComentarios() {
        return comentarios;
    }

    public void setComentarios(String comentarios) {
        this.comentarios = comentarios;
    }

    public Date getFechaEdicion() {
        return fechaEdicion;
    }

    public void setFechaEdicion(Date fechaEdicion) {
        this.fechaEdicion = fechaEdicion;
    }

    public Long getTutorId() {
        return tutorId;
    }

    public void setTutorId(Long tutorId) {
        this.tutorId = tutorId;
    }

    public String getTutoresNombre() {
        return tutoresNombre;
    }

    public void setTutoresNombre(String tutoresNombre) {
        this.tutoresNombre = tutoresNombre;
    }
}
