package es.uji.apps.tfg.dto;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;

/**
 * The persistent class for the TFG_EXT_NOTAS_ALUMNOS database table.
 * 
 */
@Entity
@Table(name = "TFG_EXT_NOTAS_ALUMNOS")
public class NotasAlumnoDTO implements Serializable
{
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private NotasAlumnoDTOPK id;

    private BigDecimal media;

    // bi-directional many-to-one association to PersonaDTO
    @ManyToOne
    @JoinColumn(name = "PERSONA_ID", referencedColumnName = "ID", updatable = false, insertable = false)
    @NotFound(action= NotFoundAction.IGNORE)
    private PersonaDTO tfgExtPersona;

    public NotasAlumnoDTO()
    {
    }

    public NotasAlumnoDTOPK getId()
    {
        return this.id;
    }

    public void setId(NotasAlumnoDTOPK id)
    {
        this.id = id;
    }

    public BigDecimal getMedia()
    {
        return this.media;
    }

    public void setMedia(BigDecimal media)
    {
        this.media = media;
    }

    public PersonaDTO getTfgExtPersona()
    {
        return this.tfgExtPersona;
    }

    public void setTfgExtPersona(PersonaDTO tfgExtPersona)
    {
        this.tfgExtPersona = tfgExtPersona;
    }

}
