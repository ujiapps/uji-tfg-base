package es.uji.apps.tfg.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "TFG_VW_ESTANCIAS_PRACTICAS")
public class EstanciasPracticasVWDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "CURSO_ACA")
    private Integer cursoAca;
    @Id
    @Column(name = "ESTUDIO_ID")
    private Long estudioId;
    @Id
    @Column(name = "ASI_ID")
    private String asignaturaId;
    @Id
    @Column(name = "ALUMNO_ID")
    private Long alumnoId;
    @Column(name = "ALUMNO_CUENTA")
    private String alumnoCuenta;
    @Column(name = "TITULO")
    private String titulo;

    @Column(name = "TAREAS")
    private String tareas;

    @Column(name = "CONOCIMIENTOS")
    private String conocimientos;

    @Column(name = "OBSERVACIONES")
    private String observaciones;

    @Column(name = "TUTOR_ID")
    private Long tutorId;
    @Column(name = "TUTOR_NOMBRE")
    private String tutorNombre;

    public Integer getCursoAca() {
        return cursoAca;
    }

    public void setCursoAca(Integer cursoAca) {
        this.cursoAca = cursoAca;
    }

    public Long getEstudioId() {
        return estudioId;
    }

    public void setEstudioId(Long estudioId) {
        this.estudioId = estudioId;
    }

    public String getAsignaturaId() {
        return asignaturaId;
    }

    public void setAsignaturaId(String asignaturaId) {
        this.asignaturaId = asignaturaId;
    }

    public Long getAlumnoId() {
        return alumnoId;
    }

    public void setAlumnoId(Long alumnoId) {
        this.alumnoId = alumnoId;
    }

    public String getAlumnoCuenta() {
        return alumnoCuenta;
    }

    public void setAlumnoCuenta(String alumnoCuenta) {
        this.alumnoCuenta = alumnoCuenta;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getTareas() {
        return tareas;
    }

    public void setTareas(String tareas) {
        this.tareas = tareas;
    }

    public String getConocimientos() {
        return conocimientos;
    }

    public void setConocimientos(String conocimientos) {
        this.conocimientos = conocimientos;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public Long getTutorId() {
        return tutorId;
    }

    public void setTutorId(Long tutorId) {
        this.tutorId = tutorId;
    }

    public String getTutorNombre() {
        return tutorNombre;
    }

    public void setTutorNombre(String tutorNombre) {
        this.tutorNombre = tutorNombre;
    }
}
