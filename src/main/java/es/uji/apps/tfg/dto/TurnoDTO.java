package es.uji.apps.tfg.dto;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;

/**
 * The persistent class for the TFG_TURNOS database table.
 * 
 */
@Entity
@Table(name = "TFG_TURNOS")
public class TurnoDTO implements Serializable
{
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private TurnoDTOPK id;

    private Timestamp fecha;

    @Column(name = "EJERCICIO")
    private int ejercicio;

    @Autowired
    @ManyToOne
    @JoinColumns ({
            @JoinColumn(name = "CODIGO_ASIGNATURA", referencedColumnName = "CODIGO", updatable = false, insertable = false),
            @JoinColumn(name = "EJERCICIO", referencedColumnName = "EJERCICIO", updatable = false, insertable = false)
    })
    @NotFound(action= NotFoundAction.IGNORE)
    private AsignaturaDTO tfgExtAsignatura;

    // bi-directional many-to-one association to PersonaDTO
    @ManyToOne
    @JoinColumn(name = "PERSONA_ID", referencedColumnName = "ID", updatable = false, insertable = false)
    @NotFound(action= NotFoundAction.IGNORE)
    private PersonaDTO tfgExtPersona;

    public TurnoDTO()
    {
    }

    public TurnoDTOPK getId()
    {
        return this.id;
    }

    public void setId(TurnoDTOPK id)
    {
        this.id = id;
    }

    public Timestamp getFecha()
    {
        return this.fecha;
    }

    public void setFecha(Timestamp fecha)
    {
        this.fecha = fecha;
    }

    public AsignaturaDTO getTfgExtAsignatura()
    {
        return this.tfgExtAsignatura;
    }

    public void setTfgExtAsignatura(AsignaturaDTO tfgExtAsignatura)
    {
        this.tfgExtAsignatura = tfgExtAsignatura;
    }

    public PersonaDTO getTfgExtPersona()
    {
        return this.tfgExtPersona;
    }

    public void setTfgExtPersona(PersonaDTO tfgExtPersona)
    {
        this.tfgExtPersona = tfgExtPersona;
    }

    public int getEjercicio() {
        return ejercicio;
    }

    public void setEjercicio(int ejercicio) {
        this.ejercicio = ejercicio;
    }
}
