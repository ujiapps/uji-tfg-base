package es.uji.apps.tfg.dto;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;

/**
 * The persistent class for the TFG_EXT_ASIGNATURAS_PERSONAS database table.
 * 
 */
@Entity
@Table(name = "TFG_EXT_ASIGNATURAS_PERSONAS")
public class AsignaturasPersonaDTO implements Serializable
{
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private AsignaturasPersonaDTOPK id;

    private BigDecimal creditos;

    private int ejercicio;

    // bi-directional many-to-one association to AsignaturaDTO
    @ManyToOne
    @JoinColumns({
            @JoinColumn(name = "CODIGO_ASIGNATURA", referencedColumnName = "CODIGO", updatable = false, insertable = false),
            @JoinColumn(name = "EJERCICIO", referencedColumnName = "EJERCICIO", updatable = false, insertable = false)
    })
    private AsignaturaDTO tfgExtAsignatura;

    // bi-directional many-to-one association to PersonaDTO
    @ManyToOne
    @JoinColumn(name = "PERSONA_ID", referencedColumnName = "ID", updatable = false, insertable = false, nullable = true)
    @NotFound(action= NotFoundAction.IGNORE)
    private PersonaDTO tfgExtPersona;

    public AsignaturasPersonaDTO()
    {
    }

    public AsignaturasPersonaDTOPK getId()
    {
        return this.id;
    }

    public void setId(AsignaturasPersonaDTOPK id)
    {
        this.id = id;
    }

    public BigDecimal getCreditos()
    {
        return this.creditos;
    }

    public void setCreditos(BigDecimal creditos)
    {
        this.creditos = creditos;
    }

    public AsignaturaDTO getTfgExtAsignatura()
    {
        return this.tfgExtAsignatura;
    }

    public void setTfgExtAsignatura(AsignaturaDTO tfgExtAsignatura)
    {
        this.tfgExtAsignatura = tfgExtAsignatura;
    }

    public PersonaDTO getTfgExtPersona()
    {
        return this.tfgExtPersona;
    }

    public void setTfgExtPersona(PersonaDTO tfgExtPersona)
    {
        this.tfgExtPersona = tfgExtPersona;
    }

    public int getEjercicio() {
        return ejercicio;
    }

    public void setEjercicio(int ejercicio) {
        this.ejercicio = ejercicio;
    }
}
