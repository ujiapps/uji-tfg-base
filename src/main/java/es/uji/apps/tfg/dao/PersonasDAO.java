package es.uji.apps.tfg.dao;

import com.mysema.query.Tuple;
import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;
import es.uji.apps.tfg.dto.*;
import es.uji.apps.tfg.model.Carga;
import es.uji.apps.tfg.model.Profesor;
import es.uji.apps.tfg.model.QProfesor;
import es.uji.apps.tfg.utils.AcademicYear;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.db.LookupDAO;
import es.uji.commons.db.Utils;
import es.uji.commons.rest.StringUtils;
import es.uji.commons.rest.xml.lookup.LookupItem;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import javax.persistence.Query;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Repository
public class PersonasDAO extends BaseDAODatabaseImpl implements LookupDAO {
    private QPersonaDTO qPersonaDTO = QPersonaDTO.personaDTO;
    private QAsignaturasPersonaDTO qAsignaturasPersonaDTO = QAsignaturasPersonaDTO.asignaturasPersonaDTO;
    private QCargaProfesorDTO qCargaProfesorDTO = QCargaProfesorDTO.cargaProfesorDTO;
    private QTurnoDTO qTurnoDTO = QTurnoDTO.turnoDTO;

    public List<PersonaDTO> getUsuarios() {
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qPersonaDTO).list(qPersonaDTO);
    }

    public PersonaDTO getUsuario(Long id) {
        if (id == null) {
            throw new EntityNotFoundException();
        }
        JPAQuery query = new JPAQuery(entityManager);
        List<PersonaDTO> listado = query.from(qPersonaDTO).where(qPersonaDTO.id.eq(id)).limit(1).list(qPersonaDTO);
        if (listado.size() > 0)
            return listado.get(0);
        return null;
    }

    public Profesor getUsuarioProfesor(Long id) {
        if (id == null) {
            throw new EntityNotFoundException();
        }
        JPAQuery query = new JPAQuery(entityManager);
        QCargaProfesorDTO tfgProfesores = new QCargaProfesorDTO("tfgProfesores");
        QAsignaturasPersonaDTO tfgAsignaturasPersonas = new QAsignaturasPersonaDTO("tfgAsignaturasPersonas");
        QPropuestaDTO tfgPropuestas = new QPropuestaDTO("tfgPropuestas");
        QPropuestaTutorDTO qPropuestaTutorDTO = QPropuestaTutorDTO.propuestaTutorDTO;
        List<Profesor> listado = query.from(qPersonaDTO)
                .innerJoin(qPersonaDTO.tfgExtAsignaturasPersonas, tfgAsignaturasPersonas).fetch()
                .leftJoin(qPersonaDTO.tfgCargaProfesores, tfgProfesores).fetch()
                .leftJoin(qPersonaDTO.propuestaTutores, qPropuestaTutorDTO).fetch()
                .leftJoin(qPropuestaTutorDTO.propuesta, tfgPropuestas).fetch()
                .where(qPersonaDTO.id.eq(id)
                        .and(tfgAsignaturasPersonas.ejercicio.eq(AcademicYear.getCurrent()))
                ).limit(1).list(new QProfesor(qPersonaDTO.id, qPersonaDTO.nombre, qPersonaDTO.apellido1, qPersonaDTO.apellido2, qCargaProfesorDTO.carga, qAsignaturasPersonaDTO.creditos));
        ;
        if (listado.size() > 0)
            return listado.get(0);
        return null;
    }

    public List<Profesor> getProfesoresByAsignatura(String codigo_asignatura) {
        JPAQuery query = new JPAQuery(entityManager);
        QAsignaturasPersonaDTO qAsignaturasPersonaDTO = new QAsignaturasPersonaDTO("apJOIN");
        QCargaProfesorDTO qCargaProfesorDTO = new QCargaProfesorDTO("qpJOIN");
        QPropuestaDTO qPropuestaDTO = new QPropuestaDTO("pJOIN");
        QPropuestaTutorDTO qPropuestaTutorDTO = QPropuestaTutorDTO.propuestaTutorDTO;

        return query.from(qPersonaDTO).join(qPersonaDTO.tfgExtAsignaturasPersonas, qAsignaturasPersonaDTO)
                .leftJoin(qPersonaDTO.tfgCargaProfesores, qCargaProfesorDTO).on(qCargaProfesorDTO.ejercicio.eq(AcademicYear.getCurrent()).and(qCargaProfesorDTO.codigoAsignatura.eq(codigo_asignatura)))
                .leftJoin(qPersonaDTO.propuestaTutores, qPropuestaTutorDTO)
                .leftJoin(qPropuestaTutorDTO.propuesta, qPropuestaDTO).on(qPropuestaDTO.ejercicio.eq(AcademicYear.getCurrent()).and(qPropuestaDTO.codigo_asignatura.eq(codigo_asignatura)))
                .where(qAsignaturasPersonaDTO.id.codigoAsignatura.eq(codigo_asignatura)
                        .and(qAsignaturasPersonaDTO.id.tipoRelacion.eq("PROFESOR"))
                        .and(qAsignaturasPersonaDTO.ejercicio.eq(AcademicYear.getCurrent()))
                ).groupBy(qPersonaDTO.id, qPersonaDTO.nombre, qPersonaDTO.apellido1, qPersonaDTO.apellido2, qCargaProfesorDTO.carga, qAsignaturasPersonaDTO.creditos)
                .orderBy(qPersonaDTO.apellido1.asc()).orderBy(qPersonaDTO.apellido2.asc()).orderBy(qPersonaDTO.nombre.asc())
                .list(new QProfesor(qPersonaDTO.id, qPersonaDTO.nombre, qPersonaDTO.apellido1, qPersonaDTO.apellido2, qCargaProfesorDTO.carga.coalesce(BigDecimal.ZERO), qAsignaturasPersonaDTO.creditos, qPropuestaDTO.plazas.sum().coalesce(BigDecimal.ZERO)));

    }

    public List<Profesor> getProfesoresByAsignaturaPasado(String codigo_asignatura) {
        JPAQuery query = new JPAQuery(entityManager);
        QAsignaturasPersonaDTO qAsignaturasPersonaDTO = new QAsignaturasPersonaDTO("apJOIN");
        QCargaProfesorDTO qCargaProfesorDTO = new QCargaProfesorDTO("qpJOIN");
        QPropuestaDTO qPropuestaDTO = new QPropuestaDTO("pJOIN");
        QPropuestaTutorDTO qPropuestaTutorDTO = QPropuestaTutorDTO.propuestaTutorDTO;

        return query.from(qPersonaDTO).join(qPersonaDTO.tfgExtAsignaturasPersonas, qAsignaturasPersonaDTO)
                .leftJoin(qPersonaDTO.tfgCargaProfesores, qCargaProfesorDTO)
                .on(qCargaProfesorDTO.ejercicio.eq(AcademicYear.getCurrent() - 1).and(qCargaProfesorDTO.codigoAsignatura.eq(codigo_asignatura)))
                .leftJoin(qPersonaDTO.propuestaTutores, qPropuestaTutorDTO)
                .leftJoin(qPropuestaTutorDTO.propuesta, qPropuestaDTO)
                .on(qPropuestaDTO.ejercicio.eq(AcademicYear.getCurrent() - 1).and(qPropuestaDTO.codigo_asignatura.eq(codigo_asignatura)))
                .where(qAsignaturasPersonaDTO.id.codigoAsignatura.eq(codigo_asignatura)
                        .and(qAsignaturasPersonaDTO.id.tipoRelacion.eq("PROFESOR"))
                        .and(qAsignaturasPersonaDTO.ejercicio.eq(AcademicYear.getCurrent() - 1))
                ).groupBy(qPersonaDTO.id, qPersonaDTO.nombre, qPersonaDTO.apellido1, qPersonaDTO.apellido2, qCargaProfesorDTO.carga, qAsignaturasPersonaDTO.creditos)
                .list(new QProfesor(qPersonaDTO.id, qPersonaDTO.nombre, qPersonaDTO.apellido1, qPersonaDTO.apellido2, qCargaProfesorDTO.carga.coalesce(BigDecimal.ZERO), qAsignaturasPersonaDTO.creditos, qPropuestaDTO.plazas.sum().coalesce(BigDecimal.ZERO)));

    }

    public List<PersonaDTO> getProfesoresByCoordinador(Long id) {
        if (id == null) {
            throw new EntityNotFoundException();
        }
        List<PersonaDTO> salida = new ArrayList<PersonaDTO>();
        JPAQuery query = new JPAQuery(entityManager);
        //1111 Modificado para evitar las desapariciones en tfg_ext_persona
        QPersonaDTO existen = new QPersonaDTO("existen");
        query.from(qAsignaturasPersonaDTO).innerJoin(qAsignaturasPersonaDTO.tfgExtPersona, existen)
                .where(qAsignaturasPersonaDTO.id.personaId.eq(id)
                        .and(qAsignaturasPersonaDTO.id.tipoRelacion.eq("COORDINADOR"))
//                        .and(qAsignaturasPersonaDTO.ejercicio.eq(AcademicYear.getCurrent()))
                );
        List<String> listadoAsignaturas = query.distinct().list(qAsignaturasPersonaDTO.id.codigoAsignatura);
        JPAQuery query2 = new JPAQuery(entityManager);
        if (listadoAsignaturas.size() == 0) {
            return salida;
        }
        query2.from(qAsignaturasPersonaDTO).innerJoin(qAsignaturasPersonaDTO.tfgExtPersona, existen).where(
                qAsignaturasPersonaDTO.id.codigoAsignatura.in(listadoAsignaturas)
                        .and(qAsignaturasPersonaDTO.id.tipoRelacion.eq("PROFESOR"))
                        .and(qAsignaturasPersonaDTO.ejercicio.eq(AcademicYear.getCurrent())));
        List<Long> listado = query2.distinct().list(qAsignaturasPersonaDTO.id.personaId);
        if (listado.size() > 0) {
            for (Long idPersona : listado) {
                PersonaDTO usuarioDTO = getUsuario(idPersona);
                salida.add(usuarioDTO);
            }
        }
        return salida;
    }

    public Integer getCargaProfesor(Long id, String codigo_asignatura) {
        if (id == null) {
            throw new EntityNotFoundException();
        }
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qCargaProfesorDTO).where(qCargaProfesorDTO.id.personaId.eq(id)
                .and(qCargaProfesorDTO.id.codigoAsignatura.eq(codigo_asignatura))
                .and(qCargaProfesorDTO.ejercicio.eq(AcademicYear.getCurrent())));
        BigDecimal result = query.singleResult(qCargaProfesorDTO.carga);
        if (result != null) {
            return result.intValue();
        }
        return null;
    }

    public Integer getCargaTotalProfesor(Long id) {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qCargaProfesorDTO).where(qCargaProfesorDTO.id.personaId.eq(id).and(qCargaProfesorDTO.ejercicio.eq(AcademicYear.getCurrent())));
        BigDecimal result = query.singleResult(qCargaProfesorDTO.carga.sum());
        if (result != null) {
            return result.intValue();
        }
        return null;
    }

    public CargaProfesorDTO getCargaProfesorDTO(Long persona_id, String codigo_asignatura) {
        if (persona_id == null) {
            throw new EntityNotFoundException();
        }
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qCargaProfesorDTO).where(qCargaProfesorDTO.id.personaId.eq(persona_id)
                .and(qCargaProfesorDTO.id.codigoAsignatura.eq(codigo_asignatura)
                        .and(qCargaProfesorDTO.ejercicio.eq(AcademicYear.getCurrent())))
        );
        return query.singleResult(QCargaProfesorDTO.cargaProfesorDTO);
    }

    @Transactional
    public long updateCargaProfesor(Long persona_id, String codigo_asignatura, Float carga, long editor_id) {
        if (persona_id == null) {
            throw new EntityNotFoundException();
        }
        JPAUpdateClause update = new JPAUpdateClause(entityManager, qCargaProfesorDTO);
        update.set(qCargaProfesorDTO.tfgExtEditor.id, editor_id).set(qCargaProfesorDTO.carga, new BigDecimal(carga))
                .where(qCargaProfesorDTO.id.codigoAsignatura.eq(codigo_asignatura)
                        .and(qCargaProfesorDTO.ejercicio.eq(AcademicYear.getCurrent())
                                .and(qCargaProfesorDTO.id.personaId.eq(persona_id))));
        long result = update.execute();
        return result;
    }

    @Transactional
    public CargaProfesorDTO createCargaProfesor(Long persona_id, String codigo_asignatura, Integer carga, Long editor_id) {
        if (persona_id == null) {
            throw new EntityNotFoundException();
        }
        deleteCargaProfesor(persona_id, codigo_asignatura);
        CargaProfesorDTO cargaProfesorDTO = new CargaProfesorDTO();
        CargaProfesorDTOPK cargaProfesorDTOPK = new CargaProfesorDTOPK();
        cargaProfesorDTOPK.setCodigoAsignatura(codigo_asignatura);
        cargaProfesorDTOPK.setPersonaId(persona_id);
        cargaProfesorDTO.setId(cargaProfesorDTOPK);
        cargaProfesorDTO.setCarga(new BigDecimal(carga));
        cargaProfesorDTO.setTfgExtAsignatura(new AsignaturaDTO(codigo_asignatura, AcademicYear.getCurrent()));
        cargaProfesorDTO.setCargaOriginal(new BigDecimal(carga));
        cargaProfesorDTO.setEjercicio(AcademicYear.getCurrent());
        if (editor_id != null) {
            cargaProfesorDTO.setTfgExtEditor(new PersonaDTO(editor_id));
        }
        cargaProfesorDTO.setTfgExtPersona(new PersonaDTO(persona_id));
        entityManager.persist(cargaProfesorDTO);
        return cargaProfesorDTO;
    }

    @Transactional
    public boolean deleteCargaProfesor(Long persona_id, String codigo_asignatura) {
        if (persona_id == null) {
            throw new EntityNotFoundException();
        }
        JPADeleteClause del = new JPADeleteClause(entityManager, qCargaProfesorDTO);
        long result = del.where(qCargaProfesorDTO.id.personaId.eq(persona_id)
                .and(qCargaProfesorDTO.id.codigoAsignatura.eq(codigo_asignatura)
                        .and(qCargaProfesorDTO.ejercicio.eq(AcademicYear.getCurrent())))
        ).execute();

        return result > 0L;
    }

    @Transactional
    public boolean deleteCargaProfesoresAsignatura(String codigo_asignatura) {
        JPADeleteClause del = new JPADeleteClause(entityManager, qCargaProfesorDTO);
        long result = del.where(qCargaProfesorDTO.id.codigoAsignatura.eq(codigo_asignatura)
                .and(qCargaProfesorDTO.ejercicio.eq(AcademicYear.getCurrent()))
        ).execute();

        return result > 0L;
    }

    @Transactional
    public boolean distribuirCargaProfesores(String codigo_asignatura, Long editor_id) {

        if (editor_id == null) {
            throw new EntityNotFoundException();
        }
        String sql = "select sum((case when tipo_relacion = 'ESTUDIANTE' THEN  1 ELSE 0 END)) as matriculas, "
                + "sum((case when tipo_relacion = 'PROFESOR' then creditos else 0 end)) as creditos "
                + "from tfg_ext_asignaturas_personas ap "
                + "inner join tfg_ext_personas p on p.id = ap.persona_id "  //1111 Añadido para corregir la desaparición de usuarios en tfg_ext_personas
                + "where codigo_asignatura = '" + codigo_asignatura + "' and ap.ejercicio = " + AcademicYear.getCurrent();
        Query cnq = entityManager.createNativeQuery(sql);
        Object[] salida = (Object[]) cnq.getSingleResult();
        BigDecimal matriculas = new BigDecimal(salida[0].toString());
        BigDecimal creditos = new BigDecimal(salida[1].toString());
        JPAQuery query = new JPAQuery(entityManager);
        QPersonaDTO profesoresDTO = new QPersonaDTO("profesoresDTO");
        query.from(qAsignaturasPersonaDTO)
                .innerJoin(qAsignaturasPersonaDTO.tfgExtPersona, profesoresDTO)
                .where(qAsignaturasPersonaDTO.id.tipoRelacion.eq("PROFESOR")
                        .and(qAsignaturasPersonaDTO.id.codigoAsignatura.eq(codigo_asignatura))
                        .and(qAsignaturasPersonaDTO.ejercicio.eq(AcademicYear.getCurrent()))
                        .and(qAsignaturasPersonaDTO.tfgExtAsignatura.ejercicio.eq(AcademicYear.getCurrent())));

        List<AsignaturasPersonaDTO> asignaturasPersonaDTO = query.list(qAsignaturasPersonaDTO);

        BigDecimal ratio = matriculas.subtract(new BigDecimal(asignaturasPersonaDTO.size())).divide(creditos, 2, RoundingMode.HALF_DOWN);
        if (ratio.floatValue() <= 0) {
            return false;
        }
        deleteCargaProfesoresAsignatura(codigo_asignatura);
        List<Carga> distribucion = distribuyeCargas(matriculas, ratio, asignaturasPersonaDTO);
        distribucion.stream().forEach(cargaProfesor -> {
            createCargaProfesor(cargaProfesor.getProfesorId(), codigo_asignatura, cargaProfesor.getCarga(), editor_id);
        });
        return true;
    }

    private List<Carga> distribuyeCargas(BigDecimal matriculas, BigDecimal ratio, List<AsignaturasPersonaDTO> asignaturasPersonaDTO) {
        List<Carga> distribucion = new ArrayList<>();
        Integer asignados = 0;
        for (AsignaturasPersonaDTO profeAsig : asignaturasPersonaDTO) {
            BigDecimal cargaOriginal = profeAsig.getCreditos().multiply(ratio);
            Integer cargaResultado = cargaOriginal.setScale(0, RoundingMode.FLOOR).intValue();
            if (cargaOriginal.intValue() == 0) {
                cargaResultado = 0;
            } else {
                cargaResultado += 1;
            }
            asignados += cargaResultado;
            long profesor_id = profeAsig.getId().getPersonaId();
            distribucion.add(new Carga(profesor_id, cargaResultado, cargaOriginal));
        }
        distribucion.sort(Comparator.comparing(Carga::getCargaOriginal).reversed());
        if (distribucion.size() > 0) {
            for (Integer indice = 0; (matriculas.intValue() - asignados) > 0; indice++) {
                distribucion.get(indice).setCarga(distribucion.get(indice).getCarga() + 1);
                asignados++;
                if (distribucion.size() - 1 == indice) {
                    indice = -1;
                }
            }
        }
        return distribucion;
    }

    public boolean coordinaAsignatura(Long persona_id, String codigo_asignatura) {
        if (persona_id == null) {
            throw new EntityNotFoundException();
        }
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qAsignaturasPersonaDTO).where(
                qAsignaturasPersonaDTO.id.codigoAsignatura.eq(codigo_asignatura)
                        .and(qAsignaturasPersonaDTO.id.personaId.eq(persona_id))
//                        .and(qAsignaturasPersonaDTO.ejercicio.eq(AcademicYear.getCurrent()))
                        .and(qAsignaturasPersonaDTO.id.tipoRelacion.eq("COORDINADOR")));
        return query.count() > 0;
    }

    public List<PersonaDTO> getEstudiantesByAsignatura(String codigo_asignatura) {
        JPAQuery query = new JPAQuery(entityManager);
        //1111 Modificado para evitar las desapariciones en tfg_ext_persona
        QPersonaDTO existen = new QPersonaDTO("existen");
        query.from(qAsignaturasPersonaDTO).innerJoin(qAsignaturasPersonaDTO.tfgExtPersona, existen).where(
                qAsignaturasPersonaDTO.id.codigoAsignatura.eq(codigo_asignatura)
                        .and(qAsignaturasPersonaDTO.id.tipoRelacion.eq("ESTUDIANTE")
                                .and(qAsignaturasPersonaDTO.ejercicio.eq(AcademicYear.getCurrent()))));
        List<Long> listado = query.list(qAsignaturasPersonaDTO.id.personaId);
        List<PersonaDTO> salida = new ArrayList<PersonaDTO>();
        for (Long id : listado) {
            salida.add(getUsuario(id));
        }
        return salida;
    }

    @SuppressWarnings("unchecked")
    public List<Object[]> getPlanAsignacionesByNotaMedia(String codigo_asignatura) {
        int year = AcademicYear.getCurrent();
        String sqlString = "select distinct p.id as persona_id, p.nombre as nombre, p.apellido1 as apellido1, p.apellido2 as apellido2, p.email as email, "
                + "a.codigo as codigo_asignatura, nvl(na.media, 0) as criterio, na.titulado AS titulado, t.fecha as fecha_apertura from "
                + "tfg_ext_personas p inner join tfg_ext_asignaturas_personas ap on ap.persona_id = p.id and ap.tipo_relacion = 'ESTUDIANTE' "
                + "and ap.ejercicio = " + year + " "
                + "inner join tfg_ext_asignaturas a on a.codigo = ap.codigo_asignatura and a.ejercicio = " + year + " "
                + "inner join tfg_asignaturas_config ac on a.codigo = ac.codigo_asignatura and ac.ejercicio = " + year + " "
                + "inner join tfg_ext_notas_alumnos na on (na.estudio_id = 100 or na.estudio_id = a.estudio_id)  and na.persona_id = p.id "
                + "left join tfg_turnos t on t.persona_id = p.id and t.codigo_asignatura = a.codigo and t.ejercicio = " + year + " where a.codigo = '" + codigo_asignatura + "' and a.ejercicio = " + year + " "
                + "and not EXISTS (select * from TFG_PERSONAS_PROPUESTAS prop join tfg_propuestas propu on prop.propuesta_id=propu.id "
                + "where prop.persona_id=p.id and propu.codigo_asignatura= '" + codigo_asignatura + "' and aceptada=1 and propu.ejercicio=" + year + ")"
                + "order by na.titulado DESC, criterio DESC";
        
        Query cnq = entityManager.createNativeQuery(sqlString);
        List<Object[]> salida = (List<Object[]>) cnq.getResultList();
        return salida;
    }

    @SuppressWarnings("unchecked")
    public List<Date> getExcludedDays() {
        String sql = "select * from tfg_ext_festivos";
        Query cnq = entityManager.createNativeQuery(sql);
        List<Date> salida = (List<Date>) cnq.getResultList();
        return salida;
    }

    @Transactional
    public boolean updateAsignacionPlan(List<TurnoDTO> turnos, String codigo_asignatura) {
        int year = AcademicYear.getCurrent();
        JPADeleteClause delete = new JPADeleteClause(entityManager, qTurnoDTO);

        delete.where(qTurnoDTO.id.codigoAsignatura.eq(codigo_asignatura)
                .and(qTurnoDTO.id.personaId.in(turnos.stream().map(v -> v.getId().getPersonaId()).collect(Collectors.toList())))
                .and(qTurnoDTO.ejercicio.eq(year)));

        delete.execute();
        String sql = "INSERT ALL ";
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        for (TurnoDTO turno : turnos) {
            sql += "INTO tfg_turnos (persona_id, codigo_asignatura, fecha, ejercicio) VALUES ";
            sql += "(" + turno.getId().getPersonaId() + ", ";
            sql += "'" + codigo_asignatura + "', ";
            sql += "TO_DATE('" + sdf.format(new Date(turno.getFecha().getTime())) + "', 'DD/MM/YYYY HH24:MI'), ";
            sql += year + ") ";

        }
        sql += " SELECT 1 FROM DUAL";
        Query cnq = entityManager.createNativeQuery(sql);
        cnq.executeUpdate();
        return true;
    }

    public TurnoDTO getTurno(long persona_id, String codigo_asignatura) {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qTurnoDTO).where(qTurnoDTO.id.personaId.eq(persona_id)
                .and(qTurnoDTO.ejercicio.eq(AcademicYear.getCurrent())
                        .and(qTurnoDTO.id.codigoAsignatura.eq(codigo_asignatura))));
        List<TurnoDTO> turnos = query.list(QTurnoDTO.turnoDTO);
        if (turnos.size() > 0) {
            return turnos.get(0);
        }
        return null;
    }

    public HashMap<Long, PersonaDTO> getMailEstudiantesByAsignatura(String codigo_asignatura) {
        HashMap<Long, PersonaDTO> salida = new HashMap<Long, PersonaDTO>();
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qAsignaturasPersonaDTO).where(
                qAsignaturasPersonaDTO.id.codigoAsignatura.eq(codigo_asignatura)
                        .and(qAsignaturasPersonaDTO.id.tipoRelacion.eq("ESTUDIANTE"))
                        .and(qAsignaturasPersonaDTO.ejercicio.eq(AcademicYear.getCurrent())));
        List<Tuple> listado = query.list(qAsignaturasPersonaDTO.id.personaId, qAsignaturasPersonaDTO.tfgExtPersona.email,
                qAsignaturasPersonaDTO.tfgExtPersona.nombre);

        for (Tuple fila : listado) {
            PersonaDTO persona = new PersonaDTO();

            Long idPersona = fila.get(0, Long.class);
            String mail = fila.get(1, String.class);
            String nombre = fila.get(2, String.class);
            persona.setId(idPersona);
            persona.setEmail(mail);
            persona.setNombre(nombre);
            salida.put(idPersona, persona);
        }
        return salida;
    }

    public List<Object[]> getPlanAsignacionesByNotaAcceso(String codigo_asignatura) {
        int year = AcademicYear.getCurrent();
        String sqlString = "select distinct p.id as persona_id, p.nombre as nombre, p.apellido1 as apellido1, p.apellido2 as apellido2, p.email as email, "
                + "a.codigo as codigo_asignatura, na.nota_acceso as criterio, na.titulado as titulado, t.fecha as fecha_apertura from "
                + "tfg_ext_personas p inner join tfg_ext_asignaturas_personas ap on ap.persona_id = p.id and ap.tipo_relacion = 'ESTUDIANTE' "
                + "and ap.ejercicio = " + year + " "
                + "inner join tfg_ext_asignaturas a on a.codigo = ap.codigo_asignatura and a.ejercicio = ap.ejercicio "
                + "inner join tfg_asignaturas_config ac on a.codigo = ac.codigo_asignatura and ac.ejercicio = a.ejercicio "
                + "inner join tfg_ext_notas_alumnos na on na.estudio_id = a.estudio_id  and na.persona_id = p.id "
                + "left join tfg_turnos t on t.persona_id = p.id and t.codigo_asignatura = a.codigo and a.ejercicio = t.ejercicio "
                + "where a.codigo = '" + codigo_asignatura + "' and a.ejercicio = " + year + " "
                + "and not EXISTS (select * from TFG_PERSONAS_PROPUESTAS prop join tfg_propuestas propu on prop.propuesta_id=propu.id "
                + "where prop.persona_id=p.id and aceptada=1 and propu.ejercicio=" + year + " and propu.codigo_asignatura='" + codigo_asignatura + "')"
                + "order by na.titulado DESC, na.nota_acceso DESC";
        Query cnq = entityManager.createNativeQuery(sqlString);
        List<Object[]> salida = (List<Object[]>) cnq.getResultList();
        return salida;
    }

    public Map<String, PersonaDTO> getPersonasAsignaturaByMails(String asignaturaCodigo, List<String> correos) {

        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qAsignaturasPersonaDTO).join(qAsignaturasPersonaDTO.tfgExtPersona, qPersonaDTO)
                .where(qPersonaDTO.email.trim().in(correos)
                        .and(qAsignaturasPersonaDTO.ejercicio.eq(AcademicYear.getCurrent()).and(qAsignaturasPersonaDTO.tfgExtAsignatura.codigo.eq(asignaturaCodigo))
                                .and(qPersonaDTO.id.eq(qAsignaturasPersonaDTO.tfgExtPersona.id)))
                ).distinct().list(qPersonaDTO).
                stream().collect(Collectors.toMap(personaDTO -> personaDTO.getEmail().trim(), Function.identity()));

    }

    @Override
    public List search(String s) {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qPersonaDTO);

        Long stringToLong;
        try {
            stringToLong = Long.parseLong(s);
        } catch (NumberFormatException e) {
            stringToLong = null;
        }

        if (stringToLong == null) {
            query.where(Utils.limpia(qPersonaDTO.nombre)
                    .concat(" ")
                    .concat(Utils.limpia(qPersonaDTO.apellido1))
                    .concat(" ")
                    .concat(Utils.limpia(qPersonaDTO.apellido2))
                    .concat(", ")
                    .concat(Utils.limpia(qPersonaDTO.nombre)).containsIgnoreCase(StringUtils.limpiaAcentos(s))
            );
        } else {
            query.where(qPersonaDTO.id.eq(stringToLong));
        }

        return query.list(qPersonaDTO)
                .stream().map(personaDTO -> {
                    LookupItem item = new LookupItem();
                    item.setId(String.valueOf(personaDTO.getId()));

                    String apellido1 = personaDTO.getApellido1() != null ? personaDTO.getApellido1() : "";
                    String apellido2 = personaDTO.getApellido2() != null ? " " + personaDTO.getApellido2() : "";
                    String nombre = "";

                    if (!apellido1.isEmpty() && !apellido2.isEmpty())
                        nombre = apellido1 + apellido2 + ", " + personaDTO.getNombre();
                    else if (!apellido1.isEmpty())
                        nombre = apellido1 + ", " + personaDTO.getNombre();
                    else
                        nombre = personaDTO.getNombre();

                    item.setNombre(nombre);

                    item.addExtraParam("nombre", personaDTO.getNombre());
                    item.addExtraParam("apellido1", personaDTO.getApellido1());
                    item.addExtraParam("apellido2", personaDTO.getApellido2());
                    return item;
                }).collect(Collectors.toList());
    }

    public boolean coordinaAsignatura(Long userId, String codigoAsignatura, Long cursoAca) {
        if (userId == null) {
            throw new EntityNotFoundException();
        }
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qAsignaturasPersonaDTO).where(
                qAsignaturasPersonaDTO.id.codigoAsignatura.eq(codigoAsignatura)
                        .and(qAsignaturasPersonaDTO.id.personaId.eq(userId))
//                        .and(qAsignaturasPersonaDTO.ejercicio.eq(cursoAca.intValue()))
                        .and(qAsignaturasPersonaDTO.id.tipoRelacion.eq("COORDINADOR")));
        return query.count() > 0;
    }
}
