package es.uji.apps.tfg.dao;

import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.tfg.dto.QAdministrativosDTO;
import es.uji.apps.tfg.dto.QPersonasAdministrativoVWDTO;
import es.uji.apps.tfg.model.Administrativo;
import es.uji.apps.tfg.model.QAdministrativo;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.StringUtils;
import es.uji.commons.rest.json.lookup.LookupItem;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

@Repository
public class AccesosSPIDAO extends BaseDAODatabaseImpl {

    private QAdministrativosDTO qAdministrativosDTO = QAdministrativosDTO.administrativosDTO;
    private QPersonasAdministrativoVWDTO qPersonasAdministrativoVWDTO = QPersonasAdministrativoVWDTO.personasAdministrativoVWDTO;

    public List<Administrativo> getAccesosSPI(String codigoAsignatura) {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qAdministrativosDTO).join(qPersonasAdministrativoVWDTO)
                .on(qAdministrativosDTO.personaId.eq(qPersonasAdministrativoVWDTO.id))
                .where(qAdministrativosDTO.codigoAsignatura.eq(codigoAsignatura));

        return query.list(new QAdministrativo(qAdministrativosDTO.id,
                qAdministrativosDTO.personaId,
                qPersonasAdministrativoVWDTO.email,
                qPersonasAdministrativoVWDTO.nombre,
                qPersonasAdministrativoVWDTO.apellido1,
                qPersonasAdministrativoVWDTO.apellido2,
                qAdministrativosDTO.codigoAsignatura
        ));
    }

    public List<LookupItem> searchAdministrativos(String queryString) {
        JPAQuery query = new JPAQuery(entityManager);

        String cadenaLimpia = StringUtils.limpiaAcentos(queryString).toUpperCase();
        Long queryStringLong = 0L;

        try {
            queryStringLong = Long.parseLong(queryString);
        } catch (Exception e) {
        }

        return query.from(qPersonasAdministrativoVWDTO).where(
                        qPersonasAdministrativoVWDTO.nombreCompletoBusqueda.contains(cadenaLimpia)
                                .or(qPersonasAdministrativoVWDTO.email.toUpperCase().contains(cadenaLimpia.toUpperCase()))
                                .or(qPersonasAdministrativoVWDTO.id.eq(queryStringLong)))
                .orderBy(qPersonasAdministrativoVWDTO.nombreCompletoBusqueda.asc())
                .list(qPersonasAdministrativoVWDTO.id, qPersonasAdministrativoVWDTO.nombre, qPersonasAdministrativoVWDTO.apellido1, qPersonasAdministrativoVWDTO.email).stream().map(persona -> {
                    LookupItem item = new LookupItem();
                    item.setId(String.valueOf(persona.get(qPersonasAdministrativoVWDTO.id)));
                    item.setNombre(persona.get(qPersonasAdministrativoVWDTO.nombre).concat(" ").concat(persona.get(qPersonasAdministrativoVWDTO.apellido1)));
                    item.addExtraParam("cuenta", persona.get(qPersonasAdministrativoVWDTO.email));
                    return item;
                }).collect(Collectors.toList());
    }

    public Long deleteAdministrativo(Long id) {
        JPADeleteClause delete = new JPADeleteClause(entityManager, qAdministrativosDTO);

        return delete.where(qAdministrativosDTO.id.eq(id)).execute();
    }
}
