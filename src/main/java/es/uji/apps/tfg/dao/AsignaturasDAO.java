package es.uji.apps.tfg.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;
import es.uji.apps.tfg.dto.*;
import es.uji.apps.tfg.model.Asignatura;
import es.uji.apps.tfg.utils.AcademicYear;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class AsignaturasDAO extends BaseDAODatabaseImpl {
    private QAsignaturasPersonaDTO qAsignaturasPersonaDTO = QAsignaturasPersonaDTO.asignaturasPersonaDTO;
    private QAsignaturasConfigDTO qAsignaturasConfigDTO = QAsignaturasConfigDTO.asignaturasConfigDTO;

    public List<AsignaturaDTO> getAsignaturas() {
        QAsignaturaDTO qAsignaturaDTO = new QAsignaturaDTO("qAsignaturaDTO");
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qAsignaturaDTO).leftJoin(qAsignaturaDTO.tfgAsignaturasConfig, qAsignaturasConfigDTO).fetch()
                .where(qAsignaturaDTO.ejercicio.eq(AcademicYear.getCurrent())).list(qAsignaturaDTO);
    }

    public List<AsignaturaDTO> getAsignaturasByAnyo(Long anyo) {
        QAsignaturaDTO qAsignaturaDTO = new QAsignaturaDTO("qAsignaturaDTO");
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qAsignaturaDTO).leftJoin(qAsignaturaDTO.tfgAsignaturasConfig, qAsignaturasConfigDTO).fetch()
                .where(qAsignaturaDTO.ejercicio.eq(Math.toIntExact(anyo))).list(qAsignaturaDTO);
    }

    public AsignaturaDTO getAsignatura(String codigo)
    {
        QAsignaturaDTO qAsignaturaDTO = QAsignaturaDTO.asignaturaDTO;
        JPAQuery query = new JPAQuery(entityManager);

        List<AsignaturaDTO> val = query.from(qAsignaturaDTO).where(
                qAsignaturaDTO.codigo.eq(codigo).and(qAsignaturaDTO.ejercicio.eq(AcademicYear.getCurrent()))
        ).list(qAsignaturaDTO);
        if (val.size() == 0 )
            return new AsignaturaDTO();
        return val.get(0);
    }

    private List<AsignaturaDTO> getAsignaturasByRelacion(Long persona_id, String role)
    {
        QAsignaturasPersonaDTO qAsignaturasPersonaDTO = QAsignaturasPersonaDTO.asignaturasPersonaDTO;
        if (persona_id == null) {
            return null;
        }
        List<String> busqueda = new ArrayList<String>();
        List<AsignaturaDTO> salida = new ArrayList<AsignaturaDTO>();
        if (role.contains("|"))
        {
            busqueda = new ArrayList<String>(Arrays.asList(role.split("\\|")));
        }
        else
        {
            busqueda.add(role);
        }
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qAsignaturasPersonaDTO)
                .where(qAsignaturasPersonaDTO.id.personaId.eq(persona_id)
                        .and(qAsignaturasPersonaDTO.id.tipoRelacion.in(busqueda)));

        if (!role.equals("COORDINADOR")) {
            query.where(qAsignaturasPersonaDTO.ejercicio.eq(AcademicYear.getCurrent()));
        }

        List<AsignaturaDTO> listado = query.distinct().where(qAsignaturasPersonaDTO.tfgExtAsignatura.ejercicio.eq(AcademicYear.getCurrent()))
                .list(qAsignaturasPersonaDTO.tfgExtAsignatura);
        if (listado.size() > 0)
        {
            for (AsignaturaDTO asignaturaDTO : listado)
            {
                salida.add(creaConfiguracionSiNoExiste(asignaturaDTO));
            }
        }
        return salida;
    }

    private List<AsignaturaDTO> getAsignaturasByRelacionByAnyo(Long persona_id, String role, Long anyo) {
        QAsignaturasPersonaDTO qAsignaturasPersonaDTO = QAsignaturasPersonaDTO.asignaturasPersonaDTO;

        if (persona_id == null) return null;

        List<String> busqueda = new ArrayList<String>();
        List<AsignaturaDTO> salida = new ArrayList<AsignaturaDTO>();

        if (role.contains("|"))
            busqueda = new ArrayList<String>(Arrays.asList(role.split("\\|")));
        else
            busqueda.add(role);

        JPAQuery query = new JPAQuery(entityManager);
        query.from(qAsignaturasPersonaDTO)
                .where(qAsignaturasPersonaDTO.id.personaId.eq(persona_id)
                        .and(qAsignaturasPersonaDTO.id.tipoRelacion.in(busqueda)));

        if (!role.equals("COORDINADOR"))
            query.where(qAsignaturasPersonaDTO.ejercicio.eq(anyo.intValue()));

        List<AsignaturaDTO> listado = query.distinct().where(qAsignaturasPersonaDTO.tfgExtAsignatura.ejercicio.eq(anyo.intValue()))
                .list(qAsignaturasPersonaDTO.tfgExtAsignatura);

        if (!listado.isEmpty()) {
            for (AsignaturaDTO asignaturaDTO : listado)
                salida.add(creaConfiguracionSiNoExiste(asignaturaDTO));
        }

        return salida;
    }

    @Transactional
    public AsignaturaDTO creaConfiguracionSiNoExiste(AsignaturaDTO asignaturaDTO) {
        AsignaturasConfigDTO config = asignaturaDTO.getTfgAsignaturasConfig();
        boolean needToPersist = false;
        if (config == null) {
            needToPersist = true;
            config = new AsignaturasConfigDTO(asignaturaDTO.getCodigo(), asignaturaDTO.getEjercicio());
        }
        if (config.isHabilitar_espera() == null) {
            needToPersist = true;
            config.setHabilitar_espera(true);
        }
        if (config.getOrdenacion() == null) {
            needToPersist = true;
            config.setOrdenacion("NOTA_MEDIA");
        }
        if (needToPersist) {
            entityManager.persist(config);
            asignaturaDTO.setTfgAsignaturasConfig(config);
        }
        return asignaturaDTO;
    }

    public List<AsignaturaDTO> getAsignaturasByCoordinador(Long persona_id)
    {
        return getAsignaturasByRelacion(persona_id, "COORDINADOR");
    }

    public List<AsignaturaDTO> getAsignaturasByCoordinadorByAnyo(Long persona_id, Long anyo) {
        return getAsignaturasByRelacionByAnyo(persona_id, "COORDINADOR", anyo);
    }

    public List<AsignaturaDTO> getAsignaturasByProfesor(Long persona_id)
    {
        return getAsignaturasByRelacion(persona_id, "PROFESOR");
    }

    public Date getFechaLimite(String codigo_asignatura)
    {
        QAsignaturasConfigDTO qAsignaturasConfigDTO = QAsignaturasConfigDTO.asignaturasConfigDTO;
        JPAQuery query = new JPAQuery(entityManager);
        List<Date> fecha_limite = query.from(qAsignaturasConfigDTO).where(
                qAsignaturasConfigDTO.codigoAsignatura.eq(codigo_asignatura).and(qAsignaturasConfigDTO.ejercicio.eq(AcademicYear.getCurrent()))
        ).list(qAsignaturasConfigDTO.fechaLimite);
        if (fecha_limite.size() > 0)
        {
            return fecha_limite.get(0);
        }
        return null;
    }

    @Transactional
    public long setFechaLimite(String codigo_asignatura, Date fechaLimite) {
        QAsignaturasConfigDTO qAsignaturasConfigDTO = QAsignaturasConfigDTO.asignaturasConfigDTO;
        JPAUpdateClause update = new JPAUpdateClause(entityManager, qAsignaturasConfigDTO);
        update.set(qAsignaturasConfigDTO.fechaLimite, fechaLimite).where(
                qAsignaturasConfigDTO.codigoAsignatura.eq(codigo_asignatura).and(qAsignaturasConfigDTO.ejercicio.eq(AcademicYear.getCurrent()))
        );
        long result = update.execute();
        return result;
    }

    @Transactional
    public long setPermitirSolicitudes(String codigo_asignatura, Date fecha) {
        QAsignaturasConfigDTO qAsignaturasConfigDTO = QAsignaturasConfigDTO.asignaturasConfigDTO;
        JPAUpdateClause update = new JPAUpdateClause(entityManager, qAsignaturasConfigDTO);
        update.set(qAsignaturasConfigDTO.permitirSolicitudes, fecha).where(
                qAsignaturasConfigDTO.codigoAsignatura.eq(codigo_asignatura).and(qAsignaturasConfigDTO.ejercicio.eq(AcademicYear.getCurrent()))
        );
        long result = update.execute();
        return result;
    }

    public String getOrdenacion(String codigo_asignatura) {
        QAsignaturasConfigDTO qAsignaturasConfigDTO = QAsignaturasConfigDTO.asignaturasConfigDTO;
        JPAQuery query = new JPAQuery(entityManager);
        List<String> ordenacion = query.from(qAsignaturasConfigDTO).where(
                qAsignaturasConfigDTO.codigoAsignatura.eq(codigo_asignatura).and(qAsignaturasConfigDTO.ejercicio.eq(AcademicYear.getCurrent()))
        ).list(qAsignaturasConfigDTO.ordenacion);
        if (ordenacion.size() > 0) {
            return ordenacion.get(0);
        }
        return null;
    }

    @Transactional
    public long setOrdenacion(String codigo_asignatura, String ordenacion)
    {
        QAsignaturasConfigDTO qAsignaturasConfigDTO = QAsignaturasConfigDTO.asignaturasConfigDTO;
        JPAUpdateClause update = new JPAUpdateClause(entityManager, qAsignaturasConfigDTO);
        update.set(qAsignaturasConfigDTO.ordenacion, ordenacion).where(
                qAsignaturasConfigDTO.codigoAsignatura.eq(codigo_asignatura).and(qAsignaturasConfigDTO.ejercicio.eq(AcademicYear.getCurrent())));
        long result = update.execute();
        return result;
    }

    public List<AsignaturaDTO> getAsignaturasByEstudiante(Long persona_id)
    {
        return getAsignaturasByRelacion(persona_id, "ESTUDIANTE");
    }

    public List<AsignaturaDTO> getAsignaturasByCoordinadorOrProfesor(Long persona_id)
    {
        return getAsignaturasByRelacion(persona_id, "COORDINADOR|PROFESOR");
    }

    @Transactional
    public long updateAsignaturaConfig(String codigo_asignatura, Date fechaLimite, String ordenacion, boolean habilitada, Date permitirSolicitudes, Boolean notificarCoordinador) {
        long result = 0;
        result = setFechaLimite(codigo_asignatura, fechaLimite);
        result += setOrdenacion(codigo_asignatura, ordenacion);
        result += setEsperaHabilitada(codigo_asignatura, habilitada);
        result += setPermitirSolicitudes(codigo_asignatura, permitirSolicitudes);
        result += setNotificarCoordinador(codigo_asignatura, notificarCoordinador);
        return result;
    }

    @Transactional
    public long setEsperaHabilitada(String codigo_asignatura, boolean habilitada) {
        QAsignaturasConfigDTO qAsignaturasConfigDTO = QAsignaturasConfigDTO.asignaturasConfigDTO;
        JPAUpdateClause update = new JPAUpdateClause(entityManager, qAsignaturasConfigDTO);
        update.set(qAsignaturasConfigDTO.habilitar_espera, habilitada).where(
                qAsignaturasConfigDTO.codigoAsignatura.eq(codigo_asignatura).and(qAsignaturasConfigDTO.ejercicio.eq(AcademicYear.getCurrent()))
        );
        long result = update.execute();
        return result;
    }

    @Transactional
    public long setNotificarCoordinador(String codigo_asignatura, Boolean notificarCoordinador) {
        QAsignaturasConfigDTO qAsignaturasConfigDTO = QAsignaturasConfigDTO.asignaturasConfigDTO;
        JPAUpdateClause update = new JPAUpdateClause(entityManager, qAsignaturasConfigDTO);
        update.set(qAsignaturasConfigDTO.notificarCoordinador, notificarCoordinador).where(
                qAsignaturasConfigDTO.codigoAsignatura.eq(codigo_asignatura).and(qAsignaturasConfigDTO.ejercicio.eq(AcademicYear.getCurrent()))
        );
        long result = update.execute();
        return result;
    }

    public List<PersonaDTO> getCoordinadoresByAsignatura(String codigo_asignatura) {
        QAsignaturasPersonaDTO qAsignaturasPersonaDTO = QAsignaturasPersonaDTO.asignaturasPersonaDTO;
        JPAQuery query = new JPAQuery(entityManager);
        QPersonaDTO existen = new QPersonaDTO("existen");
        //1111 Modificado para evitar los usuarios que desaparecen
        query.from(qAsignaturasPersonaDTO).innerJoin(qAsignaturasPersonaDTO.tfgExtPersona, existen).where(
                qAsignaturasPersonaDTO.id.tipoRelacion.eq("COORDINADOR")
                        .and(qAsignaturasPersonaDTO.id.codigoAsignatura.eq(codigo_asignatura)
//                                .and(qAsignaturasPersonaDTO.ejercicio.eq(AcademicYear.getCurrent()))
                                ));
        return query.list(qAsignaturasPersonaDTO.tfgExtPersona);
    }

    @Transactional
    public AsignaturasConfigDTO createAsignaturaConfig(String codigo_asignatura, Date fechaLimite, String ordenacion, boolean habilitada, Date permitirSolicitudes, Boolean notificarCoordinador) {
        AsignaturasConfigDTO asignaturasConfigDTO = new AsignaturasConfigDTO(codigo_asignatura, AcademicYear.getCurrent());
        if (fechaLimite != null)
            asignaturasConfigDTO.setFechaLimite(fechaLimite);
        if (ordenacion == null || ordenacion.equals("")) {
            ordenacion = "NOTA_MEDIA";
        }
        if (permitirSolicitudes != null) {
            asignaturasConfigDTO.setPermitirSolicitudes(permitirSolicitudes);
        }
        asignaturasConfigDTO.setOrdenacion(ordenacion);
        asignaturasConfigDTO.setHabilitar_espera(habilitada);
        asignaturasConfigDTO.setNotificarCoordinador(notificarCoordinador);
        entityManager.persist(asignaturasConfigDTO);
        return asignaturasConfigDTO;
    }

    public List<Integer> anyosConAsignaturas(Long userId) {
        QAsignaturasPersonaDTO qAsignaturasPersonaDTO = QAsignaturasPersonaDTO.asignaturasPersonaDTO;
        QPropuestaDTO qPropuestaDTO = QPropuestaDTO.propuestaDTO;
        QPropuestaTutorDTO qPropuestaTutorDTO = QPropuestaTutorDTO.propuestaTutorDTO;
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qAsignaturasPersonaDTO).join(qPropuestaDTO).on(
                        qAsignaturasPersonaDTO.ejercicio.eq(qPropuestaDTO.ejercicio),
                        qAsignaturasPersonaDTO.tfgExtAsignatura.codigo.eq(qPropuestaDTO.codigo_asignatura)
                ).join(qPropuestaTutorDTO).on(qPropuestaTutorDTO.propuesta.id.eq(qPropuestaDTO.id))
                .where(qAsignaturasPersonaDTO.id.personaId.eq(userId)
                        .and(qAsignaturasPersonaDTO.id.tipoRelacion.in("PROFESOR", "COORDINADOR")
                                .and(qAsignaturasPersonaDTO.ejercicio.goe(AcademicYear.getCurrent() -1)))
                );
        return query.distinct().orderBy(qAsignaturasPersonaDTO.ejercicio.desc()).list(qAsignaturasPersonaDTO.ejercicio);
    }

    public List<Integer> anyosConAsignaturasAdmin() {
        QAsignaturasPersonaDTO qAsignaturasPersonaDTO = QAsignaturasPersonaDTO.asignaturasPersonaDTO;
        QPropuestaDTO qPropuestaDTO = QPropuestaDTO.propuestaDTO;
        QPropuestaTutorDTO qPropuestaTutorDTO = QPropuestaTutorDTO.propuestaTutorDTO;

        JPAQuery query = new JPAQuery(entityManager)
                .from(qAsignaturasPersonaDTO)
                .join(qPropuestaDTO).on(qAsignaturasPersonaDTO.ejercicio.eq(qPropuestaDTO.ejercicio),
                        qAsignaturasPersonaDTO.tfgExtAsignatura.codigo.eq(qPropuestaDTO.codigo_asignatura))
                .join(qPropuestaTutorDTO).on(qPropuestaTutorDTO.propuesta.id.eq(qPropuestaDTO.id));
        return query.distinct().orderBy(qAsignaturasPersonaDTO.ejercicio.desc()).list(qAsignaturasPersonaDTO.ejercicio);
    }

    public List<Asignatura> getAsignaturasCombo(Long cursoAca) {
        QAsignaturaDTO qAsignaturaDTO = new QAsignaturaDTO("qAsignaturaDTO");
        JPAQuery query = new JPAQuery(entityManager)
                .from(qAsignaturaDTO)
                .leftJoin(qAsignaturasConfigDTO).on(qAsignaturasConfigDTO.codigoAsignatura.eq(qAsignaturaDTO.codigo));
        if (null != cursoAca)
            query.where(qAsignaturaDTO.ejercicio.eq(cursoAca.intValue()));
        return query.distinct()
                .list(qAsignaturaDTO.codigo, qAsignaturaDTO.nombre)
                .stream()
                .map(tuple -> {
                    Asignatura asignaturas = new Asignatura();
                    asignaturas.setCodigo(tuple.get(qAsignaturaDTO.codigo));
                    asignaturas.setNombre(tuple.get(qAsignaturaDTO.codigo) + " - " + tuple.get(qAsignaturaDTO.nombre));
                    return asignaturas;
                }).collect(Collectors.toList());
    }

    public List<Asignatura> getAsignaturasComboByProfesor(Long id, Long cursoAca) {
        return getAsignaturasComboByRelacion(id, "PROFESOR", cursoAca);
    }

    public List<Asignatura> getAsignaturasComboByCoordinador(Long id, Long cursoAca) {
        return getAsignaturasComboByRelacion(id, "COORDINADOR", cursoAca);
    }

    public List<Asignatura> getAsignaturasComboByCoordinadorOrProfesor(Long id, Long cursoAca) {
        return getAsignaturasComboByRelacion(id, "COORDINADOR|PROFESOR", cursoAca);
    }

    private List<Asignatura> getAsignaturasComboByRelacion(Long id, String role, Long cursoAca) {
        List<String> busqueda = new ArrayList<String>();

        if (id == null) return null;

        if (role.contains("|")) busqueda = new ArrayList<String>(Arrays.asList(role.split("\\|")));
        else busqueda.add(role);

        JPAQuery query = new JPAQuery(entityManager)
                .from(qAsignaturasPersonaDTO)
                .where(qAsignaturasPersonaDTO.id.personaId.eq(id).
                        and(qAsignaturasPersonaDTO.id.tipoRelacion.in(busqueda)));
        if (cursoAca != null)
            query.where(qAsignaturasPersonaDTO.ejercicio.eq(cursoAca.intValue()));
        return query.distinct()
                .list(qAsignaturasPersonaDTO.tfgExtAsignatura.codigo, qAsignaturasPersonaDTO.tfgExtAsignatura.nombre)
                .stream()
                .map(tuple -> {
                    Asignatura asignaturas = new Asignatura();
                    asignaturas.setCodigo(tuple.get(qAsignaturasPersonaDTO.tfgExtAsignatura.codigo));
                    asignaturas.setNombre(tuple.get(qAsignaturasPersonaDTO.tfgExtAsignatura.codigo) + " - " + tuple.get(qAsignaturasPersonaDTO.tfgExtAsignatura.nombre));
                    return asignaturas;
                }).collect(Collectors.toList());
    }
}
