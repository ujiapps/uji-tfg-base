package es.uji.apps.tfg.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.tfg.dto.AlumnosAsignadosDTO;
import es.uji.apps.tfg.dto.QAlumnosAsignadosDTO;
import es.uji.apps.tfg.dto.QAsignaturasPersonaDTO;
import es.uji.apps.tfg.dto.QPropuestaTutorDTO;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.sso.dao.ApaDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Repository
public class AlumnosAsignadosDAO extends BaseDAODatabaseImpl
{
    @Autowired
    private ApaDAO apaDAO;

    private QAlumnosAsignadosDTO qAlumnosAsignadosDTO = QAlumnosAsignadosDTO.alumnosAsignadosDTO;
    private QPropuestaTutorDTO qPropuestaTutorDTO = QPropuestaTutorDTO.propuestaTutorDTO;

    public List<AlumnosAsignadosDTO> getAlumnosasignadosByAsignatura(String codigoAsignatura, Long userId, Integer anyo) {
        QAsignaturasPersonaDTO qAsignaturasPersonaDTO = QAsignaturasPersonaDTO.asignaturasPersonaDTO;

        boolean isAdmin = apaDAO.hasPerfil("TFG", "ADMIN", userId);

        JPAQuery query = new JPAQuery(entityManager);
        JPAQuery queryIsCoordinador = new JPAQuery(entityManager);

        if (anyo == null) {
            return Collections.emptyList();
        }

        String isCoordinador = queryIsCoordinador.from(qAsignaturasPersonaDTO)
                .where(qAsignaturasPersonaDTO.id.personaId.eq(userId)
                        .and(qAsignaturasPersonaDTO.ejercicio.eq(anyo)
                                .and(qAsignaturasPersonaDTO.id.codigoAsignatura.eq(codigoAsignatura))))
                .singleResult(qAsignaturasPersonaDTO.id.tipoRelacion);

        query.from(qAlumnosAsignadosDTO).join(qPropuestaTutorDTO).on(qAlumnosAsignadosDTO.id.eq(qPropuestaTutorDTO.propuesta.id));

        if (Objects.equals(isCoordinador, "COORDINADOR") || isAdmin)
            query.where(qAlumnosAsignadosDTO.codigoAsignatura.eq(codigoAsignatura)
                            .and(qAlumnosAsignadosDTO.ejercicio.eq(anyo)));
         else
             query.where(qAlumnosAsignadosDTO.codigoAsignatura.eq(codigoAsignatura)
                            .and(qAlumnosAsignadosDTO.ejercicio.eq(anyo)
                                    .and(qPropuestaTutorDTO.tutor.id.eq(userId))));

        return query.list(qAlumnosAsignadosDTO);
    }
}
