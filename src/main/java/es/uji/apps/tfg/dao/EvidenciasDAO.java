package es.uji.apps.tfg.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.tfg.dto.EvidenciaDTO;
import es.uji.apps.tfg.dto.QEvidenciaDTO;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class EvidenciasDAO extends BaseDAODatabaseImpl {
    private QEvidenciaDTO qEvidenciaDTO = QEvidenciaDTO.evidenciaDTO;


    public List<EvidenciaDTO> getEvidencias(String codigoAsignatura, Integer ejercicio) {
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qEvidenciaDTO).where(
                qEvidenciaDTO.codigoAsignatura.eq(codigoAsignatura).and(qEvidenciaDTO.ejercicio.eq(ejercicio))
        ).orderBy(qEvidenciaDTO.fechaLectura.asc(), qEvidenciaDTO.nombreAlumno.asc()).list(qEvidenciaDTO);
    }
}
