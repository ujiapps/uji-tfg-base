package es.uji.apps.tfg.dao;

import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;
import es.uji.apps.tfg.dto.ConvocatoriaFechaDTO;
import es.uji.apps.tfg.dto.QConvocatoriaFechaDTO;
import es.uji.apps.tfg.model.Convocatoria;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public class ConvocatoriaDAO extends BaseDAODatabaseImpl {

    QConvocatoriaFechaDTO qConvocatoriaFechaDTO = QConvocatoriaFechaDTO.convocatoriaFechaDTO;

    public List<ConvocatoriaFechaDTO> getConvocatoriasFechas(String codigoAsignatura) {
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qConvocatoriaFechaDTO).where(qConvocatoriaFechaDTO.asignaturaId.eq(codigoAsignatura))
                .list(qConvocatoriaFechaDTO);
    }

    @Transactional
    public Convocatoria createOrUpdateConvocatoria(Convocatoria convocatoria) {
        if (convocatoria.getId() != null) {
            return update(convocatoria) > 0 ? convocatoria : new Convocatoria();
        } else {
            return new Convocatoria(insert(convocatoria));
        }
    }

    private ConvocatoriaFechaDTO insert(Convocatoria convocatoria) {
        ConvocatoriaFechaDTO convocatoriaFechaDTO = new ConvocatoriaFechaDTO(convocatoria);
        return insert(convocatoriaFechaDTO);
    }

    private long update(Convocatoria convocatoria) {
        JPAUpdateClause update = new JPAUpdateClause(entityManager, qConvocatoriaFechaDTO);
        update.set(qConvocatoriaFechaDTO.fechaInicio, convocatoria.getFechaInicio());
        update.set(qConvocatoriaFechaDTO.fechaFin, convocatoria.getFechaFin());
        update.where(qConvocatoriaFechaDTO.cursoAcademico.eq(convocatoria.getCursoAcademico())
                .and(qConvocatoriaFechaDTO.asignaturaId.eq(convocatoria.getAsignaturaId()))
                .and(qConvocatoriaFechaDTO.id.eq(convocatoria.getId())));
        return update.execute();
    }

    @Transactional
    public void deleteConvocatoria(Long id) {
        JPADeleteClause delete = new JPADeleteClause(entityManager, qConvocatoriaFechaDTO);
        delete.where(qConvocatoriaFechaDTO.id.eq(id));
        delete.execute();
    }
}
