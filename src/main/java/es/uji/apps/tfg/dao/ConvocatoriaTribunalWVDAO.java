package es.uji.apps.tfg.dao;


import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.tfg.dto.ConvocatoriaTribunalVWDTO;
import es.uji.apps.tfg.dto.QConvocatoriaTribunalVWDTO;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ConvocatoriaTribunalWVDAO extends BaseDAODatabaseImpl {

    QConvocatoriaTribunalVWDTO qConvocatoriaTribunalVWDTO = QConvocatoriaTribunalVWDTO.convocatoriaTribunalVWDTO;

    public List<ConvocatoriaTribunalVWDTO> getListado(String codigoAsignatura, Long cursoAca) {

        JPAQuery query = new JPAQuery(entityManager);

        query.from(qConvocatoriaTribunalVWDTO)
                .where(qConvocatoriaTribunalVWDTO.codigoAsignatura.eq(codigoAsignatura)
                        .and(qConvocatoriaTribunalVWDTO.ejercicio.eq(cursoAca.intValue())))
                .orderBy(qConvocatoriaTribunalVWDTO.titulo.asc());
        return query.list(qConvocatoriaTribunalVWDTO);
    }

}
