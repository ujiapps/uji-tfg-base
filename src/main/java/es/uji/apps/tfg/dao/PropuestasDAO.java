package es.uji.apps.tfg.dao;

import com.mysema.query.jpa.JPASubQuery;
import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;
import es.uji.apps.tfg.dto.*;
import es.uji.apps.tfg.excepciones.AsignaturaNoConfiguradaException;
import es.uji.apps.tfg.excepciones.PropuestaException;
import es.uji.apps.tfg.model.Asignacion;
import es.uji.apps.tfg.model.Propuesta;
import es.uji.apps.tfg.model.QAsignacion;
import es.uji.apps.tfg.model.QPropuesta;
import es.uji.apps.tfg.utils.AcademicYear;
import es.uji.apps.tfg.utils.DateUtils;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.ParamUtils;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class PropuestasDAO extends BaseDAODatabaseImpl {
    private QPropuestaDTO qPropuestaDTO = QPropuestaDTO.propuestaDTO;
    private QPersonasPropuestaDTO qPersonasPropuestaDTO = QPersonasPropuestaDTO.personasPropuestaDTO;
    private QPropuestaTutorDTO qPropuestaTutorDTO = QPropuestaTutorDTO.propuestaTutorDTO;
    private QPersonaDTO qPersonaDTO = QPersonaDTO.personaDTO;
    private QAsignacionesPropuestaVWDTO qAsignacionesPropuestaVWDTO = QAsignacionesPropuestaVWDTO.asignacionesPropuestaVWDTO;


    private List<PropuestaDTO> distinctPropuestaDTO(List<PropuestaDTO> listado) {
        List<PropuestaDTO> salida = new ArrayList<>();
        List<Long> repetidos = new ArrayList<>();
        for (PropuestaDTO propuestaDTO : listado) {
            if (!repetidos.contains(propuestaDTO.getId())) {
                repetidos.add(propuestaDTO.getId());
                salida.add(propuestaDTO);
            }
        }
        return salida;
    }

    public List<PropuestaDTO> getPropuestasByAutor(Long persona_id, boolean isAdmin, boolean isCoord) {
        //1111 Habría que corregir las consultas, tal como están ahora no sirven (pero esta función se llama únicamente por otra en desuso y tests)
        if (persona_id == null) {
            throw new EntityNotFoundException();
        }
        int year = AcademicYear.getCurrent();
        JPAQuery query = new JPAQuery(entityManager);
        QPersonasPropuestaDTO qPersonasPropuestaDTO = new QPersonasPropuestaDTO("qPersonasPropuestaDTO");
        if (isAdmin) {
            query.from(qPropuestaDTO).leftJoin(qPropuestaDTO.tfgPersonasPropuestas, qPersonasPropuestaDTO).fetch()
                    .where(qPropuestaDTO.ejercicio.eq(year));
        } else if (isCoord) {
            query.from(qPropuestaDTO).leftJoin(qPropuestaDTO.tfgPersonasPropuestas, qPersonasPropuestaDTO).fetch()
                    .where(qPropuestaDTO.ejercicio.eq(year));
        } else {
            query.from(qPropuestaDTO).leftJoin(qPropuestaDTO.tfgPersonasPropuestas, qPersonasPropuestaDTO).fetch()
                    .leftJoin(qPropuestaDTO.propuestaTutores, qPropuestaTutorDTO)
                    .where(qPropuestaTutorDTO.tutor.id.eq(persona_id).and(qPropuestaDTO.ejercicio.eq(year)));
        }
        List<PropuestaDTO> salida = query.list(qPropuestaDTO);
        return distinctPropuestaDTO(salida);
    }

    public List<PropuestaDTO> getPropuestasByEstudiante(PersonaDTO personaDTO) {
        JPAQuery query = new JPAQuery(entityManager);
        QPersonasPropuestaDTO qPersonasPropuestaDTO = new QPersonasPropuestaDTO("qPersonasPropuestaDTO");
        //1111 Corregido para que no quede afectado por personas inexistentes
        QPersonaDTO existen = new QPersonaDTO("existen");
        query.from(qPropuestaDTO).join(qPropuestaDTO.tfgPersonasPropuestas, qPersonasPropuestaDTO).join(qPersonasPropuestaDTO.tfgExtPersona, existen)
                .where(qPersonasPropuestaDTO.tfgExtPersona.id.eq(personaDTO.getId())
                        .and(qPersonasPropuestaDTO.tfgPropuesta.ejercicio.eq(AcademicYear.getCurrent())));
        List<PropuestaDTO> salida = query.list(qPropuestaDTO);
        return distinctPropuestaDTO(salida);
    }

    public PropuestaDTO getPropuesta(long id) {
        JPAQuery query = new JPAQuery(entityManager);
        QAsignaturaDTO aJOIN = new QAsignaturaDTO("aJOIN");
        QAsignaturasConfigDTO acJOIN = new QAsignaturasConfigDTO("acJOIN");
        List<PropuestaDTO> salida = query.from(qPropuestaDTO)
                .leftJoin(qPropuestaDTO.tfgExtAsignatura, aJOIN).fetch()
                .leftJoin(qPropuestaDTO.propuestaTutores, qPropuestaTutorDTO).fetch()
                .leftJoin(qPropuestaTutorDTO.tutor, qPersonaDTO).fetch()
                .leftJoin(aJOIN.tfgAsignaturasConfig, acJOIN).fetch()
                .where(qPropuestaDTO.id.eq(id).and(qPropuestaDTO.ejercicio.eq(AcademicYear.getCurrent()))).list(qPropuestaDTO);

        if (ParamUtils.isNotNull(salida)) {
            return salida.get(0);
        }
        return null;
    }

    @Transactional
    public long create(String nombre, List<Long> tutores, String codigo_asignatura, String objetivos, String bibliografia, String descripcion, String observaciones,
                       BigDecimal plazas, BigDecimal espera, Long perIdCreador) {
        if (!ParamUtils.isNotNull(tutores)) {
            throw new EntityNotFoundException();
        }
        PropuestaDTO propuestaDTO = new PropuestaDTO();
        propuestaDTO.setTfgExtAsignatura(new AsignaturaDTO(codigo_asignatura, AcademicYear.getCurrent()));
        propuestaDTO.setObjetivos(objetivos);
        propuestaDTO.setBibliografia(bibliografia);
        propuestaDTO.setDescripcion(descripcion);
        propuestaDTO.setObservaciones(observaciones);
        propuestaDTO.setPlazas(plazas);
        propuestaDTO.setEspera(espera);
        propuestaDTO.setNombre(nombre);
        propuestaDTO.setFechaCreacion(DateUtils.getCurrent());
        propuestaDTO.setEjercicio(AcademicYear.getCurrent());
        propuestaDTO.setCodigo_asignatura(codigo_asignatura);
        propuestaDTO.setPerIdCreacion(perIdCreador);

        propuestaDTO.setPropuestaTutores(tutores.stream().map(tutorId -> {
            PropuestaTutorDTO propuestaTutorDTO = new PropuestaTutorDTO();
            PersonaDTO personaDTO = new PersonaDTO();
            personaDTO.setId(tutorId);
            propuestaTutorDTO.setTutor(personaDTO);
            propuestaTutorDTO.setPropuesta(propuestaDTO);
            return propuestaTutorDTO;
        }).collect(Collectors.toSet()));
        entityManager.persist(propuestaDTO);

        return propuestaDTO.getId();
    }

    @Transactional
    public boolean delete(PropuestaDTO propuestaDTO) {
        long result;

        JPADeleteClause delete = new JPADeleteClause(entityManager, qPersonasPropuestaDTO);
        delete.where(qPersonasPropuestaDTO.id.propuestaId.eq(propuestaDTO.getId())).execute();
        JPADeleteClause delTutores = new JPADeleteClause(entityManager, qPropuestaTutorDTO);
        delTutores.where(qPropuestaTutorDTO.propuesta.id.eq(propuestaDTO.getId())).execute();
        JPADeleteClause del = new JPADeleteClause(entityManager, qPropuestaDTO);
        result = del.where(qPropuestaDTO.id.eq(propuestaDTO.getId())).execute();
        return result > 0L;
    }

    @Transactional
    public PropuestaDTO update(long id, String name, List<Long> tutores, String codigo_asignatura, String objetivos, String bibliografia, String descripcion,
                               String observaciones, BigDecimal plazas, BigDecimal espera) {
        if (!ParamUtils.isNotNull(tutores)) {
            throw new EntityNotFoundException();
        }
        PropuestaDTO propuestaDTO = getPropuesta(id);
        propuestaDTO.setNombre(name);
        propuestaDTO.setTfgExtAsignatura(new AsignaturaDTO(codigo_asignatura, AcademicYear.getCurrent()));
        propuestaDTO.setEspera(espera);
        propuestaDTO.setPlazas(plazas);
        propuestaDTO.setObjetivos(objetivos);
        propuestaDTO.setBibliografia(bibliografia);
        propuestaDTO.setDescripcion(descripcion);
        propuestaDTO.setObservaciones(observaciones);
        entityManager.persist(propuestaDTO);

        return propuestaDTO;
    }

    public List<Asignacion> getAsignacionesByAsignatura(String codigo_asignatura) {
        JPAQuery query = new JPAQuery(entityManager);
        Integer year = AcademicYear.getCurrent();
        query.from(qAsignacionesPropuestaVWDTO)
                .where(qAsignacionesPropuestaVWDTO.codigoAsignatura.eq(codigo_asignatura)
                        .and(qAsignacionesPropuestaVWDTO.ejercicio.eq(year)));
        return query.distinct().list(new QAsignacion(qAsignacionesPropuestaVWDTO.personaId,
                qAsignacionesPropuestaVWDTO.nombre,
                qAsignacionesPropuestaVWDTO.apellido1,
                qAsignacionesPropuestaVWDTO.apellido2,
                qAsignacionesPropuestaVWDTO.propuestaId,
                qAsignacionesPropuestaVWDTO.propuestaNombre,
                qAsignacionesPropuestaVWDTO.codigoAsignatura,
                qAsignacionesPropuestaVWDTO.tutoresNombre,
                qAsignacionesPropuestaVWDTO.aceptada,
                qAsignacionesPropuestaVWDTO.comentarios,
                qAsignacionesPropuestaVWDTO.fechaEdicion
        ));

    }

    public List<Asignacion> getAsignacionesDeProfesorByAsignatura(String codigo_asignatura, Long profesor_id) {
        JPAQuery query = new JPAQuery(entityManager);
        Integer year = AcademicYear.getCurrent();
        query.from(qAsignacionesPropuestaVWDTO)
                .where(qAsignacionesPropuestaVWDTO.codigoAsignatura.eq(codigo_asignatura)
                        .and(qAsignacionesPropuestaVWDTO.ejercicio.eq(year))
                        .and(qAsignacionesPropuestaVWDTO.tutorId.eq(profesor_id)));
        return query.list(new QAsignacion(qAsignacionesPropuestaVWDTO.personaId,
                qAsignacionesPropuestaVWDTO.nombre,
                qAsignacionesPropuestaVWDTO.apellido1,
                qAsignacionesPropuestaVWDTO.apellido2,
                qAsignacionesPropuestaVWDTO.propuestaId,
                qAsignacionesPropuestaVWDTO.propuestaNombre,
                qAsignacionesPropuestaVWDTO.codigoAsignatura,
                qAsignacionesPropuestaVWDTO.tutoresNombre,
                qAsignacionesPropuestaVWDTO.aceptada,
                qAsignacionesPropuestaVWDTO.comentarios,
                qAsignacionesPropuestaVWDTO.fechaEdicion
        ));
    }

    public PersonasPropuestaDTO getAsignacion(long persona_id, long propuesta_id) {
        JPAQuery query = new JPAQuery(entityManager);
        //1111 Modificado por si deja de existir la persona asignada
        QPersonaDTO existen = new QPersonaDTO("existen");
        query.from(qPersonasPropuestaDTO).innerJoin(qPersonasPropuestaDTO.tfgExtPersona, existen)
                .where(qPersonasPropuestaDTO.id.propuestaId.eq(propuesta_id)
                        .and(qPersonasPropuestaDTO.id.personaId.eq(persona_id))
                        .and(qPersonasPropuestaDTO.tfgPropuesta.ejercicio.eq(AcademicYear.getCurrent())));
        PersonasPropuestaDTO result = query.singleResult(QPersonasPropuestaDTO.personasPropuestaDTO);
        return result;
    }

    @Transactional
    public PropuestaDTO updateAsignaciones(Long persona_id, Long propuesta_id, Long editor_persona_id, String comentarios, Date current) throws AsignaturaNoConfiguradaException {

        PersonasPropuestaDTO personasPropuestaDTO = getAsignacion(persona_id, propuesta_id);
        PropuestaDTO propuestaDTO = getPropuesta(propuesta_id);
        if (propuestaDTO.getTfgExtAsignatura() == null || propuestaDTO.getTfgExtAsignatura().getTfgAsignaturasConfig() == null) {
            throw new AsignaturaNoConfiguradaException();
        }
        List<PersonasPropuestaDTO> asignacionesAlumno = getAsignacionesDePersonaByAsignatura(persona_id, propuestaDTO.getTfgExtAsignatura().getCodigo());
        PropuestaDTO liberada = null;
        for (PersonasPropuestaDTO asignacion : asignacionesAlumno) {
            if (asignacion.getTfgPropuesta().getId() == propuesta_id) {
                continue;
            }
            if (asignacion != null && asignacion.getAceptada().intValue() == 1) {
                liberada = asignacion.getTfgPropuesta();
                removeSolicitud(persona_id, liberada.getId());
            }
        }

        try {

            if (personasPropuestaDTO == null) {
                PersonasPropuestaDTOPK personasPropuestaDTOPK = new PersonasPropuestaDTOPK();
                personasPropuestaDTOPK.setPropuestaId(propuesta_id);
                personasPropuestaDTOPK.setPersonaId(persona_id);
                personasPropuestaDTO = new PersonasPropuestaDTO();
                personasPropuestaDTO.setId(personasPropuestaDTOPK);
                // personasPropuestaDTO.setTfgExtEditor(personasDAO.getUsuario(editor_persona_id));
                personasPropuestaDTO.setTfgExtEditor(new PersonaDTO(editor_persona_id));
                personasPropuestaDTO.setTfgExtPersona(new PersonaDTO(persona_id));
                personasPropuestaDTO.setTfgPropuesta(new PropuestaDTO(propuesta_id));
                personasPropuestaDTO.setFechaEdicion(current);
                personasPropuestaDTO.setAceptada(new BigDecimal(1));
                personasPropuestaDTO.setComentarios(comentarios);
                insert(personasPropuestaDTO);
            } else {
                personasPropuestaDTO.setTfgPropuesta(new PropuestaDTO(propuesta_id));
                personasPropuestaDTO.setFechaEdicion(current);
                personasPropuestaDTO.setTfgExtEditor(new PersonaDTO(editor_persona_id));
                personasPropuestaDTO.setAceptada(new BigDecimal(1));
                personasPropuestaDTO.setComentarios(comentarios);
                entityManager.merge(personasPropuestaDTO);
            }
        } catch (Exception e) {
            throw e;
        }
        return liberada;
    }

    @Transactional
    public PropuestaDTO liberaAsignaciones(Long persona_id, Long propuesta_id) throws AsignaturaNoConfiguradaException {

//        PersonasPropuestaDTO personasPropuestaDTO = getAsignacion(persona_id, propuesta_id);
        PropuestaDTO propuestaDTO = getPropuesta(propuesta_id);
        if (propuestaDTO.getTfgExtAsignatura() == null || propuestaDTO.getTfgExtAsignatura().getTfgAsignaturasConfig() == null) {
            throw new AsignaturaNoConfiguradaException();
        }
        List<PersonasPropuestaDTO> asignacionesAlumno = getAsignacionesDePersonaByAsignatura(persona_id, propuestaDTO.getTfgExtAsignatura().getCodigo());
        PropuestaDTO liberada = null;
        for (PersonasPropuestaDTO asignacion : asignacionesAlumno) {
            if (asignacion.getTfgPropuesta().getId() == propuesta_id) {
                continue;
            }
            if (asignacion != null && asignacion.getAceptada().intValue() == 1) {
                liberada = asignacion.getTfgPropuesta();
                removeSolicitud(persona_id, liberada.getId());
            }
        }


        return liberada;
    }


    @Transactional
    public void ampliarPlazasSiLimiteSuperado(long propuesta_id) throws PropuestaException {
        PropuestaDTO propuestaDTO = getPropuesta(propuesta_id);
        if (propuestaDTO == null) {
            throw new PropuestaException("tfg.message.exception.propuestaNoEncontrada");
        }
        long plazasPropuesta = propuestaDTO.getTfgPersonasPropuestas().size();//Obtenemos el total de plazas actuales en uso
        if (propuestaDTO.getPlazas().longValue() < plazasPropuesta)//Si tiene más de las que refleja se actualiza
        {
            propuestaDTO.setPlazas(new BigDecimal(plazasPropuesta));
            entityManager.merge(propuestaDTO);
        }
    }

    public List<Propuesta> getPropuestasDePersonaByAsignatura(Long persona_id, String codigo_asignatura, Integer ejercicio) {
        JPAQuery query = new JPAQuery(entityManager);
        QPropuestaVWDTO qPropuestaVWDTO = QPropuestaVWDTO.propuestaVWDTO;

        int year = AcademicYear.getCurrent();

        if (ejercicio != null) {
            year = ejercicio;
        }

        return query.from(qPropuestaVWDTO)
                .where(qPropuestaVWDTO.codigoAsignatura.eq(codigo_asignatura)
                        .and(qPropuestaVWDTO.tutorId.eq(persona_id))
                        .and(qPropuestaVWDTO.ejercicio.eq(year))
                ).list(new QPropuesta(qPropuestaVWDTO.id,
                        qPropuestaVWDTO.fechaCreacion,
                        qPropuestaVWDTO.nombre,
                        qPropuestaVWDTO.codigoAsignatura,
                        qPropuestaVWDTO.plazas,
                        qPropuestaVWDTO.espera,
                        qPropuestaVWDTO.ejercicio,
                        qPropuestaVWDTO.tutores,
                        qPropuestaVWDTO.tutoresNombre,
                        qPropuestaVWDTO.enEspera,
                        qPropuestaVWDTO.aceptados));
    }

    public List<Propuesta> getPropuestasAsignadasByAsignatura(Long user_id, boolean isAdmin, boolean isCoord, String codigo_asignatura, Integer ejercicio) {
        //1111 Se corrige por si se llega aquí con un user_id inexistente

        JPAQuery query = new JPAQuery(entityManager);
        QPropuestaVWDTO qPropuestaVWDTO = QPropuestaVWDTO.propuestaVWDTO;

        int year = AcademicYear.getCurrent();

        if (ejercicio != null) {
            year = ejercicio;
        }

        if (isAdmin || isCoord) {
            return query.from(qPropuestaVWDTO)
                    .where(qPropuestaVWDTO.codigoAsignatura.eq(codigo_asignatura)
                            .and(qPropuestaVWDTO.ejercicio.eq(year)))
                    .distinct().orderBy(qPropuestaVWDTO.tutoresNombre.asc())
                    .list(new QPropuesta(qPropuestaVWDTO.id,
                            qPropuestaVWDTO.fechaCreacion,
                            qPropuestaVWDTO.nombre,
                            qPropuestaVWDTO.codigoAsignatura,
                            qPropuestaVWDTO.plazas,
                            qPropuestaVWDTO.espera,
                            qPropuestaVWDTO.ejercicio,
                            qPropuestaVWDTO.tutores,
                            qPropuestaVWDTO.tutoresNombre,
                            qPropuestaVWDTO.enEspera,
                            qPropuestaVWDTO.aceptados));
        } else {
            return query.from(qPersonasPropuestaDTO, qPropuestaVWDTO)
                    .where(qPersonasPropuestaDTO.tfgPropuesta.id.eq(qPropuestaVWDTO.id).and(
                                    qPersonasPropuestaDTO.tfgExtPersona.id.eq(user_id))
                            .and(qPersonasPropuestaDTO.tfgPropuesta.tfgExtAsignatura.codigo.eq(codigo_asignatura))
                            .and(qPersonasPropuestaDTO.tfgPropuesta.tfgExtAsignatura.ejercicio.eq(AcademicYear.getCurrent())))
                    .distinct()
                    .list(new QPropuesta(qPropuestaVWDTO.id,
                            qPropuestaVWDTO.fechaCreacion,
                            qPropuestaVWDTO.nombre,
                            qPropuestaVWDTO.codigoAsignatura,
                            qPropuestaVWDTO.plazas,
                            qPropuestaVWDTO.espera,
                            qPropuestaVWDTO.ejercicio,
                            qPropuestaVWDTO.tutores,
                            qPropuestaVWDTO.tutoresNombre,
                            qPropuestaVWDTO.enEspera,
                            qPropuestaVWDTO.aceptados));
        }

//        return distinctPropuestaDTO(salida);
    }

    public List<PropuestaDTO> getPropuestasADuplicar(String codigo_asignatura) {
        JPAQuery query = new JPAQuery(entityManager);
        QAsignaturasPersonaDTO qAsignaturasPersonaDTO = QAsignaturasPersonaDTO.asignaturasPersonaDTO;
        QPersonasPropuestaDTO qPersonasPropuestaDTO = new QPersonasPropuestaDTO("qPersonasPropuestaDTO");

        QPersonaDTO qPersonaDTO = QPersonaDTO.personaDTO;
        QAsignaturaDTO qAsignaturaDTO = QAsignaturaDTO.asignaturaDTO;
        QAsignaturaDTO qAsignaturaDTO2 = new QAsignaturaDTO("qAsignaturaDTO2");

        QPersonaDTO qPersonaDTO2 = new QPersonaDTO("qPersonaDTO2");

        int year = AcademicYear.getCurrent();

        return query.from(qPropuestaDTO)
                .join(qPropuestaDTO.tfgExtAsignatura, qAsignaturaDTO).fetch()
                .join(qAsignaturaDTO.tfgAsignaturasConfig).fetch()
                .join(qPropuestaDTO.propuestaTutores, qPropuestaTutorDTO).fetch()
                .join(qPropuestaTutorDTO.tutor, qPersonaDTO)
                .where(qAsignaturaDTO.codigo.eq(codigo_asignatura).and(qPropuestaDTO.ejercicio.eq(year - 1))
                        .and(qPersonaDTO.id.in(
                                new JPASubQuery()
                                        .from(qAsignaturasPersonaDTO).join(qAsignaturasPersonaDTO.tfgExtAsignatura, qAsignaturaDTO2)
                                        .leftJoin(qAsignaturasPersonaDTO.tfgExtPersona, qPersonaDTO2)
                                        .where(qAsignaturasPersonaDTO.id.tipoRelacion.eq("PROFESOR").and(
                                                qAsignaturasPersonaDTO.ejercicio.eq(year)).and(qAsignaturaDTO2.codigo.eq(codigo_asignatura)))
                                        .list(qAsignaturasPersonaDTO.id.personaId)
                        ))
                )
                .list(qPropuestaDTO);
    }

    public List<PropuestaDTO> getSolicitudesByStudent(Long user_id, String codigo_asignatura) {
        JPAQuery query = new JPAQuery(entityManager);
        QAsignaturasPersonaDTO qAPJOIN = new QAsignaturasPersonaDTO("qAPJOIN");
        QPersonaDTO qPJOIN = new QPersonaDTO("qPJOIN");
        query.from(qAPJOIN).join(qAPJOIN.tfgExtPersona, qPJOIN)
                .where(qAPJOIN.id.personaId.eq(user_id)
                        .and(qAPJOIN.id.codigoAsignatura.eq(codigo_asignatura))
                        .and(qAPJOIN.id.tipoRelacion.eq("ESTUDIANTE")));
        List<AsignaturasPersonaDTO> matriculas = query.list(qAPJOIN);
        List<PropuestaDTO> salida = new ArrayList<PropuestaDTO>();
        for (AsignaturasPersonaDTO ap : matriculas) {
            AsignaturasPersonaDTOPK apPK = ap.getId();
            JPAQuery q = new JPAQuery(entityManager);
            QPropuestaDTO qProJOIN = new QPropuestaDTO("QProJOIN");
            q.from(qPersonasPropuestaDTO).join(qPersonasPropuestaDTO.tfgPropuesta, qProJOIN)
                    .where(qPersonasPropuestaDTO.id.personaId.eq(apPK.getPersonaId()).and(qProJOIN.tfgExtAsignatura.codigo.eq(apPK.getCodigoAsignatura())));
            List<PropuestaDTO> solicitadas = new ArrayList<PropuestaDTO>();
            solicitadas = q.list(qProJOIN);
            if (solicitadas.size() > 0) {
                salida.add(solicitadas.get(0));
            }
        }
        return salida;
    }

    public List<PropuestaDTO> getPropuestasByAsignatura(String codigo_asignatura) {
        QAsignaturaDTO qAsignaturaDTO = QAsignaturaDTO.asignaturaDTO;
        QAsignaturasConfigDTO qAsignaturasConfigDTO = QAsignaturasConfigDTO.asignaturasConfigDTO;
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qPropuestaDTO)
                .leftJoin(qPropuestaDTO.propuestaTutores, qPropuestaTutorDTO).fetch()
                .leftJoin(qPropuestaDTO.tfgPersonasPropuestas, qPersonasPropuestaDTO).fetch()
                .leftJoin(qPropuestaDTO.tfgExtAsignatura, qAsignaturaDTO)
                .leftJoin(qAsignaturaDTO.tfgAsignaturasConfig, qAsignaturasConfigDTO)
                .where(qPropuestaDTO.tfgExtAsignatura.codigo.eq(codigo_asignatura)
                        .and(qPropuestaDTO.tfgExtAsignatura.ejercicio.eq(AcademicYear.getCurrent()))
                        .and(qAsignaturasConfigDTO.permitirSolicitudes.isNull().or(qAsignaturasConfigDTO.permitirSolicitudes.loe(new Date())))
                );
        return distinctPropuestaDTO(query.list(qPropuestaDTO));
    }

    @Transactional
    public void addSolicitud(PropuestaDTO propuestaDTO, long personaId, boolean aceptada, String comentarios) {
        PersonasPropuestaDTOPK pppk = new PersonasPropuestaDTOPK();
        pppk.setPropuestaId(propuestaDTO.getId());
        pppk.setPersonaId(personaId);
        PersonasPropuestaDTO personasPropuestaDTO = new PersonasPropuestaDTO();
        personasPropuestaDTO.setId(pppk);
        personasPropuestaDTO.setAceptada(new BigDecimal(aceptada ? 1 : 0));
        personasPropuestaDTO.setFechaEdicion(DateUtils.getCurrent());
        personasPropuestaDTO.setComentarios(comentarios);
        entityManager.persist(personasPropuestaDTO);
    }

    @Transactional
    public void removeSolicitud(long persona_id, long propuesta_id) {
        JPADeleteClause delete = new JPADeleteClause(entityManager, qPersonasPropuestaDTO);
        delete.where(qPersonasPropuestaDTO.id.personaId.eq(persona_id).and(qPersonasPropuestaDTO.id.propuestaId.eq(propuesta_id)));
        delete.execute();
    }

    public List<PersonasPropuestaDTO> getAsignacionesByPropuesta(long propuesta_id) {
        JPAQuery query = new JPAQuery(entityManager);
        QPersonaDTO existen = new QPersonaDTO("existen");
        //1111 Modificamos para que solo coja asignaciones de personas que existen en tfg_Ext_persona
        query.from(qPersonasPropuestaDTO).join(qPersonasPropuestaDTO.tfgExtPersona, existen)
                .where(qPersonasPropuestaDTO.id.propuestaId.eq(propuesta_id));
        return query.list(qPersonasPropuestaDTO);
    }

    @Transactional
    public void aceptarSolicitud(PersonasPropuestaDTO solicitud) {
        JPAUpdateClause update = new JPAUpdateClause(entityManager, qPersonasPropuestaDTO);
        update.where(qPersonasPropuestaDTO.id.eq(solicitud.getId())).set(qPersonasPropuestaDTO.aceptada, new BigDecimal(1)).execute();
    }

    public List<PersonasPropuestaDTO> getAsignacionesDePersonaByAsignatura(long persona_id, String codigo_asignatura) {
        JPAQuery query = new JPAQuery(entityManager);
        QPropuestaDTO prJOIN = new QPropuestaDTO("prJOIN");
        QPersonaDTO existen = new QPersonaDTO("existen");
        //1111 Modificamos para que solo coja asignaciones de personas que existen en tfg_Ext_persona
        query.from(qPersonasPropuestaDTO).join(qPersonasPropuestaDTO.tfgPropuesta, prJOIN).join(qPersonasPropuestaDTO.tfgExtPersona, existen)
                .where(qPersonasPropuestaDTO.id.personaId.eq(persona_id)
                        .and(prJOIN.tfgExtAsignatura.codigo.eq(codigo_asignatura))
                        .and(prJOIN.tfgExtAsignatura.ejercicio.eq(AcademicYear.getCurrent()))
                );
        return query.list(qPersonasPropuestaDTO);
    }

    public Integer getCargaAcumulada(String codigo_asignatura, Long persona_id) {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qPropuestaDTO).leftJoin(qPropuestaDTO.propuestaTutores, qPropuestaTutorDTO).where(qPropuestaDTO.tfgExtAsignatura.codigo.eq(codigo_asignatura)
                .and(qPropuestaDTO.tfgExtAsignatura.ejercicio.eq(AcademicYear.getCurrent()))
                .and(qPropuestaTutorDTO.tutor.id.eq(persona_id)));
        BigDecimal result = query.singleResult(qPropuestaDTO.plazas.sum());
        if (result == null) {
            result = new BigDecimal(0);
        }
        return result.intValue();
    }

    @Transactional
    public Long updateComentariosAsignacion(Long asignado_id, Long propuesta_id, String comentarios, Long editor_id, Date current) {
        JPAUpdateClause update = new JPAUpdateClause(entityManager, qPersonasPropuestaDTO);
        update.set(qPersonasPropuestaDTO.comentarios, comentarios).set(qPersonasPropuestaDTO.fechaEdicion, current)
                .set(qPersonasPropuestaDTO.tfgExtEditor.id, editor_id)
                .where(qPersonasPropuestaDTO.id.personaId.eq(asignado_id).and(qPersonasPropuestaDTO.id.propuestaId.eq(propuesta_id)));
        Long result = update.execute();
        return result;
    }

    @Transactional
    public void deleteAsignacion(Asignacion asignacion) {
        JPADeleteClause del = new JPADeleteClause(entityManager, qPersonasPropuestaDTO);
        del.where(qPersonasPropuestaDTO.id.personaId.eq(asignacion.getPersona_id()).and(qPersonasPropuestaDTO.id.propuestaId.eq(asignacion.getPropuesta_id())));
        del.execute();
    }

    @Transactional
    public void aceptarAsignacion(Long persona_id, Long propuesta_id, Long editor_id, Date current) {
        PropuestaDTO confirmada = getPropuesta(propuesta_id);

        List<PersonasPropuestaDTO> inscritas = getAsignacionesDePersonaByAsignatura(persona_id, confirmada.getTfgExtAsignatura().getCodigo());
        for (PersonasPropuestaDTO personasPropuestaDTO : inscritas) {
            if (personasPropuestaDTO.getTfgPropuesta().getId() == propuesta_id) {
                JPAUpdateClause update = new JPAUpdateClause(entityManager, qPersonasPropuestaDTO);
                update.set(qPersonasPropuestaDTO.aceptada, new BigDecimal(1)).set(qPersonasPropuestaDTO.fechaEdicion, current)
                        .set(qPersonasPropuestaDTO.tfgExtEditor.id, editor_id)
                        .where(qPersonasPropuestaDTO.id.personaId.eq(persona_id).and(qPersonasPropuestaDTO.id.propuestaId.eq(propuesta_id)));
                update.execute();
            } else {
                JPADeleteClause del = new JPADeleteClause(entityManager, qPersonasPropuestaDTO);
                del.where(qPersonasPropuestaDTO.eq(personasPropuestaDTO));
                del.execute();
            }
        }
    }

    @Transactional
    public void addUpdateTutores(Long propuestaId, List<Long> tutores) {
        JPADeleteClause delTutores = new JPADeleteClause(entityManager, qPropuestaTutorDTO);
        delTutores.where(qPropuestaTutorDTO.propuesta.id.eq(propuestaId)).execute();
        tutores.stream().forEach(tutorId -> {
            PropuestaTutorDTO propuestaTutorDTO = new PropuestaTutorDTO();
            PersonaDTO personaDTO = new PersonaDTO();
            personaDTO.setId(tutorId);
            propuestaTutorDTO.setTutor(personaDTO);

            PropuestaDTO propuestaDTO = new PropuestaDTO();
            propuestaDTO.setId(propuestaId);
            propuestaTutorDTO.setPropuesta(propuestaDTO);
            insert(propuestaTutorDTO);
        });

    }

    @Transactional
    public void addUpdateTutoresCambio(Long propuestaId, List<Long> tutores, String nombre) {
        new JPAUpdateClause(entityManager, qPropuestaDTO).set(qPropuestaDTO.nombre, nombre).where(qPropuestaDTO.id.eq(propuestaId)).execute();
        new JPADeleteClause(entityManager, qPropuestaTutorDTO).where(qPropuestaTutorDTO.propuesta.id.eq(propuestaId)).execute();

        tutores.forEach(tutorId -> {
            PropuestaTutorDTO propuestaTutorDTO = new PropuestaTutorDTO();
            PersonaDTO personaDTO = new PersonaDTO();
            personaDTO.setId(tutorId);
            propuestaTutorDTO.setTutor(personaDTO);

            PropuestaDTO propuestaDTO = new PropuestaDTO();
            propuestaDTO.setId(propuestaId);
            propuestaTutorDTO.setPropuesta(propuestaDTO);

            insert(propuestaTutorDTO);
        });
    }

    public List<PropuestaVWDTO> getPropuestas(String codigoAsignatura) {

        QPropuestaVWDTO qPropuestaVWDTO = QPropuestaVWDTO.propuestaVWDTO;
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qPropuestaVWDTO).where(qPropuestaVWDTO.ejercicio.eq(AcademicYear.getCurrent()).and(qPropuestaVWDTO.codigoAsignatura.eq(codigoAsignatura)));

        return query.list(qPropuestaVWDTO);
    }

    @Transactional
    public PersonasPropuestaDTO creaAsignaciones(Long personaId, Long propuestaId, Long editorPersonaId, String comentarios, Date current) {
        PersonasPropuestaDTO personasPropuestaDTO = new PersonasPropuestaDTO();
        PersonasPropuestaDTOPK personasPropuestaDTOPK = new PersonasPropuestaDTOPK();
        personasPropuestaDTOPK.setPropuestaId(propuestaId);
        personasPropuestaDTOPK.setPersonaId(personaId);
        personasPropuestaDTO = new PersonasPropuestaDTO();
        personasPropuestaDTO.setId(personasPropuestaDTOPK);
        personasPropuestaDTO.setTfgExtEditor(new PersonaDTO(editorPersonaId));
        personasPropuestaDTO.setTfgExtPersona(new PersonaDTO(personaId));
        personasPropuestaDTO.setTfgPropuesta(new PropuestaDTO(propuestaId));
        personasPropuestaDTO.setFechaEdicion(current);
        personasPropuestaDTO.setAceptada(new BigDecimal(1));
        personasPropuestaDTO.setComentarios(comentarios);
        return insert(personasPropuestaDTO);
    }

    @Transactional
    public PropuestaDTO createPropuestaMinima(String nombre, List<Long> tutores, String codigoAsignatura, Long perIdCreador) {
        if (!ParamUtils.isNotNull(tutores)) {
            throw new EntityNotFoundException();
        }
        PropuestaDTO propuestaDTO = new PropuestaDTO();
        propuestaDTO.setTfgExtAsignatura(new AsignaturaDTO(codigoAsignatura, AcademicYear.getCurrent()));
        propuestaDTO.setPlazas(BigDecimal.valueOf(1));
        propuestaDTO.setEspera(BigDecimal.valueOf(1));
        propuestaDTO.setNombre(nombre);
        propuestaDTO.setFechaCreacion(DateUtils.getCurrent());
        propuestaDTO.setEjercicio(AcademicYear.getCurrent());
        propuestaDTO.setCodigo_asignatura(codigoAsignatura);
        propuestaDTO.setPerIdCreacion(perIdCreador);
        propuestaDTO = insert(propuestaDTO);

        addUpdateTutores(propuestaDTO.getId(), tutores);

        return propuestaDTO;
    }

    public Boolean tienePropuestasAceptadas(Long alumnoId, String codigoAsignatura) {
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qPersonasPropuestaDTO).where(qPersonasPropuestaDTO.tfgExtPersona.id.eq(alumnoId)
                .and(qPersonasPropuestaDTO.tfgPropuesta.ejercicio.eq(AcademicYear.getCurrent()))
                .and(qPersonasPropuestaDTO.tfgPropuesta.codigo_asignatura.eq(codigoAsignatura))
                .and(qPersonasPropuestaDTO.aceptada.eq(new BigDecimal(1)))
        ).exists();
    }

    public List<EstanciasPracticasVWDTO> propuestaEstanciaPracticas(String codigoAsignatura) {

        JPAQuery query = new JPAQuery(entityManager);

        QEstanciasPracticasVWDTO qEstanciasPracticasVWDTO = QEstanciasPracticasVWDTO.estanciasPracticasVWDTO;

        return query.from(qEstanciasPracticasVWDTO).where(qEstanciasPracticasVWDTO.asignaturaId.eq(codigoAsignatura)
                .and(qEstanciasPracticasVWDTO.cursoAca.eq(AcademicYear.getCurrent()))).list(qEstanciasPracticasVWDTO);
    }
}
