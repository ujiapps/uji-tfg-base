package es.uji.apps.tfg.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.tfg.dto.*;
import es.uji.apps.tfg.model.PropuestaCambio;
import es.uji.apps.tfg.model.QPropuestaCambio;
import es.uji.apps.tfg.utils.AcademicYear;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.ParamUtils;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public class CambioTutoresDAO extends BaseDAODatabaseImpl {

    private QPropuestaCambioVWDTO qPropuestaCambioVWDTO = QPropuestaCambioVWDTO.propuestaCambioVWDTO;
    private QPropuestaDTO qPropuestaDTO = QPropuestaDTO.propuestaDTO;
    private QPropuestaTutorDTO qPropuestaTutorDTO = QPropuestaTutorDTO.propuestaTutorDTO;
    private QPersonaDTO qPersonaDTO = QPersonaDTO.personaDTO;


    public List<PropuestaCambio> getPropuestasCambio(String codigoAsignatura, Integer cursoAca) {

        JPAQuery query = new JPAQuery(entityManager);

        int year = (cursoAca != null) ? cursoAca : AcademicYear.getCurrent() - 1;

        return query.from(qPropuestaCambioVWDTO)
                .where(qPropuestaCambioVWDTO.codigoAsignatura.eq(codigoAsignatura)
                        .and(qPropuestaCambioVWDTO.ejercicio.eq(year))
                        .and(qPropuestaCambioVWDTO.fechaLectura.isNull().or(qPropuestaCambioVWDTO.fechaLectura.before(new Date()))))
                .distinct()
                .list(new QPropuestaCambio(qPropuestaCambioVWDTO.id,
                        qPropuestaCambioVWDTO.fechaCreacion,
                        qPropuestaCambioVWDTO.nombre,
                        qPropuestaCambioVWDTO.codigoAsignatura,
                        qPropuestaCambioVWDTO.plazas,
                        qPropuestaCambioVWDTO.espera,
                        qPropuestaCambioVWDTO.ejercicio,
                        qPropuestaCambioVWDTO.tutores,
                        qPropuestaCambioVWDTO.tutoresNombre,
                        qPropuestaCambioVWDTO.enEspera,
                        qPropuestaCambioVWDTO.aceptados,
                        qPropuestaCambioVWDTO.alumnoId,
                        qPropuestaCambioVWDTO.alumnoNombre));
    }

    public List<PropuestaCambio> getPropuestasCambioTutor(Long personaId, String codigoAsignatura, Integer cursoAca) {
        JPAQuery query = new JPAQuery(entityManager);

        int year = (cursoAca != null) ? cursoAca : AcademicYear.getCurrent() - 1;

        return query.from(qPropuestaCambioVWDTO)
                .where(qPropuestaCambioVWDTO.codigoAsignatura.eq(codigoAsignatura)
                        .and(qPropuestaCambioVWDTO.tutorId.eq(personaId))
                        .and(qPropuestaCambioVWDTO.ejercicio.eq(year)
                                .and(qPropuestaCambioVWDTO.fechaLectura.isNull().or(qPropuestaCambioVWDTO.fechaLectura.before(new Date()))))
                ).list(new QPropuestaCambio(qPropuestaCambioVWDTO.id,
                        qPropuestaCambioVWDTO.fechaCreacion,
                        qPropuestaCambioVWDTO.nombre,
                        qPropuestaCambioVWDTO.codigoAsignatura,
                        qPropuestaCambioVWDTO.plazas,
                        qPropuestaCambioVWDTO.espera,
                        qPropuestaCambioVWDTO.ejercicio,
                        qPropuestaCambioVWDTO.tutores,
                        qPropuestaCambioVWDTO.tutoresNombre,
                        qPropuestaCambioVWDTO.enEspera,
                        qPropuestaCambioVWDTO.aceptados,
                        qPropuestaCambioVWDTO.alumnoId,
                        qPropuestaCambioVWDTO.alumnoNombre));
    }

    public PropuestaDTO getPropuesta(long id) {
        JPAQuery query = new JPAQuery(entityManager);
        QAsignaturaDTO aJOIN = new QAsignaturaDTO("aJOIN");
        QAsignaturasConfigDTO acJOIN = new QAsignaturasConfigDTO("acJOIN");
        List<PropuestaDTO> salida = query.from(qPropuestaDTO)
                .leftJoin(qPropuestaDTO.tfgExtAsignatura, aJOIN).fetch()
                .leftJoin(qPropuestaDTO.propuestaTutores, qPropuestaTutorDTO).fetch()
                .leftJoin(qPropuestaTutorDTO.tutor, qPersonaDTO).fetch()
                .leftJoin(aJOIN.tfgAsignaturasConfig, acJOIN).fetch()
                .where(qPropuestaDTO.id.eq(id).and(qPropuestaDTO.ejercicio.eq(AcademicYear.getCurrent() - 1))).list(qPropuestaDTO);

        if (ParamUtils.isNotNull(salida)) {
            return salida.get(0);
        }
        return null;
    }
}
