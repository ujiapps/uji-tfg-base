package es.uji.apps.tfg.dao;


import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;
import es.uji.apps.tfg.dto.*;
import es.uji.apps.tfg.model.QTribunal;
import es.uji.apps.tfg.model.QTribunalAlumno;
import es.uji.apps.tfg.model.Tribunal;
import es.uji.apps.tfg.model.TribunalAlumno;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.db.Utils;
import es.uji.commons.rest.StringUtils;
import es.uji.commons.rest.json.lookup.LookupItem;
import org.springframework.stereotype.Repository;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class TribunalesDAO extends BaseDAODatabaseImpl {

    QTribunalDTO qTribunalDTO = QTribunalDTO.tribunalDTO;
    QTribunalAlumnoDTO qTribunalAlumnoDTO = QTribunalAlumnoDTO.tribunalAlumnoDTO;

    QPersonaDTO qPersonaDTO = QPersonaDTO.personaDTO;

    QAsignaturasPersonaDTO qAsignaturasPersonaDTO = QAsignaturasPersonaDTO.asignaturasPersonaDTO;


    public List<Tribunal> getTribunales(String codigoAsignatura, Long cursoAca) {
        QPersonaDTO qPresidente = new QPersonaDTO("qPresidente");
        QPersonaDTO qMiembro1 = new QPersonaDTO("qMiembro1");
        QPersonaDTO qMiembro2 = new QPersonaDTO("qMiembro2");

        JPAQuery query = new JPAQuery(entityManager);

        query.from(qTribunalDTO)
                .leftJoin(qPresidente).on(qTribunalDTO.presidenteId.eq(qPresidente.id))
                .leftJoin(qMiembro1).on(qTribunalDTO.miembro1Id.eq(qMiembro1.id))
                .leftJoin(qMiembro2).on(qTribunalDTO.miembro2Id.eq(qMiembro2.id))
                .where(qTribunalDTO.codigoAsignatura.eq(codigoAsignatura)
                        .and(qTribunalDTO.cursoAca.eq(cursoAca.intValue())))
                .orderBy(qTribunalDTO.nombre.asc());
        return query.list(new QTribunal(
                qTribunalDTO.id,
                qTribunalDTO.nombre,
                qTribunalDTO.cursoAca,
                qTribunalDTO.codigoAsignatura,
                qTribunalDTO.presidenteId,
                qPresidente.nombre,
                qPresidente.apellido1,
                qTribunalDTO.miembro1Id,
                qMiembro1.nombre,
                qMiembro1.apellido1,
                qTribunalDTO.miembro2Id,
                qMiembro2.nombre,
                qMiembro2.apellido1,
                qTribunalDTO.presidenteUJI,
                qTribunalDTO.nombrePresidente,
                qTribunalDTO.correoPresidente,
                qTribunalDTO.miembro1UJI,
                qTribunalDTO.nombreMiembro1,
                qTribunalDTO.correoMiembro1,
                qTribunalDTO.miembro2UJI,
                qTribunalDTO.nombreMiembro2,
                qTribunalDTO.correoMiembro2

        ));
    }

    public void deleteTribunal(Long id) {
        QTribunalAlumnoDTO qTribunalAlumnoDTO = QTribunalAlumnoDTO.tribunalAlumnoDTO;

        JPADeleteClause deleteAlumnos = new JPADeleteClause(entityManager, qTribunalAlumnoDTO);
        deleteAlumnos.where(qTribunalAlumnoDTO.tribunalId.eq(id)).execute();

        JPADeleteClause deleteTribunales = new JPADeleteClause(entityManager, qTribunalDTO);
        deleteTribunales.where(qTribunalDTO.id.eq(id)).execute();
    }

    public Long updateTribunal(Tribunal tribunal) {
        JPAUpdateClause update = new JPAUpdateClause(entityManager, qTribunalDTO);
        update.set(qTribunalDTO.id, tribunal.getId());
        update.set(qTribunalDTO.presidenteId, tribunal.getPresidenteId());
        update.set(qTribunalDTO.miembro1Id, tribunal.getMiembro1Id());
        update.set(qTribunalDTO.miembro2Id, tribunal.getMiembro2Id());
        update.set(qTribunalDTO.nombre, tribunal.getNombre());

        update.set(qTribunalDTO.presidenteUJI, tribunal.getPresidenteUJI());
        update.set(qTribunalDTO.nombrePresidente, tribunal.getNombrePresidente());
        update.set(qTribunalDTO.correoPresidente, tribunal.getCorreoPresidente());

        update.set(qTribunalDTO.miembro1UJI, tribunal.getMiembro1UJI());
        update.set(qTribunalDTO.nombreMiembro1, tribunal.getNombreMiembro1());
        update.set(qTribunalDTO.correoMiembro1, tribunal.getCorreoMiembro1());

        update.set(qTribunalDTO.miembro2UJI, tribunal.getMiembro2UJI());
        update.set(qTribunalDTO.nombreMiembro2, tribunal.getNombreMiembro2());
        update.set(qTribunalDTO.correoMiembro2, tribunal.getCorreoMiembro2());


        update.where(qTribunalDTO.id.eq(tribunal.getId()));
        return update.execute();
    }

    public List<TribunalAlumno> getAlumnosTribunal(String codigoAsignatura, Long id) {
        QPersonaDTO qPersonaDTO = new QPersonaDTO("qPersonaDTO");
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qTribunalAlumnoDTO)
                .join(qPersonaDTO).on(qTribunalAlumnoDTO.alumnoId.eq(qPersonaDTO.id))
                .where(qTribunalAlumnoDTO.tribunalId.eq(id));

        return query.list(new QTribunalAlumno(qTribunalAlumnoDTO.id, qTribunalAlumnoDTO.tribunalId, qTribunalAlumnoDTO.alumnoId, qPersonaDTO.nombre, qPersonaDTO.apellido1));
    }

    public void deleteAlumnoTribunal(Long alumnoId) {
        QTribunalAlumnoDTO qTribunalAlumnoDTO = QTribunalAlumnoDTO.tribunalAlumnoDTO;
        JPADeleteClause deleteAlumnos = new JPADeleteClause(entityManager, qTribunalAlumnoDTO);
        deleteAlumnos.where(qTribunalAlumnoDTO.id.eq(alumnoId)).execute();
    }

    public List<LookupItem> buscarAlumnos(String codigoAsignatura, String s, Long cursoAca) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qPersonaDTO)
                .join(qAsignaturasPersonaDTO).on(qPersonaDTO.id.eq(qAsignaturasPersonaDTO.id.personaId)
                        .and(qAsignaturasPersonaDTO.id.codigoAsignatura.eq(codigoAsignatura))
                        .and(qAsignaturasPersonaDTO.id.tipoRelacion.eq("ESTUDIANTE"))
                        .and(qAsignaturasPersonaDTO.ejercicio.eq(cursoAca.intValue())));

        Long stringToLong;
        try {
            stringToLong = Long.parseLong(s);
        } catch (NumberFormatException e) {
            stringToLong = null;
        }

        if (stringToLong == null) {
            query.where(Utils.limpia(qPersonaDTO.nombre)
                    .concat(" ")
                    .concat(Utils.limpia(qPersonaDTO.apellido1))
                    .concat(" ")
                    .concat(Utils.limpia(qPersonaDTO.apellido2))
                    .concat(", ")
                    .concat(Utils.limpia(qPersonaDTO.nombre)).containsIgnoreCase(StringUtils.limpiaAcentos(s))
            );
        } else {
            query.where(qPersonaDTO.id.eq(stringToLong));
        }

        return query.list(qPersonaDTO)
                .stream().map(personaDTO -> toLookUpItem(personaDTO)).collect(Collectors.toList());
    }

    private static LookupItem toLookUpItem(PersonaDTO personaDTO) {
        LookupItem item = new LookupItem();
        item.setId(String.valueOf(personaDTO.getId()));

        String nombre = formatNombre(personaDTO.getApellido1(), personaDTO.getApellido2(), personaDTO.getNombre());
        item.setNombre(nombre);

        item.addExtraParam("nombre", personaDTO.getNombre());
        item.addExtraParam("apellido1", personaDTO.getApellido1());
        item.addExtraParam("apellido2", personaDTO.getApellido2());
        return item;
    }

    private static String formatNombre(String apellido1, String apellido2, String nombre) {
        return Arrays.stream(new String[]{apellido1, apellido2, nombre})
                .filter(value -> null != value)
                .collect(Collectors.joining(" "));
    }
}
