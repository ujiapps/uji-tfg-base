Ext.define('TFG.view.profesor.PanelCumplimiento',
    {
        extend: 'Ext.Panel',
        alias: 'widget.panelProfesores',

        title: UI.i18n.title.loadAccomplishment,

        requires: [ 'TFG.view.profesor.GridProfesores' ],

        closable: true,

        layout: {
            type: 'vbox',
            align: 'stretch'
        },

        items: [
            {
                xtype: 'gridCumplimiento',
                flex: 1
            }
        ]
    });
