Ext.define('TFG.view.profesor.GridCumplimiento',
    {
        extend: 'TFG.view.base.BaseGrid',

        alias: 'widget.gridCumplimiento',

        store: 'ListaProfesores',

        title: UI.i18n.title.loadAccomplishment,
        allowEdit: false,
        showSearchField : false,
        showAddButton : false,
        showRemoveButton : false,
        showReloadButton : false,
        showExportbutton : false,
        showTopToolbar : true,
        showBottomToolbar : false,
        showFilterCoordinadas : true,
        columns: [{
                header: 'id',
                dataIndex: 'id',
                hidden: true
            },{
                header: UI.i18n.column.name,
                dataIndex: 'nombre',
                flex: 60
            },{
                header: UI.i18n.column.firstSurname,
                dataIndex: 'apellido1',
                flex: 60
            },{
                header: UI.i18n.column.secondSurname,
                dataIndex: 'apellido2',
                flex: 60
            },{                                
                header: UI.i18n.column.load,
                dataIndex: 'carga',
                flex: 30,
                summaryType: 'sum',
                summaryRenderer : function(value, summaryData, dataIndex) {
                                    return '<span style="font-weight:bold;">' + UI.i18n.column.total + ': ' + value.toFixed(0) + "</span>";
                                }
            },{
                header: UI.i18n.column.credits,
                dataIndex: 'creditos',
                hidden: true,
                summaryType: 'sum',
                summaryRenderer : function(value, summaryData, dataIndex) {
                                    return '<span style="font-weight:bold;">' + UI.i18n.column.total + ': ' + value.toFixed(2) + "</span>";
                                }
            },{
                header: UI.i18n.column.offer,
                dataIndex: 'oferta',
                flex: 30,
                summaryType: 'sum',
                renderer : function(value, metaData, record, row, col, store, gridView) {
                                    if (record.data.carga > value) {
                                        return '<span style="font-weight: bold; color: red;">' + value.toFixed(0) + '</span>';
                                    }
                                    return value;
                                }
            }
        ],
        features: [{
            ftype: 'summary'
        }],
        buttons: []
    });