Ext.define('TFG.view.profesor.GridResumenProfesores',
    {
        extend: 'Ext.ux.uji.grid.Panel',
        alias: 'widget.gridResumenProfesores',

        store: 'ListadoProfesores',

        title: 'Detalle Profesor',
        closable: true,
        allowEdit: false,
        showSearchField : false,
        showAddButton : false,
        showRemoveButton : false,
        showReloadButton : false,
        showExportbutton : false,
        showTopToolbar : false,
        showBottomToolbar : false,
        columns: [{
                header: 'id',
                dataIndex: 'id',
                hidden: true
            },{
                header: UI.i18n.column.code,
                dataIndex: 'codigo',
                flex: 50,   
                editor: {
                    xtype: 'textfield',
                    allowBlank: false
                }
            },{
                header: UI.i18n.column.name,
                dataIndex: 'nombre',
                flex: 60,
                editor: {
                    xtype: 'textfield',
                    allowBlank: false
                }
            },{
                header: UI.i18n.column.load,
                dataIndex: 'carga',
                flex: 60,
                editor: {
                    xtype: 'textfield',
                    allowBlank: false
                }
            }
        ]
    });