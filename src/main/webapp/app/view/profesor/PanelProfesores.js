Ext.define('TFG.view.profesor.PanelProfesores',
    {
        extend: 'Ext.Panel',
        alias: 'widget.panelProfesores',

        title: UI.i18n.title.workLoad,

        requires: [ 'TFG.view.profesor.GridProfesores' ],

        closable: true,

        layout: {
            type: 'vbox',
            align: 'stretch'
        },

        items: [
            {
                xtype: 'gridProfesores',
                flex: 1
            }
        ]
    });
