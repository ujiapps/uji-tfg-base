Ext.define('TFG.view.profesor.GridProfesores',
    {
        extend: 'TFG.view.base.BaseGrid',

        alias: 'widget.gridProfesores',

        store: 'ListaProfesores',

        title: UI.i18n.title.workLoad,
        allowEdit: true,
        showSearchField : false,
        showAddButton : false,
        showRemoveButton : false,
        showReloadButton : false,
        showExportbutton : false,
        showTopToolbar : true,
        showBottomToolbar : false,
        showFilterCoordinadas : true,
        columns: [{
                header: 'id',
                dataIndex: 'id',
                hidden: true
            },{
                header: UI.i18n.column.name,
                dataIndex: 'nombre',
                flex: 60
            },{
                header: UI.i18n.column.firstSurname,
                dataIndex: 'apellido1',
                flex: 60
            },{
                header: UI.i18n.column.secondSurname,
                dataIndex: 'apellido2',
                flex: 60
            },{                
                header: UI.i18n.column.load,
                dataIndex: 'carga',
                flex: 30,
                summaryType: 'sum',
                renderer: function(value, metaData, record, row, col, store, gridView) {
                    if (record.data.carga == null) {
                        return '<span style="color: red">' + UI.i18n.message.unassigned + '</span>';
                    }
                    return value;
                },
                editor: {
                    xtype:'textfield',
                    allowBlank:false
                },
                summaryRenderer : function(value, summaryData, dataIndex) {
                                    return '<span style="font-weight:bold;">' + UI.i18n.column.total + ': ' + value.toFixed(2) + "</span>";
                                }
            },{
                header: UI.i18n.column.credits,
                dataIndex: 'creditos',
                flex: 30,
                summaryType: 'sum',
                summaryRenderer : function(value, summaryData, dataIndex) {
                                    return '<span style="font-weight:bold;">' + UI.i18n.column.total + ': ' + value.toFixed(2) + "</span>";
                                }
            },{
                header: UI.i18n.column.offer,
                dataIndex: 'oferta',
                flex: 30,
                hidden: true,
                summaryType: 'sum',
                summaryRenderer : function(value, summaryData, dataIndex) {
                                    return '<span style="font-weight:bold;">' + UI.i18n.column.total + ': ' + value.toFixed(2) + "</span>";
                                }
            }
        ],
        features: [{
            ftype: 'summary'
        }],
        buttons: [{
            name: 'loot',
            action: 'prorrateo',
            text: UI.i18n.button.loot
        }]
    });