Ext.define('TFG.view.base.BaseGrid',
	{
		extend: 'Ext.ux.uji.grid.Panel',
		viewConfig: {
			loadingText: UI.i18n.message.loading
		},
		showAsignaturaFilter: false,
        initComponent: function() {
            this.callParent(arguments);
            
            if (this.showAsignaturaFilter === true || this.showFilterCoordinadas === true) {            
            	var store = this.showFilterCoordinadas?'AsignaturasCoordinadas':'Asignaturas'
	            var tbar = this.getDockedItems('toolbar[dock="top"]')[0];
	            if (tbar) {	            	
		            tbar.insert(tbar.items.length,
		            {
		                xtype : 'combobox',
		                name: 'asignatura_filter',
		                emptyText : UI.i18n.button.searchMatter,
		                store: store,
		                multiSelect: false,
		                valueField: 'codigo',
		                displayField: 'codinom',
		                width: 400,
		                queryMode: 'local'
		            });
	            }           
            }
        },
        getSelectedRecord: function() {
			var selectedRows = this.getSelectionModel().getSelection();
			var indiceFilaSeleccionada = this.getStore().indexOf(selectedRows[0]);
			return this.getStore().getAt(indiceFilaSeleccionada);
		},
		hasRowSelected: function() {
			var records = this.getSelectionModel().getSelection();
			if (records.length == 0)
				return false;
			else
				return true;
		},

});