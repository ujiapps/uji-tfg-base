Ext.define('TFG.view.base.BaseForm', {
  extend: 'Ext.FormPanel',
  tbar: [],
  initComponent: function() {
      this.callParent(arguments);
      
      if (this.showAsignaturaFilter === true || this.showFilterCoordinadas === true) {     
       var store = this.showFilterCoordinadas?'AsignaturasCoordinadas':'Asignaturas';
       var tbar = this.getDockedItems('toolbar[dock="top"]')[0];
        if (tbar) {               
          tbar.insert(tbar.items.length,
          {
              xtype : 'combobox',
              name: 'asignatura_filter',
              emptyText : UI.i18n.button.searchMatter,
              store: store,
              multiSelect: false,
              valueField: 'codigo',
              displayField: 'codinom',
              width: 400,
              queryMode: 'local'
          });
        }       
      }
  },

  autoHeight: true,
  bodyPadding: '20 20 10 20',

  buttons: [{
    xtype: 'button',
    text: UI.i18n.button.save,
    action: 'save'
  }, {
    xtype: 'button',
    text: UI.i18n.button.cancel,
    handler: function() {
      this.up('window').close();
    }
  }],
  saveFormData: function(grid, url, method, callback) {
    var me = this;
    var id = me.getForm().findField('id').getValue();
    var methodHTTP;
    var formURL;
    if (me.getForm().isValid()) {
      if (method) {
        methodHTTP = method;
        formURL = url;
      }
      else {
        methodHTTP = (id) ? 'PUT' : 'POST';
        formURL = url + '/' + ((id) ? id : '');
      }

      me.setLoading(UI.i18n.message.saving);
      me.getForm().submit({
        method: methodHTTP,
        url: formURL,
        success: function(form, action) {
          if (callback.success) {
            callback.success(form, action);               
          }
          else {
            me.up('window').close();
            grid.removeSelection();
            grid.store.load();
          }
          me.setLoading(false);
        },
        failure: function(form, action) {
          if (callback.failure)
            callback.failure(form, action);
          else
            alert(UI.i18n.error.formSave);
          me.setLoading(false);
        }
      });
    } else {
      alert(UI.i18n.error.emptyFields);
    }
  },

  addRedMarkIfRequired: function(component) {
    if (component.fieldLabel && !component.allowBlank && component.xtype != 'checkbox')
      component.fieldLabel += ' <span class="req" style="color:red">*</span>';
  },

  cargaStoreElemento: function(elemento) {
      if (elemento.store.count() == 0)
         elemento.store.load(function(records, operation, success) {
            if (success)
               elemento.setDisabled(false);
         });
      else
         elemento.setDisabled(false);
  },

  listeners: {
    'beforeadd': function(container, component, index, opts) {
      if (component.xtype != 'fieldset')
        this.addRedMarkIfRequired(component);
      else {
        for (var i=0;i<component.items.length;i++) {
          this.addRedMarkIfRequired(component.items.items[i]);
        }
      }
    }
  }
});