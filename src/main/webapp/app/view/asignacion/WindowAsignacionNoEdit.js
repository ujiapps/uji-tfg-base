Ext.define('TFG.view.asignacion.WindowAsignacionNoEdit',
{
    extend : 'Ext.window.Window',
    alias : 'widget.windowAsignacionNoEdit',
    title : UI.i18n.title.formAsignacion,
    reference : 'windowAsignacionNoEdit',
    width : 500,
    height : 450,
    frame : false,
    bodyStyle : 'padding:1em; background-color:white;',
    modal : true,
    closable : false,
    layout : 'fit',
    items : [{
        xtype : 'formAsignacion'        
    }],

    buttons : [
    {
        text : UI.i18n.button.cancel,
        action : 'close'
    }],

    load : function(record)
    {
        var form = this.down('form').getForm();
        form.loadRecord(record);        
    }    
});
