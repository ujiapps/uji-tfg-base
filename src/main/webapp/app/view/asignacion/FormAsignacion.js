Ext.define('TFG.view.asignacion.FormAsignacion',
{
    extend: 'TFG.view.base.BaseForm',
    alias: 'widget.formAsignacion',
    border : false,
    store: 'Asignaciones',
    
    layout :
    {
        type : 'form',
        align : 'stretch'
    },
    defaults: {
        allowBlank: false,
        alignTo: 'right',
        width: '100%',
        labelAlign: 'right',
        labelWidth: 120
    },
    autoScroll: true,
    tbar: false,
    items: [{
        fieldLabel: UI.i18n.field.comments,
        labelAlign: 'top',
        allowBlank: true,
        name: 'comentarios',
        xtype: 'textarea',
        height: 300
    }],
    buttons:[]
});
    
