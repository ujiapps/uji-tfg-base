Ext.define('TFG.view.asignacion.GridAsignaciones',
    {
        extend: 'TFG.view.base.BaseGrid',

        alias: 'widget.gridAsignaciones',

        store: 'Asignaciones',

        title: UI.i18n.title.assignations,
        allowEdit: true,
        showSearchField : false,
        showAddButton : false,
        showRemoveButton : false,
        showReloadButton : false,
        showExportbutton : false,
        showTopToolbar : true,
        showBottomToolbar : false,
        showFilterCoordinadas: false,
        initComponent: function() {
            var hideAceptada = arguments[0];
           
            this.columns = [{
                    header: 'persona_id',
                    dataIndex: 'persona_id',
                    hidden: true
                },{
                    header: UI.i18n.column.name,
                    dataIndex: 'nombre',
                    flex: 60
                },{
                    header: UI.i18n.column.firstSurname,
                    dataIndex: 'apellido1',
                    flex: 60
                },{
                    header: UI.i18n.column.secondSurname,
                    dataIndex: 'apellido2',
                    flex: 60
                },{                                
                    header: UI.i18n.column.matter,
                    dataIndex: 'asignatura',
                    hidden: true,
                    flex: 60
                },{
                    header: UI.i18n.column.matterCode,
                    dataIndex: 'codigo_asignatura',
                    hidden: true
                },{
                    header: hideAceptada?UI.i18n.column.acceptedProposal:UI.i18n.column.proposal,
                    dataIndex: 'propuesta_id',
                    flex: 60,
                    editor: {
                        xtype: 'combobox',
                        name: 'comboPropuesta',
                        hiddenValue: 'propuesta_id',
                        valueField: 'id',
                        hiddenName: 'propuesta_nombre',
                        displayField: 'nombreConAutor',
                        editable: false,
                        lastQuery: '',
                        forceSelection: true       
                    },
                    renderer: function(value, metaData, record, row, col, store, gridView) {
                                if (!value || value == null || value == "") {
                                    return '<span style="color: red">' + UI.i18n.message.unassigned + '</span>';
                                }                    
                                metaData.tdAttr = 'data-qtip="' + record.data.propuesta_nombre + '"';
                                return record.data.propuesta_nombre;
                            }   
                },{
                    header: UI.i18n.column.acceptedProposal,
                    dataIndex: 'propuesta_aceptada',
                    flex: 60,
                    hidden: hideAceptada,
                    hideable: !hideAceptada,
                    renderer: function(value, metaData, record, row, col, store, gridView) {
                                if (!value || value == null || value == "") {
                                    return '<span style="color: red">' + UI.i18n.message.unassigned + '</span>';
                                }                    
                                metaData.tdAttr = 'data-qtip="' + record.data.propuesta_aceptada + '"';
                                return record.data.propuesta_aceptada;
                            }
                },{ 
                    dataIndex: 'editor_persona_id',
                    hidden: true
                },{
                    header: UI.i18n.column.updateDate,
                    dataIndex: 'fecha_edicion',
                    dateFormat: 'd/m/Y H:i',
                    hidden: true,
                    renderer: function(value, p, record) {
                        if (!value) {
                            return "";
                        }
                        var date = new Date(value.getTime()) ;
                        return Ext.Date.format(date, 'd/m/Y H:i');
                    },
                },{
                    header: UI.i18n.column.status,
                    dataIndex: 'aceptada',
                    flex: 40,
                    renderer: function(value, metaData, record, row, col, store, gridView) {
                                if (record.data.propuesta_id == 0) {
                                    return '<span style="color: red">' + UI.i18n.message.unassigned + '</span>';
                                }   
                                else if (record.data.aceptada == 0) {
                                    return '<span style="color: orange">' + UI.i18n.message.assignationAwaiting + '</span>';
                                }                         
                                return '<span style="color: green">' + UI.i18n.message.assignationAccepted + '</span>';
                            }
                },{
                    header: UI.i18n.column.comments,
                    flex: 30,
                    xtype: 'actioncolumn',                
                    items: [{
                        text: UI.i18n.message.seeComments,
                        icon: 'http://static.uji.es/js/extjs/extjs-4.2.0/examples/shared/icons/fam/user_comment.png',
                        tooltip: UI.i18n.message.seeComments,
                        handler: function(grid, rowIndex, colIndex) {
                            TFG.getApplication().getController('ControllerAsignaciones').editComentarios(grid, rowIndex, colIndex);
                        }
                    }],
                    renderer: function (val, metadata, record) {
                        if (record.data.propuesta_id) {
                            this.items[0].icon = 'http://static.uji.es/js/extjs/extjs-4.2.0/examples/shared/icons/fam/user_comment.png';
                            this.items[0].tooltip = UI.i18n.message.seeComments;
                            metadata.style = 'cursor: pointer;';                            
                        } else {
                            this.items[0].icon = '';
                            this.items[0].tooltip = '';
                        }

                        return val;
                    }
                }];
            this.callParent(arguments);
        }
    });