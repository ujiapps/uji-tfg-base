Ext.define('TFG.view.asignacion.FormPlanAsignaciones',
{
    extend: 'TFG.view.base.BaseForm',
    alias: 'widget.formPlanAsignaciones',
    border : false,
    title: UI.i18n.title.assignationInnings,
    frame: false,
    layout : 'column',
    defaults: {
        alignTo: 'right',
        labelAlign: 'right',
        labelWidth: 130,
        padding: '0 0 10px 0'
    },
    autoScroll: true,
    items: [{
        fieldLabel: UI.i18n.field.matter,
        columnWidth: 1/3,
        xtype : 'combobox',
        name: 'asignatura_filter',
        emptyText : UI.i18n.button.searchMatter,
        store: 'AsignaturasCoordinadas',
        multiSelect: false,
        valueField: 'codigo',
        displayField: 'codinom',
        queryMode: 'local'
    },{
        columnWidth: 1/3,
        fieldLabel: UI.i18n.field.turnSize,
        xtype: 'combo',
        multiSelect: false,
        loading: true,
        forceSelection: true,
        editable: false,
        store: new Ext.data.SimpleStore({
                data: [['1', '1'],['5', '5'],['10', '10'],['15', '15'],['20', '20'],['25', '25'],['30', '30'],['35', '35'],['40', '40'],['50', '50']],
                fields: ['value', 'text']
            }),
        valueField: 'value',
        displayField: 'text',       
        value: '20', 
        allowBlank: false,
        name: 'size'//20
    },{
        columnWidth: 1/3,
        fieldLabel: UI.i18n.field.turnDuration,
        xtype: 'combo',
        multiSelect: false,
        loading: true,
        forceSelection: true,
        editable: false,
        store: new Ext.data.SimpleStore({
                data: [['5', '5 min.'],['10', '10 min.'],['15', '15 min.'],['20', '20 min.'],['25', '25 min.'],['30', '30 min.'],['35', '35 min.'],['40', '40 min.'],['50', '50 min.'],['55', '55 min.'],['60', '60 min.']],
                fields: ['value', 'text']
            }),
        value: '15',
        valueField: 'value',
        displayField: 'text',
        allowBlank: false,
        name: 'duration'//15'
    },{
        columnWidth: 1/3,
        fieldLabel: UI.i18n.field.beginDate,
        xtype: 'datefield',
        name: 'beginDate',
        format: 'd-m-Y',
        allowBlank: false
    },{
        columnWidth: 1/3,
        fieldLabel: UI.i18n.field.turnOpen,
        xtype: 'timefield',
        increment: 10,
        value: '08:00',
        growToLongestValue: true,
        format: 'H:i',        
        allowBlank: false,
        name: 'open'//08:00
    },{
        columnWidth: 1/3,
        fieldLabel: UI.i18n.field.turnClose,
        xtype: 'timefield',
        increment: 10,
        value: '19:00',
        growToLongestValue: true,
        format: 'H:i',
        allowBlank: false,
        name: 'close'//19:00            
    }],
    buttons:[{
        text : UI.i18n.button.sortInning,
        name : 'sort',
        action: 'sort'
    }]
});
