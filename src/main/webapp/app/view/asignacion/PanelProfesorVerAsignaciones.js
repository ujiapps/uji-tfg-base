Ext.define('TFG.view.asignacion.PanelProfesorVerAsignaciones',
    {
        extend: 'Ext.Panel',
        alias: 'widget.panelProfesorVerAsignaciones',

        title: UI.i18n.title.assignationsTeacher,

        requires: [ 'TFG.view.asignacion.GridAsignaciones' ],

        closable: true,

        layout: 'border',

        items: [{
            xtype : 'combobox',
            region: 'north',
            name: 'asignatura_filter',
            emptyText : UI.i18n.button.searchMatter,
            store: 'Asignaturas',
            multiSelect: false,
            valueField: 'codigo',
            displayField: 'codinom',
            maxWidth: 400,
            queryMode: 'local'
        },{
            xtype: 'tabpanel',
            region: 'center',
            activeTab: 0,
            items: [{
                title: UI.i18n.title.assigned,
                xtype: 'gridProfesorVerAsignacionesAsignadas'
            },{
                title: UI.i18n.title.waiting,
                xtype: 'gridProfesorVerAsignacionesEnEspera'
            }]
        }]
    });