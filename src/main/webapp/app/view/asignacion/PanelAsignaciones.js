Ext.define('TFG.view.asignacion.PanelAsignaciones',
    {
        extend: 'Ext.Panel',
        alias: 'widget.panelAsignaciones',

        title: UI.i18n.title.assignations,

        requires: [ 'TFG.view.asignacion.GridAsignaciones' ],

        closable: true,

        layout: 'border',

        items: [{
            xtype: 'container',
            region: 'north',
            width: 550,
            layout: {
                type: 'hbox',
                pack: 'start',
                align: 'stretch'
            },
            items: [{
                xtype : 'combobox',
                name: 'asignatura_filter',
                emptyText : UI.i18n.button.searchMatter,
                store: 'AsignaturasCoordinadas',
                multiSelect: false,
                valueField: 'codigo',
                displayField: 'codinom',
                maxWidth: 400,
                minWidth: 250,
                queryMode: 'local'
            },{
                xtype : 'button',
                name: 'exportExcel',
                text: UI.i18n.button.exportExcel,
                action: 'exportexcel',
                width:150,
                margin: "0 0 0 10",
                disabled: true
            }]        
        },{
            xtype: 'tabpanel',
            region: 'center',
            activeTab: 0,
            items: [{
                title: UI.i18n.title.assigned,
                xtype: 'gridAsignacionesAsignadas'
            },{
                title: UI.i18n.title.waiting,
                xtype: 'gridAsignacionesEnEspera'
            },{
                title: UI.i18n.title.unassigned,
                xtype: 'gridAsignacionesSinAsignar'
            }]
        }]
    });