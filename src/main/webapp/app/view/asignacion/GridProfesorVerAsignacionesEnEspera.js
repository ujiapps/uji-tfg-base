Ext.define('TFG.view.asignacion.GridProfesorVerAsignacionesEnEspera',
    {
        extend: 'TFG.view.asignacion.GridAsignaciones',

        alias: 'widget.gridProfesorVerAsignacionesEnEspera',

        store: 'AsignacionesProfesor',

        title: UI.i18n.title.assignations,
        allowEdit: false,        
             
        initComponent: function() {
            this.callParent([false]);
        }
    });