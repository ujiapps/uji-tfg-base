Ext.define('TFG.view.asignacion.PanelPlanAsignaciones',
    {
        extend: 'Ext.Panel',
        alias: 'widget.panelPlanAsignaciones',

        title: UI.i18n.title.assignationsPlan,

        requires: [ 'TFG.view.asignacion.GridPlanAsignaciones', 'TFG.view.asignacion.FormPlanAsignaciones' ],

        closable: true,

        layout: 'border',

        items: [{
                    region: 'north',
                    xtype: 'formPlanAsignaciones',
                    autoHeight: true
                },{
                    region: 'center',
                    xtype: 'gridPlanAsignaciones'
                }]
    });