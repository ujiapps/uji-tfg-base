Ext.define('TFG.view.asignacion.GridProfesorVerAsignacionesAsignadas',
    {
        extend: 'TFG.view.asignacion.GridAsignaciones',

        alias: 'widget.gridProfesorVerAsignacionesAsignadas',

        store: 'AsignacionesProfesor',

        title: UI.i18n.title.assignations,
        allowEdit: false,

        initComponent: function() {
            this.callParent([true]);
        }
    });