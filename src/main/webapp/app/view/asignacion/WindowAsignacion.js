Ext.define('TFG.view.asignacion.WindowAsignacion',
{
    extend : 'Ext.window.Window',
    alias : 'widget.windowAsignacion',
    title : UI.i18n.title.formAsignacion,
    reference : 'windowAsignacion',
    width : 500,
    height : 450,
    frame : false,
    bodyStyle : 'padding:1em; background-color:white;',
    modal : true,
    closable : false,
    layout : 'fit',
    items : [{
        xtype : 'formAsignacion'        
    }],

    buttons : [
    {
        text : UI.i18n.button.save,
        action : 'save'
    },
    {
        text : UI.i18n.button.cancel,
        action : 'close'
    }],

    clearForm : function()
    {
        this.down('form').getForm().reset();
    },

    clearAndHide : function()
    {
        this.clearForm();
        this.hide();
    },

    save : function(callback)
    {
        var form = this.down('form').getForm();

        if (form.isValid())
        {
            form.submit(
            {
                success : function(form, action)
                {
                    callback();
                }
            });
        }
    },

    load : function(record)
    {
        var form = this.down('form').getForm();
        form.loadRecord(record);        
    }    
});
