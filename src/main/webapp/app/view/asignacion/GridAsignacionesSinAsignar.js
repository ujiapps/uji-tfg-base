Ext.define('TFG.view.asignacion.GridAsignacionesSinAsignar',
    {
        extend: 'TFG.view.asignacion.GridAsignaciones',

        alias: 'widget.gridAsignacionesSinAsignar',

        store: 'Asignaciones',

        title: UI.i18n.title.assignations,

        initComponent: function() {
            this.callParent([true]);
        }
       
    });