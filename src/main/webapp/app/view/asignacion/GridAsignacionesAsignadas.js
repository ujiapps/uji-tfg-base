Ext.define('TFG.view.asignacion.GridAsignacionesAsignadas',
    {
        extend: 'TFG.view.asignacion.GridAsignaciones',

        alias: 'widget.gridAsignacionesAsignadas',

        store: 'Asignaciones',

        title: UI.i18n.title.assignations,

        initComponent: function() {
            this.callParent([true]);
        }
    });