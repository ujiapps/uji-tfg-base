Ext.define('TFG.view.asignacion.GridVerAsignaciones',
    {
        extend: 'TFG.view.base.BaseGrid',

        alias: 'widget.gridVerAsignaciones',

        store: 'PlanAsignaciones',

        title: UI.i18n.title.seeAssignationsPlan,
        closable: true,
        allowEdit: false,
        showSearchField : false,
        showAddButton : false,
        showRemoveButton : false,
        showReloadButton : false,
        showExportbutton : false,
        showTopToolbar : true,
        showBottomToolbar : false,
        showAsignaturaFilter: true,
        columns: [{
                header: 'persona_id',
                dataIndex: 'persona_id',                
                hidden: true
            },{
                header: UI.i18n.column.name,
                dataIndex: 'nombre',
                flex: 60
            },{
                header: UI.i18n.column.firstSurname,
                dataIndex: 'apellido1',
                flex: 60
            },{
                header: UI.i18n.column.secondSurname,
                dataIndex: 'apellido2',
                flex: 60
            },{         
                header: UI.i18n.column.email,
                dataIndex: 'email',
                flex: 60
            },{                
                header: UI.i18n.column.matterCode,
                dataIndex: 'codigo_asignatura',
                hidden: true
            },{
                header: UI.i18n.column.sorting,
                dataIndex: 'criterio',
                flex: 60
            },{
                header: UI.i18n.column.openingDate,
                dataIndex: 'fecha_apertura',
                xtype:  'datecolumn',
                renderer: function(value, p, record) {
                    if (!value) {
                        return UI.i18n.message.unassigned;
                    }
                    var date = new Date(value.getTime()) ;
                    return Ext.Date.format(date, 'd/m/Y H:i');
                },
                flex: 60
            }
        ],
        buttons: []
});