Ext.define('TFG.view.asignacion.GridAsignacionesEnEspera',
    {
        extend: 'TFG.view.asignacion.GridAsignaciones',

        alias: 'widget.gridAsignacionesEnEspera',

        store: 'Asignaciones',

        title: UI.i18n.title.assignations,
        
        tbar: [{
            text: UI.i18n.button.delAwaiting, 
            xtype: 'button',
            action: 'delawaiting' 
        },{
            text: UI.i18n.button.confirmAwaiting,
            xtype: 'button',
            action: 'confirmawaiting'
        }
        ],
             
        initComponent: function() {
            this.callParent([false]);
        }
    });