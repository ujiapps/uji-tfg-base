    Ext.define('TFG.view.asignacion.GridPlanAsignaciones',
    {
        extend: 'TFG.view.base.BaseGrid',

        alias: 'widget.gridPlanAsignaciones',

        store: 'PlanAsignaciones',

        title: UI.i18n.title.assignationsPlan,
        allowEdit: true,
        showSearchField : false,
        showAddButton : false,
        showRemoveButton : false,
        showReloadButton : false,
        showExportbutton : false,
        showTopToolbar : true,
        showBottomToolbar : false,
        columns: [{
                header: 'persona_id',
                dataIndex: 'persona_id',                
                hidden: true
            },{
                header: UI.i18n.column.name,
                dataIndex: 'nombre',
                flex: 60
            },{                                
                header: UI.i18n.column.firstSurname,
                dataIndex: 'apellido1',
                flex: 60
            },{
                header: UI.i18n.column.secondSurname,
                dataIndex: 'apellido2',
                flex: 60
            },{
                header: UI.i18n.column.email,
                dataIndex: 'email',
                flex: 60
            },{                
                header: UI.i18n.column.matterCode,
                dataIndex: 'codigo_asignatura',
                hidden: true
            },{
                header: UI.i18n.column.sorting,
                dataIndex: 'criterio',
                flex: 60
            },{
                header: UI.i18n.column.openingDate,
                dataIndex: 'fecha_apertura',
                xtype:  'datecolumn',
                dateFormat: 'd/m/Y',
                renderer: function(value, p, record) {
                    if (!value) {
                        return UI.i18n.message.unassigned;
                    }
                    var date = new Date(value.getTime()) ;
                    return Ext.Date.format(date, 'd/m/Y');
                },
                editor: {
                    xtype: 'datefield',
                    allowBlank: false,
                    format: 'd/m/Y'
                },
                flex: 60
            },{
                header: UI.i18n.column.openingHour,
                dataIndex: 'hora_apertura',
                xtype: 'datecolumn',
                renderer: function(value, p, record) {
                    if (!value) {
                        return UI.i18n.message.unassigned;
                    }
                    var date = new Date(value.getTime()) ;
                    return Ext.Date.format(date, 'H:i');                  
                },
                editor: {
                    xtype: 'timefield',
                    increment: 10,
                    format: 'H:i',        
                    allowBlank: false
                },
                flex: 60
            }
        ],
        buttons: [{
            text : UI.i18n.button.setInning,
            name : 'set',
            action: 'set',
            disabled: true
        }]
});