Ext.define('TFG.view.propuesta.FormPropuestas',
{
    extend: 'TFG.view.base.BaseForm',
    alias: 'widget.formPropuestas',
    border : false,
    store: 'Propuestas',
    // url : '/tfg/rest/propuestas', 
    layout :
    {
        type : 'form',
        align : 'stretch'
    },
    defaults: {
        allowBlank: false,
        alignTo: 'right',
        width: '100%',
        labelAlign: 'right',
        labelWidth: 120
    },
    autoScroll: true,
    tbar: false,
    items: [{
        fieldLabel: UI.i18n.field.author,
        name: 'autor_id',
        xtype: 'combo',
        store: 'Profesores',
        multiSelect: false,
        valueField: 'id',
        displayField: 'nombreCompleto'
    },{
        fieldLabel: UI.i18n.field.matter,
        name: 'codigo_asignatura',
        xtype: 'combo',
        store: 'Asignaturas',
        multiSelect: false,
        valueField: 'codigo',
        displayField: 'codinom'
    },{
        fieldLabel: UI.i18n.field.code,
        name: 'id',
        allowBlank: true,
        hidden: true,
        xtype: 'textfield'
    },{
        fieldLabel: UI.i18n.field.title,
        name: 'nombre',
        xtype: 'textfield'
    },{
        fieldLabel: UI.i18n.field.pax,
        xtype: 'numberfield',
        name: 'plazas',
        minValue: 1
    },{
        xtype: 'displayfield',
        name: 'carga_restante',
        allowBlank: true,
        fieldLabel: UI.i18n.field.paxLeft
    },{
        fieldLabel: UI.i18n.field.goals,
        allowBlank: true,
        name: 'objetivos',
        xtype: 'htmleditor',
        //xtype: 'textarea',
        height: 115
    },{
        fieldLabel: UI.i18n.field.bibliography,
        allowBlank: true,
        name: 'bibliografia',
        xtype: 'htmleditor',
        //xtype: 'textarea',
        height: 115
    },{
        fieldLabel: UI.i18n.field.description,
        allowBlank: true,
        name: 'descripcion',
        xtype: 'htmleditor',
        //xtype: 'textarea',
        height: 115
    },{
        fieldLabel: UI.i18n.field.notes,
        allowBlank: true,
        name: 'observaciones',
        xtype: 'htmleditor',
        //xtype: 'textarea',        
        height: 115
    }],
    buttons:[]
});
    
