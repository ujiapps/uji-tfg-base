Ext.define('TFG.view.propuesta.GridPropuestas',
    {
        extend: 'TFG.view.base.BaseGrid',

        alias: 'widget.gridPropuestas',

        title: UI.i18n.title.propuestas,

        allowEdit: false,
        showSearchField : false,
        showAddButton : true,
        showRemoveButton : true,
        showReloadButton : true,
        showExportbutton : false,
        showTopToolbar : true,
        showBottomToolbar : true,
        showAsignaturaFilter : true,     
        handlerRemoveButton : function() 
        {
            var ref = this.up('grid');
            var records = ref.getSelectionModel().getSelection();

            if (records.length > 0) 
            {
                Ext.Msg.confirm(UI.i18n.title.confirmDelete, UI.i18n.message.confirmDelete, function(btn, text)
                {
                    if (btn == 'yes')
                    {
                        var rowEditor = ref.getPlugin();                        
                        rowEditor.cancelEdit();

                        TFG.getApplication().getController('ControllerPropuestas').removePropuesta(ref,records[0], function(res, opt) {                            
                            ref.store.remove(records);
                        });
                    }
                });
            }
        },
        columns: [{
                header: UI.i18n.column.code,
                dataIndex: 'id',
                hidden: true                
            },{
                header: UI.i18n.column.title,
                dataIndex: 'nombre',
                flex: 60
            },{
                header: UI.i18n.column.author,
                dataIndex: 'autor',
                flex: 60
            },{
                header: UI.i18n.column.firstSurname,
                dataIndex: 'apellido1',
                flex: 60
            },{
                header: UI.i18n.column.secondSurname,
                dataIndex: 'apellido2',
                flex: 60
            },{
                header: UI.i18n.column.authorCode,
                hidden: true,
                dataIndex: 'autor_id'
            },{
                header: UI.i18n.column.matter,
                dataIndex: 'asignatura',
                hidden: true
            },{
                header: UI.i18n.column.matterCode,
                dataIndex: 'codigo_asignatura',
                hidden: true
            }, {
                header: UI.i18n.column.pax,
                dataIndex: 'plazas'
            },{
                header: UI.i18n.column.pax_available,
                dataIndex: 'plazas_disponibles',
                flex: 65
            },{
                header: UI.i18n.column.people_awaiting,
                dataIndex: 'gente_esperando',
                flex: 65
            }, {
                header: UI.i18n.column.status,
                dataIndex: 'disponibilidad',
                flex: 55,
                renderer: function(value, p, record) {
                    var salida = UI.i18n.message.undefined;
                    if (record.data.plazas_disponibles > 0) 
                        return '<span style="color: green">' + UI.i18n.message.availablePax + '</span>';
                    if (record.data.gente_esperando == 0)
                        return '<span style="color: red">' +  UI.i18n.message.noPax + '</span>';
                    return '<span style="color: orange">' + UI.i18n.message.bookedVacancies + '</span>';
                }
            }, {
                header: UI.i18n.column.awaiting,
                dataIndex: 'espera',
                hidden: true
            }
        ],    


        buttons: [],
        initComponent: function() {
            this.callParent(arguments);
            var tbar = this.getDockedItems('toolbar[dock="top"]')[0];

            tbar.insert(0,
            {
                xtype : 'button',
                text : 'Editar',
                action : 'edit',
                iconCls : 'application-edit'
            });

        }
    });