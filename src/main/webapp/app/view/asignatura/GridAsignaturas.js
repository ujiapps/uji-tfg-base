Ext.define('TFG.view.asignatura.GridAsignaturas',
    {
        extend: 'Ext.ux.uji.grid.Panel',
        alias: 'widget.gridAsignaturas',

        store: 'AsignaturasCoordinadas',

        title: 'Asignaturas',
        closable: true,
        allowEdit: true,
        showSearchField : false,
        showAddButton : false,
        showRemoveButton : false,
        showReloadButton : false,
        showExportbutton : false,
        showTopToolbar : false,
        showBottomToolbar : false,
        columns: [{               
                header: 'id',
                dataIndex: 'codigo',
                hidden: true
            },{
                header: UI.i18n.column.code,
                dataIndex: 'codigo',
                flex: 50   
            },{
                header: UI.i18n.column.name,
                dataIndex: 'nombre',
                flex: 60
            },{
                header: UI.i18n.column.name,
                dataIndex: 'codinom',
                flex: 60,
                hidden: true,
            },{
                header: UI.i18n.column.dateLimit,
                dataIndex: 'fechaLimite',
                flex: 60,
                editor: {
                    xtype: 'datefield',
                    format: 'd/m/Y'
                },
                renderer: function(value, metaData, record, row, col, store, gridView) {
                    if (value != null) {
                        return Ext.Date.format(value, 'd/m/Y');
                    }
                    else {
                        return UI.i18n.message.unset;
                    }
                }
            },{
                header: UI.i18n.column.sorting,
                dataIndex: 'ordenacion',
                flex: 90,
                editor: {
                    xtype: 'combobox',
                    store: new Ext.data.SimpleStore({
                        data: [['NOTA_MEDIA', UI.i18n.sorting.NOTA_MEDIA],['NOTA_ACCESO', UI.i18n.sorting.NOTA_ACCESO]],
                        id: 0,
                        fields: ['value', 'text']
                    }),
                    valueField: 'value',
                    displayField: 'text',
                    editable: false,
                    allowBlank:false                    
                },
                renderer: function(value, metaData, record, row, col, store, gridview) {
                    if (value != null)
                        return UI.i18n.sorting[value];
                    return UI.i18n.message.unset;                     
                }
            },{
                header: UI.i18n.column.queueAvailable,
                dataIndex: 'espera_habilitada',
                flex: 30,
                renderer: function(value, metaData, record, row, col, store, gridview) {
                    if (value != null)
                        return value==0?UI.i18n.message.disabled:UI.i18n.message.enabled;
                    return UI.i18n.message.unset; 
                },
                editor: {
                    xtype: 'combobox',
                    store: new Ext.data.SimpleStore({
                        data: [[true, UI.i18n.message.enabled],[false, UI.i18n.message.disabled]],
                        id: 0,
                        fields: ['value', 'text']
                    }),
                    valueField: 'value',
                    displayField: 'text',
                    editable: false,
                    allowBlank:false                    
                },

            }

        ]
    });