var lang = 'ca';
if (Ext.util.Cookies.get('uji-lang')) {
    lang = Ext.util.Cookies.get('uji-lang');
}
else {
    Ext.util.Cookies.set('uji-lang', lang);
}
Ext.Loader.setConfig(
{
    enable : true,
    paths :
    {
        'Ext.ux' : 'http://static.uji.es/js/extjs/extjs-4.2.0/examples/ux/',
        'Ext.i18n': 'app/locale/'+lang+'.js'
    }
});

var UI = Ext.create('Ext.i18n');
Ext.require('Ext.ux.TabCloseMenu');
Ext.require('Ext.grid.plugin.RowEditing');


Ext.define('TFG.Application',
{
    extend : 'Ext.ux.uji.Application',

    name : 'TFG',
    appFolder : 'app',
    autoCreateViewport : false,

    models : [ 'Asignatura', 'Propuesta', 'Usuario' ],

    views : [ ],

    controllers : [ 'ControllerAsignaturas', 'ControllerPropuestas', 'ControllerUsuarios', 'ControllerProfesores', 'ControllerAsignaciones'],

    launch : function()
    {
        
        Ext.create('Ext.ux.uji.ApplicationViewport',
        {
            codigoAplicacion : 'TFG',
            tituloAplicacion : UI.i18n.application.title,
            dashboard : false
        }); 
    }
});
