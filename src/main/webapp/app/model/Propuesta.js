Ext.define('TFG.model.Propuesta',
    {
        extend: 'Ext.data.Model',

        fields: [
            {
                name: 'id',
                type: 'int',
                useNull: true
            },{
                name: 'autor',
                type: 'string'
            },{
                name: 'apellido1',
                type: 'string'
            },{
                name: 'apellido2',
                type: 'string',
                useNull: true                
            },{
                name: 'autor_id',
                type: 'int'
            },{
                name: 'fechaCreacion',
                type: 'date'
            },{
                name: 'asignatura',
                type: 'string'                
            },{
                name: 'codigo_asignatura',
                type: 'string'
            },{
                name: 'nombre',
                type: 'string'
            },{
                name: 'objetivos',
                type: 'string'
            },{
                name: 'bibliografia',
                type: 'string'
            },{
                name: 'descripcion',
                type: 'string'
            },{
                name: 'observaciones',
                type: 'string'
            },{
                name: 'disponibilidad',
                type: 'int'
            },{
                name: 'espera',
                type: 'int'
            },{
                name: 'plazas',
                type: 'int'
            },{
                name: 'gente_esperando',
                type: 'int'
            },{
                name: 'plazas_disponibles',
                type: 'int'
            },{
                name: 'nombreConAutor',
                type: 'string',
                persist: false,
                convert: function( v, record ) {
                    return record.get('nombre') + ' (' + record.get( 'autor' ) + ' ' + record.get( 'apellido1' ) + (record.get('apellido2')?' ' + record.get( 'apellido2' ):'') + ')';
                }            
            }
        ]
    });
