Ext.define('TFG.model.PlanAsignacion',
{
    extend: 'Ext.data.Model',

    fields: [{
        name: 'persona_id',
        type: 'int'
    },{
        name: 'nombre',
        type: 'string'
    },{
        name: 'apellido1',
        type: 'string'
    },{
        name: 'apellido2',
        type: 'string',
        useNull: true        
    },{
        name: 'email',
        type: 'string'        
    },{
    	name: 'codigo_asignatura',
    	type: 'string',
    },{
        name: 'criterio',
        type: 'string'
    },{    
    	name: 'fecha_apertura',
    	type: 'date',
        dateFormat: 'time'
    },{
        name: 'hora_apertura',
        type: 'date',
        dateFormat: 'time'
    }]
});
