Ext.define('TFG.model.Asignacion',
{
    extend: 'Ext.data.Model',

    fields: [{
        name: 'persona_id',
        type: 'int'
    },{
        name: 'nombre',
        type: 'string'
    },{
        name: 'apellido1',
        type: 'string'
    },{
        name: 'apellido2',
        type: 'string',
        useNull: true        
    },{
    	name: 'codigo_asignatura',
    	type: 'string',
    },{
    	name: 'asignatura',
    	type: 'string'
	},{
    	name: 'propuesta_id',
    	type: 'int'
    },{
    	name: 'propuesta_nombre',
    	type: 'string'
    },{
      	name: 'editor_persona_id',
    	type: 'int',
        userNull: true
    },{
    	name: 'editor_nombre',
    	type: 'string',
        userNull: true
    },{
        name: 'editor_apellido1',
        type: 'string',
        userNull: true
    },{
        name: 'editor_apellido2',
        type: 'string',
        useNull: true        
    },{
        name: 'fecha_edicion',
        type: 'date',
        dateFormat: 'time',
        userNull: true
    },{
        name: 'aceptada',
        type: 'int',
        allowBlank: true
    },{
        name: 'comentarios',
        type: 'string',
        allowBlank: true
    }, {
        name: 'propuesta_aceptada',
        type: 'string',
        userNull: true,
        persist: false
    }]
});
