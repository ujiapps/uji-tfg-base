Ext.define('TFG.model.Usuario',
{
    extend: 'Ext.data.Model',

    fields: [{
        name: 'id',
        type: 'int',
        useNull: true
    },{
        name: 'nombre',
        type: 'string'
    },{
        name: 'apellido1',
        type: 'string'
    },{
        name: 'apellido2',
        type: 'string',
        useNull: true      
    },{
        name: 'nombreCompleto',
        type: 'string',
        persist: false,
        convert: function( v, record ) {
            return record.get( 'nombre' ) + ' ' + record.get( 'apellido1' ) + ' ' + record.get( 'apellido2' )
        }
    }]
});
