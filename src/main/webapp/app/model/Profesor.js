Ext.define('TFG.model.Profesor',
{
    extend: 'Ext.data.Model',

    fields: [{
        name: 'id',
        type: 'int'
    },{
        name: 'nombre',
        type: 'string'
    },{
        name: 'apellido1',
        type: 'string'
    },{
        name: 'apellido2',
        type: 'string',
        useNull: true        
    },{
    	name: 'carga',
    	type: 'int',
        useNull: true 
    },{
        name: 'creditos',
        type: 'float'
    },{
        name: 'oferta',
        type: 'int'
    },{
        name: 'nombreCompleto',
        type: 'string',
        persist: false,
        convert: function( v, record ) {
            return record.get( 'nombre' ) + ' ' + record.get( 'apellido1' ) + ' ' + record.get( 'apellido2' )
        }
    }]
});
