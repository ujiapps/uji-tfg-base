Ext.define('TFG.model.Asignatura',
{
    extend: 'Ext.data.Model',

    fields: [{
        name: 'nombre',
        type: 'string'
    },{
        name: 'codigo',
        type: 'string'
    },{
        name: 'codinom',
        type: 'string'
    },{
        name: 'fechaLimite',
        type: 'date',
        dateFormat: 'Y-m-d',
        useNull: true
    },{
        name: 'ordenacion',
        type: 'string',
        useNull: true
    }, {
        name: 'espera_habilitada',
        type: 'boolean',
        useNull: true
    }]
});
