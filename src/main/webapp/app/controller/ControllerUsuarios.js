Ext.define('TFG.controller.ControllerUsuarios',
{
    extend: 'TFG.controller.BaseController',

    stores: [ 'Usuarios', 'Profesores' ],
    model: [ 'Usuario' ]
});