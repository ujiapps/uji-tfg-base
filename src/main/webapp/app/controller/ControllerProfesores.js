Ext.define('TFG.controller.ControllerProfesores',
    {
        extend: 'TFG.controller.BaseController',

        stores: ['Propuestas', 'Asignaturas', 'Profesores','ListaProfesores'],
        model: [ 'Profesor' ],
        views: ['profesor.GridProfesores','profesor.PanelProfesores', 'profesor.PanelCumplimiento', 'profesor.GridCumplimiento'],
        refs: [{
            selector: 'gridProfesores',
            ref: 'gridProfesores'
        },{
            selector: 'panelProfesores',
            ref: 'panelProfesores'
        },{
            selector: 'gridProfesores combo[name=asignatura_filter]',
            ref: 'asignaturaFilter'
        },{
            selector: 'panelCumplimiento',
            ref: 'panelCumplimiento'
        }],
        init: function () {
            this.storeFiltrado = this.instanciaStore('ListaProfesores'),
            this.control(
            {
                // 'gridProfesores' :
                // {
                //     itemdblclick : this.resumenProfesor                  
                // },
                'viewport > treepanel': {
                    itemclick: this.onTreeItemSelected
                },
                'gridProfesores combo[name=asignatura_filter]':
                {
                    change: this.filtrarProfesores
                },
                'gridCumplimiento combo[name=asignatura_filter]':
                {
                    change: this.filtrarProfesores
                },
                'gridProfesores button[name=loot]':
                {
                    click: this.distribucionPorporcionalCarga
                }       
            });
        },
        onTreeItemSelected: function (grid, record, item, index, event, opts) {
            grid.up("viewport").addNewTab(record.data.id);            
        },
        resumenProfesor: function (grid, record, item, index, event, opts) {
            console.log(record,item,index,event,opts);
            return false;
        },
        filtrarProfesores: function(combo, newValue, oldValue, eOpts) { 
            var grid = combo.up('grid');
            this.filtrarStorePorAsignaturas(grid, this.storeFiltrado, newValue);
        },
        distribucionPorporcionalCarga: function(button, event, eOpts) {
            var asignatura = this.getAsignaturaFilter().getValue();
            if (asignatura == null || !/^[a-zA-Z0-9]+$/.test(asignatura)) {
                alert(UI.i18n.error.filterAsignaturaError);
            }
            else if (confirm(UI.i18n.message.resetLoad)) {
                Ext.Ajax.request({
                                    method: 'GET',
                                    action: 'update',
                                    url: 'rest/profesores/loot/' + asignatura,  
                                    scope: this,                                  
                                    success: function() {   
                                        this.getGridProfesores().store.reload();
                                    },
                                    failure: function() {
                                        alert(UI.i18n.error.lootError);
                                    }
                                });
            }
        }
    }); 