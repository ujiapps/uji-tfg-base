Ext.define('TFG.controller.ControllerAsignaciones',
    {
        extend: 'TFG.controller.BaseController',

        stores: ['Asignaciones', 'PlanAsignaciones', 'AsignacionesProfesor'],
        model: [ 'Asignacion', 'PlanAsignacion' ],
        views: ['asignacion.PanelAsignaciones', 'asignacion.GridAsignaciones', 'asignacion.GridAsignacionesAsignadas', 'asignacion.GridAsignacionesEnEspera', 'asignacion.GridAsignacionesSinAsignar', 'asignacion.PanelPlanAsignaciones', 'asignacion.GridPlanAsignaciones', 'asignacion.FormPlanAsignaciones', 'asignacion.GridVerAsignaciones', 'asignacion.WindowAsignacion', 'asignacion.FormAsignacion', 'asignacion.PanelProfesorVerAsignaciones', 'asignacion.GridProfesorVerAsignacionesAsignadas', 'asignacion.GridProfesorVerAsignacionesEnEspera', 'asignacion.WindowAsignacionNoEdit'],
        refs: [{
            selector: 'gridAsignaciones',
            ref: 'gridAsignaciones'
        },{
            selector: 'panelAsignaciones',
            ref: 'panelAsignaciones'
        },{
            selector: 'panelPlanAsignaciones',
            ref: 'panelPlanAsignaciones'
        },{
            selector: 'gridPlanAsignaciones',
            ref: 'gridPlanAsignaciones'
        },{
            selector: 'formPlanAsignaciones',
            ref: 'formPlanAsignaciones'
        },{
            selector: 'gridVerAsignaciones',
            ref: 'gridVerAsignaciones'
        },{
            selector: 'panelAsignaciones combo[name=asignatura_filter]',
            ref: 'asignaturaFilter'
        },{
            selector: 'gridAsignaciones combo[name=propuestasAsignatura]',
            ref: 'propuestasAsignatura'
        },{
            selector: 'formPlanAsignaciones combo[name=asignatura_filter]',
            ref: 'asignaturaPlanFilter'
        },{
            selector: 'gridVerAsignaciones combo[name=asignatura_filter]',
            ref: 'asignaturaVerPlanFilter'
        },{            
           selector: 'gridPlanAsignaciones button[name=set]',
           ref: 'setInningButton' 
        },{
           selector: 'formPlanAsignaciones button[name=sort]',
           ref: 'sortInningButton' 
        }, {
            selector: 'formAsignacion',
            ref: 'formAsignacion'
        }, {
            selector: 'formAsignacion button[action=open]',
            ref: 'saveAsignacionButton'
        }, {
            selector: 'formAsignacion button[action=del]',
            ref: 'closeFormAsignacionButton'
        },{
            selector: 'gridAsignacionesAsignadas',
            ref: 'gridAsignacionesAsignadas'
        },{
            selector: 'gridAsignacionesEnEspera',
            ref: 'gridAsignacionesEnEspera'
        },{
            selector: 'gridAsignacionesSinAsignar',
            ref: 'gridAsignacionesSinAsignar'
        }, {
            selector: 'panelAsignaciones tabpanel',
            ref: 'tabPanelAsignaciones'
        },{
            selector: 'gridProfesorVerAsignacionesAsignadas',
            ref: 'gridProfesorVerAsignacionesAsignadas'
        },{
            selector: 'gridProfesorVerAsignacionesEnEspera',
            ref: 'gridProfesorVerAsignacionesEnEspera'
        }, {
            selector: 'panelProfesorVerAsignaciones tabpanel',
            ref: 'tabPanelProfesorVerAsignaciones'     
        },{
            selector: 'panelProfesorVerAsignaciones combo[name=asignatura_filter]',
            ref: 'asignaturaProfesorFilter'
        },{
            selector: 'windowAsignacion',
            ref: 'windowAsignacion'                   
        },{
            selector: 'windowAsignacionNoEdit',
            ref: 'windowAsignacionNoEdit'                   
        },{
            selector: 'panelAsignaciones button[action=exportexcel]',
            ref: 'exportExcelButton'
        }],
        init: function () {
            this.storeFiltrado = this.instanciaStore('Asignaciones');
            this.storeFiltradoProfesor = this.instanciaStore('AsignacionesProfesor');
            this.storePropuestas = this.instanciaStore('Propuestas');
            this.storePlanAsignaciones = this.instanciaStore('PlanAsignaciones');
            this.storeVerAsignaciones = this.instanciaStore('PlanAsignaciones');
            this.control(
            {
                'viewport > treepanel': {
                    itemclick: this.onTreeItemSelected
                },
                'panelAsignaciones combo[name=asignatura_filter]': {
                    change: this.filtrarPorAsignatura
                },
                'formPlanAsignaciones combo[name=asignatura_filter]': {
                    change: this.filtrarPlanPorAsignatura
                },
                'formPlanAsignaciones button[name=sort]': {
                    click: this.sortInning
                },
                'gridPlanAsignaciones': {
                    edit: this.setInningOneRow
                },
                'gridPlanAsignaciones button[action=set]': {
                    click: this.setInning,
                },
                'gridVerAsignaciones combo[name=asignatura_filter]': {
                    change: this.filtrarVerPlanPorAsignatura
                },
                'windowAsignacion button[action=save]': {
                    click: this.saveAsignacion
                },
                'windowAsignacion button[action=close]': {
                    click: this.closeFormAsignacion
                },
                'windowAsignacionNoEdit button[action=close]': {
                    click: this.closeFormAsignacion
                },                
                'gridAsignacionesAsignadas': {
                    reconfigure: this.renovarEditor,
                    activate : this.activateTab,
                    beforeedit: this.alertAsignaciones
                },
                'gridAsignacionesEnEspera': {
                    reconfigure: this.renovarEditor,
                    activate : this.activateTab,
                    beforeedit: this.alertAsignaciones
                 },
                'gridAsignacionesEnEspera button[action=delawaiting]': {
                    click: this.deleteAsignacion
                },
                'gridAsignacionesEnEspera button[action=confirmawaiting]': {
                    click: this.confirmAsignacion
                },                
                'gridAsignacionesSinAsignar': {
                    reconfigure: this.renovarEditor,
                    activate : this.activateTab
                },
                'gridProfesorVerAsignacionesAsignadas': {
                    activate : this.activateTabProfesor
                },
                'gridProfesorVerAsignacionesEnEspera': {
                    activate : this.activateTabProfesor
                },
                'panelProfesorVerAsignaciones combo[name=asignatura_filter]': {
                    change: this.filtrarAsignacionesProfesorPorAsignatura
                },
                'panelAsignaciones button[action=exportexcel]': {
                    click: this.exportExcel
                }
            });
        },
        onTreeItemSelected: function (grid, record, item, index, event, opts) {
            grid.up("viewport").addNewTab(record.data.id);            
            if (record.data.id == 'TFG.view.asignacion.PanelAsignaciones')
                alert(UI.i18n.message.assignationUpdate);
        },
        filtrarPorAsignatura: function(combo, newValue, oldValue, eOpts) {   
            var me = this;
            this.filtrarStorePorAsignaturas(this.getActiveGrid(), this.storeFiltrado, newValue, function() {
                me.activateTab(me.getActiveGrid())
                me.getExportExcelButton().enable();
            });
        },
        filtrarAsignacionesProfesorPorAsignatura: function(combo, newValue, oldValue, eOpts) {   
            var me = this;
            this.filtrarStorePorAsignaturas(this.getActiveGridProfesor(), this.storeFiltradoProfesor, newValue, function() {
                me.activateTabProfesor(me.getActiveGridProfesor())
            });
        },
        filtrarPlanPorAsignatura: function(combo, newValue, oldValue, eOpts) {
            this.filtrarStorePorAsignaturas(this.getGridPlanAsignaciones(), this.storePlanAsignaciones, newValue);
        },        
        filtrarVerPlanPorAsignatura: function(combo, newValue, oldValue, eOpts) {
            this.filtrarStorePorAsignaturas(this.getGridVerAsignaciones(), this.storeVerAsignaciones, newValue);
        },
        renovarEditor: function (grid, store, columns, oldStore, The, eOpts) {
            if (this.getAsignaturaFilter().getValue() == undefined || this.getAsignaturaFilter().getValue() == null || this.getAsignaturaFilter().getValue() == 0)
                return;
            this.storePropuestas.proxy.url = this.storePropuestas.baseUrl + '/' + this.getAsignaturaFilter().getValue();
            this.storePropuestas.load({
                callback: function(records, options, success) {
                    for (var i = 0;i<The.length;i++) {
                        if(The[i].dataIndex == "propuesta_id") {
                            The[i].getEditor().store.removeAll();
                            for (var rec in records) {
                                The[i].getEditor().store.add(records[rec]);
                            }
                        }
                    }                    
                }
            });
        },
        sortInning: function(button, e, eOpts) {
            var asignatura = this.getAsignaturaPlanFilter().getValue();
            if (asignatura == null) {
                alert(UI.i18n.error.filterAsignaturaError);
            }
            else {
                var formInning = this.getFormPlanAsignaciones().getForm();
                var values = formInning.getValues();    
                var excluded_days = [];
                if (!formInning.isValid()) {
                    alert(UI.i18n.error.form);
                    return;
                }
                Ext.Ajax.request({
                    method: 'GET',
                    action: 'update',
                    url: 'rest/usuarios/getExcludedDays',  
                    scope: this,                                  
                    success: function(response, options) {   
                        var festivos = Ext.decode(response.responseText);                        
                        var tokens = values.beginDate.split('-');
                        var dia = tokens[0];
                        var mes = tokens[1];
                        var anyo = tokens[2];
                        var tokens = values.open.split(':');
                        var horaInicio = tokens[0];
                        var minutoInicio = tokens[1];
                        var tokens = values.close.split(':');
                        var horaFin = tokens[0];
                        var minutoFin = tokens[1];
                        var items = this.getGridPlanAsignaciones().store.data.items;
                        var fechaAsignacion = new Date(anyo, mes - 1, dia, horaInicio, minutoInicio, 0, 0);
                        var fechaTurno = null;
                        var ahora = new Date();
                        if (fechaAsignacion < ahora) {
                            if (!confirm(UI.i18n.error.datePast)) {
                                return;
                            }
                        }
                        //Generamos los turnos
                        for (i = 0; i < items.length;) {
                            while (!fechaTurno) {
                                var fechaSinHoras = new Date(fechaAsignacion.getFullYear(), fechaAsignacion.getMonth(), fechaAsignacion.getDate(), 0,0,0,0);
                                var fechaCierre = new Date(fechaAsignacion.getFullYear(), fechaAsignacion.getMonth(), fechaAsignacion.getDate(), horaFin, minutoFin, 0, 0);
                                var siguienteAsignacion = new Date(fechaAsignacion.getTime() + (values.duration * 60 * 1000));
                                if (festivos.indexOf(fechaSinHoras.getTime()) == -1 && fechaSinHoras.getDay() != 0 && fechaSinHoras.getDay() != 6) {//Ni festivo ni domingo ni sábado
                                    fechaTurno = fechaAsignacion;
                                    if (siguienteAsignacion <= fechaCierre) { // El día es válido y todavía no se ha cerrado el horario previsto para el día
                                        fechaAsignacion = siguienteAsignacion;
                                    }
                                    else {//Pasamos al siguiente día
                                        fechaAsignacion = new Date(new Date(fechaAsignacion.getFullYear(), fechaAsignacion.getMonth(), fechaAsignacion.getDate(), horaInicio, minutoInicio, 0, 0).getTime() + (24*60*60*1000));
                                    }
                                }                                
                                else {
                                    fechaAsignacion = new Date(new Date(fechaAsignacion.getFullYear(), fechaAsignacion.getMonth(), fechaAsignacion.getDate(), horaInicio, minutoInicio, 0, 0).getTime() + (24*60*60*1000));
                                }
                            }
                            for (var k = 0; k < values.size && i < items.length; k++) {
                                this.getGridPlanAsignaciones().store.data.items[i].set('fecha_apertura',new Date(new Date(fechaTurno.getFullYear(), fechaTurno.getMonth(), fechaTurno.getDate(), 0, 0, 0, 0).getTime()));
                                this.getGridPlanAsignaciones().store.data.items[i].set('hora_apertura',fechaTurno);
                                i++;
                            }

                            fechaTurno = null;
                        }
                        alert(UI.i18n.message.sortInning);   
                        this.getSetInningButton().enable();
                    },
                    failure: function() {
                        alert(UI.i18n.error.excludedDays);
                    }
                });

            } 
        },
        setInning: function(button, e, eOpts) {
            if (confirm(UI.i18n.message.setInningAlert)) {                
                this.getGridPlanAsignaciones().store.sync();
            }                
        },
        setInningOneRow: function(editor, field) {
            if (confirm(UI.i18n.message.setInningAlert)) {                
                this.getGridPlanAsignaciones().store.sync();
            }         
        },
        editComentarios: function(grid, rowIndex, colIndex) {
            var record = grid.getStore().getAt(rowIndex);
            if (record.data.propuesta_id) {        
                var me = this;
                if (me.window == null) {
                    if (grid.getRefOwner().xtype.indexOf('ProfesorVer') < 0)
                        me.window = Ext.create('TFG.view.asignacion.WindowAsignacion');
                    else 
                        me.window = Ext.create('TFG.view.asignacion.WindowAsignacionNoEdit');
                }                   
                me.window.show();
                me.window.load(record);
            }
        },
        saveAsignacion: function(button, ev) {
            var me = this;

            var asignacion = me.getActiveGrid().getSelectionModel().getSelection()[0].data;

            var comentarios = asignacion.comentarios;
            if (me.getFormAsignacion() && me.getFormAsignacion().getForm() && me.getFormAsignacion().getForm().getValues()["comentarios"]) {
                comentarios = me.getFormAsignacion().getForm().getValues()["comentarios"];
            }
            var post_params = {
                comentarios: comentarios,
                persona_id: asignacion.persona_id,
                propuesta_id: asignacion.propuesta_id
            };            
            var url = 'rest/propuestas/asignaciones/' + asignacion.codigo_asignatura; 
            if (button === false && ev === false) {
                url = 'rest/propuestas/asignaciones/activar/' + asignacion.codigo_asignatura; 
            }
            Ext.Ajax.request({
                method: 'POST',
                url: url,
                params: post_params,
                scope: this,                                  
                success: function(response, options) {   
                    var data = Ext.decode(response.responseText);
                    if (data.success === false) {
                          if (data.length > 0) {
                            alert(UI.i18n.error[data.datos]);
                        }
                        else {
                            alert(UI.i18n.error.form);
                        }                      
                    }
                    else {      
                        asignacion.comentarios = comentarios;                  
                        me.closeFormAsignacion();                         
                    }                    
                },
                failure: function(form, action) {
                    var data = me.parseResponse(action);
                    if (data.length > 0) {
                        alert(UI.i18n.error[data.datos]);
                    }
                    else {
                        alert(UI.i18n.error.form);
                    }
                }
            });
        },
        closeFormAsignacion: function(button, ev) {
            if (this.window != null) {  
                this.window.close();
                this.window = null;
            }
        },
        deleteAsignacion: function() {
            if (!this.getGridAsignacionesEnEspera().hasRowSelected()) {
                alert(UI.i18n.error.selectRow);
                return;
            }

            var me = this;
            var grid = me.getGridAsignacionesEnEspera();
            var record = grid.getSelectedRecord();
            if (record.data.propuesta_id && record.data.aceptada == 0) {        
                Ext.MessageBox.confirm(UI.i18n.title.confirmDelete, UI.i18n.message.confirmDelete, function(btn) {
                    if (btn == 'yes') {
                        grid.getStore().remove(record);
                    }
                })
            }
            this.activateTab(this.getGridAsignacionesEnEspera());
        },
        confirmAsignacion: function() {
            if (!this.getGridAsignacionesEnEspera().hasRowSelected()) {
                alert(UI.i18n.error.selectRow);
                return;
            }

            var me = this;
            var grid = me.getGridAsignacionesEnEspera();
            var record = grid.getSelectedRecord();
            if (record.data.propuesta_id && record.data.aceptada == 0) {        
                Ext.MessageBox.confirm(UI.i18n.title.askConfirm, UI.i18n.message.askConfirm, function(btn) {
                    if (btn == 'yes') {
                        record.data.aceptada = 1;
                        me.saveAsignacion(false, false);
                        me.activateTab(me.getGridAsignacionesEnEspera());
                        return;
                    }
                })
            }
            this.activateTab(this.getGridAsignacionesEnEspera());
        },        
        getActiveGrid: function() {
            return this.getTabPanelAsignaciones().activeTab;
        },
        getActiveGridProfesor: function() {
            return this.getTabPanelProfesorVerAsignaciones().activeTab;
        },
        activateTab: function(grid, previousGrid) {
            if (this.getAsignaturaFilter().getValue() == undefined || this.getAsignaturaFilter().getValue() == null || this.getAsignaturaFilter().getValue() == 0)
                return;                        
            var filterfn = undefined;
            switch(grid.xtype) {
                case "gridAsignacionesSinAsignar":
                    filterfn = function (record, id) { return record.data.propuesta_id == 0; };
                    break;
                case "gridAsignacionesAsignadas":
                    filterfn = function (record, id) { return record.data.propuesta_id > 0 && record.data.aceptada == 1; };
                    break;
                case "gridAsignacionesEnEspera":
                    filterfn = function (record, id) { return record.data.propuesta_id > 0 && record.data.aceptada == 0; };
                    break;                    
            }
            this.storeFiltrado.clearFilter(true);
            this.storeFiltrado.filterBy(filterfn);
            grid.reconfigure(this.storeFiltrado);
        },
        activateTabProfesor: function(grid, previousGrid) {
            if (this.getAsignaturaProfesorFilter().getValue() == undefined || this.getAsignaturaProfesorFilter().getValue() == null || this.getAsignaturaProfesorFilter().getValue() == 0)
                return;                        
            var filterfn = undefined;
            switch(grid.xtype) {
                case "gridProfesorVerAsignacionesAsignadas":
                    filterfn = function (record, id) { return record.data.propuesta_id > 0 && record.data.aceptada == 1; };
                    break;
                case "gridProfesorVerAsignacionesEnEspera":
                    filterfn = function (record, id) { return record.data.propuesta_id > 0 && record.data.aceptada == 0; };
                    break;                    
            }
            this.storeFiltradoProfesor.clearFilter(true);
            this.storeFiltradoProfesor.filterBy(filterfn);
            grid.reconfigure(this.storeFiltradoProfesor);
        },
        alertAsignaciones: function() {
            alert(UI.i18n.message.assignationUpdate);
        },
        exportExcel: function() {
            window.open('rest/propuestas/asignaciones/excel/' + this.getAsignaturaFilter().getValue(), 'Export');            
        }
    }
); 

