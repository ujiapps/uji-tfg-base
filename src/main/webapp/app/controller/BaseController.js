Ext.define('TFG.controller.BaseController',
	{
		extend: 'Ext.app.Controller',
		parseResponse: function (request) {
            if (request && request.response && request.response.responseText) {
				return Ext.JSON.decode(request.response.responseText);
			}
			else 
				return '';
		},

		instanciaStore: function(store) {
			return Ext.create('TFG.store.'+store);
		},

		filtrarStorePorAsignaturas: function(grid, store, value, callback) {
            store.proxy.url = store.baseUrl + '/' + value;
            store.load(function() {
            	if (callback != undefined && typeof callback == 'function') {
            		callback();
            	}
            });
            grid.reconfigure(store);
		}
	});