Ext.define('TFG.controller.ControllerAsignaturas',
    {
        extend: 'Ext.app.Controller',

        stores: [ 'Asignaturas', 'AsignaturasCoordinadas' ],
        model: [ 'Asignatura' ],
        views: ['asignatura.GridAsignaturas', 'asignatura.PanelAsignaturas'],
        refs: [
            {
                selector: 'gridAsignaturas',
                ref: 'gridAsignaturas'
            }
        ],
        init: function () {
            this.control(
            {
                'viewport > treepanel': {
                    itemclick: this.onTreeItemSelected
                }                
            });
        },
        onTreeItemSelected: function (grid, record, item, index, event, opts) {
            grid.up("viewport").addNewTab(record.data.id);            
        }
    });