Ext.define('TFG.controller.ControllerPropuestas',
    {
        extend: 'TFG.controller.BaseController',

        views:  [ 'propuesta.FormPropuestas', 'propuesta.GridPropuestas', 'propuesta.WindowPropuestas'],
        stores: ['Propuestas', 'Asignaturas', 'Profesores', 'ListaProfesores'],
        models: ['Propuesta', 'Asignatura', 'Usuario','Profesor'],

        refs:   [{
            ref: 'panelPropuestas',
            selector: 'panelPropuestas'
        },{
            ref: 'formPropuestas',
            selector: 'formPropuestas'       
        },{
            ref: 'savePropuestaButton',
            selector: 'formPropuestas button[action=open]'
        },{
            ref: 'resetPropuestaButton',
            selector: 'formPropuestas button[action=del]'
        },{            
            ref: 'gridPropuestas',
            selector: 'gridPropuestas'
        },{
            ref: 'viewport',
            selector: 'viewport'
        },{
            ref: 'asignaturaFilter',
            selector: 'gridPropuestas combo[name=asignatura_filter]'
        },{
            ref: 'asignaturaFormFilter',
            selector: 'formPropuestas combo[name=codigo_asignatura]'
        },{
            ref: 'comboFormProfesores',
            selector: 'formPropuestas combo[name=autor_id]'
        }],

        init: function () {
            this.storeFiltrado = this.instanciaStore('Propuestas');
            this.control(
            {
                'gridPropuestas button[action=add]' :
                {
                    click : this.mostrarModalFormulario
                },

                'gridPropuestas' :
                {
                    itemdblclick : this.editarPropuesta
                },

                'gridPropuestas combo[name=asignatura_filter]':
                {
                    change: this.filtrarPropuestas
                },

                'windowPropuestas button[action=save]' :
                {
                    click : this.guardarPropuesta
                },

                'gridPropuestas button[action=edit]' :
                {
                    click: this.editarSeleccionada
                },
                
                'windowPropuestas button[action=close]' :
                {
                    click : this.closeWindowPropuestas
                },
                // 'formPropuestas combo[name=autor_id]':
                // {
                //     render: this.deshabilitarProfesores
                // },
                'viewport > treepanel': {
                    itemclick: this.onTreeItemSelected
                },
                'formPropuestas combo[name=codigo_asignatura]':
                {
                    change: this.renovarProfesores
                }

            });
            this.window = null;//Ext.create('TFG.view.propuesta.WindowPropuestas');            
        },
        onTreeItemSelected: function (grid, record, item, index, event, opts) {
            grid.up("viewport").addNewTab(record.data.id);            
        },
        editarPropuesta : function(grid, record, item, index, e, eOpts)
        {
            var me = this;
            var store = this.getStore('Profesores');
            store.getProxy().extraParams = {codigo_asignatura: this.getAsignaturaFilter().value};
            store.load(function() {            
                Ext.Ajax.request({
                    method: 'GET',
                    action: 'get',
                    url: 'rest/propuestas/' + record.data.id,  
                    scope: this,                                  
                    success: function(response, options) {
                        Ext.Ajax.request({
                            method: 'GET',
                            action: 'get',
                            url: 'rest/profesores/cargaRestante/' + me.getAsignaturaFilter().value,
                            scope: me,
                            success: function (res, opt) {
                                var data = Ext.decode(response.responseText);  
                                var carga = Ext.decode(res.responseText);
                                if (record.data.id == data.id) {
                                    record.set('objetivos', data.objetivos);
                                    record.set('descripcion', data.descripcion);
                                    record.set('observaciones', data.observaciones);
                                    record.set('bibliografia', data.bibliografia);
                                    
                                    if (me.window == null) {
                                        me.window = Ext.create('TFG.view.propuesta.WindowPropuestas');
                                    }                                                            
                                    me.getFormPropuestas().getForm().findField('carga_restante').setVisible(carga.datos != "ADMIN");
                                    var carga_datos= carga.datos <= 0?UI.i18n.message.fullPax:carga.datos;

                                    me.getFormPropuestas().getForm().setValues({carga_restante: carga_datos});
                                    me.window.show();
                                    me.window.load(record);
                                }
                                else {
                                    alert(UI.i18n.error.proposalNotLoad);
                                }
                            },
                            failure: function() {
                                alert(UI.i18n.error.proposalNotLoad);
                            }
                        })
                    },
                    failure: function() {
                        alert(UI.i18n.error.proposalNotLoad);
                    }
                });
            });
        },        
        guardarPropuesta: function () {
            var url = 'rest/propuestas';
            var grid = this.getGridPropuestas();
            var me = this;
            if (!me.getFormPropuestas().getForm().isValid())  {
                return;
            }

            var post_params = me.getFormPropuestas().getForm().getValues();
            var method = post_params.id?'PUT':'POST';
            if (method == 'PUT') {
                url += '/' + post_params.id;
            }
            Ext.Ajax.request({
                    method: method,
                    action: method,
                    url: url,
                    scope: this,   
                    params: post_params,
                    success: function(response, request) {
                        var data = Ext.decode(response.responseText);
                        if (data.success === false) {
                            alert(UI.i18n.error[data.datos]);
                        }
                        else {                        
                            me.getGridPropuestas().getStore().load();  
                            me.closeWindowPropuestas();                  
                        }
                    },
                    failure: function(form, action) {
                        var data = me.parseResponse(action);
                        if (data.length > 0) {
                            alert(UI.i18n.error[data.datos]);
                        }
                        else {
                            alert(UI.i18n.error.form);
                        }
                    }
            } );
        },
        closeWindowPropuestas: function () {
            if (this.window != null)    
                    this.window.clearAndHide();
            this.reloadGridPropuestas();            
        },
        mostrarModalFormulario: function () {
            var me = this;
            var store = this.getStore('Profesores');
            store.getProxy().extraParams = {codigo_asignatura: this.getAsignaturaFilter().value};
            store.load(function() {
                if (me.window == null) {
                    me.window = Ext.create('TFG.view.propuesta.WindowPropuestas');            
                }            
                me.window.show();
            })
            return false;                
        },
        // deshabilitarProfesores: function(combo, eOpts) {
        //     if (combo.store.data.length == 1) {                  
        //         combo.setValue(combo.store.data.keys[0]);
        //     }
        // },
        filtrarPropuestas: function(combo, newValue, oldValue, eOpts) {                
            this.filtrarStorePorAsignaturas(this.getGridPropuestas(), this.storeFiltrado, newValue);
        },
        reloadGridPropuestas: function() {
            this.filtrarPropuestas(null, this.getAsignaturaFilter().value);
        },
        editarSeleccionada: function() {
            var records = this.getGridPropuestas().getSelectionModel().getSelection();
            if (records.length == 0)
                alert(UI.i18n.error.selectRow);
            else
                this.editarPropuesta(this.getGridPropuestas(), records[0]);
        },
        removePropuesta: function(grid, record, callback) {
            var cod_asignatura = this.getAsignaturaFilter().value;
            Ext.Ajax.request({
                method: 'DELETE',
                action: 'get',
                url: 'rest/propuestas/remove/' + cod_asignatura + '/' + record.data.id,  
                scope: this,                                  
                success: function(response, options) {
                    var res = Ext.decode(response.responseText);
                    if (res.success == true) {
                        callback();
                    }
                    else {
                        if (res.datos.length > 0) {
                            alert(UI.i18n.error[res.datos]);
                        }
                    }
                },
                failure: function(response, options) {
                    console.log(response);
                    // alert(UI.i18n.error.proposalNotLoad);
                }
            });
        },
        renovarProfesores: function(combo, newValue, oldValue, eOpts) {
            var me = this;
            var oldProfesor = me.getComboFormProfesores().value;
            me.filtrarStorePorAsignaturas(me.getGridPropuestas(), me.storeFiltrado, newValue);
            me.getComboFormProfesores().getStore().getProxy().extraParams.codigo_asignatura = newValue;
            me.getComboFormProfesores().getStore().load(function(records, operation, success) {
                var enAmbasAsignaturas = false;
                if (success && records.length > 0) {
                    for (i = 0; i<records.length; i++) {
                        if (records[0].data.id == oldProfesor) {
                            enAmbasAsignaturas = true;
                            break;
                        }
                    }
                }
                if (!enAmbasAsignaturas) {
                    //Limpiamos los datos actuales del combo
                    me.getComboFormProfesores().clearValue();
                }                
            });
        }      
    });