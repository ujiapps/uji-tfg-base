Ext.define('TFG.store.Asignaciones',
{
    extend : 'Ext.data.Store',
    model : 'TFG.model.Asignacion',

    autoLoad : false,
    autoSync: true,
    clearOnPageLoad: true,
    baseUrl: '/tfg/rest/propuestas/asignaciones',
    proxy :
    {
        type : 'rest',
        url : '/tfg/rest/propuestas/asignaciones/0',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },
        writer :
        {
            type : 'json',
            successProperty : 'success'
        }
    },
    onUpdateRecords: function(records, operation, success) {           
        if (!success) {
            console.log(records, operation, success);
            alert(UI.i18n.error.AsignacionFallida);
        }
        else {
            this.reload();
        }
    },
    onDestroyRecords: function(records, operation, success) {
        if (success) {
            this.reload();
        }
    }
});