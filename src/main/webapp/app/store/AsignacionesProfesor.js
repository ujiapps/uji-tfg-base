Ext.define('TFG.store.AsignacionesProfesor',
{
    extend : 'Ext.data.Store',
    model : 'TFG.model.Asignacion',

    autoLoad : false,
    autoSync: false,
    clearOnPageLoad: true,
    baseUrl: '/tfg/rest/propuestas/asignacionesprofesor',
    proxy :
    {
        type : 'rest',
        url : '/tfg/rest/propuestas/asignacionesprofesor/0',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },
        writer :
        {
            type : 'json',
            successProperty : 'success'
        }
    }
});