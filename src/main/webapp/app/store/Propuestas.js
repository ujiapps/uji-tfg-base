Ext.define('TFG.store.Propuestas',
{
    extend : 'Ext.data.Store',
    model : 'TFG.model.Propuesta',

    autoLoad : false,
    autoSync: false,
    clearOnPageLoad: true,
    baseUrl: '/tfg/rest/propuestas/listado',
    proxy :
    {
        type : 'rest',
        url : '/tfg/rest/propuestas/listado/0',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }
});