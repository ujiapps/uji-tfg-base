Ext.define('TFG.store.AsignaturasCoordinadas',
{
    extend : 'Ext.data.Store',
    model : 'TFG.model.Asignatura',

    autoLoad : true,
    autoSync: true,
    clearOnPageLoad: true,

    proxy :
    {
        type : 'rest',
        url : '/tfg/rest/asignaturas/coord',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }
}); 