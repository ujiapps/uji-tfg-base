Ext.define('TFG.store.Profesores',
{
    extend : 'Ext.data.Store',
    model : 'TFG.model.Usuario',

    autoLoad : false,
    autoSync: true,
    clearOnPageLoad: true,

    proxy :
    {
        type : 'rest',
        url : '/tfg/rest/usuarios/profesores',
        extraParams: {codigo_asignatura: "0"},
        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }
});