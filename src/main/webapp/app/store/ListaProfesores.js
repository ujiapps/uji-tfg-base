Ext.define('TFG.store.ListaProfesores',
{
    extend : 'Ext.data.Store',
    model : 'TFG.model.Profesor',

    autoLoad : false,
    autoSync: true,
    clearOnPageLoad: true,
    baseUrl: '/tfg/rest/profesores',
    proxy : 
    {
        type : 'rest',
        url : '/tfg/rest/profesores',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        }
    }
});