Ext.define('TFG.store.PlanAsignaciones',
{
    extend : 'Ext.data.Store',
    model : 'TFG.model.PlanAsignacion',
    autoLoad : false,
    autoSync: false,
    clearOnPageLoad: true,
    baseUrl: '/tfg/rest/usuarios/plan',
    proxy :
    {
        type : 'rest',
        url : '/tfg/rest/usuarios/plan/0',
        batchActions: true,
        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },
        writer :
        {
            allowSingle: false,
            type : 'json',
            model: 'TFG.model.PlanAsignacion',
            successProperty : 'success'
        }
    }
});