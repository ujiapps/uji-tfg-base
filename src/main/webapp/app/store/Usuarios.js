Ext.define('TFG.store.Usuarios',
{
    extend : 'Ext.data.Store',
    model : 'TFG.model.Usuario',

    autoLoad : false,
    autoSync: true,
    clearOnPageLoad: true,

    proxy :
    {
        type : 'rest',
        url : '/tfg/rest/usuarios',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }
});