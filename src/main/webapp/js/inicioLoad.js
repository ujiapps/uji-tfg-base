$(function() {

	function cargarPropuestas(asignaturaCodigo) {
		$.get(location.href.substring(0,location.href.indexOf('inicio')) + 'inicio/'+ asignaturaCodigo + '/filter', {},
			function(data) {
				$('#listaPropuestasNew').html(data);
				$(document).foundation();
				if ($(data).find('li').length > 0) {
					$('#cabeceraListaPropuestasNew').show();
				} else {
					$('#cabeceraListaPropuestasNew').hide();
				}
			}, 'html');
	}

	$('div#asignaturasSelect select').on('change', function() {
		cargarPropuestas($(this).val());
	});

	$(document).on('click', '#solicitarPropuesta', function () {
		const proposalId = $(this).data('proposal-id');

		$.ajax({
			type: "PUT",
			url: location.href.substring(0, location.href.indexOf('inicio')) + 'solicitud/' + proposalId,
			data: {
				"comentarios": $(this).closest('.propuesta').find('.comentariosDiv textarea').val()
			},
			success: function () {
				$('div#asignaturasSelect select').val(0);
				location.reload();
			},
			failure: function (data) {
				alert(data);
			},
			dataType: 'HTML'
		});
	});

	$(document).on('click', '#eliminarPropuesta', function () {
		const proposalId = $(this).data('proposal-id');

		$.ajax({
			type: "POST",
			url: location.href.substring(0, location.href.indexOf('inicio')) + 'solicitud/' + proposalId + '/remove',
			data: {},
			success: function () {
				$(this).closest('.propuesta').find('.comentariosDiv textarea').val(0);
				location.reload();
			},
			failure: function (data) {
				alert(data);
			},
			dataType: 'HTML'
		});
	});

	const select = $('div#asignaturasSelect select');
	const opciones = select.find("option").not(":first");

	if (opciones.length === 1) {
		const codigo = opciones.first().val();
		select.val(codigo);
		cargarPropuestas(codigo)
	}
});