var UI = 
{
    "i18n": {
        "message": {
            "name":"Nom",
            "teacher": "Professor/a",
            "availability": "Disponibilitat",
            "detail": "Detall",
            "paxAvailable": "Amb places",
            "paxWaiting": "En espera",
            "noPax": "Tancat",
            "see": "Veure",
            "esperaDeshabilitada": "El periode de selecció no està disponible"
        },
        "button": {
            "solicitar": "Sol·licitar",
            "solicitarEspera": "Entrar en llista d'espera",
            "cancelar": "Cancel·lar",
            "cancelarInscripcion": "Cancel·lar inscripció",
            "cerrar": "Tancar"
        }
    }
};