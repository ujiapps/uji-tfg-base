var UI = 
{
    "i18n": {
        "message": {
            "name":"Nombre",
            "teacher": "Profesor/a",
            "availability": "Disponibilidad",
            "detail": "Detalle",
            "paxAvailable": "Con plazas",
            "paxWaiting": "En espera",
            "noPax": "Cerrado",
            "see": "Ver",
            "changeApply": "Solicitar la adjudicación",
            "esperaDeshabilitada": "El periodo de selección no está disponible"
        },
        "button": {
            "solicitar": "Solicitar",
            "solicitarEspera": "Entrar en lista de espera",
            "cancelar": "Cancelar",
            "cancelarInscripcion": "Cancelar inscripcion",
            "cerrar": "Cerrar"
        }
    }
};